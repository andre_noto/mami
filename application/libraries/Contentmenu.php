<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Contentmenu {
	public function __construct()
	{
	    $this->CI =& get_instance();
	}

    public function generatemenu()
    {
    	$company =$this->CI->companymodel->getCompany();

    	foreach ($company as $key) { ?>	
        <li class="nav-item <?php if($this->CI->input->get('cat') == $key->id) { echo "active open"; } ?>">
            <a href="javascript:;" class="nav-link nav-toggle">
                <i class="fa fa-building-o "></i>
                <span class="title"><?php echo $key->name; ?></span>
                <span class="arrow"></span>
            </a>
            <ul class="sub-menu">
                <li class="nav-item <?php if($this->CI->uri->segment(2) == 'news' && $this->CI->input->get('cat') == $key->id) { echo 'active'; }  ?> ">
                    <a href="<?php echo base_url('backend/news?cat='.$key->id); ?>" class="nav-link">
                        <i class="fa fa-newspaper-o"></i>
                        <span class="title">News</span>
                    </a>
                </li>
                <li class="nav-item  <?php if($this->CI->uri->segment(2) == 'product' && $this->CI->input->get('cat') == $key->id) { echo 'active'; }  ?>">
                    <a href="<?php echo base_url('backend/product?cat='.$key->id); ?>" class="nav-link">
                        <i class="fa fa-tag"></i>
                        <span class="title">Products</span>
                    </a>
                </li>
                <li class="nav-item  <?php if($this->CI->uri->segment(2) == 'about' && $this->CI->input->get('cat') == $key->id) { echo 'active'; }  ?>">
                    <a href="<?php echo base_url('backend/about?cat='.$key->id); ?>" class="nav-link ">
                        <i class="fa fa-users"></i>
                        <span class="title">About Us</span>
                    </a>
                </li>   
                <li class="nav-item  <?php if($this->CI->uri->segment(2) == 'contact' && $this->CI->input->get('cat') == $key->id) { echo 'active'; }  ?>">
                    <a href="<?php echo base_url('backend/contact?cat='.$key->id); ?>" class="nav-link ">
                        <i class="fa fa-envelope"></i>
                        <span class="title">Contact Us</span>
                    </a>
                </li>                   
            </ul>
        </li>
    <?php	
    		
    	}
    }
}