<?php
	class Backendmodel extends CI_Model {
   
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    function getSettings()
    {
        $query = $this->db->get_where('setting', array('id' => '1'));
        if($query->num_rows() > 0)
        {   return $query->row();  } 
        else
        { return false; } 
    }

    function cekLogin($username, $password)
    {
        $queryGetSalt = $this->db->get_where('admin', array('username' => $username));
        if($queryGetSalt->num_rows() > 0) {
            $queryGetSalt = $queryGetSalt->row();
            $garam = $queryGetSalt->salt;
            $password = do_hash($password.$garam, 'md5');
            /*echo $queryGetSalt->password;
            echo '<br/>PAss '.$password;
            echo '<br/>garam '.$garam;

            echo '<br/>new '.do_hash('password2f5EJmc9iZGJDmbb', 'md5');*/
            
            $query = $this->db->get_where('admin', array('username' => $username, 'password' => $password));
            if($query->num_rows() > 0)
            { return true; }
            else
            { return false; } 
        } else {
            return false;
        }
    }
    
    /* META */
    
        function cekURL($url) {
            $query = $this->db->query("SELECT * FROM meta WHERE url = '".$url."';");
            if($query->num_rows() > 0) {
                return false;
            } else {
                return true;
            }
        }
        
        function loadCurrentMeta($url) {
            $query = $this->db->query('SELECT * FROM meta WHERE url = \''.$url.'\' LIMIT 1;');
            
            if($query->num_rows() > 0) {
                $rslt = $query->result();
                return $rslt[0]->meta_tag;
            } else {
                // DEFAULT META 
                $query = $this->db->query('SELECT * FROM setting WHERE id = 1 LIMIT 1; ');
                $rslt = $query->result();
                return $rslt[0]->default_meta;
            }
        }
        
        function deleteMeta($id) {
            $this->db->delete('meta', array('meta_id' => $id)); 
            if($this->db->affected_rows() > 0) {
                return true;
            } else {
                return false;
            }
        }
    
	function saveNewMeta($url, $tags) {
		$query = $this->db->query('INSERT INTO meta(url, meta_tag) VALUES(\''.$url.'\', \''.$tags.'\'); ');
	}
	
	function saveMeta($idmeta, $url, $tags) {
		$query = $this->db->query('UPDATE meta SET url = \''.$url.'\', meta_tag = \''.$tags.'\' WHERE meta_id = \''.$idmeta.'\'; ');
	}
	
	function loadAllMeta() {
		$query = $this->db->query('SELECT * FROM meta ORDER BY meta_id ASC;');
		if($query->num_rows() >0) {
			return $query->result_array();			
		} else {return false; }
	}

	function loadMeta($idmeta) {
		$query = $this->db->query('SELECT * FROM meta WHERE meta_id = \''.$idmeta.'\';');
		if($query->num_rows() >0) {
			return $query->result_array();			
		} else {return false; }
	}
        
        // BANK
        
        function loadBank($idbank) {
		$query = $this->db->query('SELECT * FROM bank_account WHERE idbank_account = \''.$idbank.'\';');
		if($query->num_rows() >0) {
			return $query->result_array();			
		} else {return false; }
	}
        
        function loadAllBank() {
		$query = $this->db->query('SELECT b.idbank_account, b.bank_idbank, bk.bank, b.accountnumber, b.accountname, b.branch FROM bank_account b, bank bk WHERE bk.idbank = b.bank_idbank  ORDER BY b.bank_idbank ASC;');
		if($query->num_rows() >0) {
			return $query->result_array();			
		} else {return false; }
	}
        
        function loadAllBankMaster() {
            	$query = $this->db->query('SELECT * FROM bank ORDER BY bank ASC;');
		if($query->num_rows() >0) {
			return $query->result_array();			
		} else {return false; }
        }
        
        function saveBank($idbank, $account, $atasnama, $bank_pilih,  $cabang) {
		$query = $this->db->query('UPDATE bank_account SET bank_idbank = '.$bank_pilih.', accountnumber = \''.$account.'\', accountname = \''.$atasnama.'\', branch = \''.$cabang.'\' WHERE idbank_account = \''.$idbank.'\'; ');
	}
        
        function saveNewBank($account, $atasnama, $bank_pilih,  $cabang) {
		$query = $this->db->query('INSERT INTO bank_account(idbank_account, bank_idbank, accountnumber,accountname,branch) VALUES(\'\', '.$bank_pilih.', \''.$account.'\', \''.$atasnama.'\', \''.$cabang.'\'); ');
	}
        
        function deleteBank($id) {
            // cek relasi
          
                $this->db->delete('bank_account', array('idbank_account' => $id)); 
                if($this->db->affected_rows() > 0) {
                    return true;
                } else {
                    return false;
                }          
                        
        }
        //End of Bank

        function editTerms($isi) {
           $this->db->trans_start();
            $data = array(
                            'terms' => $isi

                            );
            $this->db->where('id',1);
            $this->db->update('setting', $data);
            $this->db->trans_complete();    
        }
	
	
        
        function getProyekTotal() {
            $query = $this->db->query('SELECT COUNT("project_id") AS "total" FROM project;');
            if($query->num_rows() > 0)
            {	$query =  $query->result_array();
                return $query[0]['total'];
            
            } 
            else
            {   return false; } 
        }
        
        function getBeritaTotal() {
            $query = $this->db->query('SELECT COUNT("id") AS "total" FROM artikel WHERE kategori_id = 2;');
            if($query->num_rows() > 0)
            {	$query =  $query->result_array();
                return $query[0]['total'];
            
            } 
            else
            {   return false; } 
        }
        
        function getNotificationTitipan() {
            // titipan
            $query = $this->db->query('SELECT COUNT("idkoleksi") as "total" FROM koleksi WHERE final = 1 AND status = 0 AND expireddate <=  DATE_ADD(NOW(), INTERVAL 31 DAY);');
            if($query->num_rows() > 0)
            {	$query =  $query->result_array();
                return $query[0]['total'];
            
            } 
            else
            {   return false; } 
            
        }
        
        function getTitipanTotal() {
            $query = $this->db->query('SELECT COUNT("idkoleksi") AS "total" FROM koleksi WHERE final = 1 AND status = 0;');
            if($query->num_rows() > 0)
            {	$query =  $query->result_array();
                return $query[0]['total'];
            
            } 
            else
            {   return false; } 
        }
        
        function getArtikelTotal() {
            $query = $this->db->query('SELECT COUNT("id") AS "total" FROM artikel;');
            if($query->num_rows() > 0)
            {	$query =  $query->result_array();
                return $query[0]['total'];
            
            } 
            else
            {   return false; } 
        }
      
        function getSlideshowTotal() {
            $query = $this->db->query('SELECT COUNT("id") AS "total" FROM slide;');
            if($query->num_rows() > 0)
            {	$query =  $query->result_array();
                return $query[0]['total'];
            
            } 
            else
            {   return false; } 
        }
        
        function simpanSambutan($sambutan) {
            $this->db->trans_start();
            $data = array('sambutan' => $sambutan);
            $this->db->where('id',1);
            $this->db->update('setting', $data);
            $this->db->trans_complete(); 
        }
        
        function simpanTanggalPenting($tanggalpenting) {
            $this->db->trans_start();
            $data = array('tanggal_penting' => $tanggalpenting);
            $this->db->where('id',1);
            $this->db->update('setting', $data);
            $this->db->trans_complete(); 
        }

        function simpanHome($judul_kiri, $konten_kiri, $url_kiri, $judul_tengah, $konten_tengah, $url_tengah, $judul_kanan, $konten_kanan, $url_kanan) {
            $this->db->trans_start();
                
            $data = array('judul_kiri' => $judul_kiri, 'konten_kiri' => $konten_kiri, 'url_kiri' => $url_kiri, 'judul_tengah' => $judul_tengah, 'konten_tengah' => $konten_tengah, 'url_tengah' => $url_tengah, 'judul_kanan' => $judul_kanan, 'konten_kanan' => $konten_kanan, 'url_kanan' => $url_kanan);
            $this->db->where('id',1);
            $this->db->update('setting', $data);
            $this->db->trans_complete(); 
        }
	
	function simpanSetting($judul, $title, $alamat, $address, $phone, $fax, $email, $facebook, $twitter, $article_width, $article_height, $slide_height, $slide_width, $project_height, $project_width, $tags)
	{
		$this->db->trans_start();
                
		$data = array('judul' => $judul, 'title' => $title, 'alamat' => $alamat, 'address' => $address, 'contact' => $phone, 'fax' => $fax, 'email_to' => $email, 'facebook' => $facebook, 'twitter' => $twitter, 'article_width' => $article_width, 'article_height' => $article_height, 'slide_width' => $slide_width, 'slide_height' => $slide_height, 'project_width' => $project_width, 'project_height' => $project_height,'default_meta' => $tags);
		$this->db->where('id',1);
		$this->db->update('setting', $data);
		$this->db->trans_complete(); 
	}

    function saveSetting($judul, $alamat, $long, $lat, $telp, $fax, $email, $email_to, $default_meta, $products_meta, $contact_meta, $news_meta, $analytic, $deskripsi, $fb, $twitter, $instagram, $googleplus, $line, $youtube, $password ) {
        $this->db->trans_start();
        $data = array(
                        'judul' => $judul,
                        'alamat' => nl2br($alamat),
                        'long' => $long,
                        'lat' => $lat,
                        'contact' => $telp,
                        'fax' => $fax,
                        'email' => $email,
                        'email_to' => $email_to,
                        'default_meta' => $default_meta,
                        'products_meta' => $products_meta,
                        'contact_meta' => $contact_meta,
                        'news_meta' => $news_meta,
                        'analytic' => $analytic,
                        'desc' => $deskripsi,
                        'facebook' => $fb,
                        'twitter' => $twitter,
                        'instagram' => $instagram,
                        'googleplus' => $googleplus,
                        'password_email' => $password,
'line' => $line,
'youtube' => $youtube

                        );
        $this->db->where('id',1);
        $this->db->update('setting', $data);
        $this->db->trans_complete(); 
    }
	
	
	
	function ubahPassword($username, $password)
	{
		$this->db->trans_start();
                $this->load->helper('security');
            
                $queryGetSalt = $this->db->query('SELECT salt FROM admin WHERE username =\''.$username.'\';');
                if($queryGetSalt->num_rows() > 0) {
                    $queryGetSalt = $queryGetSalt->result_array();
                    $garam = $queryGetSalt[0]['salt'];
                    $password = do_hash($password.$garam, 'md5');
                    
                    $query = $this->db->query('UPDATE admin SET password = \''.$password.'\' WHERE username = \''.$username.'\';');
                }
		$this->db->trans_complete(); 
    }
} ?>