<?php
	class Companymodel extends CI_Model {
   
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
		$this->load->database();
    }
    
    function getCompany() {    	
		$this->db->order_by('name', 'asc');
		$q = $this->db->get_where('company', array('is_deleted' => 0));
		return $q->result();
    }

    function getCompanyId($id='') {   	
   		$q = $this->db->get_where('company', array('id' => $id));   		
		return $q->row();
    }

    function getCompanyByAlias($alias) {
    	$q = $this->db->get_where('company', array('alias' => $alias));   		
		return $q->row();
    }
    
    function addCompany($name='', $description='', $alias='')
	{
		$this->db->trans_start();
		$data = array( 	'name' => $name,
					    'description' => $description,
					    'alias' => $alias
					);

		$this->db->insert('company', $data);
		$x = $this->db->insert_id(); 
		$this->db->trans_complete(); 
		return $x;
	}	

	function delCompany($id) {
		$this->db->trans_start();

		$data = array( 	
					    'is_deleted' => 1
					);

		$this->db->where('id', $id);
		$this->db->update('company', $data);
		$this->db->trans_complete(); 
	}

	function editCompanyContact($id, $email, $email_system, $email_feedback, $email_feedback_en) {
		$this->db->trans_start();

		$data = array( 	
					    'email' => $email,
					    'email_system' => $email_system,
					    'email_feedback' => $email_feedback,
					    'email_feedback_en' => $email_feedback_en
					);

		$this->db->where('id', $id);
		$this->db->update('company', $data);
		$this->db->trans_complete(); 
	}

	function editCompany($id,  $name='', $description='', $alias='')
	{
		$this->db->trans_start();

		$data = array( 	
					    'name' => $name,
					    'description' => $description,
					    'alias' => $alias
					);

		$this->db->where('id', $id);
		$this->db->update('company', $data);
		$this->db->trans_complete(); 
	}
} ?>