<?php
	class Settingmodel extends CI_Model {
   
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
		$this->load->database();
    }
    
  	function getSetting() { 
		$q = $this->db->get_where('setting', array('id' => 1));
		return $q->row();
    }

    function updateSettingData($judul, $address, $desc, $contact, $fax, $email, $long, $lat) {
    	$this->db->trans_start();
    	$data = array( 	
					    'judul' => $judul,
					    'address' => $address,					  
					    'desc' => $desc,
					    'contact' => $contact,
					    'fax' => $fax,
					    'email' => $email,
					    'long' => $long,
					    'lat' => $lat
					);

		$this->db->where('id', 1);
		$this->db->update('setting', $data);
    	$this->db->trans_complete(); 
    }



    function updateSettingSocial($facebook, $twitter, $googleplus, $instagram, $line, $youtube) {
    	$this->db->trans_start();
    	$data = array( 	
					    'facebook' => $facebook,
					    'twitter' => $twitter,					  
					    'googleplus' => $googleplus,
					    'instagram' => $instagram,
					    'line' => $line,
					    'youtube' => $youtube
					);

		$this->db->where('id', 1);
		$this->db->update('setting', $data);
    	$this->db->trans_complete(); 
    }

	function updateSettingMeta($default_meta, $products_meta, $news_meta, $contact_meta) {
    	$this->db->trans_start();
    	$data = array( 	
					    'default_meta' => $default_meta,
					    'products_meta' => $products_meta,					  
					    'news_meta' => $news_meta,
					    'contact_meta' => $contact_meta
					);

		$this->db->where('id', 1);
		$this->db->update('setting', $data);
    	$this->db->trans_complete(); 
    }
	
} ?>