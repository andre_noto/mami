<?php
	class Aboutmodel extends CI_Model {
   
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
		$this->load->database();
    }
    
    function getAbout($company_id='') {   		
		$q = $this->db->get_where('about', array('company_id' => $company_id));
		
		if($q->num_rows()==0) {
			$data = array(
			   'company_id' => $company_id
			);

			$this->db->insert('about', $data);
			$q = $this->db->get_where('about', array('company_id' => $company_id)); 
		}
		return $q->row();
    }

    function editContent($company_id,  $vision, $vision_en, $mission, $mission_en, $value, $value_en)
	{
		$this->db->trans_start();
		$data = array( 	
					    'vision' => $vision,
					    'vision_en' => $vision_en,
					    'mission' => $mission,
					    'mission_en' => $mission_en,
					    'value' => $value,
					    'value_en' => $value_en
					);

		$this->db->where('company_id', $company_id);
		$this->db->update('about', $data);
		$this->db->trans_complete(); 
	}
} ?>