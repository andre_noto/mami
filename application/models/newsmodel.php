<?php
	class Newsmodel extends CI_Model {
   
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
		$this->load->database();
    }
    
    function getNews($company_id, $limit = 0, $orderby='', $order='asc') {    
    	
    	if($orderby != '') {
    		$this->db->order_by($orderby, $order);
    	} else {
    		$this->db->order_by('created', 'desc');	
    	}
    	if($limit > 0 ) {
    		$this->db->limit($limit);
    	}
    	$q = $this->db->get_where('news', array('company_id' => $company_id));
		
		return $q->result();
    }

    function getProductByCat($cat = 'all'){
    	if($cat =='all') {
    		$this->db->order_by('name','asc');
    		$q = $this->db->get('product');
    		return $q->result();
    	} else {
    		$this->db->order_by('name', 'asc');
    		$q = $this->db->get_where('product', array('category_id' => $cat, 'visibility' => 1));
    		return $q->result();
    	}
    }

   function getNewsId($id='', $visibility = false) {
   		if($visibility == true) {
   			$q = $this->db->get_where('news', array('id' => $id, 'visibility' => 1));
   		} else {
   			$q = $this->db->get_where('news', array('id' => $id));
   		}  	
		return $q->row();
    }

    function getFirstImages($product, $big = false) {
    	$data = array();
    	$k = 0;
    	foreach ($product as $key) {
    		$this->db->limit(1);
    		$this->db->order_by('order','asc');
    		$q = $this->db->get_where('news_image', array('news_id' => $key->id));
    		if($q->num_rows() >0) {
    			$hq = $q->row();
    			if($big == false){
    				$data[$k] = $hq->thumbimg;
    			} else {
    				$data[$k] = $hq->img;
    			}
    		} else {
    			$data[$k] = false;
    		}
    		$k++;
    	}

    	return $data;
    }

    function updateViewed($id) {
    	$this->db->query("UPDATE news SET viewed = viewed + 1 WHERE id ='$id'");
    }

    function updateNewsImages($product_id, $img, $thumbimg) {
    	$this->db->trans_start();
		// cek order
		$q = $this->db->get_where('news_image', array('news_id' => $product_id));
		$urutan = $q->num_rows() + 1;
		$data = array( 	'news_id' => $product_id,
					    'img' => $img,					  
					    'thumbimg' => $thumbimg,
					    'order' => $urutan
					);

		$this->db->insert('news_image', $data); 
		$this->db->trans_complete(); 

    }

    function getNewsImages($article_id) {
    	$this->db->order_by('order', 'asc');
    	$q = $this->db->get_where('news_image', array('news_id' => $article_id));
    	return $q->result();
    }

    function getCategory() {
    	$q = $this->db->get_where('product_category');
    	return $q->result();
    }

    function getUnit() {
    	$q = $this->db->get_where('unit');
    	return $q->result();
    }

    function addNews($company_id, $title='', $title_en='', $short_desc='', $short_desc_en='', $content='', $content_en='', $published='', $meta='', $visibility=true)
	{
		$this->db->trans_start();
		$data = array( 	'title' => $title,
						'title_en' => $title_en,
					    'content' => $content,					  
					    'content_en' => $content_en,	
					    'short_desc' => $short_desc,
					    'short_desc_en' => $short_desc_en,
					    'published' => $published,
					    'meta' => $meta,
					    'company_id' => $company_id,
					    'visibility' => $visibility
					);

		$this->db->insert('news', $data);
		$x = $this->db->insert_id(); 
		$this->db->trans_complete(); 
		return $x;
	}

	function editNews($id,  $title='', $title_en='', $short_desc='', $short_desc_en='', $content='', $content_en='', $published='', $meta='', $visibility=true)
	{
		$this->db->trans_start();

		$data = array( 	'title' => $title,
						'title_en' => $title_en,
					    'content' => $content,					  
					    'content_en' => $content_en,					  
					    'short_desc' => $short_desc,
					    'short_desc_en' => $short_desc_en,
					    'published' => $published,
					    'meta' => $meta,
					    'visibility' => $visibility
					);

		$this->db->where('id', $id);
		$this->db->update('news', $data);
		$this->db->trans_complete(); 
	}

	function delImage($id) {
		$q = $this->db->get_where('news_image', array('id' =>$id));
		if($q->num_rows() >0) {
			// unlink gambar
			$this->load->helper("file");
			$hq = $q->row();
			delete_files('images/news/'.$hq->img);
			delete_files('images/news/'.$hq->thumbimg);

			$q = $this->db->delete("news_image", array("id" => $id));
		}
		
	}

	function delNews($id) {
		$q = $this->db->get_where('news', array('id' =>$id));
		if($q->num_rows() >0) {
			
			// unlink gambar
			$this->load->helper("file");
			$hq = $q->row();

			$p = $this->db->get_where('news_image', array('news_id' => $id));
			if($p->num_rows() >0) {
				$hp = $p->result();
				foreach ($hp as $key) {
					@delete_files('images/news/'.$key->img);
					@delete_files('images/news/'.$key->thumbimg);					
					$this->db->delete("news_image", array('id' => $key->id));
					
				}
			}
			$q = $this->db->delete("news", array("id" => $id));
		}
	}

    //// END OF SLIDEMODEL //// 

    function toggleTampilan($id) {
    	$this->db->query('UPDATE slide
   						  SET visibility = !visibility
 						  WHERE id = '.$id);
    	if($this->db->affected_rows() > 0) {
    		return true;
    	} else {
    		return false;
    	}
    }
    

    function switchOrder($id,  $sort_order) {
		// cek order 
		$query = $this->db->get_where('slide', array( 'id' => $id));
		$hasil = $query->row();

		$urutanlama = $hasil->order;

		$query = $this->db->get_where('slide', array( 'order' => $sort_order));
		$hasil = $query->row();
		$idlama = $hasil->id;

		// switch
		$data = array(
					'order' => $sort_order
				);

		$this->db->where('id', $id);
		$this->db->update('slide', $data); 

		// switch
		$data = array(
					'order' => $urutanlama
				);

		$this->db->where('id', $idlama);
		$this->db->update('slide', $data); 
	}
	
	function tukarOrder($idA, $idB)
	{
		
		$this->db->trans_start();
		
		if($idA != $idB)
		{
		
			$query = $this->db->query('SELECT * FROM slide WHERE `order` = '.$idA.';');		
			$rowA = $query->row();
			
			$query = $this->db->query('SELECT id FROM slide WHERE `order` = '.$idB.';');		
			$rowB = $query->row(); 
			
			$data = array('order' => $idB);
			$this->db->where('id', $rowA->id);
			$this->db->update('slide', $data);
			
			$data = array('order' => $idA);
			$this->db->where('id', $rowB->id);
			$this->db->update('slide', $data);
		}
		
		$this->db->trans_complete(); 
	}
	
	function loadSlideID($id)
	{
		$query = $this->db->get_where('slide', array('id' => $id));
		if($query->num_rows() > 0)
		{	return $query->row();	} 
		else
		{ return false; } 
	}
	
	function loadSlide()
	{
		//$this->db->order_by("tanggal", "desc"); 
		
		$query = $this->db->query('SELECT * FROM slide ORDER BY `slide`.`order` ASC;');
		

	//	$query = $this->db->get('artikel', $limit, $offset);
		if($query->num_rows() >0) 
		{	return $query->result_array();	}
		else
		{	return 0;	}
	}
	
	
	function tambahSlide($judul, $deskripsi, $url)
	{
		$this->db->trans_start();
		
		// find order
		$query = $this->db->query('SELECT COUNT(id) AS jml FROM slide');
		$row = $query->row(); 

		$data = array('id' => '' ,
					  'judul' => $judul ,
					  'subjudul' => $deskripsi, 
					  'url' => $url,
					  'order' => $row->jml	+1	  
					           );
		$this->db->insert('slide', $data); 		
		$lastid = $this->db->insert_id();
		$this->db->trans_complete(); 
		
		return $lastid;
		
	}
	
	
} ?>