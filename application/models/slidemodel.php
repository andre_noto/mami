<?php
	class Slidemodel extends CI_Model {
   
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
		$this->load->database();
    }
    
    function getSlide($id = '', $limit=0) {
    	if($id !='') {
    		$q = $this->db->get_where('slide', array('id' => $id));
    		return $q->row();
    	} else {
    		$this->db->order_by('order', 'asc');
    		if($limit > 0) {
    			$this->db->limit($limit);
    		}
    		$q = $this->db->get_where('slide');
    		return $q->result();
    	}
    }

    function getSlides($limit = 5) {
    	$q = $this->db->get_where('slide', array('visibility' => 1));
    	return $q->result();
    }

    function addSlide($url='', $judul='', $subjudul='', $text_position='left', $cta='', $visibility=true, $img = '', $thumbimg = '')
	{
		$this->db->trans_start();
		// cek order
		$q = $this->db->get('slide');
		$urutan = $q->num_rows() + 1;

		$data = array( 	'url' => $url,
					    'judul' => $judul,					  
					    'subjudul' => $subjudul,
					    'text_position' => $text_position,
					    'cta' => $cta,
					    'visibility' => $visibility,
					    'order' => $urutan,
					    'img' => $img,
					    'thumbimg' => $thumbimg
					);

		$this->db->insert('slide', $data); 
		$this->db->trans_complete(); 
	}

	function editSlide($id, $url='', $judul='', $subjudul='', $text_position='left', $cta='', $visibility=true, $img = '', $thumbimg = '')
	{
		$this->db->trans_start();

		$data = array( 	'url' => $url,
					    'judul' => $judul,					  
					    'subjudul' => $subjudul,
					    'text_position' => $text_position,
					    'cta' => $cta,
					    'visibility' => $visibility,
					    'img' => $img,
					    'thumbimg' => $thumbimg
					);

		$this->db->where('id', $id);
		$this->db->update('slide', $data);
		$this->db->trans_complete(); 
	}

	function delSlide($id) {
		$q = $this->db->get_where('slide', array('id' =>$id));
		if($q->num_rows() >0) {
			// unlink gambar
			$this->load->helper("file");
			$hq = $q->row();
			delete_files('images/slides/'.$hq->img);
			delete_files('images/slides/'.$hq->thumbimg);

			$order = $hq->order;
			// update semua slide 
			$q = $this->db->query("UPDATE slide SET `order` = `order`- 1 WHERE `order` > $order;");
			$q = $this->db->delete("slide", array("id" => $id));
		}
	}

    //// END OF SLIDEMODEL //// 

    function toggleTampilan($id) {
    	$this->db->query('UPDATE slide
   						  SET visibility = !visibility
 						  WHERE id = '.$id);
    	if($this->db->affected_rows() > 0) {
    		return true;
    	} else {
    		return false;
    	}
    }
    

    function switchOrder($id,  $sort_order) {
		// cek order 
		$query = $this->db->get_where('slide', array( 'id' => $id));
		$hasil = $query->row();

		$urutanlama = $hasil->order;

		$query = $this->db->get_where('slide', array( 'order' => $sort_order));
		$hasil = $query->row();
		$idlama = $hasil->id;

		// switch
		$data = array(
					'order' => $sort_order
				);

		$this->db->where('id', $id);
		$this->db->update('slide', $data); 

		// switch
		$data = array(
					'order' => $urutanlama
				);

		$this->db->where('id', $idlama);
		$this->db->update('slide', $data); 
	}
	
	function tukarOrder($idA, $idB)
	{
		
		$this->db->trans_start();
		
		if($idA != $idB)
		{
		
			$query = $this->db->query('SELECT * FROM slide WHERE `order` = '.$idA.';');		
			$rowA = $query->row();
			
			$query = $this->db->query('SELECT id FROM slide WHERE `order` = '.$idB.';');		
			$rowB = $query->row(); 
			
			$data = array('order' => $idB);
			$this->db->where('id', $rowA->id);
			$this->db->update('slide', $data);
			
			$data = array('order' => $idA);
			$this->db->where('id', $rowB->id);
			$this->db->update('slide', $data);
		}
		
		$this->db->trans_complete(); 
	}
	
	function loadSlideID($id)
	{
		$query = $this->db->get_where('slide', array('id' => $id));
		if($query->num_rows() > 0)
		{	return $query->row();	} 
		else
		{ return false; } 
	}
	
	function loadSlide()
	{
		//$this->db->order_by("tanggal", "desc"); 
		
		$query = $this->db->query('SELECT * FROM slide ORDER BY `slide`.`order` ASC;');
		

	//	$query = $this->db->get('artikel', $limit, $offset);
		if($query->num_rows() >0) 
		{	return $query->result_array();	}
		else
		{	return 0;	}
	}
	
	
	function tambahSlide($judul, $deskripsi, $url)
	{
		$this->db->trans_start();
		
		// find order
		$query = $this->db->query('SELECT COUNT(id) AS jml FROM slide');
		$row = $query->row(); 

		$data = array('id' => '' ,
					  'judul' => $judul ,
					  'subjudul' => $deskripsi, 
					  'url' => $url,
					  'order' => $row->jml	+1	  
					           );
		$this->db->insert('slide', $data); 		
		$lastid = $this->db->insert_id();
		$this->db->trans_complete(); 
		
		return $lastid;
		
	}
	
	
} ?>