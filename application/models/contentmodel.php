<?php
	class Contentmodel extends CI_Model {
   
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
		$this->load->database();
    }

    function getContentType() {    	
		$this->db->order_by('name', 'asc');
		$q = $this->db->get('content_type');
		return $q->result();
    }
    
    function getContent($company='') {    	
		$this->db->order_by('content.menu_order', 'asc');
		$this->db->join('company', 'company.id = content.company_id');
		$this->db->join('content_type', 'content_type.id=content.content_type');
		$this->db->select('content.*, company.name as "company", content_type.name as "contentype"');

		if($company!=''){
			$q= $this->db->get_where('content', array('content.company_id' => $company));
		} else {
			$q= $this->db->get('content');
		}

		return $q->result();
    }

    function getContentId($id='') {   	
   		$q = $this->db->get_where('content', array('id' => $id));   		
		return $q->row();
    }

    function addContent($name='', $description='', $description_english='', $alias='', $company_id=0, $content_type=0)
	{
		$this->db->trans_start();
		$q = $this->db->get_where('content', array('company_id' => $company_id));
		$rank= $q->num_rows()+1;
		$data = array( 	'name' => $name,
					    'description' => $description,
					    'description_en' => $description_english,
					    'alias' => $alias,
					    'company_id' => $company_id,
					    'menu_order' => $rank,
					    'content_type' => $content_type
					);

		$this->db->insert('content', $data);
		$x = $this->db->insert_id(); 
		$this->db->trans_complete(); 
		return $x;
	}	

	function updateRank($id, $newrank) {
		$this->db->trans_start();
		$data = array( 'menu_order' => $newrank);
		$this->db->where('id', $id);
		$this->db->update('content', $data);		
		$this->db->trans_complete(); 
	}

	function delContent($id) {
		$q = $this->db->get_where('content', array('id' =>$id));
		if($q->num_rows() >0) {
			$hq = $q->row();
			$company_id = $hq->company_id;
			$order = $hq->menu_order;
			// update semua slide 
			$q = $this->db->query("UPDATE content SET `menu_order` = `menu_order`- 1 WHERE `menu_order` > $order AND `company_id` = $company_id;");
			$q = $this->db->delete("content", array("id" => $id));
		}
	}

	function editContent($id,  $name='', $alias='', $description='', $description_en='', $company_id='', $content_type='')
	{
		$this->db->trans_start();

		$data = array( 	
					    'name' => $name,
					    'alias' => $alias,
					    'description' => $description,
					    'description_en' => $description_en,
					    'company_id' => $company_id,
					    'content_type' => $content_type,
					);

		$this->db->where('id', $id);
		$this->db->update('content', $data);
		$this->db->trans_complete(); 
	}
} ?>