<?php
	class Productmodel extends CI_Model {
   
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
		$this->load->database();
    }
    
    function getProduct($company_id) {    	
		$this->db->order_by('product_order', 'asc');
		$q = $this->db->get_where('product', array('company_id' => $company_id));
		return $q->result();
    }

    function getProductByCat($cat = 'all'){
    	if($cat =='all') {
    		$this->db->order_by('name','asc');
    		$q = $this->db->get('product');
    		return $q->result();
    	} else {
    		$this->db->order_by('name', 'asc');
    		$q = $this->db->get_where('product', array('category_id' => $cat, 'visibility' => 1));
    		return $q->result();
    	}
    }

   function getProductId($id='', $visibility = false) {
   		if($visibility == true) {
   			$q = $this->db->get_where('product', array('id' => $id, 'visibility' => 1));
   		} else {
   			$q = $this->db->get_where('product', array('id' => $id));
   		}  	
		return $q->row();
    }

    function getFirstImages($product, $big = false) {
    	$data = array();
    	$k = 0;
    	foreach ($product as $key) {
    		$this->db->limit(1);
    		$this->db->order_by('order','asc');
    		$q = $this->db->get_where('product_images', array('product_id' => $key->id));
    		if($q->num_rows() >0) {
    			$hq = $q->row();
    			if($big == false){
    				$data[$k] = $hq->thumbimg;
    			} else {
    				$data[$k] = $hq->img;
    			}
    		} else {
    			$data[$k] = false;
    		}
    		$k++;
    	}

    	return $data;
    }


    function updateProductImages($product_id, $img, $thumbimg) {
    	$this->db->trans_start();
		// cek order
		$q = $this->db->get_where('product_images', array('product_id' => $product_id));
		$urutan = $q->num_rows() + 1;
		$data = array( 	'product_id' => $product_id,
					    'img' => $img,					  
					    'thumbimg' => $thumbimg,
					    'order' => $urutan
					);

		$this->db->insert('product_images', $data); 
		$this->db->trans_complete(); 

    }

    function getProductImages($article_id) {
    	$this->db->order_by('order', 'asc');
    	$q = $this->db->get_where('product_images', array('product_id' => $article_id));
    	return $q->result();
    }

    function getCategory() {
    	$q = $this->db->get_where('product_category');
    	return $q->result();
    }

    function getUnit() {
    	$q = $this->db->get_where('unit');
    	return $q->result();
    }

    function addProduct($company_id, $name='', $desc='', $desc_en='', $price='', $short_desc='', $short_desc_en='', $status='ready', $meta='', $visibility=true)
	{
		$this->db->trans_start();
		$q = $this->db->get_where('product', array('company_id' => $company_id));
		$rank= $q->num_rows()+1;

		$data = array( 	'name' => $name,
					    'desc' => $desc,
					    'desc_en' => $desc_en,					  
					    'short_desc' => $short_desc,
					    'short_desc_en' => $short_desc_en,					   
					    'status' => $status,
					    'price' => $price,					   
					    'meta' => $meta,
					    'product_order' => $rank,
					    'company_id' => $company_id,
					    'visibility' => $visibility
					);

		$this->db->insert('product', $data);
		$x = $this->db->insert_id(); 
		$this->db->trans_complete(); 
		return $x;
	}

	function updateRank($id, $newrank) {
		$this->db->trans_start();
		$data = array( 'product_order' => $newrank);
		$this->db->where('id', $id);
		$this->db->update('product', $data);		
		$this->db->trans_complete(); 
	}


	function editProduct($id,  $name='', $desc='', $desc_en, $price='', $short_desc='', $short_desc_en='', $status='ready', $meta='', $visibility=true)
	{
		$this->db->trans_start();

		$data = array( 	
					    'name' => $name,
					    'desc' => $desc,	
					    'desc_en' => $desc_en,				  
					    'short_desc' => $short_desc,
					    'short_desc_en' => $short_desc_en,					    
					    'status' => $status,
					    'price' => $price,					    
					    'meta' => $meta,
					    'visibility' => $visibility
					);

		$this->db->where('id', $id);
		$this->db->update('product', $data);
		$this->db->trans_complete(); 
	}

	function delImage($id) {
		$q = $this->db->get_where('product_images', array('id' =>$id));
		if($q->num_rows() >0) {
			// unlink gambar
			$this->load->helper("file");
			$hq = $q->row();
			delete_files('images/products/'.$hq->img);
			delete_files('images/products/'.$hq->thumbimg);

			$q = $this->db->delete("product_images", array("id" => $id));
		}
		
	}

	function delProduct($id) {
		$q = $this->db->get_where('product', array('id' =>$id));
		if($q->num_rows() >0) {
			// unlink gambar
			$this->load->helper("file");
			$hq = $q->row();
			$company_id = $hq->company_id;
			$rank = $hq->product_order;

			$p = $this->db->get_where('product_images', array('product_id' => $id));
			if($p->num_rows() >0) {
				$hp = $p->result();
				foreach ($hp as $key) {
					delete_files('images/products/'.$key->img);
					delete_files('images/products/'.$key->thumbimg);
					$this->db->delete("product_images", array('id' => $key->id));
				}
			}
			

			$q = $this->db->query("UPDATE product SET `product_order` = `product_order`- 1 WHERE `product_order` > $rank AND `company_id` = $company_id;");
			$q = $this->db->delete("product", array("id" => $id));
		}
	}

    //// END OF SLIDEMODEL //// 

    function toggleTampilan($id) {
    	$this->db->query('UPDATE slide
   						  SET visibility = !visibility
 						  WHERE id = '.$id);
    	if($this->db->affected_rows() > 0) {
    		return true;
    	} else {
    		return false;
    	}
    }
    

    function switchOrder($id,  $sort_order) {
		// cek order 
		$query = $this->db->get_where('slide', array( 'id' => $id));
		$hasil = $query->row();

		$urutanlama = $hasil->order;

		$query = $this->db->get_where('slide', array( 'order' => $sort_order));
		$hasil = $query->row();
		$idlama = $hasil->id;

		// switch
		$data = array(
					'order' => $sort_order
				);

		$this->db->where('id', $id);
		$this->db->update('slide', $data); 

		// switch
		$data = array(
					'order' => $urutanlama
				);

		$this->db->where('id', $idlama);
		$this->db->update('slide', $data); 
	}
	
	function tukarOrder($idA, $idB)
	{
		
		$this->db->trans_start();
		
		if($idA != $idB)
		{
		
			$query = $this->db->query('SELECT * FROM slide WHERE `order` = '.$idA.';');		
			$rowA = $query->row();
			
			$query = $this->db->query('SELECT id FROM slide WHERE `order` = '.$idB.';');		
			$rowB = $query->row(); 
			
			$data = array('order' => $idB);
			$this->db->where('id', $rowA->id);
			$this->db->update('slide', $data);
			
			$data = array('order' => $idA);
			$this->db->where('id', $rowB->id);
			$this->db->update('slide', $data);
		}
		
		$this->db->trans_complete(); 
	}
	
	function loadSlideID($id)
	{
		$query = $this->db->get_where('slide', array('id' => $id));
		if($query->num_rows() > 0)
		{	return $query->row();	} 
		else
		{ return false; } 
	}
	
	function loadSlide()
	{
		//$this->db->order_by("tanggal", "desc"); 
		
		$query = $this->db->query('SELECT * FROM slide ORDER BY `slide`.`order` ASC;');
		

	//	$query = $this->db->get('artikel', $limit, $offset);
		if($query->num_rows() >0) 
		{	return $query->result_array();	}
		else
		{	return 0;	}
	}
	
	
	function tambahSlide($judul, $deskripsi, $url)
	{
		$this->db->trans_start();
		
		// find order
		$query = $this->db->query('SELECT COUNT(id) AS jml FROM slide');
		$row = $query->row(); 

		$data = array('id' => '' ,
					  'judul' => $judul ,
					  'subjudul' => $deskripsi, 
					  'url' => $url,
					  'order' => $row->jml	+1	  
					           );
		$this->db->insert('slide', $data); 		
		$lastid = $this->db->insert_id();
		$this->db->trans_complete(); 
		
		return $lastid;
		
	}
	
	
} ?>