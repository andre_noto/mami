<?php
	class Articlemodel extends CI_Model {
   
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
		$this->load->database();
    }
    
    function getArticle($category_id='') {    	
		$this->db->order_by('order', 'asc');
		$q = $this->db->get_where('article', array('category_id' => $category_id));
		return $q->result();
    }

    function getArticleMenu($category_id) {
    	$q = $this->db->get_where('article', array('category_id' => $category_id, 'visibility' => 1));
    	return $q->result();
    }

    function getArticleId($id='') {   	
		$q = $this->db->get_where('article', array('id' => $id));
		return $q->row();
	
    }

    function updateViewed($id) {
    	$this->db->query("UPDATE article SET viewed = viewed + 1 WHERE id ='$id'");
    }

    function updateArticleImages($article_id, $img, $thumbimg) {
    	$this->db->trans_start();
		// cek order
		$q = $this->db->get_where('article_images', array('article_id' => $article_id));
		$urutan = $q->num_rows() + 1;
		$data = array( 	'article_id' => $article_id,
					    'img' => $img,					  
					    'thumbimg' => $thumbimg,
					    'order' => $urutan
					);

		$this->db->insert('article_images', $data); 
		$this->db->trans_complete(); 

    }

    function getArticleImages($article_id) {
    	$this->db->order_by('order', 'asc');
    	$q = $this->db->get_where('article_images', array('article_id' => $article_id));
    	return $q->result();
    }

    function getCategory() {
    	$q = $this->db->get_where('category');
    	return $q->result();
    }

    function addArticle($category_id=0, $title='', $short_desc='', $content='', $meta='', $visibility=true)
	{
		$this->db->trans_start();
		// cek order
		$q = $this->db->get_where('article', array('category_id'=>$category_id));
		$urutan = $q->num_rows() + 1;

		$data = array( 	'category_id' => $category_id,
					    'title' => $title,					  
					    'short_desc' => $short_desc,
					    'content' => $content,
					    'meta' => $meta,
					    'visibility' => $visibility,
					    'order' => $urutan
					);

		$this->db->insert('article', $data);
		$x = $this->db->insert_id(); 
		$this->db->trans_complete(); 
		return $x;
	}

	function editArticle($id,  $title='', $short_desc='', $content='', $meta='', $visibility=true)
	{
		$this->db->trans_start();

		$data = array( 	
					    'title' => $title,					  
					    'short_desc' => $short_desc,
					    'content' => $content,
					    'meta' => $meta,
					    'visibility' => $visibility
					);

		$this->db->where('id', $id);
		$this->db->update('article', $data);
		$this->db->trans_complete(); 
	}

	function delImage($id) {
		$q = $this->db->get_where('article_images', array('id' =>$id));
		if($q->num_rows() >0) {
			// unlink gambar
			$this->load->helper("file");
			$hq = $q->row();
			delete_files('images/articles/'.$hq->img);
			delete_files('images/articles/'.$hq->thumbimg);

			$order = $hq->order;
			$article_id = $hq->article_id;
			// update semua urutan 
			$q = $this->db->query("UPDATE article_images SET `order` = `order`- 1 WHERE `order` > $order AND article_id = ".$article_id.";");
			$q = $this->db->delete("article_images", array("id" => $id));
		}
		
	}

	function delArticle($id) {
		$q = $this->db->get_where('article', array('id' =>$id));
		if($q->num_rows() >0) {
			// unlink gambar
			$this->load->helper("file");
			$hq = $q->row();

			$p = $this->db->get_where('article_images', array('article_id' => $id));
			if($p->num_rows() >0) {
				$hp = $p->result();
				foreach ($hp as $key) {
					delete_files('images/articles/'.$hp->img);
					delete_files('images/articles/'.$hp->thumbimg);
					$this->db->delete("article_images", array('id' => $key->id));
				}
			}

			$order = $hq->order;
			// update semua urutan 
			$q = $this->db->query("UPDATE article SET `order` = `order`- 1 WHERE `order` > $order AND category_id = ".$hq->category_id.";");
			$q = $this->db->delete("article", array("id" => $id));
		}
	}

    //// END OF SLIDEMODEL //// 

    function toggleTampilan($id) {
    	$this->db->query('UPDATE slide
   						  SET visibility = !visibility
 						  WHERE id = '.$id);
    	if($this->db->affected_rows() > 0) {
    		return true;
    	} else {
    		return false;
    	}
    }
    

    function switchOrder($id,  $sort_order) {
		// cek order 
		$query = $this->db->get_where('slide', array( 'id' => $id));
		$hasil = $query->row();

		$urutanlama = $hasil->order;

		$query = $this->db->get_where('slide', array( 'order' => $sort_order));
		$hasil = $query->row();
		$idlama = $hasil->id;

		// switch
		$data = array(
					'order' => $sort_order
				);

		$this->db->where('id', $id);
		$this->db->update('slide', $data); 

		// switch
		$data = array(
					'order' => $urutanlama
				);

		$this->db->where('id', $idlama);
		$this->db->update('slide', $data); 
	}
	
	function tukarOrder($idA, $idB)
	{
		
		$this->db->trans_start();
		
		if($idA != $idB)
		{
		
			$query = $this->db->query('SELECT * FROM slide WHERE `order` = '.$idA.';');		
			$rowA = $query->row();
			
			$query = $this->db->query('SELECT id FROM slide WHERE `order` = '.$idB.';');		
			$rowB = $query->row(); 
			
			$data = array('order' => $idB);
			$this->db->where('id', $rowA->id);
			$this->db->update('slide', $data);
			
			$data = array('order' => $idA);
			$this->db->where('id', $rowB->id);
			$this->db->update('slide', $data);
		}
		
		$this->db->trans_complete(); 
	}
	
	function loadSlideID($id)
	{
		$query = $this->db->get_where('slide', array('id' => $id));
		if($query->num_rows() > 0)
		{	return $query->row();	} 
		else
		{ return false; } 
	}
	
	function loadSlide()
	{
		//$this->db->order_by("tanggal", "desc"); 
		
		$query = $this->db->query('SELECT * FROM slide ORDER BY `slide`.`order` ASC;');
		

	//	$query = $this->db->get('artikel', $limit, $offset);
		if($query->num_rows() >0) 
		{	return $query->result_array();	}
		else
		{	return 0;	}
	}
	
	
	function tambahSlide($judul, $deskripsi, $url)
	{
		$this->db->trans_start();
		
		// find order
		$query = $this->db->query('SELECT COUNT(id) AS jml FROM slide');
		$row = $query->row(); 

		$data = array('id' => '' ,
					  'judul' => $judul ,
					  'subjudul' => $deskripsi, 
					  'url' => $url,
					  'order' => $row->jml	+1	  
					           );
		$this->db->insert('slide', $data); 		
		$lastid = $this->db->insert_id();
		$this->db->trans_complete(); 
		
		return $lastid;
		
	}
	
	
} ?>