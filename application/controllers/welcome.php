<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {
	public function index()
	{
		$data = array();
		$this->load->view('frontend/v_home', $data);		
	}

	public function metal() {
		$data = array();
		$this->load->view('frontend/v_header', $data);
		$this->load->view('frontend/v_metal', $data);
		$this->load->view('frontend/v_footer', $data);
	}
}
