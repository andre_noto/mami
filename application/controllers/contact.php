<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contact extends CI_Controller {
	public function index()
	{
		$data = array();
		$data['slides'] = $this->slidemodel->getSlides(5);
		$data['about'] = $this->articlemodel->getArticleMenu(1);
		$data['guides'] = $this->articlemodel->getArticleMenu(2);
		$data['beauty'] = $this->articlemodel->getArticleMenu(3);
		$data['setting'] = $this->settingmodel->getSetting(); 
		$data['error'] = '';
		

		if($this->input->post('btnsubmit')) {
			$response = $_POST["g-recaptcha-response"];
        	$url = 'https://www.google.com/recaptcha/api/siteverify';
        	$datax = array(
        		'secret' => '6Lc4NDcUAAAAAAymTE6telHtawdMtEiu4-SDcQHl',
        		'response' => $_POST["g-recaptcha-response"]
        	);
        	$options = array(
        		'http' => array (
        			'method' => 'POST',
        			 'header' => "Content-Type: application/x-www-form-urlencoded\r\n".
                    "User-Agent:MyAgent/1.0\r\n",
        			'content' => http_build_query($datax)
        		)
        	);
        	$context  = stream_context_create($options);
        	$verify = file_get_contents($url, false, $context);
        	$captcha_success=json_decode($verify);
        	
        	if ($captcha_success->success==false) {
        		$data['error'] = '<li>Invalid captcha please try again</li>';
        	} else {
        		$this->load->library('form_validation');
				$this->form_validation->set_rules('name', 'Name', 'trim|required');
				$this->form_validation->set_rules('message', 'Message', 'trim|required');
				$this->form_validation->set_rules('subject', 'Subject', 'trim|required');
				$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
				$this->form_validation->set_error_delimiters('<li style="color:white;">', '</li>');

				if ($this->form_validation->run())
				{
						$this->load->helper('security');

	                    $email = xss_clean($this->input->post('email'));
	                    $name = xss_clean($this->input->post('name'));
	                    $subject = xss_clean($this->input->post('subject'));
	                    $message = xss_clean($this->input->post('message'));

	                    $this->load->library('email');
	                    
	                    
	                    $this->email->set_mailtype("text");
	                    $this->email->set_newline("\r\n");
	                    
	                    $this->email->from($data['setting']->email_to, $name);
	                    $this->email->to($data['setting']->email);
	                    $this->email->reply_to($email);

	                    $this->email->subject($subject);
	                    $this->email->message(nl2br($message));

	                    if ( ! $this->email->send())
	                    {
	                        $this->session->set_flashdata('notif', 'failed');
	                    } else {
	                        $this->session->set_flashdata('notif', 'success');
	                    }

	                   redirect('contact');
				} 
        	}
		}

		$data['meta'] = html_entity_decode($data['setting']->contact_meta);
		$data['footerjs'] ='
		<script>var longc = '.$data['setting']->long.'; var latc='.$data['setting']->lat.';</script>
		<script src="'.base_url('js/google-map.js').'"></script>
  <script src="http://maps.google.com/maps/api/js?key=AIzaSyDA-TNP0I8buILCa3Xg3ExRveGcjd9ojEU&callback=initMap"></script>';

  		if($this->session->flashdata('notif') == 'success') {
  			$data['footerjs'] .= '<script>$("#suksesmodal").modal();</script>;';	
  		} else if($this->session->flashdata('notif') == 'failed') {
  			$data['footerjs'] .= '<script>$("#failedmodal").modal();</script>;';	
  		}
  		
		$this->load->view('frontend/v_header', $data);
		$this->load->view('frontend/v_contact', $data);
		$this->load->view('frontend/v_footer', $data);

	}
}
