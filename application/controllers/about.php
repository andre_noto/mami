<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class About extends CI_Controller {
	public function index()
	{
		redirect('notfound');
	}

	public function view($id = -1, $label='') {
		$data = array();
		$data['about'] = $this->articlemodel->getArticleMenu(1);
		$data['guides'] = $this->articlemodel->getArticleMenu(2);
		$data['beauty'] = $this->articlemodel->getArticleMenu(3);

		$data['content'] = $this->articlemodel->getArticleId($id);
		$data['images'] = $this->articlemodel->getArticleImages($id);
		$data['setting'] = $this->settingmodel->getSetting();

		if(count($data['content'])) {
			// update view
			if(!$this->session->userdata('view_article_'.$id)) {
				$this->session->set_userdata('view_article_'.$id, 'true');
				$this->articlemodel->updateViewed($id);
			}
			$data['meta'] = html_entity_decode($data['content']->meta);
			$this->load->view('frontend/v_header', $data);
			$this->load->view('frontend/v_about', $data);
			$this->load->view('frontend/v_footer', $data);
		} else {
			redirect('notfound');
		}
	}
}
