<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Company extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('user')) 
		{	redirect('backend/login'); }
	}

	public function index() // slideshow
	{

		$data = array();
		$data['setting'] = $this->backendmodel->getSettings();
		$data['title'] = $data['setting']->judul.' - Company';
		$data['pagetitle'] = 'Master Company';
		$data['pagedesc'] = 'manage master company as parent data for others related content';
		$data['js'] = '';

		if($this->session->flashdata('notif')) {
			$data['js'] .= $this->session->flashdata('notif');
		}

		// ADD NEW PRODUCT
		if($this->input->post('btnadd')) {

			$newid = $this->companymodel->addCompany($this->input->post('name'), $this->input->post('description'), $this->input->post('alias'));
			$data['js'] .= ' toastr.success("Data saved successfully!");';
			$this->session->set_flashdata('notif', $data['js']);
			redirect('backend/company');				
		}

		// EDIT PRODUCT
		if($this->input->post('btnedit')) {
		$this->companymodel->editCompany($this->input->post('hiddenid'),  $this->input->post('name'), $this->input->post('description'), $this->input->post('alias'));
		
			$data['js'] .= ' toastr.success("Data updated successfully!");';
			$this->session->set_flashdata('notif', $data['js']);
			redirect('backend/company');
		}
		
		$data['company'] = $this->companymodel->getCompany();
		
		$data['footerjs'] = '<!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="../assets/global/scripts/datatable.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
        <script src="../assets/pages/scripts/table-datatables-managed.min.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/bootstrap-toastr/toastr.min.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js" type="text/javascript"></script>
        <script src="../assets/pages/scripts/components-bootstrap-select.min.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js" type="text/javascript"></script>';

        $data['js'] .= '
		if ($(".wysihtml5").size() > 0) {
            $(".wysihtml5").wysihtml5({"color": true,
                "stylesheets": ["../assets/global/plugins/bootstrap-wysihtml5/wysiwyg-color.css"]
            });
        }

        $(".delbtn").on("click", function() {
        	var dataid = $(this).attr("dataid");
        	swal({
			  title: "Are you sure?",
			  text: "You will not be able to recover this data!",
			  type: "warning",
			  showCancelButton: true,
			  confirmButtonClass: "btn-danger",
			  confirmButtonText: "Yes, delete it!",
			  cancelButtonText: "No, cancel please!",
			  showLoaderOnConfirm: true,
			  closeOnConfirm: false,
			  closeOnCancel: false
			},
			function(isConfirm) {
			  if (isConfirm) {
			    $.post( "'.base_url('backend/company/deletecompany').'", { idslide: dataid })
				  .done(function( data ) {
				  	swal({
			            title: "Deleted!", 
			            text: "Your data has been deleted.", 
			            type: "success"
			        },function() {
		        	 	setTimeout(function () {
							location.reload();
					  	}, 100);
			        });
				  				    
				  });
			  } else {
			    swal("Cancelled", "Your data is safe :)", "error");
			  }
			});
        });
        


        	$(".editdata").on("click", function() {
        		$("#editproduct").modal("show");
        		var id = $(this).attr("dataid");
        		$.post( "'.base_url('backend/company/getcompany').'", { idslide: id })
				  .done(function( data ) {
				    $("#containerarticle").html( data );
				    if ($(".wysihtml5").size() > 0) {
				            $(".wysihtml5").wysihtml5({"color": true,
				                "stylesheets": ["../assets/global/plugins/bootstrap-wysihtml5/wysiwyg-color.css"]
				            });
				        }
				  });
        	});
        ';

		$this->load->view('backend/v_header', $data);
		$this->load->view('backend/company/v_company', $data);
		$this->load->view('backend/v_footer', $data);
	}

	public function deletecompany() {
		if($this->input->post('idslide')) {
			$id = $this->input->post('idslide');
			$this->companymodel->delCompany($id);
		}
	}

	public function deleteimage() {
		if($this->input->post('idslide')) {
			$id = $this->input->post('idslide');
			$this->productmodel->delImage($id);
		}
	}

	public function getcompany(){
		if($this->input->post('idslide')) {
			$data = array();
			if($this->input->post('errorlabel')) {
				$data['errorlabel'] = $this->input->post('errorlabel');
			}


			if($this->input->post('errorvalidation')) {
				$data['errorvalidation'] = $this->input->post('errorvalidation');
			}

			$data['company'] = $this->companymodel->getCompanyId($this->input->post('idslide'));
			if($data['company']) {
				echo $this->load->view('backend/company/v_company_edit', $data, TRUE);
			} else {
				redirect('notfound');
			}
		} else {
			redirect('notfound');
		}
	}
}
