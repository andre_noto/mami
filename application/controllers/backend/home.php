<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('user')) 
		{	redirect('backend/login'); }
	}

	public function index() // dashboard
	{
		$data = array();
		$data['setting'] = $this->backendmodel->getSettings();
		$data['title'] = $data['setting']->judul.' - Home';
		$data['pagetitle'] = 'Backend Home Page';
		$data['pagedesc'] = 'display summary of transactions and notifications';

		$this->load->view('backend/v_header', $data);
		$this->load->view('backend/v_home', $data);
		$this->load->view('backend/v_footer', $data);
	}

	public function logout() {
		$this->session->sess_destroy();
		redirect('backend/login');
	}

}
