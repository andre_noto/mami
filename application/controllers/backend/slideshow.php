<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Slideshow extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('user')) 
		{	redirect('backend/login'); }
	}

	public function index() // slideshow
	{
		$data = array();
		$data['setting'] = $this->backendmodel->getSettings();
		$data['title'] = $data['setting']->judul.' - Slideshow';
		$data['pagetitle'] = 'Slideshow Page';
		$data['pagedesc'] = 'manage slideshow image on frontend home page';
		$data['js'] = '';

		// ADD NEW SLIDESHOW
		if($this->input->post('btnadd')) {
			if (empty($_FILES['imageupload']['name'])) {
				$this->form_validation->set_rules('imageupload', 'Image Upload', 'required');
				$data['js'] .= '$("#addslideshow").modal("show"); ';
				if($this->form_validation->run() == FALSE) { }
			} else {
				// proses image
				$config['upload_path'] = './images/slides/';
				$config['allowed_types'] = 'gif|jpg|png';
				$config['max_size']	= '10000';
				$config['max_width']  = '3000';
				$config['max_height']  = '3000';
				$config['remove_spaces'] = TRUE;

				$this->load->library('upload', $config);

				if ( ! $this->upload->do_upload('imageupload'))
				{
					$error = $this->upload->display_errors();
					$data['error'] = '<p>'.$error.'</p>';
					//$this->load->view('upload_form', $error);
				}
				else
				{
					$dataadd = array('upload_data' => $this->upload->data());
					//print_r($dataadd);
					$config = array();
					$config['image_library'] = 'gd2';
					$config['source_image']	= './images/slides/'.$dataadd['upload_data']['file_name'];
					$config['new_image'] = './images/slides/'.$dataadd['upload_data']['raw_name'].'.jpg';
					$config['create_thumb'] = FALSE;
					$config['maintain_ratio'] = TRUE;
					$config['width']	= 1920;
					$config['height']	= 800;

					$this->load->library('image_lib', $config); 

					if ( ! $this->image_lib->resize())	{
					    $data['error'] = '<p>'.$this->image_lib->display_errors().'</p>';
					} else {
						// create thumbnail
						$config = array();
						$config['image_library'] = 'gd2';						
						$config['source_image'] = './images/slides/'.$dataadd['upload_data']['raw_name'].'.jpg';
						$config['new_image'] = './images/slides/'.$dataadd['upload_data']['raw_name'].'_thumb.jpg';
						$config['create_thumb'] = TRUE;
						$config['maintain_ratio'] = TRUE;
						$config['width']	= 100;
						$config['height']	= 75;
						$this->image_lib->initialize($config); 

						if ( ! $this->image_lib->resize())	{
							$data['error'] = '<p>'.$this->image_lib->display_errors().'</p>';
						} else {
							///READY TO SAVE
							$img = $dataadd['upload_data']['raw_name'].'.jpg';
							$thumbimg= $dataadd['upload_data']['raw_name'].'_thumb.jpg';
							$this->slidemodel->addSlide($this->input->post('url'), 
								$this->input->post('title'), $this->input->post('subtitle'), $this->input->post('optionposition'), $this->input->post('cta'), $this->input->post('optionvisibility'), $img, $thumbimg);

							$this->session->set_flashdata('notif', 'toastr.success("Data saved successfully!");');
								redirect('backend/slideshow');
						}
					}
				}
			}
		}

		// EDIT SLIDESHOW
		if($this->input->post('btnedit')) {
			$slide = $this->slidemodel->getSlide($this->input->post('hiddenid'));
			$img = $slide->img;
			$thumbimg = $slide->thumbimg;
			$adaerror = false;

			if (!empty($_FILES['imageupload']['name'])) {				
				// proses image
				$config['upload_path'] = './images/slides/';
				$config['allowed_types'] = 'gif|jpg|png';
				$config['max_size']	= '10000';
				$config['max_width']  = '3000';
				$config['max_height']  = '3000';
				$config['remove_spaces'] = TRUE;

				$this->load->library('upload', $config);

				if ( ! $this->upload->do_upload('imageupload'))
				{
					$error = $this->upload->display_errors();
					$data['error'] = '<p>'.$error.'</p>';
					$adaerror = true;
					//$data['js'] .= '$("#editslideshow").modal("show"); ';
					//$this->load->view('upload_form', $error);
				}
				else
				{
					$dataadd = array('upload_data' => $this->upload->data());
					//print_r($dataadd);
					$config = array();
					$config['image_library'] = 'gd2';
					$config['source_image']	= './images/slides/'.$dataadd['upload_data']['file_name'];
					$config['new_image'] = './images/slides/'.$dataadd['upload_data']['raw_name'].'.jpg';
					$config['create_thumb'] = FALSE;
					$config['maintain_ratio'] = TRUE;
					$config['width']	= 1920;
					$config['height']	= 800;

					$this->load->library('image_lib', $config); 

					if ( ! $this->image_lib->resize())	{
					    $data['error'] = '<p>'.$this->image_lib->display_errors().'</p>';
					    $adaerror = true;
					} else {
						// create thumbnail
						$config = array();
						$config['image_library'] = 'gd2';						
						$config['source_image'] = './images/slides/'.$dataadd['upload_data']['raw_name'].'.jpg';
						$config['new_image'] = './images/slides/'.$dataadd['upload_data']['raw_name'].'_thumb.jpg';
						$config['create_thumb'] = TRUE;
						$config['maintain_ratio'] = TRUE;
						$config['width']	= 100;
						$config['height']	= 75;
						$this->image_lib->initialize($config); 

						if ( ! $this->image_lib->resize())	{
							$data['error'] = '<p>'.$this->image_lib->display_errors().'</p>';
							$adaerror = true;
						}  else {
							$img = $dataadd['upload_data']['raw_name'].'.jpg';
							$thumbimg= $dataadd['upload_data']['raw_name'].'_thumb.jpg';
						}
					}
				}
			}

			// SAVE TO DB			
			$this->slidemodel->editSlide($this->input->post('hiddenid'), $this->input->post('url'), 
					$this->input->post('title'), $this->input->post('subtitle'), $this->input->post('optionposition'), $this->input->post('cta'), $this->input->post('optionvisibility'), $img, $thumbimg);

			if($adaerror == true) {
				$data['js'] .= '$("#editslideshow").modal("show"); 
							var id = "'.$this->input->post('hiddenid').'";
			        		$.post( "'.base_url('backend/slideshow/getslideshow').'", { idslide: id, errorlabel: "'.$data['error'].'", errorvalidation: "'.validation_errors().'" })
							  .done(function( data ) {
							    $("#container").html( data );
							  });
							';				
			} else {
				$this->session->set_flashdata('notif', 'toastr.success("Data updated successfully!");');
				redirect('backend/slideshow');
			}
						
		}
		
		// GET SLIDE DATA
		$data['slideshow'] = $this->slidemodel->getSlide();

		$data['footerjs'] = '<!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="../assets/global/scripts/datatable.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
        <script src="../assets/pages/scripts/table-datatables-managed.min.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/bootstrap-toastr/toastr.min.js" type="text/javascript"></script>';

        $data['js'] .= '$("#url").on("keyup", function() {
        	if($(this).val() != "") {
        		$("#cta").removeAttr("disabled");
        	} else {
        		$("#cta").attr("disabled", "disabled");
        	}
        });

        $(".delbtn").on("click", function() {
        	var dataid = $(this).attr("dataid");
        	swal({
			  title: "Are you sure?",
			  text: "You will not be able to recover this data!",
			  type: "warning",
			  showCancelButton: true,
			  confirmButtonClass: "btn-danger",
			  confirmButtonText: "Yes, delete it!",
			  cancelButtonText: "No, cancel please!",
			  showLoaderOnConfirm: true,
			  closeOnConfirm: false,
			  closeOnCancel: false
			},
			function(isConfirm) {
			  if (isConfirm) {
			    $.post( "'.base_url('backend/slideshow/deleteslide').'", { idslide: dataid })
				  .done(function( data ) {
				  	swal({
			            title: "Deleted!", 
			            text: "Your data has been deleted.", 
			            type: "success"
			        },function() {
		        	 	setTimeout(function () {
							location.reload();
					  	}, 100);
			        });
				  				    
				  });
			  } else {
			    swal("Cancelled", "Your data is safe :)", "error");
			  }
			});
        });
        


        	$(".editdata").on("click", function() {
        		$("#editslideshow").modal("show");
        		var id = $(this).attr("dataid");
        		$.post( "'.base_url('backend/slideshow/getslideshow').'", { idslide: id })
				  .done(function( data ) {
				    $("#container").html( data );
				  });
        	});
        ';

		$this->load->view('backend/v_header', $data);
		$this->load->view('backend/slideshow/v_slideshow', $data);
		$this->load->view('backend/v_footer', $data);
	}

	public function deleteslide() {
		if($this->input->post('idslide')) {
			$id = $this->input->post('idslide');
			$this->slidemodel->delSlide($id);
		}
	}

	public function getslideshow(){
		if($this->input->post('idslide')) {
			$data = array();
			if($this->input->post('errorlabel')) {
				$data['errorlabel'] = $this->input->post('errorlabel');
			}


			if($this->input->post('errorvalidation')) {
				$data['errorvalidation'] = $this->input->post('errorvalidation');
			}

			$data['slide'] = $this->slidemodel->getSlide($this->input->post('idslide'));
			if($data['slide']) {
				echo $this->load->view('backend/slideshow/v_slideshow_edit', $data, TRUE);
			} else {
				redirect('notfound');
			}
		} else {
			redirect('notfound');
		}
	}
}
