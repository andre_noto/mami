<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('user')) 
		{	redirect('backend/login'); }
	}

	public function index() // slideshow
	{
		$data = array();
		$data['setting'] = $this->backendmodel->getSettings();
		$data['title'] = $data['setting']->judul.' - Product';
		$data['pagetitle'] = 'Master Product';
		$data['pagedesc'] = 'manage master product displayed on website';
		$data['js'] = '';

		if(!$this->input->get('cat')) {				
			redirect('backend/home');					
		} else {
			$data['company'] = $this->companymodel->getCompanyId($this->input->get('cat'));					
		}

		if($this->session->flashdata('notif')) {
			$data['js'] .= $this->session->flashdata('notif');
		}

		// SORTING 
		if($this->input->post('sortingaction')) {			
			$i = 1;
			foreach ($this->input->post('sortid') as $key ) {
				$this->productmodel->updateRank($key, $i);
				$i++;
			}						
			redirect('backend/product?cat='.$this->input->get('cat'));							
		}

		// ADD NEW PRODUCT
		if($this->input->post('btnadd')) {

			$newid = $this->productmodel->addProduct($this->input->get('cat'), $this->input->post('name'), $this->input->post('desc'),$this->input->post('desc_en'),  $this->input->post('price'), $this->input->post('short_desc'), $this->input->post('short_desc_en'), $this->input->post('status'), $this->input->post('meta'), $this->input->post('visibility'));



			$data['js'] .= ' toastr.success("Data saved successfully!");';

			$tot  =$this->input->post('totalimg');
			for($k=1; $k<= $tot; $k++ ) {
				
				if (!empty($_FILES['imageupload'.$k]['name'])) {
					$config['upload_path'] = './images/products/';
					$config['allowed_types'] = 'gif|jpg|png';
					$config['max_size']	= '10000';
					$config['max_width']  = '3000';
					$config['max_height']  = '3000';
					$config['remove_spaces'] = TRUE;

					$this->load->library('upload', $config);
					
					if ( ! $this->upload->do_upload('imageupload'.$k))
					{
						$error = $this->upload->display_errors();
						$data['error'] = '<p>'.$error.'</p>';
						$data['js'] .= ' toastr.error("Error when uploading file: $error");';
					} else
					{
						$dataadd = array('upload_data' => $this->upload->data());
						//print_r($dataadd);
						$config = array();
						$config['image_library'] = 'gd2';
						$config['source_image']	= './images/products/'.$dataadd['upload_data']['file_name'];
						$config['new_image'] = './images/products/'.$dataadd['upload_data']['raw_name'].'.jpg';
						$config['create_thumb'] = FALSE;
						$config['maintain_ratio'] = TRUE;
						$config['width']	= 500;
						$config['height']	= 550;

						$this->load->library('image_lib', $config); 

						if ( ! $this->image_lib->resize())	{
						    $data['error'] = '<p>'.$this->image_lib->display_errors().'</p>';
						    $data['js'] .= ' toastr.error("Error when uploading file: '.$data['error'].'");';
						} else {
							// create thumbnail
							$config = array();
							$config['image_library'] = 'gd2';						
							$config['source_image'] = './images/products/'.$dataadd['upload_data']['raw_name'].'.jpg';
							$config['new_image'] = './images/products/'.$dataadd['upload_data']['raw_name'].'_thumb.jpg';
							$config['create_thumb'] = TRUE;
							$config['maintain_ratio'] = TRUE;
							$config['width']	= 100;
							$config['height']	= 75;
							$this->image_lib->initialize($config); 

							if ( ! $this->image_lib->resize())	{
								$data['error'] = '<p>'.$this->image_lib->display_errors().'</p>';
								$data['js'] .= ' toastr.error("Error when uploading file: '.$data['error'].'");';
							} else {
								///READY TO UPDATE
								$img = $dataadd['upload_data']['raw_name'].'.jpg';
								$thumbimg= $dataadd['upload_data']['raw_name'].'_thumb.jpg';


								$this->productmodel->updateProductImages($newid, $img, $thumbimg);

							}
						}
					}
				}
			}

			$this->session->set_flashdata('notif', $data['js']);
			redirect('backend/product?cat='.$this->input->get('cat'));
				
		}

		// EDIT PRODUCT
		if($this->input->post('btnedit')) {
			$this->productmodel->editProduct($this->input->post('hiddenid'),  $this->input->post('name'), $this->input->post('desc'),$this->input->post('desc_en'),  $this->input->post('price'), $this->input->post('short_desc'),  $this->input->post('short_desc_en'), $this->input->post('status'), $this->input->post('meta'), $this->input->post('visibility'));
			
			$adaerror = false;

			$tot  =$this->input->post('totalimgedit');
			for($k=1; $k<= $tot; $k++ ) {
				
				if (!empty($_FILES['imageupload'.$k]['name'])) {
					$config['upload_path'] = './images/products/';
					$config['allowed_types'] = 'gif|jpg|png';
					$config['max_size']	= '10000';
					$config['max_width']  = '3000';
					$config['max_height']  = '3000';
					$config['remove_spaces'] = TRUE;

					$this->load->library('upload', $config);
					
					if ( ! $this->upload->do_upload('imageupload'.$k))
					{
						$error = $this->upload->display_errors();
						$data['error'] = '<p>'.$error.'</p>';
						$data['js'] .= ' toastr.error("Error when uploading file: $error");';
					} else
					{
						$dataadd = array('upload_data' => $this->upload->data());
						//print_r($dataadd);
						$config = array();
						$config['image_library'] = 'gd2';
						$config['source_image']	= './images/products/'.$dataadd['upload_data']['file_name'];
						$config['new_image'] = './images/products/'.$dataadd['upload_data']['raw_name'].'.jpg';
						$config['create_thumb'] = FALSE;
						$config['maintain_ratio'] = TRUE;
						$config['width']	= 500;
						$config['height']	= 550;

						$this->load->library('image_lib', $config); 

						if ( ! $this->image_lib->resize())	{
						    $data['error'] = '<p>'.$this->image_lib->display_errors().'</p>';
						    $data['js'] .= ' toastr.error("Error when uploading file: '.$data['error'].'");';
						} else {
							// create thumbnail
							$config = array();
							$config['image_library'] = 'gd2';						
							$config['source_image'] = './images/products/'.$dataadd['upload_data']['raw_name'].'.jpg';
							$config['new_image'] = './images/products/'.$dataadd['upload_data']['raw_name'].'_thumb.jpg';
							$config['create_thumb'] = TRUE;
							$config['maintain_ratio'] = TRUE;
							$config['width']	= 100;
							$config['height']	= 75;
							$this->image_lib->initialize($config); 

							if ( ! $this->image_lib->resize())	{
								$data['error'] = '<p>'.$this->image_lib->display_errors().'</p>';
								$data['js'] .= ' toastr.error("Error when uploading file: '.$data['error'].'");';
							} else {
								///READY TO UPDATE
								$img = $dataadd['upload_data']['raw_name'].'.jpg';
								$thumbimg= $dataadd['upload_data']['raw_name'].'_thumb.jpg';


								$this->productmodel->updateProductImages($this->input->post('hiddenid'), $img, $thumbimg);

							}
						}
					}
				}
			}

			

			if($adaerror == true) {
				$data['js'] .= '$("#editproduct").modal("show"); 
							var id = "'.$this->input->post('hiddenid').'";
			        		$.post( "'.base_url('backend/product/getproduct').'", { idslide: id, errorlabel: "'.$data['error'].'", errorvalidation: "'.validation_errors().'" })
							  .done(function( data ) {
							    $("#containeredit").html( data );
							  });
							';				
			} else {
				$data['js'] .= ' toastr.success("Data updated successfully!");';
				$this->session->set_flashdata('notif', $data['js']);
				redirect('backend/product?cat='.$this->input->get('cat'));
			}
						
		}
		
		$data['product'] = $this->productmodel->getProduct($this->input->get('cat'));
		$data['images'] = $this->productmodel->getFirstImages($data['product']);
		

		$data['footerjs'] = '<!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="../assets/global/scripts/datatable.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
        <script src="../assets/pages/scripts/table-datatables-managed.min.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/bootstrap-toastr/toastr.min.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js" type="text/javascript"></script>
        <script src="../assets/pages/scripts/components-bootstrap-select.min.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js" type="text/javascript"></script>
         <script
  src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"
  integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU="
  crossorigin="anonymous"></script>';

        $data['js'] .= '

        $( function() {
		     $( "#sortable" ).sortable({
		        update: function( ) {
		           $("#sortingform").submit();
		        }
		    });
    		$( "#sortable" ).disableSelection();
		  } );


        $("#btnaddmore").on("click", function() {
        	var tot = $("#totalimg").val();
        	tot++;
        	var str = "<input type=\"file\" id=\"imageupload" + tot + "\" name=\"imageupload" + tot + "\">"+
                      "<p class=\"help-block\">(Required. Recommendation size: 1920 x 800 pixels.)</p>";
            $("#container").append(str);
            $("#totalimg").val(tot);
        });

        $("body").on("click", ".delimg", function() {
        	var id = $(this).attr("dataid");
        	
        	$.post( "'.base_url('backend/product/deleteimage').'", { idslide: id })
			  .done(function( data ) {
			  	$("#imgcontainer" + id).remove();			  				    
			  });
        });

        $("body").on("click", "#btnaddmoreedit", function() {
        	
        	var tot = $("body #totalimgedit").val();
        	tot++;
        	var str = "<input type=\"file\" id=\"imageupload" + tot + "\" name=\"imageupload" + tot + "\">"+
                      "<p class=\"help-block\">(Required. Recommendation size: 500 x 550 pixels.)</p>";
            $("body #containeredit").append(str);
            $("body #totalimgedit").val(tot);
        });

		if ($(".wysihtml5").size() > 0) {
            $(".wysihtml5").wysihtml5({"color": true,
                "stylesheets": ["../assets/global/plugins/bootstrap-wysihtml5/wysiwyg-color.css"]
            });
        }

        $(".delbtn").on("click", function() {
        	var dataid = $(this).attr("dataid");
        	swal({
			  title: "Are you sure?",
			  text: "You will not be able to recover this data!",
			  type: "warning",
			  showCancelButton: true,
			  confirmButtonClass: "btn-danger",
			  confirmButtonText: "Yes, delete it!",
			  cancelButtonText: "No, cancel please!",
			  showLoaderOnConfirm: true,
			  closeOnConfirm: false,
			  closeOnCancel: false
			},
			function(isConfirm) {
			  if (isConfirm) {
			    $.post( "'.base_url('backend/product/deleteproduct').'", { idslide: dataid })
				  .done(function( data ) {
				  	swal({
			            title: "Deleted!", 
			            text: "Your data has been deleted.", 
			            type: "success"
			        },function() {
		        	 	setTimeout(function () {
							location.reload();
					  	}, 100);
			        });
				  				    
				  });
			  } else {
			    swal("Cancelled", "Your data is safe :)", "error");
			  }
			});
        });
        


        	$(".editdata").on("click", function() {
        		$("#editproduct").modal("show");
        		var id = $(this).attr("dataid");
        		$.post( "'.base_url('backend/product/getproduct').'", { idslide: id })
				  .done(function( data ) {
				    $("#containerarticle").html( data );
				    if ($(".wysihtml5").size() > 0) {
				            $(".wysihtml5").wysihtml5({"color": true,
				                "stylesheets": ["../assets/global/plugins/bootstrap-wysihtml5/wysiwyg-color.css"]
				            });
				        }
				  });
        	});
        ';

		$this->load->view('backend/v_header', $data);
		$this->load->view('backend/product/v_product', $data);
		$this->load->view('backend/v_footer', $data);
	}

	public function deleteproduct() {
		if($this->input->post('idslide')) {
			$id = $this->input->post('idslide');
			$this->productmodel->delProduct($id);
		}
	}

	public function deleteimage() {
		if($this->input->post('idslide')) {
			$id = $this->input->post('idslide');
			$this->productmodel->delImage($id);
		}
	}

	public function getproduct(){
		if($this->input->post('idslide')) {
			$data = array();
			if($this->input->post('errorlabel')) {
				$data['errorlabel'] = $this->input->post('errorlabel');
			}


			if($this->input->post('errorvalidation')) {
				$data['errorvalidation'] = $this->input->post('errorvalidation');
			}

			$data['product'] = $this->productmodel->getProductId($this->input->post('idslide'));
			if($data['product']) {

				$data['category'] =  $this->productmodel->getCategory();
				$data['unit'] =  $this->productmodel->getUnit();
				$data['images'] = $this->productmodel->getProductImages($this->input->post('idslide'));	
				echo $this->load->view('backend/product/v_product_edit', $data, TRUE);
			} else {
				redirect('notfound');
			}
		} else {
			redirect('notfound');
		}
	}
}
