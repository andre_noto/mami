<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Article extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('user')) 
		{	redirect('backend/login'); }
	}

	public function index() // slideshow
	{
		$data = array();
		$data['setting'] = $this->backendmodel->getSettings();
		$data['title'] = $data['setting']->judul.' - Article';
		$data['pagetitle'] = 'Web Article';
		$data['pagedesc'] = 'manage article pages displayed on website';
		$data['js'] = '';

		if($this->session->flashdata('notif')) {
			$data['js'] .= $this->session->flashdata('notif');
		}

		// ADD NEW ARTICLE
		if($this->input->post('btnadd')) {

			$newid = $this->articlemodel->addArticle($this->input->post('category_id'), $this->input->post('title'), $this->input->post('short_desc'), $this->input->post('content'), $this->input->post('meta'), $this->input->post('visibility'));

			$data['js'] .= ' toastr.success("Data saved successfully!");';

			$tot  =$this->input->post('totalimg');
			for($k=1; $k<= $tot; $k++ ) {
				
				if (!empty($_FILES['imageupload'.$k]['name'])) {
					$config['upload_path'] = './images/articles/';
					$config['allowed_types'] = 'gif|jpg|png';
					$config['max_size']	= '10000';
					$config['max_width']  = '3000';
					$config['max_height']  = '3000';
					$config['remove_spaces'] = TRUE;

					$this->load->library('upload', $config);
					
					if ( ! $this->upload->do_upload('imageupload'.$k))
					{
						$error = $this->upload->display_errors();
						$data['error'] = '<p>'.$error.'</p>';
						$data['js'] .= ' toastr.error("Error when uploading file: $error");';
					} else
					{
						$dataadd = array('upload_data' => $this->upload->data());
						//print_r($dataadd);
						$config = array();
						$config['image_library'] = 'gd2';
						$config['source_image']	= './images/articles/'.$dataadd['upload_data']['file_name'];
						$config['new_image'] = './images/articles/'.$dataadd['upload_data']['raw_name'].'.jpg';
						$config['create_thumb'] = FALSE;
						$config['maintain_ratio'] = TRUE;
						$config['width']	= 770;
						$config['height']	= 400;

						$this->load->library('image_lib', $config); 

						if ( ! $this->image_lib->resize())	{
						    $data['error'] = '<p>'.$this->image_lib->display_errors().'</p>';
						    $data['js'] .= ' toastr.error("Error when uploading file: '.$data['error'].'");';
						} else {
							// create thumbnail
							$config = array();
							$config['image_library'] = 'gd2';						
							$config['source_image'] = './images/articles/'.$dataadd['upload_data']['raw_name'].'.jpg';
							$config['new_image'] = './images/articles/'.$dataadd['upload_data']['raw_name'].'_thumb.jpg';
							$config['create_thumb'] = TRUE;
							$config['maintain_ratio'] = TRUE;
							$config['width']	= 100;
							$config['height']	= 75;
							$this->image_lib->initialize($config); 

							if ( ! $this->image_lib->resize())	{
								$data['error'] = '<p>'.$this->image_lib->display_errors().'</p>';
								$data['js'] .= ' toastr.error("Error when uploading file: '.$data['error'].'");';
							} else {
								///READY TO UPDATE
								$img = $dataadd['upload_data']['raw_name'].'.jpg';
								$thumbimg= $dataadd['upload_data']['raw_name'].'_thumb.jpg';


								$this->articlemodel->updateArticleImages($newid, $img, $thumbimg);

							}
						}
					}
				}
			}

			$this->session->set_flashdata('notif', $data['js']);
			if($this->input->get('cat')) {
				redirect('backend/article?cat='.$this->input->get('cat'));
			} else {
				redirect('backend/article');
			}		
		}

		// EDIT SLIDESHOW
		if($this->input->post('btnedit')) {
			$this->articlemodel->editArticle($this->input->post('hiddenid'),  $this->input->post('title'), $this->input->post('short_desc'), $this->input->post('content'), $this->input->post('meta'), $this->input->post('visibility'));
			
			$adaerror = false;

			$tot  =$this->input->post('totalimgedit');
			for($k=1; $k<= $tot; $k++ ) {
				
				if (!empty($_FILES['imageupload'.$k]['name'])) {
					$config['upload_path'] = './images/articles/';
					$config['allowed_types'] = 'gif|jpg|png';
					$config['max_size']	= '10000';
					$config['max_width']  = '3000';
					$config['max_height']  = '3000';
					$config['remove_spaces'] = TRUE;

					$this->load->library('upload', $config);
					
					if ( ! $this->upload->do_upload('imageupload'.$k))
					{
						$error = $this->upload->display_errors();
						$data['error'] = '<p>'.$error.'</p>';
						$data['js'] .= ' toastr.error("Error when uploading file: $error");';
					} else
					{
						$dataadd = array('upload_data' => $this->upload->data());
						//print_r($dataadd);
						$config = array();
						$config['image_library'] = 'gd2';
						$config['source_image']	= './images/articles/'.$dataadd['upload_data']['file_name'];
						$config['new_image'] = './images/articles/'.$dataadd['upload_data']['raw_name'].'.jpg';
						$config['create_thumb'] = FALSE;
						$config['maintain_ratio'] = TRUE;
						$config['width']	= 770;
						$config['height']	= 400;

						$this->load->library('image_lib', $config); 

						if ( ! $this->image_lib->resize())	{
						    $data['error'] = '<p>'.$this->image_lib->display_errors().'</p>';
						    $data['js'] .= ' toastr.error("Error when uploading file: '.$data['error'].'");';
						} else {
							// create thumbnail
							$config = array();
							$config['image_library'] = 'gd2';						
							$config['source_image'] = './images/articles/'.$dataadd['upload_data']['raw_name'].'.jpg';
							$config['new_image'] = './images/articles/'.$dataadd['upload_data']['raw_name'].'_thumb.jpg';
							$config['create_thumb'] = TRUE;
							$config['maintain_ratio'] = TRUE;
							$config['width']	= 100;
							$config['height']	= 75;
							$this->image_lib->initialize($config); 

							if ( ! $this->image_lib->resize())	{
								$data['error'] = '<p>'.$this->image_lib->display_errors().'</p>';
								$data['js'] .= ' toastr.error("Error when uploading file: '.$data['error'].'");';
							} else {
								///READY TO UPDATE
								$img = $dataadd['upload_data']['raw_name'].'.jpg';
								$thumbimg= $dataadd['upload_data']['raw_name'].'_thumb.jpg';


								$this->articlemodel->updateArticleImages($this->input->post('hiddenid'), $img, $thumbimg);

							}
						}
					}
				}
			}

			

			if($adaerror == true) {
				$data['js'] .= '$("#editarticle").modal("show"); 
							var id = "'.$this->input->post('hiddenid').'";
			        		$.post( "'.base_url('backend/article/getarticle').'", { idslide: id, errorlabel: "'.$data['error'].'", errorvalidation: "'.validation_errors().'" })
							  .done(function( data ) {
							    $("#containeredit").html( data );
							  });
							';				
			} else {
				$data['js'] .= ' toastr.success("Data updated successfully!");';
				$this->session->set_flashdata('notif', $data['js']);
				if($this->input->get('cat')) {
					redirect('backend/article?cat='.$this->input->get('cat'));
				} else {
					redirect('backend/article');
				}
			}
						
		}
		
		// GET ARTICLE DATA
		if($this->input->get('cat')) {
			$data['article'] = $this->articlemodel->getArticle($this->input->get('cat'));
		
		} else {
			$data['article'] = $this->articlemodel->getArticle(1);
		}

		$data['category'] =  $this->articlemodel->getCategory();

		$data['footerjs'] = '<!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="../assets/global/scripts/datatable.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
        <script src="../assets/pages/scripts/table-datatables-managed.min.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/bootstrap-toastr/toastr.min.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js" type="text/javascript"></script>
        <script src="../assets/pages/scripts/components-bootstrap-select.min.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js" type="text/javascript"></script>';

        $data['js'] .= '

        $("#selfilter").on("change", function() {
        	window.location = "'.base_url('backend/article?cat=').'" + $(this).val();
        });

        $("#btnaddmore").on("click", function() {
        	var tot = $("#totalimg").val();
        	tot++;
        	var str = "<input type=\"file\" id=\"imageupload" + tot + "\" name=\"imageupload" + tot + "\">"+
                      "<p class=\"help-block\">(Required. Recommendation size: 1920 x 800 pixels.)</p>";
            $("#container").append(str);
            $("#totalimg").val(tot);
        });

        $("body").on("click", ".delimg", function() {
        	var id = $(this).attr("dataid");
        	
        	$.post( "'.base_url('backend/article/deleteimage').'", { idslide: id })
			  .done(function( data ) {
			  	$("#imgcontainer" + id).remove();			  				    
			  });
        });

        $("body").on("click", "#btnaddmoreedit", function() {
        	
        	var tot = $("body #totalimgedit").val();
        	tot++;
        	var str = "<input type=\"file\" id=\"imageupload" + tot + "\" name=\"imageupload" + tot + "\">"+
                      "<p class=\"help-block\">(Required. Recommendation size: 1920 x 800 pixels.)</p>";
            $("body #containeredit").append(str);
            $("body #totalimgedit").val(tot);
        });

		if ($(".wysihtml5").size() > 0) {
            $(".wysihtml5").wysihtml5({"color": true,
                "stylesheets": ["../assets/global/plugins/bootstrap-wysihtml5/wysiwyg-color.css"]
            });
        }

        $("#url").on("keyup", function() {
        	if($(this).val() != "") {
        		$("#cta").removeAttr("disabled");
        	} else {
        		$("#cta").attr("disabled", "disabled");
        	}
        });

        $(".delbtn").on("click", function() {
        	var dataid = $(this).attr("dataid");
        	swal({
			  title: "Are you sure?",
			  text: "You will not be able to recover this data!",
			  type: "warning",
			  showCancelButton: true,
			  confirmButtonClass: "btn-danger",
			  confirmButtonText: "Yes, delete it!",
			  cancelButtonText: "No, cancel please!",
			  showLoaderOnConfirm: true,
			  closeOnConfirm: false,
			  closeOnCancel: false
			},
			function(isConfirm) {
			  if (isConfirm) {
			    $.post( "'.base_url('backend/article/deletearticle').'", { idslide: dataid })
				  .done(function( data ) {
				  	swal({
			            title: "Deleted!", 
			            text: "Your data has been deleted.", 
			            type: "success"
			        },function() {
		        	 	setTimeout(function () {
							location.reload();
					  	}, 100);
			        });
				  				    
				  });
			  } else {
			    swal("Cancelled", "Your data is safe :)", "error");
			  }
			});
        });
        


        	$(".editdata").on("click", function() {
        		$("#editarticle").modal("show");
        		var id = $(this).attr("dataid");
        		$.post( "'.base_url('backend/article/getarticle').'", { idslide: id })
				  .done(function( data ) {
				    $("#containerarticle").html( data );
				    if ($(".wysihtml5").size() > 0) {
				            $(".wysihtml5").wysihtml5({"color": true,
				                "stylesheets": ["../assets/global/plugins/bootstrap-wysihtml5/wysiwyg-color.css"]
				            });
				        }
				  });
        	});
        ';

		$this->load->view('backend/v_header', $data);
		$this->load->view('backend/article/v_article', $data);
		$this->load->view('backend/v_footer', $data);
	}

	public function deletearticle() {
		if($this->input->post('idslide')) {
			$id = $this->input->post('idslide');
			$this->articlemodel->delArticle($id);
		}
	}

	public function deleteimage() {
		if($this->input->post('idslide')) {
			$id = $this->input->post('idslide');
			$this->articlemodel->delImage($id);
		}
	}

	public function getarticle(){
		if($this->input->post('idslide')) {
			$data = array();
			if($this->input->post('errorlabel')) {
				$data['errorlabel'] = $this->input->post('errorlabel');
			}


			if($this->input->post('errorvalidation')) {
				$data['errorvalidation'] = $this->input->post('errorvalidation');
			}

			$data['article'] = $this->articlemodel->getArticleId($this->input->post('idslide'));
			if($data['article']) {

				$data['category'] =  $this->articlemodel->getCategory();
				$data['images'] = $this->articlemodel->getArticleImages($this->input->post('idslide'));	
				echo $this->load->view('backend/article/v_article_edit', $data, TRUE);
			} else {
				redirect('notfound');
			}
		} else {
			redirect('notfound');
		}
	}
}
