<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Setting extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('user')) 
		{	redirect('backend/login'); }
	}

	public function index() // slideshow
	{
		$data = array();
		$data['setting'] = $this->backendmodel->getSettings();
		$data['title'] = $data['setting']->judul.' - Web Setting';
		$data['pagetitle'] = 'Web Setting';
		$data['pagedesc'] = 'manage settings and company information';
		$data['js'] = '';

		if($this->session->flashdata('notif')) {
			$data['js'] .= $this->session->flashdata('notif');
		}

		if($this->session->flashdata('activetab')) {
			$data['js'] .= "$('.nav-tabs a[href=\"".$this->session->flashdata('activetab')."\"]').tab('show');";
		}

		/// SAVE COMPANY DATA
		if($this->input->post('submitinfo')) {
			$this->load->library('form_validation');
			$this->form_validation->set_rules('title', 'Company Title', 'trim|required');
			$this->form_validation->set_rules('address', 'Address', 'trim|required');
			$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
			$this->form_validation->set_rules('contact', 'Phone', 'trim|required');
			$this->form_validation->set_rules('long', 'Longitude', 'trim|required');
			$this->form_validation->set_rules('lat', 'Latitude', 'trim|required');
			$this->form_validation->set_rules('desc', 'Web Description', 'trim|required');

			if ($this->form_validation->run() == TRUE)
			{
				$this->settingmodel->updateSettingData($this->input->post('title'), $this->input->post('address'), $this->input->post('desc'), $this->input->post('contact'), $this->input->post('fax'), $this->input->post('email'), $this->input->post('long'), $this->input->post('lat'));
				$data['js'] .= ' toastr.success("Data updated successfully!");';
				$this->session->set_flashdata('notif', $data['js']);
				$this->session->set_flashdata('activetab', '#tab_0');
				redirect('backend/setting');
			}
		}

		/// SAVE SOCIAL MEDIA
		if($this->input->post('submitsocial')) {
			
				$this->settingmodel->updateSettingSocial($this->input->post('facebook'), $this->input->post('twitter'), $this->input->post('googleplus'), $this->input->post('instagram'), $this->input->post('line'), $this->input->post('youtube'));
				$data['js'] .= ' toastr.success("Data updated successfully!");';
				$this->session->set_flashdata('notif', $data['js']);
				$this->session->set_flashdata('activetab', '#tab_1');
				redirect('backend/setting');
		}

		/// SAVE META
		if($this->input->post('submitmeta')) {
			$this->load->library('form_validation');
			$this->form_validation->set_rules('default_meta', 'Home Meta', 'trim|required');
			$this->form_validation->set_rules('products_meta', 'Product Meta', 'trim|required');
			$this->form_validation->set_rules('news_meta', 'News Meta', 'trim|required');
			$this->form_validation->set_rules('contact_meta', 'Contact Meta', 'trim|required');

			if ($this->form_validation->run() == TRUE)
			{
				$this->settingmodel->updateSettingMeta($this->input->post('default_meta'), $this->input->post('products_meta'),$this->input->post('news_meta'), $this->input->post('contact_meta'));

				
				$data['js'] .= ' toastr.success("Data updated successfully!");';
				$this->session->set_flashdata('notif', $data['js']);
				$this->session->set_flashdata('activetab', '#tab_2');
				redirect('backend/setting');
			}
		}

		
		$data['setting'] = $this->settingmodel->getSetting();
		

		$data['footerjs'] = '<!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="../assets/global/scripts/datatable.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
        <script src="../assets/pages/scripts/table-datatables-managed.min.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/bootstrap-toastr/toastr.min.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js" type="text/javascript"></script>
        <script src="../assets/pages/scripts/components-bootstrap-select.min.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js" type="text/javascript"></script>';

        $data['js'] .= '

       

        $("#btnaddmore").on("click", function() {
        	var tot = $("#totalimg").val();
        	tot++;
        	var str = "<input type=\"file\" id=\"imageupload" + tot + "\" name=\"imageupload" + tot + "\">"+
                      "<p class=\"help-block\">(Required. Recommendation size: 1920 x 800 pixels.)</p>";
            $("#container").append(str);
            $("#totalimg").val(tot);
        });

        $("body").on("click", ".delimg", function() {
        	var id = $(this).attr("dataid");
        	
        	$.post( "'.base_url('backend/product/deleteimage').'", { idslide: id })
			  .done(function( data ) {
			  	$("#imgcontainer" + id).remove();			  				    
			  });
        });

        $("body").on("click", "#btnaddmoreedit", function() {
        	
        	var tot = $("body #totalimgedit").val();
        	tot++;
        	var str = "<input type=\"file\" id=\"imageupload" + tot + "\" name=\"imageupload" + tot + "\">"+
                      "<p class=\"help-block\">(Required. Recommendation size: 500 x 550 pixels.)</p>";
            $("body #containeredit").append(str);
            $("body #totalimgedit").val(tot);
        });

		if ($(".wysihtml5").size() > 0) {
            $(".wysihtml5").wysihtml5({"color": true,
                "stylesheets": ["../assets/global/plugins/bootstrap-wysihtml5/wysiwyg-color.css"]
            });
        }

        $("#url").on("keyup", function() {
        	if($(this).val() != "") {
        		$("#cta").removeAttr("disabled");
        	} else {
        		$("#cta").attr("disabled", "disabled");
        	}
        });

        $(".delbtn").on("click", function() {
        	var dataid = $(this).attr("dataid");
        	swal({
			  title: "Are you sure?",
			  text: "You will not be able to recover this data!",
			  type: "warning",
			  showCancelButton: true,
			  confirmButtonClass: "btn-danger",
			  confirmButtonText: "Yes, delete it!",
			  cancelButtonText: "No, cancel please!",
			  showLoaderOnConfirm: true,
			  closeOnConfirm: false,
			  closeOnCancel: false
			},
			function(isConfirm) {
			  if (isConfirm) {
			    $.post( "'.base_url('backend/product/deleteproduct').'", { idslide: dataid })
				  .done(function( data ) {
				  	swal({
			            title: "Deleted!", 
			            text: "Your data has been deleted.", 
			            type: "success"
			        },function() {
		        	 	setTimeout(function () {
							location.reload();
					  	}, 100);
			        });
				  				    
				  });
			  } else {
			    swal("Cancelled", "Your data is safe :)", "error");
			  }
			});
        });
        


        	$(".editdata").on("click", function() {
        		$("#editproduct").modal("show");
        		var id = $(this).attr("dataid");
        		$.post( "'.base_url('backend/product/getproduct').'", { idslide: id })
				  .done(function( data ) {
				    $("#containerarticle").html( data );
				    if ($(".wysihtml5").size() > 0) {
				            $(".wysihtml5").wysihtml5({"color": true,
				                "stylesheets": ["../assets/global/plugins/bootstrap-wysihtml5/wysiwyg-color.css"]
				            });
				        }
				  });
        	});
        ';

		$this->load->view('backend/v_header', $data);
		$this->load->view('backend/v_setting', $data);
		$this->load->view('backend/v_footer', $data);
	}

	public function deleteproduct() {
		if($this->input->post('idslide')) {
			$id = $this->input->post('idslide');
			$this->productmodel->delProduct($id);
		}
	}

	public function deleteimage() {
		if($this->input->post('idslide')) {
			$id = $this->input->post('idslide');
			$this->productmodel->delImage($id);
		}
	}

	public function getproduct(){
		if($this->input->post('idslide')) {
			$data = array();
			if($this->input->post('errorlabel')) {
				$data['errorlabel'] = $this->input->post('errorlabel');
			}


			if($this->input->post('errorvalidation')) {
				$data['errorvalidation'] = $this->input->post('errorvalidation');
			}

			$data['product'] = $this->productmodel->getProductId($this->input->post('idslide'));
			if($data['product']) {

				$data['category'] =  $this->productmodel->getCategory();
				$data['unit'] =  $this->productmodel->getUnit();
				$data['images'] = $this->productmodel->getProductImages($this->input->post('idslide'));	
				echo $this->load->view('backend/product/v_product_edit', $data, TRUE);
			} else {
				redirect('notfound');
			}
		} else {
			redirect('notfound');
		}
	}
}
