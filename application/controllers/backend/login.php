<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
	
	public function index() // dashboard
	{
		$data = array();
		$data['setting'] = $this->backendmodel->getSettings();

		$this->form_validation->set_rules('username', 'Username', 'trim|required|min_length[5]|max_length[12]');
$this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[5]');

		if ($this->form_validation->run() == FALSE)
		{
			$data['error'] = validation_errors();
		} else {
			// check submit
			if($this->input->post('username') && $this->input->post('password')) {
				if($this->backendmodel->cekLogin($this->input->post('username'), $this->input->post('password'))) {
					// create session
					$this->session->set_userdata('user', $this->input->post('username'));
					redirect('backend/home');
				} else {
					$data['error'] = "Username or password is incorrect";
				}
			}
		}

		$this->load->view('backend/v_login', $data);
	}

}
