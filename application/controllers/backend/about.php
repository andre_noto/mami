<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class About extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('user')) 
		{	redirect('backend/login'); }
	}

	public function index() // about
	{
		$data = array();
		$data['setting'] = $this->backendmodel->getSettings();
		$data['title'] = $data['setting']->judul.' - About';
		$data['pagetitle'] = 'About Content';
		$data['pagedesc'] = 'change vision, mission, and values content';
		$data['js'] = '';

		if(!$this->input->get('cat')) {				
			redirect('backend/home');					
		} else {
			$data['company'] = $this->companymodel->getCompanyId($this->input->get('cat'));	
			$data['about'] = $this->aboutmodel->getAbout($this->input->get('cat'));		
		}

		if($this->session->flashdata('notif')) {
			$data['js'] .= $this->session->flashdata('notif');
		}
		
		// EDIT ABOUT
		if($this->input->post('submitinfo')) {
		$this->aboutmodel->editContent($this->input->get('cat'),  $this->input->post('vision'), $this->input->post('vision_en'), $this->input->post('mission'), $this->input->post('mission_en'), $this->input->post('value'), $this->input->post('value_en'));
		
			$data['js'] .= ' toastr.success("Data updated successfully!");';
			$this->session->set_flashdata('notif', $data['js']);
			redirect('backend/about?cat='.$this->input->get('cat'));		
		}
		
		
		$data['footerjs'] = '<!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="../assets/global/scripts/datatable.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
       
      <script src="../assets/global/plugins/bootstrap-toastr/toastr.min.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js" type="text/javascript"></script>
        <script src="../assets/pages/scripts/components-bootstrap-select.min.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js" type="text/javascript"></script>';

        $data['js'] .= '
		if ($(".wysihtml5").size() > 0) {
            $(".wysihtml5").wysihtml5({"color": true,
                "stylesheets": ["../assets/global/plugins/bootstrap-wysihtml5/wysiwyg-color.css"]
            });
        }';

		$this->load->view('backend/v_header', $data);
		$this->load->view('backend/about/v_about', $data);
		$this->load->view('backend/v_footer', $data);
	}
}
