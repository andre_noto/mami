<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class News extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('user')) 
		{	redirect('backend/login'); }
	}

	public function index() // slideshow
	{
		$data = array();
		$data['setting'] = $this->backendmodel->getSettings();
		$data['title'] = $data['setting']->judul.' - News';
		$data['pagetitle'] = 'News';
		$data['pagedesc'] = 'manage news displayed on website';
		$data['js'] = '';

		if(!$this->input->get('cat')) {				
			redirect('backend/home');					
		} else {
			$data['company'] = $this->companymodel->getCompanyId($this->input->get('cat'));					
		}

		if($this->session->flashdata('notif')) {
			$data['js'] .= $this->session->flashdata('notif');
		}

		// ADD NEW PRODUCT
		if($this->input->post('btnadd')) {
			$y = substr($this->input->post('published'),-4);
			$m = substr($this->input->post('published'),0,2);
			$d = substr($this->input->post('published'),3,2);
			$published = date("Y-m-d", mktime(0, 0, 0, $m, $d, $y));
		//	print_r($_POST);
			$newid = $this->newsmodel->addNews($this->input->get('cat'), $this->input->post('title'), $this->input->post('title_en'), $this->input->post('short_desc'), $this->input->post('short_desc_en'), $this->input->post('content'), $this->input->post('content_en'), $published, $this->input->post('meta'), $this->input->post('visibility'));


			$data['js'] .= ' toastr.success("Data saved successfully!");';

			$tot  =$this->input->post('totalimg');
			for($k=1; $k<= $tot; $k++ ) {
				
				if (!empty($_FILES['imageupload'.$k]['name'])) {
					$config['upload_path'] = './images/news/';
					$config['allowed_types'] = 'gif|jpg|png';
					$config['max_size']	= '10000';
					$config['max_width']  = '3000';
					$config['max_height']  = '3000';
					$config['remove_spaces'] = TRUE;

					$this->load->library('upload', $config);
					
					if ( ! $this->upload->do_upload('imageupload'.$k))
					{
						$error = $this->upload->display_errors();
						$data['error'] = '<p>'.$error.'</p>';
						$data['js'] .= ' toastr.error("Error when uploading file: $error");';
					} else
					{
						$dataadd = array('upload_data' => $this->upload->data());
						//print_r($dataadd);
						$config = array();
						$config['image_library'] = 'gd2';
						$config['source_image']	= './images/news/'.$dataadd['upload_data']['file_name'];
						$config['new_image'] = './images/news/'.$dataadd['upload_data']['raw_name'].'.jpg';
						$config['create_thumb'] = FALSE;
						$config['maintain_ratio'] = TRUE;
						$config['width']	= 1140;
						$config['height']	= 600;

						$this->load->library('image_lib', $config); 

						if ( ! $this->image_lib->resize())	{
						    $data['error'] = '<p>'.$this->image_lib->display_errors().'</p>';
						    $data['js'] .= ' toastr.error("Error when uploading file: '.$data['error'].'");';
						} else {
							// create thumbnail
							$config = array();
							$config['image_library'] = 'gd2';						
							$config['source_image'] = './images/news/'.$dataadd['upload_data']['raw_name'].'.jpg';
							$config['new_image'] = './images/news/'.$dataadd['upload_data']['raw_name'].'_thumb.jpg';
							$config['create_thumb'] = TRUE;
							$config['maintain_ratio'] = TRUE;
							$config['width']	= 100;
							$config['height']	= 75;
							$this->image_lib->initialize($config); 

							if ( ! $this->image_lib->resize())	{
								$data['error'] = '<p>'.$this->image_lib->display_errors().'</p>';
								$data['js'] .= ' toastr.error("Error when uploading file: '.$data['error'].'");';
							} else {
								///READY TO UPDATE
								$img = $dataadd['upload_data']['raw_name'].'.jpg';
								$thumbimg= $dataadd['upload_data']['raw_name'].'_thumb.jpg';


								$this->newsmodel->updateNewsImages($newid, $img, $thumbimg);

							}
						}
					}
				}
			}

			$this->session->set_flashdata('notif', $data['js']);
			redirect('backend/news?cat='.$this->input->get('cat'));
				
		}

		// EDIT PRODUCT
		if($this->input->post('btnedit')) {
			$y = substr($this->input->post('published'),-4);
			$m = substr($this->input->post('published'),0,2);
			$d = substr($this->input->post('published'),3,2);
			$published = date("Y-m-d", mktime(0, 0, 0, $m, $d, $y));

			$this->newsmodel->editNews($this->input->post('hiddenid'),  $this->input->post('title'), $this->input->post('title_en'), $this->input->post('short_desc'), $this->input->post('short_desc_en'), $this->input->post('content'), $this->input->post('content_en'), $published, $this->input->post('meta'), $this->input->post('visibility'));
			
			$adaerror = false;

			$tot  =$this->input->post('totalimgedit');
			for($k=1; $k<= $tot; $k++ ) {
				
				if (!empty($_FILES['imageupload'.$k]['name'])) {
					$config['upload_path'] = './images/news/';
					$config['allowed_types'] = 'gif|jpg|png';
					$config['max_size']	= '10000';
					$config['max_width']  = '3000';
					$config['max_height']  = '3000';
					$config['remove_spaces'] = TRUE;

					$this->load->library('upload', $config);
					
					if ( ! $this->upload->do_upload('imageupload'.$k))
					{
						$error = $this->upload->display_errors();
						$data['error'] = '<p>'.$error.'</p>';
						$data['js'] .= ' toastr.error("Error when uploading file: $error");';
					} else
					{
						$dataadd = array('upload_data' => $this->upload->data());
						//print_r($dataadd);
						$config = array();
						$config['image_library'] = 'gd2';
						$config['source_image']	= './images/news/'.$dataadd['upload_data']['file_name'];
						$config['new_image'] = './images/news/'.$dataadd['upload_data']['raw_name'].'.jpg';
						$config['create_thumb'] = FALSE;
						$config['maintain_ratio'] = TRUE;
						$config['width']	= 1140;
						$config['height']	= 600;

						$this->load->library('image_lib', $config); 

						if ( ! $this->image_lib->resize())	{
						    $data['error'] = '<p>'.$this->image_lib->display_errors().'</p>';
						    $data['js'] .= ' toastr.error("Error when uploading file: '.$data['error'].'");';
						} else {
							// create thumbnail
							$config = array();
							$config['image_library'] = 'gd2';						
							$config['source_image'] = './images/news/'.$dataadd['upload_data']['raw_name'].'.jpg';
							$config['new_image'] = './images/news/'.$dataadd['upload_data']['raw_name'].'_thumb.jpg';
							$config['create_thumb'] = TRUE;
							$config['maintain_ratio'] = TRUE;
							$config['width']	= 100;
							$config['height']	= 75;
							$this->image_lib->initialize($config); 

							if ( ! $this->image_lib->resize())	{
								$data['error'] = '<p>'.$this->image_lib->display_errors().'</p>';
								$data['js'] .= ' toastr.error("Error when uploading file: '.$data['error'].'");';
							} else {
								///READY TO UPDATE
								$img = $dataadd['upload_data']['raw_name'].'.jpg';
								$thumbimg= $dataadd['upload_data']['raw_name'].'_thumb.jpg';


								$this->newsmodel->updateNewsImages($this->input->post('hiddenid'), $img, $thumbimg);

							}
						}
					}
				}
			}

			

			if($adaerror == true) {
				$data['js'] .= '$("#editnews").modal("show"); 
							var id = "'.$this->input->post('hiddenid').'";
			        		$.post( "'.base_url('backend/news/getnews').'", { idslide: id, errorlabel: "'.$data['error'].'", errorvalidation: "'.validation_errors().'" })
							  .done(function( data ) {
							    $("#containeredit").html( data );
							  });
							';				
			} else {
				$data['js'] .= ' toastr.success("Data updated successfully!");';
				$this->session->set_flashdata('notif', $data['js']);
				redirect('backend/news?cat='.$this->input->get('cat'));
			}
						
		}
		
		$data['news'] = $this->newsmodel->getNews($this->input->get('cat'));
		$data['images'] = $this->newsmodel->getFirstImages($data['news']);
		//$data['category'] =  $this->productmodel->getCategory();
		//$data['unit'] =  $this->productmodel->getUnit();

		
        $data['footerjs'] = '
        <script src="../assets/global/plugins/bootstrap-toastr/toastr.min.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js" type="text/javascript"></script>
        <script src="../assets/pages/scripts/components-bootstrap-select.min.js" type="text/javascript"></script>
        <script src="../assets/pages/scripts/components-date-time-pickers.min.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js" type="text/javascript"></script>
        <script src="../assets/pages/scripts/components-date-time-pickers.min.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
       ';

        if(count($data['news']) >0) {
			$data['footerjs'] .= '<!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="../assets/global/scripts/datatable.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
        <script src="../assets/pages/scripts/table-datatables-managed.min.js" type="text/javascript"></script>';
		}

        $data['js'] .= '

       

        $("#btnaddmore").on("click", function() {
        	var tot = $("#totalimg").val();
        	tot++;
        	var str = "<input type=\"file\" id=\"imageupload" + tot + "\" name=\"imageupload" + tot + "\">"+
                      "<p class=\"help-block\">(Required. Recommendation size: 1140 x 600 pixels.)</p>";
            $("#container").append(str);
            $("#totalimg").val(tot);
        });

        $("body").on("click", ".delimg", function() {
        	var id = $(this).attr("dataid");
        	
        	$.post( "'.base_url('backend/news/deleteimage').'", { idslide: id })
			  .done(function( data ) {
			  	$("#imgcontainer" + id).remove();			  				    
			  });
        });

        $("body").on("click", "#btnaddmoreedit", function() {
        	
        	var tot = $("body #totalimgedit").val();
        	tot++;
        	var str = "<input type=\"file\" id=\"imageupload" + tot + "\" name=\"imageupload" + tot + "\">"+
                      "<p class=\"help-block\">(Required. Recommendation size: 500 x 550 pixels.)</p>";
            $("body #containeredit").append(str);
            $("body #totalimgedit").val(tot);
        });

		if ($(".wysihtml5").size() > 0) {
            $(".wysihtml5").wysihtml5({"color": true,
                "stylesheets": ["../assets/global/plugins/bootstrap-wysihtml5/wysiwyg-color.css"]
            });
        }

        $(".delbtn").on("click", function() {
        	var dataid = $(this).attr("dataid");
        	swal({
			  title: "Are you sure?",
			  text: "You will not be able to recover this data!",
			  type: "warning",
			  showCancelButton: true,
			  confirmButtonClass: "btn-danger",
			  confirmButtonText: "Yes, delete it!",
			  cancelButtonText: "No, cancel please!",
			  showLoaderOnConfirm: true,
			  closeOnConfirm: false,
			  closeOnCancel: false
			},
			function(isConfirm) {
			  if (isConfirm) {
			    $.post( "'.base_url('backend/news/deletenews').'", { idslide: dataid })
				  .done(function( data ) {	
				  		  	
				  	swal({
			            title: "Deleted!", 
			            text: "Your data has been deleted.", 
			            type: "success"
			        },function() {
		        	 	setTimeout(function () {
							location.reload();
					  	}, 100);
			        });
				  				    
				  });
			  } else {
			    swal("Cancelled", "Your data is safe :)", "error");
			  }
			});
        });
        


        	$(".editdata").on("click", function() {
        		$("#editnews").modal("show");
        		var id = $(this).attr("dataid");
        		$.post( "'.base_url('backend/news/getnews').'", { idslide: id })
				  .done(function( data ) {
				    $("#containerarticle").html( data );

        			$(".date-picker").datepicker();
				    if ($(".wysihtml5").size() > 0) {
				            $(".wysihtml5").wysihtml5({"color": true,
				                "stylesheets": ["../assets/global/plugins/bootstrap-wysihtml5/wysiwyg-color.css"]
				            });
				        }
				  });
        	});
        ';

		$this->load->view('backend/v_header', $data);
		$this->load->view('backend/news/v_news', $data);
		$this->load->view('backend/v_footer', $data);
	}

	public function deletenews() {
		if($this->input->post('idslide')) {
			$id = $this->input->post('idslide');
			$this->newsmodel->delNews($id);
		}
	}

	public function deleteimage() {
		if($this->input->post('idslide')) {
			$id = $this->input->post('idslide');
			$this->newsmodel->delImage($id);
		}
	}

	public function getnews(){
		if($this->input->post('idslide')) {
			$data = array();
			if($this->input->post('errorlabel')) {
				$data['errorlabel'] = $this->input->post('errorlabel');
			}


			if($this->input->post('errorvalidation')) {
				$data['errorvalidation'] = $this->input->post('errorvalidation');
			}

			$data['news'] = $this->newsmodel->getNewsId($this->input->post('idslide'));
			if($data['news']) {

				$data['images'] = $this->newsmodel->getNewsImages($this->input->post('idslide'));	
				echo $this->load->view('backend/news/v_news_edit', $data, TRUE);
			} else {
				redirect('notfound');
			}
		} else {
			redirect('notfound');
		}
	}
}
