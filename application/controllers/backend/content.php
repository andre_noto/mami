<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Content extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('user')) 
		{	redirect('backend/login'); }
	}

	public function index() // slideshow
	{
		$data = array();
		$data['setting'] = $this->backendmodel->getSettings();
		$data['title'] = $data['setting']->judul.' - Company';
		$data['pagetitle'] = 'Menu Management';
		$data['pagedesc'] = 'manage all company menu';
		$data['js'] = '';

		if($this->session->flashdata('notif')) {
			$data['js'] .= $this->session->flashdata('notif');
		}

		//get company
		$data['company'] = $this->companymodel->getCompany();
		$data['content_type'] = $this->contentmodel->getContentType();

		// SORTING 
		if($this->input->post('sortingaction')) {			
			$i = 1;
			foreach ($this->input->post('sortid') as $key ) {
				$this->contentmodel->updateRank($key, $i);
				$i++;
			}

			if($this->input->get('cat')) {				
				redirect('backend/content?cat='.$this->input->get('cat'));				
			} else {
				redirect('backend/content');					
			}		
		}

		// ADD NEW PRODUCT
		if($this->input->post('btnadd')) {

			$newid = $this->contentmodel->addContent($this->input->post('name'), $this->input->post('description'), $this->input->post('description_english'), $this->input->post('alias'), $this->input->post('company_id'), $this->input->post('content_type'));
			
			$data['js'] .= ' toastr.success("Data saved successfully!");';
			$this->session->set_flashdata('notif', $data['js']);
			if($this->input->get('cat')) {
				redirect('backend/content?cat='.$this->input->get('cat'));				
			} else {
				redirect('backend/content');					
			}			
		}

		// EDIT PRODUCT
		if($this->input->post('btnedit')) {
		$this->contentmodel->editContent($this->input->post('hiddenid'),  $this->input->post('name'), $this->input->post('alias'), $this->input->post('description'), $this->input->post('description_en'), $this->input->post('company_id'), $this->input->post('content_type'));
		
			$data['js'] .= ' toastr.success("Data updated successfully!");';
			$this->session->set_flashdata('notif', $data['js']);
			if($this->input->get('cat')) {				
				redirect('backend/content?cat='.$this->input->get('cat'));				
			} else {
				redirect('backend/content');					
			}			
		}
		
		if($this->input->get('cat')){
			$data['content'] = $this->contentmodel->getContent($this->input->get('cat'));
			$data['activecat'] = $this->input->get('cat');
		} else {
			$data['content'] = $this->contentmodel->getContent(1);
			$data['activecat'] = 1;
		}
		
		
		$data['footerjs'] = '<!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="../assets/global/scripts/datatable.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
        <script src="../assets/pages/scripts/table-datatables-managed.min.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/bootstrap-toastr/toastr.min.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js" type="text/javascript"></script>
        <script src="../assets/pages/scripts/components-bootstrap-select.min.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js" type="text/javascript"></script>
         <script
  src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"
  integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU="
  crossorigin="anonymous"></script>';

        $data['js'] .= '
		if ($(".wysihtml5").size() > 0) {
            $(".wysihtml5").wysihtml5({"color": true,
                "stylesheets": ["../assets/global/plugins/bootstrap-wysihtml5/wysiwyg-color.css"]
            });
        }

        $( function() {
		     $( "#sortable" ).sortable({
		        update: function( ) {
		           $("#sortingform").submit();
		        }
		    });
    		$( "#sortable" ).disableSelection();
		  } );

        $("#addslideshow").on("shown.bs.modal", function () {
		    $("#name").focus();
		});

        $(".delbtn").on("click", function() {
        	var dataid = $(this).attr("dataid");
        	swal({
			  title: "Are you sure?",
			  text: "You will not be able to recover this data!",
			  type: "warning",
			  showCancelButton: true,
			  confirmButtonClass: "btn-danger",
			  confirmButtonText: "Yes, delete it!",
			  cancelButtonText: "No, cancel please!",
			  showLoaderOnConfirm: true,
			  closeOnConfirm: false,
			  closeOnCancel: false
			},
			function(isConfirm) {
			  if (isConfirm) {
			    $.post( "'.base_url('backend/content/deletecontent').'", { idslide: dataid })
				  .done(function( data ) {
				  	swal({
			            title: "Deleted!", 
			            text: "Your data has been deleted.", 
			            type: "success"
			        },function() {
		        	 	setTimeout(function () {
							location.reload();
					  	}, 100);
			        });
				  				    
				  });
			  } else {
			    swal("Cancelled", "Your data is safe :)", "error");
			  }
			});
        });
        
        $("#selfilter").on("change", function() {
        	window.location = "'.base_url('backend/content?cat=').'" + $(this).val();
        });


        	$(".editdata").on("click", function() {
        		$("#editproduct").modal("show");
        		var id = $(this).attr("dataid");
        		$.post( "'.base_url('backend/content/getcontent').'", { idslide: id })
				  .done(function( data ) {
				    $("#containerarticle").html( data );
				    if ($(".wysihtml5").size() > 0) {
				            $(".wysihtml5").wysihtml5({"color": true,
				                "stylesheets": ["../assets/global/plugins/bootstrap-wysihtml5/wysiwyg-color.css"]
				            });
				        }
				  });
        	});
        ';

		$this->load->view('backend/v_header', $data);
		$this->load->view('backend/content/v_content', $data);
		$this->load->view('backend/v_footer', $data);
	}

	public function deletecontent() {
		if($this->input->post('idslide')) {
			$id = $this->input->post('idslide');
			$this->contentmodel->delContent($id);
		}
	}

	public function deleteimage() {
		if($this->input->post('idslide')) {
			$id = $this->input->post('idslide');
			$this->productmodel->delImage($id);
		}
	}

	public function getcontent(){
		if($this->input->post('idslide')) {
			$data = array();
			$data['company'] = $this->companymodel->getCompany();
		$data['content_type'] = $this->contentmodel->getContentType();
			if($this->input->post('errorlabel')) {
				$data['errorlabel'] = $this->input->post('errorlabel');
			}


			if($this->input->post('errorvalidation')) {
				$data['errorvalidation'] = $this->input->post('errorvalidation');
			}

			$data['content'] = $this->contentmodel->getContentId($this->input->post('idslide'));
			if($data['content']) {
				echo $this->load->view('backend/content/v_content_edit', $data, TRUE);
			} else {
				redirect('notfound');
			}
		} else {
			redirect('notfound');
		}
	}
}
