<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class News extends CI_Controller {
	public function index()
	{
		$data = array();
		$data['slides'] = $this->slidemodel->getSlides(5);
		$data['about'] = $this->articlemodel->getArticleMenu(1);
		$data['guides'] = $this->articlemodel->getArticleMenu(2);
		$data['beauty'] = $this->articlemodel->getArticleMenu(3);
		$data['setting'] = $this->settingmodel->getSetting();

		$data['news'] = $this->newsmodel->getNews(10);
		$data['images'] = $this->newsmodel->getFirstImages($data['news'], true);


		$data['recent'] = $this->newsmodel->getNews(5, 'published', 'desc');
		$data['images_recent'] = $this->newsmodel->getFirstImages($data['recent'], false);

		$data['meta'] = html_entity_decode($data['setting']->news_meta);		
		$this->load->view('frontend/v_header', $data);
		$this->load->view('frontend/v_news', $data);
		$this->load->view('frontend/v_footer', $data);

	}

	public function view($id = -1, $label='') {
		$data = array();
		$data['slides'] = $this->slidemodel->getSlides(5);
		$data['about'] = $this->articlemodel->getArticleMenu(1);
		$data['guides'] = $this->articlemodel->getArticleMenu(2);
		$data['beauty'] = $this->articlemodel->getArticleMenu(3);
		
		$data['news'] = $this->newsmodel->getNewsId($id, true);		

		if(count($data['news']) ==0) {
			redirect('notfound');
		} else {

			// update view
			if(!$this->session->userdata('view_news_'.$id)) {
				$this->session->set_userdata('view_news_'.$id, 'true');
				$this->newsmodel->updateViewed($id);
			}

			$data['meta'] = html_entity_decode($data['news']->meta);	
			$data['images'] = $this->newsmodel->getNewsImages($id);
			$data['recent'] = $this->newsmodel->getNews(5, 'published', 'desc');	
			$data['images_recent'] = $this->newsmodel->getFirstImages($data['recent'], false);			
		}
		
		$this->load->view('frontend/v_header', $data);
		$this->load->view('frontend/v_news_view', $data);
		$this->load->view('frontend/v_footer', $data);
	}
}
