<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends CI_Controller {
	public function index()
	{
		$data = array();
		$data['slides'] = $this->slidemodel->getSlides(5);
		$data['about'] = $this->articlemodel->getArticleMenu(1);
		$data['guides'] = $this->articlemodel->getArticleMenu(2);
		$data['beauty'] = $this->articlemodel->getArticleMenu(3);
		$data['category'] = $this->productmodel->getCategory();

		$data['setting'] = $this->settingmodel->getSetting(); 
		$data['meta'] = html_entity_decode($data['setting']->products_meta);

		if($this->input->get('catid') != '') {
			
			$data['product'] = $this->productmodel->getProductByCat((int)$this->input->get('catid'));
		} else {
			$data['product'] = $this->productmodel->getProductByCat();	
		}

		$data['images'] = $this->productmodel->getFirstImages($data['product'], true);
		
		$this->load->view('frontend/v_header', $data);
		$this->load->view('frontend/v_product', $data);
		$this->load->view('frontend/v_footer', $data);

	}

	public function view($id = -1, $label='') {
		$data = array();
		$data['slides'] = $this->slidemodel->getSlides(5);
		$data['about'] = $this->articlemodel->getArticleMenu(1);
		$data['guides'] = $this->articlemodel->getArticleMenu(2);
		$data['beauty'] = $this->articlemodel->getArticleMenu(3);
		$data['category'] = $this->productmodel->getCategory();

		$data['product'] = $this->productmodel->getProductId($id, true);	


		if(count($data['product']) ==0) {
			redirect('notfound');
		} else {
			$data['meta'] = html_entity_decode($data['product']->meta);	
			$data['images'] = $this->productmodel->getProductImages($id);
			$data['related'] = $this->productmodel->getProductByCat($data['product']->category_id);	
			$data['images_related'] = $this->productmodel->getFirstImages($data['related'], true);			
		}
		
		$this->load->view('frontend/v_header', $data);
		$this->load->view('frontend/v_product_view', $data);
		$this->load->view('frontend/v_footer', $data);
	}
}
