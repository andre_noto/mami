<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Metal extends CI_Controller {
	public function index()
	{
		$data = array();
		$data['title'] = "PT. MITRA ADIKARYA METAL INDONESIA";
		$this->load->view('frontend/v_header', $data);
		$this->load->view('frontend/v_metal', $data);
		$this->load->view('frontend/v_footer', $data);
	}

	public function loadpage($alias) {
		if($alias == 'about') // semetara
		{
			$data = array();			
			echo $this->load->view('frontend/v_about', $data, TRUE);			
		} else if($alias == 'home') {
			$data = array();
			echo $this->load->view('frontend/v_metal', $data, TRUE);			
		} else if($alias == 'products') {
			$data = array();
			echo $this->load->view('frontend/v_product', $data, TRUE);			
		} else if($alias == 'news') {
			$data = array();
			echo $this->load->view('frontend/v_news', $data, TRUE);			
		} else if($alias == 'contact') {
			$data = array();
			echo $this->load->view('frontend/v_contact', $data, TRUE);			
		}
	}

	public function content($alias) {
		
	}
}
