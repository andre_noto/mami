<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Portal extends CI_Controller {
	public function index()
	{
		//$data = array();
		//$this->load->view('frontend/v_home', $data);		
	}

	public function loadcompany($alias = '') {		
		$data = array();
		$data['title'] = "PT. MITRA ADIKARYA METAL INDONESIA";
		$c = $this->companymodel->getCompanyByAlias($alias);
		$data['alias'] = $alias;

		if(count($c)) {
			$data['menu'] = $this->contentmodel->getContent($c->id);
			
			$this->load->view('frontend/v_header', $data);
			$this->load->view('frontend/v_metal', $data);
			$this->load->view('frontend/v_footer', $data);
		} else {
			redirect('notfound');
		}		
	}

	public function loadcontent() {
		$id = $this->input->post('id');		
		// cek
		$hasil = $this->contentmodel->getContentId($id);
		if($hasil) {
			// cek jenis
			if($hasil->content_type == 2) // news
			{
				$data = array();
				echo $this->load->view('frontend/v_contact', $data, TRUE);			
			}			
		} else {
			redirect('notfound');
		}
	}
}
