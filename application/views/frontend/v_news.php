<h1 class="text-reflect" data-text="News" style="margin-bottom: 25px;">News</h1>

<div class="news">
	<div class="datecontainer">
		<div class="month"><?php echo date('M'); ?></div>
		<div class="date"><?php echo date('d'); ?></div>
	</div>
	<div class="newsitem">
		Three Uber security managers resign after CEO criticizes practices
	</div>
</div>
<div class="news">
	<div class="datecontainer">
		<div class="month"><?php echo date('M'); ?></div>
		<div class="date"><?php echo date('d'); ?></div>
	</div>
	<div class="newsitem">
		General Electric lays off workers at power business in New York
	</div>
</div>
<div class="news">
	<div class="datecontainer">
		<div class="month"><?php echo date('M'); ?></div>
		<div class="date"><?php echo date('d'); ?></div>
	</div>
	<div class="newsitem">
		Wall Street hiking corporate profit forecasts on tax cut expectations
	</div>
</div>
<div class="pagination-container wow zoomIn mar-b-1x" data-wow-duration="0.5s">

	<ul class="pagination" style="padding:0px; margin:0px;">
		<li class="pagination-item--wide first"> <a class="pagination-link--wide first" href="#"><i class="fa fa-chevron-left"></i></a> </li>
		<li class="pagination-item first-number"> <a class="pagination-link" href="#">1</a> </li>
		<li class="pagination-item"> <a class="pagination-link" href="#">2</a> </li>
		<li class="pagination-item is-active"> <a class="pagination-link" href="#">3</a> </li>
		<li class="pagination-item"> <a class="pagination-link" href="#">4</a> </li>
		<li class="pagination-item"> <a class="pagination-link" href="#">5</a> </li>
		<li class="pagination-item--wide last"> <a class="pagination-link--wide last" href="#"><i class="fa fa-chevron-right"></i></a> </li>
	</ul>

</div>
