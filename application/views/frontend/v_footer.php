 
      </div>
    </div>
 <div class="bottombg">      
    </div>

    <div class="ribbon animated fadeInDown">
      <ul>
        <?php foreach ($menu as $key) { ?>
          <li><a href="#<?php echo $key->alias; ?>" class="glow link link--kukuri selected navi" menuid="<?php echo $key->id; ?>" href="#" data-letters="<?php echo $key->name; ?>"><?php echo $key->name; ?></a></li>
        <?php } ?>
        <!-- <li><a href="#" class="glow link link--kukuri selected navi" alias="home" href="#" data-letters="Home">Home</a></li>
        <li><a href="#about" class="glow link link--kukuri navi" alias="about" href="#about" data-letters="About Us">About Us</a></li>
        <li><a href="#products" class="glow link link--kukuri navi" alias="products" href="#" data-letters="Products">Products</a></li>
        <li><a href="#news" class="glow link link--kukuri navi" alias="news" href="#" data-letters="News">News</a></li>
        <li><a href="#contact" class="glow link link--kukuri navi" alias="contact" href="#" data-letters="Contact Us">Contact Us</a></li> --> 
      </ul>
    </div>
    <div class="topnav" id="myTopnav" >
      <a href="#"  alias="home" class="active navi">Home</a>
      <a href="#about" alias="about"  class="navi">About Us</a>
      <a href="#products" alias="products" class="navi">Products</a>
      <a href="#news" alias="news" class="navi">News</a>
      <a href="#contact" alias="contact" class="navi">Contact Us</a>
      <a href="javascript:void(0);" class="icon" onclick="myFunction()">&#9776;</a>
    </div>
  </div>

  <script
  src="https://code.jquery.com/jquery-3.2.1.min.js"
  integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
  crossorigin="anonymous"></script>

  <script type="text/javascript">
    $(document).ready(function() {
      var url      = window.location.href;       
      var hash = url.substring(url.indexOf("#")+1);
      if(hash) {
        alert(hash);
      }

      $(".navi").on('click', function() {
        var menuid =  $(this).attr('menuid');

        $( ".inside" ).fadeOut( "slow", function() {
          $.post( "<?php echo base_url($alias.'/loadcontent'); ?>", { id: menuid })
          .done(function( data ) {            
            $('.inside').html(data);
            $('.inside').fadeIn("slow");
          });
        });
        
        
      });
    });

  </script>

  <script type="text/javascript">
    function myFunction() {
      var x = document.getElementById("myTopnav");
      if (x.className === "topnav") {
          x.className += " responsive";
      } else {
          x.className = "topnav";
      }
  }
  </script>
</body>
</html>