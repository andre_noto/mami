
<!-- SINGLE PRODUCT SECTION -->
    <section class="clearfix singleProduct">
      <div class="container">
        <div class="row">
          <div class="col-sm-6 col-xs-12">
              <div id="myCarousel" class="carousel slide" data-ride="carousel">
                  <!-- Indicators -->
                  <ol class="carousel-indicators">
                    <?php $first = true; $k = 0; foreach ($images as $key) { ?>
                        <li data-target="#myCarousel" data-slide-to="<?php echo $k; $k++; ?>" class="<?php if($first == true) { echo 'active'; $first = false; }  ?>"></li>
                    <?php } ?> 
                  </ol>

                  <!-- Wrapper for slides -->
                  <div class="carousel-inner">
                    <?php $first = true; foreach ($images as $key) { ?>
                      <div class="item <?php if($first == true) { echo 'active'; $first = false; }  ?>">
                        <img src="<?php echo base_url('images/products/'.$key->img); ?>" alt="<?php echo base_url('images/products/'.$key->img); ?>" class="img-responsive">
                      </div>
                    <?php } ?>                 
                  </div>

                  <!-- Left and right controls -->
                  <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left"></span>
                    <span class="sr-only">Previous</span>
                  </a>
                  <a class="right carousel-control" href="#myCarousel" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right"></span>
                    <span class="sr-only">Next</span>
                  </a>
              
            </div>
            
          </div>
          <div class="col-sm-6 col-xs-12">
            <div class="singleProductInfo">
              <h2><?php echo $product->name; ?></h2>
              <h3>IDR <?php echo number_format($product->price); ?> <?php // ini untuk member price <del>$50</del> ?></h3>
              <p><?php echo $product->short_desc; ?></p>
              <div class="finalCart">
                <input type="number" value="1" min="1" max="999">
                <a href="cart.html" class="btn btn-primary"><i class="fa fa-shopping-basket" aria-hidden="true"></i>Add to cart</a>
              </div>
              <ul class="list-inline category">
                <li>Categories:</li>
                <?php foreach ($category as $key) {
                        if($key->id == $product->category_id) { ?>
<li><a href="<?php echo base_url('product?catid='.$key->id.'&cat='.$key->category); ?>"><?php echo $key->category; ?></a></li>
                       <?php }
                } ?>
              </ul>
              <!-- Go to www.addthis.com/dashboard to customize your tools --> <div class="addthis_inline_share_toolbox"></div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-xs-12">
            <div class="tabCommon tabOne singleTab">
              <ul class="nav nav-tabs">
              <li class="active"><a data-toggle="tab" href="#details">Details</a></li>
                
              </ul>

              <div class="tab-content patternbg">
              <div id="details" class="tab-pane fade in active">
                  <h4>Product Description</h4>
                  <p><?php echo $product->desc; ?></p>
                </div>
              </div>
            </div>
          </div>
        </div>
        <?php if(count($related) >0 ) { ?>
        <div class="row">
          <div class="col-xs-12">
            <div class="relatedTitle">
              <h2>Related Products</h2>
            </div>
          </div>
        </div>
        <div class="row">
          <?php $x=0; foreach ($related as $key) { ?>
            <div class="col-md-3 col-sm-6 col-xs-12">
              <a href="<?php echo base_url('product/'.$key->id.'/'.url_title($key->name, '_')); ?>" class="realatedInner">
                <div class="productBox">                  
                  <img src="<?php echo base_url('images/products/'.$images_related[$x]); ?>" alt="<?php echo $key->name; ?>" class="img-responsive">
                </div>
                <div class="productName"><?php echo $key->name; ?></div>
                <div class="productPrice">IDR <?php echo number_format($key->price); ?></div>
              </a>
            </div>
          <?php } ?>
          
        </div>
        <?php } ?>
      </div>
    </section>