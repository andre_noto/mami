
<!-- PAGE TITLE SECTION -->
    <section class="clearfix pageTitleArea" style="background-image: url(<?php echo base_url('images/pageTitle-bg.jpg'); ?>">
        <div class="container">
          <div class="pageTitle">
              <h1><?php echo $news->title; ?></h1>
          </div>
        </div>
    </section>

<!-- BLOG SINGLE SECTION -->
    <section class="container-fluid clearfix blogSingle">
      <div class="container">
        <div class="row">

          <div class="col-sm-4 col-xs-12">
            <div class="blogSidebar">
              <div class="input-group">
                <input type="text" class="form-control" placeholder="Search..." aria-describedby="basic-addon2">
                <a href="#" class="input-group-addon" id="basic-addon2"><i class="fa fa-search"></i></a>
              </div>
              <div class="panel panel-default recentBlogPosts">
                <div class="panel-heading">Recent Posts</div>
                <div class="panel-body">
                  <?php $k=0; foreach ($recent as $key) { ?>
                  <div class="media">
                    <?php if($images_recent[$k]) { ?>
                    <a class="media-left" href="<?php echo base_url('news/'.$key->id.'/'.url_title($key->title, '-')); ?>">
                      <img class="media-object" src="<?php echo base_url('images/news/'.$images_recent[$k]); ?>" alt="<?php echo $key->title; ?>">
                    </a>
                    <?php } ?>
                    <div class="media-body">
                      <h4 class="media-heading"><a href="<?php echo base_url('news/'.$key->id.'/'.url_title($key->title, '-')); ?>"><?php echo $key->title; ?></a></h4>
                      <ul class="list-inline">
                        <li><i class="fa fa-calendar" aria-hidden="true"></i> <?php echo strftime("%d/%m/%Y", strtotime($key->published)); ?></li>
                      </ul>
                    </div>
                  </div>
                    
                  <?php $k++; } ?>
                  
                </div>
              </div>
            </div>
          </div>

          <div class="col-sm-8 col-xs-12">

            <div class="blogPost singlePost">
              <div id="myCarousel" class="carousel slide" data-ride="carousel">
                  <!-- Indicators -->
                  <ol class="carousel-indicators">
                    <?php $first = true; $k = 0; foreach ($images as $key) { ?>
                        <li data-target="#myCarousel" data-slide-to="<?php echo $k; $k++; ?>" class="<?php if($first == true) { echo 'active'; $first = false; }  ?>"></li>
                    <?php } ?> 
                  </ol>

                  <!-- Wrapper for slides -->
                  <div class="carousel-inner">
                    <?php $first = true; foreach ($images as $key) { ?>
                      <div class="item <?php if($first == true) { echo 'active'; $first = false; }  ?>">
                        <img src="<?php echo base_url('images/news/'.$key->img); ?>" alt="<?php echo base_url('images/news/'.$key->img); ?>" class="img-responsive">
                      </div>
                    <?php } ?>                 
                  </div>

                  <!-- Left and right controls -->
                  <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left"></span>
                    <span class="sr-only">Previous</span>
                  </a>
                  <a class="right carousel-control" href="#myCarousel" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right"></span>
                    <span class="sr-only">Next</span>
                  </a>
              
            </div>
              <h2><?php echo $news->title; ?></h2>
              <h3><?php echo $news->short_desc; ?></h3>
              <p><?php echo $news->content; ?></p>

              <ul class="list-inline">
                <li><i class="fa fa-calendar" aria-hidden="true"></i> <?php echo strftime("%d/%m/%Y", strtotime($news->published)); ?></li>
                <li><i class="fa fa-user" aria-hidden="true"></i> <?php echo $news->viewed; ?> viewed</li>
                
              </ul>
            </div>
          </div>

        </div>
      </div>
    </section>