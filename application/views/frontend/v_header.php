<!DOCTYPE html>
<html>
<head>
  <title>Metal</title>
  <meta charset="UTF-8">
  <meta http-equiv="cache-control" content="no-cache" />
<meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="https://fonts.googleapis.com/css?family=Quicksand" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Raleway:900" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Playfair+Display+SC|Spectral+SC" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('css/global.css'); ?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('css/paging.css'); ?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('css/form.css'); ?>">
   <link rel="stylesheet" href="<?php echo base_url('css/animate.css'); ?>">
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" >
<script src='https://www.google.com/recaptcha/api.js'></script>
</head>
<body>
  <div class="container">
    <div class="bar">
      <h1><?php echo $title; ?></h1>
      <span class="lang">
        <a href="#"><img src="<?php echo base_url('images/indoflag.png'); ?>" style="width: 20px;" /></a>
        <a href="#"><img src="<?php echo base_url('images/englishflag.png'); ?>" style="width: 20px;" /></a>
      </span>
    </div>
    <!-- Navbar on small screens -->
        <div class="contentbg">
      <div class="inside">