
<!-- PAGE TITLE SECTION -->
    <section class="clearfix pageTitleArea" style="background-image: url(<?php echo base_url('images/pageTitle-bg.jpg'); ?>">
        <div class="container">
	        <div class="pageTitle">
	            <h1><?php echo $content->title; ?></h1>
	        </div>
        </div>
    </section>


<!-- BLOG SINGLE SECTION -->
    <section class="container-fluid clearfix blogSingle">
      <div class="container">
        <div class="row">

          <div class="col-sm-4 col-xs-12">
            <div class="blogSidebar">              
              <div class="panel panel-default">
                <div class="panel-heading"><?php echo $category_title; ?></div>
                <div class="panel-body">
                  <ul class="list-unstyle categoryList">
                    <?php foreach ($category as $key) { ?>
                     <li><a href="<?php echo base_url("guides/$key->id/".url_title($key->title,"-")); ?>"><?php echo $key->title; ?></a></li>
                  <?php } ?>
                  </ul>
                </div>
              </div>
            </div>
          </div>

          <div class="col-sm-8 col-xs-12">

            <div class="blogPost singlePost">
              <div id="myCarousel" class="carousel slide" data-ride="carousel">
                <!-- Indicators -->
                <ol class="carousel-indicators">
                  <?php $first = true; $k = 0; foreach ($images as $key) { ?>
                      <li data-target="#myCarousel" data-slide-to="<?php echo $k; $k++; ?>" class="<?php if($first == true) { echo 'active'; $first = false; }  ?>"></li>
                  <?php } ?> 
                </ol>

                <!-- Wrapper for slides -->
                <div class="carousel-inner">
                  <?php $first = true; foreach ($images as $key) { ?>
                    <div class="item <?php if($first == true) { echo 'active'; $first = false; }  ?>">
                      <img src="<?php echo base_url('images/articles/'.$key->img); ?>" alt="<?php echo base_url('images/articles/'.$key->img); ?>">
                    </div>
                  <?php } ?>                 
                </div>

                <!-- Left and right controls -->
                <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                  <span class="glyphicon glyphicon-chevron-left"></span>
                  <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#myCarousel" data-slide="next">
                  <span class="glyphicon glyphicon-chevron-right"></span>
                  <span class="sr-only">Next</span>
                </a>
              </div>

              <h2><?php echo $content->title; ?></h2>
              <h3><?php echo $content->short_desc; ?></h3>
              <p><?php echo $content->content; ?></p>

              <ul class="list-inline">
                <li><i class="fa fa-user" aria-hidden="true"></i> viewed <?php echo $content->viewed; ?> times</li>
              </ul>
              
            </div>
            
          </div>

        </div>
      </div>
    </section>