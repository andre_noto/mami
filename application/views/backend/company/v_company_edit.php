<div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            <h4 class="modal-title">Edit Company</h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN SAMPLE FORM PORTLET-->
                    <div class="portlet-body form">
                           <div class="form-body">
                                <?php if(@$errorvalidation != '' || @$errorlabel) { ?>
                                <div class="note note-warning">
                                    <h4 class="block">Warning!</h4>
                                    <?php echo @$errorvalidation; ?>
                                    <?php echo @$errorlabel; ?>
                                </div>
                                <?php  } ?>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Name</label>
                                    <div class="col-md-9">
                                        <input type="text" required name="name" class="form-control" value="<?php echo $company->name; ?>" ></span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-3 control-label">Alias</label>
                                    <div class="col-md-9">
                                        <input type="text" required name="alias" class="form-control" value="<?php echo $company->alias; ?>" ></span>
                                    </div>
                                </div>

                <div class="form-group">
                    <label class="control-label col-md-3">Description</label>
                    <div class="col-md-9">
                        <textarea required name="description" class="wysihtml5 form-control" rows="10"><?php echo $company->description; ?></textarea>
                    </div>
                </div>        
                            </div>                                          
                    </div>                            
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
            <input type="hidden" name="hiddenid" value="<?php echo $company->id; ?>" />
            
            <button type="submit" name="btnedit" value="submit" class="btn green">Submit</button>
        </div>