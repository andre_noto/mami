                    <!-- MODAL ADD NEW SLIDESHOW -->
                    <div class="modal fade bs-modal-lg" id="addslideshow" tabindex="-1" role="basic" aria-hidden="true">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <form class="form-horizontal" method="post" action="<?php echo base_url('backend/slideshow'); ?>" enctype="multipart/form-data" role="form">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                        <h4 class="modal-title">Add New Slideshow</h4>
                                    </div>
                                    <div class="modal-body">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <!-- BEGIN SAMPLE FORM PORTLET-->
                                                <div class="portlet-body form">
                                                       <div class="form-body">
                                                            <?php if(validation_errors() != '') { ?>
                                                            <div class="note note-warning">
                                                                <h4 class="block">Warning!</h4>
                                                                <?php echo validation_errors(); ?>
                                                            </div>
                                                            <?php  } ?>
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label">Title</label>
                                                                <div class="col-md-9">
                                                                    <input type="text" name="title" class="form-control" value="<?php echo $this->input->post('title'); ?>" >
                                                                    <span class="help-block">(This title will be displayed on the front page. Leave it empty if you doesnt want to display title.)</span>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label">Subtitle</label>
                                                                <div class="col-md-9">
                                                                    <textarea class="form-control" name="subtitle" rows="3" ><?php echo $this->input->post('subtitle'); ?></textarea>
                                                                     <span class="help-block">(This subtitle will be displayed on the bottom of title. Leave it empty if you doesnt want to display subtitle.)</span>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label">Text Position</label>
                                                                <div class="col-md-9">
                                                                    <div class="mt-radio-inline">
                                                                        <label class="mt-radio">
                                                                            <input type="radio" name="optionposition" value="left" <?php if($this->input->post('optionposition') == 'left' || !$this->input->post('optionposition')) { echo 'checked'; }  ?>> Left
                                                                            <span></span>
                                                                        </label>
                                                                        <label class="mt-radio">
                                                                            <input type="radio" name="optionposition" value="right" <?php if($this->input->post('optionposition') == 'right') { echo 'checked'; } ?>> Right
                                                                            <span></span>
                                                                        </label>
                                                                        
                                                                    </div>
                                                                </div>
                                                            </div>
                                                             <div class="form-group">
                                                                <label for="exampleInputFile" class="col-md-3 control-label">Image Upload</label>
                                                                <div class="col-md-9">
                                                                    <input type="file" id="imageupload" name="imageupload">
                                                                    <p class="help-block">(Required. Recommendation size: 1920 x 800 pixels.)</p>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label">URL</label>
                                                                <div class="col-md-9">
                                                                    <input type="text" id="url" value="<?php echo $this->input->post('url'); ?>" name="url" class="form-control" >
                                                                    <span class="help-block">(This will be shown as action button. It could be empty.)</span>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label">Call to Action Button</label>
                                                                <div class="col-md-9">
                                                                    <input <?php if($this->input->post('url') == '') { echo 'disabled="disabled"'; } ?> type="text" value="<?php echo $this->input->post('cta'); ?>" id="cta" name="cta" class="form-control" >
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label">Visibility</label>
                                                                <div class="col-md-9">
                                                                    <div class="mt-radio-inline">
                                                                        <label class="mt-radio">
                                                                            <input type="radio" name="optionvisibility" value="1" <?php if($this->input->post('optionvisibility') == 1 || !$this->input->post('optionvisibility')) { echo 'checked'; } ?>> True
                                                                            <span></span>
                                                                        </label>
                                                                        <label class="mt-radio">
                                                                            <input type="radio" name="optionvisibility" value="0" <?php if($this->input->post('optionvisibility') == 0) { echo 'checked'; } ?>> False
                                                                            <span></span>
                                                                        </label>
                                                                        
                                                                    </div>
                                                                </div>
                                                            </div>          
                                                        </div>                                          
                                                </div>                            
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
                                        <button type="submit" name="btnadd" value="submit" class="btn green">Submit</button>
                                    </div>
                                </form>
                            </div>
                            <!-- /.modal-content -->
                        </div>
                        <!-- /.modal-dialog -->
                    </div>

                     <!-- MODAL ADD NEW SLIDESHOW -->
                    <div class="modal fade bs-modal-lg" id="editslideshow" tabindex="-1" role="basic" aria-hidden="true">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <form class="form-horizontal" method="post" action="<?php echo base_url('backend/slideshow'); ?>" enctype="multipart/form-data" role="form">
                                    <div id="container">
                                    </div>
                                </form>
                            </div>
                            <!-- /.modal-content -->
                        </div>
                        <!-- /.modal-dialog -->
                    </div>

                    <!-- BEGIN PAGE BREADCRUMB -->
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="<?php echo base_url('backend'); ?>">Home</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span class="active">Slideshow</span>
                        </li>
                    </ul>
                    <!-- END PAGE BREADCRUMB -->
                     <!-- BEGIN PAGE BASE CONTENT -->
                    <div class="m-heading-1 border-green m-bordered">
                        <h3>DataTables jQuery Plugin</h3>
                        <p> DataTables is a plug-in for the jQuery Javascript library. It is a highly flexible tool, based upon the foundations of progressive enhancement, and will add advanced interaction controls to any HTML table. </p>
                        <p> For more info please check out
                            <a class="btn red btn-outline" href="http://datatables.net/" target="_blank">the official documentation</a>
                        </p>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                            <div class="portlet light bordered">
                                <div class="portlet-title">
                                    <div class="caption font-dark">
                                        <i class="fa fa-image"></i>
                                        <span class="caption-subject bold uppercase"> Slideshow Data</span>
                                    </div>                                    
                                </div>
                                <div class="portlet-body">
                                    <div class="table-toolbar">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="btn-group">
                                                    <a class="btn green sbold" data-toggle="modal" href="#addslideshow"> Add New <i class="fa fa-plus"></i> </a>
                                                </div>
                                            </div>
                                            <!-- tools tidak pakai -->
                                            <!-- <div class="col-md-6">
                                                <div class="btn-group pull-right">
                                                    <button class="btn green  btn-outline dropdown-toggle" data-toggle="dropdown">Tools
                                                        <i class="fa fa-angle-down"></i>
                                                    </button>
                                                    <ul class="dropdown-menu pull-right">
                                                        <li>
                                                            <a href="javascript:;">
                                                                <i class="fa fa-print"></i> Print </a>
                                                        </li>
                                                        <li>
                                                            <a href="javascript:;">
                                                                <i class="fa fa-file-pdf-o"></i> Save as PDF </a>
                                                        </li>
                                                        <li>
                                                            <a href="javascript:;">
                                                                <i class="fa fa-file-excel-o"></i> Export to Excel </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div> -->
                                        </div>
                                    </div>
                                    <table class="table table-striped table-bordered table-hover table-checkable order-column" id="">
                                        <thead>
                                            <tr>
                                                <th>
                                                    Order <i class="fa fa-arrows-v"></i>
                                                </th>
                                                <th> Title </th>
                                                <th> URL </th>
                                                <th> Visible </th>
                                                <th> Actions </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php if(@$slideshow) {
                                                $k =1;
                                            foreach ($slideshow as $key) { ?>
                                            <tr class="odd gradeX">
                                                <td>
                                                    <?php echo $k; $k++; ?>
                                                </td>
                                                <td>
                                                        <img class="img-thumbnail col-md-2" src="<?php echo base_url('images/slides/'.$key->thumbimg); ?>"/>
                                                    
                                                    <span class="col-md-10">
                                                 <?php echo "$key->judul<br/><small>$key->subjudul</small>"; ?>
                                                 </span> </td>
                                                <td>
                                                    <a href="<?php echo $key->url; ?>" target="_blank"><?php echo $key->url; ?></a>
                                                </td>
                                                <td>
                                                    <?php if($key->visibility == 1) { ?>
                                                    <span class="label label-sm label-success">Visible</span>
                                                    <?php } else {  ?>
                                                    <span class="label label-sm label-danger">Not Visible</span>
                                                    <?php } ?>
                                                </td>
                                                <td>
                                                    <div class="btn-group">
                                                        <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Actions
                                                            <i class="fa fa-angle-down"></i>
                                                        </button>
                                                        <ul class="dropdown-menu pull-left" role="menu">
                                                            <li>
                                                                <a href="javascript:;" dataid="<?php echo $key->id; ?>" class="editdata ">
                                                                    <i class="icon-docs"></i> Edit Data </a>
                                                            </li>
                                                            <li>
                                                                <a href="javascript:;" class="mt-sweetalert delbtn" dataid="<?php echo $key->id; ?>" >
                                                                    <i class="icon-tag"></i> Delete Data </a>
                                                            </li>
                                                            
                                                        </ul>
                                                    </div>
                                                </td>
                                            </tr>
                                                 
                                            <?php } 
                                             } else {  ?>
                                             <tr class="odd gradeX">
                                                <td colspan="5">
                                                    <span class="text-center">No Slideshow Data Found</span>
                                                </td>
                                            </tr>
                                             <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- END EXAMPLE TABLE PORTLET-->
                        </div>
                    </div>
                    
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->
           
        </div>
        <!-- END CONTAINER -->