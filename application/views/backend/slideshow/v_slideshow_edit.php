<div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            <h4 class="modal-title">Edit Slideshow</h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN SAMPLE FORM PORTLET-->
                    <div class="portlet-body form">
                           <div class="form-body">
                                <?php if(@$errorvalidation != '' || @$errorlabel) { ?>
                                <div class="note note-warning">
                                    <h4 class="block">Warning!</h4>
                                    <?php echo @$errorvalidation; ?>
                                    <?php echo @$errorlabel; ?>
                                </div>
                                <?php  } ?>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Judul</label>
                                    <div class="col-md-9">
                                        <input type="text" name="title" class="form-control" value="<?php echo $slide->judul; ?>" >
                                        <span class="help-block">(This title will be displayed on the front page. Leave it empty if you doesnt want to display title.)</span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Subjudul</label>
                                    <div class="col-md-9">
                                        <textarea class="form-control" name="subtitle" rows="3" ><?php echo $slide->subjudul;  ?></textarea>
                                         <span class="help-block">(This subtitle will be displayed on the bottom of title. Leave it empty if you doesnt want to display subtitle.)</span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Text Position</label>
                                    <div class="col-md-9">
                                        <div class="mt-radio-inline">
                                            <label class="mt-radio">
                                                <input type="radio" name="optionposition" value="left" <?php if($slide->text_position == 'left' ) { echo 'checked'; }  ?>> Left
                                                <span></span>
                                            </label>
                                            <label class="mt-radio">
                                                <input type="radio" name="optionposition" value="right" <?php if($slide->text_position == 'right') { echo 'checked'; } ?>> Right
                                                <span></span>
                                            </label>
                                            
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputFile" class="col-md-3 control-label">Current Image</label>
                                    <div class="col-md-9">
                                        <img class="img-thumbnail" src="<?php echo base_url('images/slides/'.$slide->thumbimg); ?>"/>
                                    </div>
                                </div>
                                 <div class="form-group">
                                    <label for="exampleInputFile" class="col-md-3 control-label">Image Upload</label>
                                    <div class="col-md-9">
                                        <input type="file" id="imageupload" name="imageupload">
                                        <p class="help-block">(Required. Recommendation size: 1920 x 800 pixels.)</p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">URL</label>
                                    <div class="col-md-9">
                                        <input type="text" id="url" value="<?php echo $slide->url; ?>" name="url" class="form-control" >
                                        <span class="help-block">(This will be shown as action button. It could be empty.)</span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Call to Action Button</label>
                                    <div class="col-md-9">
                                        <input <?php if($slide->url == '') { echo 'disabled="disabled"'; } ?> type="text" value="<?php echo $slide->cta; ?>" id="cta" name="cta" class="form-control" >
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Visibility</label>
                                    <div class="col-md-9">
                                        <div class="mt-radio-inline">
                                            <label class="mt-radio">
                                                <input type="radio" name="optionvisibility" value="1" <?php if($slide->visibility == 1 ) { echo 'checked'; } ?>> True
                                                <span></span>
                                            </label>
                                            <label class="mt-radio">
                                                <input type="radio" name="optionvisibility" value="0" <?php if($slide->visibility == 0) { echo 'checked'; } ?>> False
                                                <span></span>
                                            </label>
                                            
                                        </div>
                                    </div>
                                </div>          
                            </div>                                          
                    </div>                            
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
            <input type="hidden" name="hiddenid" value="<?php echo $slide->id; ?>" />
            <button type="submit" name="btnedit" value="submit" class="btn green">Submit</button>
        </div>