                    <!-- MODAL ADD NEW SLIDESHOW -->
                    <div class="modal fade bs-modal-lg" id="addnews" tabindex="-1" role="basic" aria-hidden="true">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <form class="form-horizontal" method="post" action="<?php echo base_url('backend/news?cat='.$this->input->get('cat'));  ?>" enctype="multipart/form-data" role="form">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                        <h4 class="modal-title">Add New News</h4>
                                    </div>
                                    <div class="modal-body">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <!-- BEGIN SAMPLE FORM PORTLET-->
                                                <div class="portlet-body form">
                                                       <div class="form-body">
                                                            <?php if(validation_errors() != '') { ?>
                                                            <div class="note note-warning">
                                                                <h4 class="block">Warning!</h4>
                                                                <?php echo validation_errors(); ?>
                                                            </div>
                                                            <?php  } ?>
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label">Title</label>
                                                                <div class="col-md-9">
                                                                    <input type="text" required name="title" class="form-control" value="<?php echo $this->input->post('title'); ?>" ></span>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label">Title (English)</label>
                                                                <div class="col-md-9">
                                                                    <input type="text" required name="title_en" class="form-control" value="<?php echo $this->input->post('title_en'); ?>" ></span>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label">Short Desc</label>
                                                                <div class="col-md-9">
                                                                    <textarea required class="form-control" name="short_desc" rows="3" ><?php echo $this->input->post('short_desc'); ?></textarea>
                                                                     
                                                                </div>
                                                            </div>  
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label">Short Desc (English)</label>
                                                                <div class="col-md-9">
                                                                    <textarea required class="form-control" name="short_desc_en" rows="3" ><?php echo $this->input->post('short_desc_en'); ?></textarea>
                                                                     
                                                                </div>
                                                            </div>  

<div class="form-group">
                                                <label class="control-label col-md-3">Content</label>
                                                <div class="col-md-9">
                                                    <textarea required name="content" class="wysihtml5 form-control" rows="10"><?php echo $this->input->post('content'); ?></textarea>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-md-3">Content (English)</label>
                                                <div class="col-md-9">
                                                    <textarea required name="content_en" class="wysihtml5 form-control" rows="10"><?php echo $this->input->post('content_en'); ?></textarea>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-md-3">Published Date</label>
                                                <div class="col-md-3">
                                                    <input class="form-control form-control-inline input-medium date-picker" name="published" size="16" type="text" value="" />
                                                    <span class="help-block"> Select date </span>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Meta Tags</label>
                                                <div class="col-md-9">
                                                    <textarea class="form-control" name="meta" rows="3" ><?php echo $this->input->post('meta'); ?></textarea>
                                                     
                                                </div>
                                            </div>  

                                                            
                                             <div class="form-group">
                                                <label for="exampleInputFile" class="col-md-3 control-label">Image Upload</label>
                                                <div class="col-md-9">
                                                    <input type="file" id="imageupload1" name="imageupload1">
                                                    <p class="help-block">(Required. Recommendation size: 1140 x 600 pixels.)</p>
                                                    <input type="hidden" name="totalimg" id="totalimg" value="1"/>

                                                    <div id="container">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="exampleInputFile" class="col-md-3 control-label"></label>
                                                <div class="col-md-9">
                                                    <a id="btnaddmore" class=" btn green sbold " href="#">Add More</a>
                                                </div>
                                            </div>
                                                            
                                                            
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Visibility</label>
                                                <div class="col-md-9">
                                                    <div class="mt-radio-inline">
                                                        <label class="mt-radio">
                                                            <input type="radio" name="visibility" value="1" <?php if($this->input->post('visibility') == 1 || !$this->input->post('visibility')) { echo 'checked'; } ?>> True
                                                            <span></span>
                                                        </label>
                                                        <label class="mt-radio">
                                                            <input type="radio" name="visibility" value="0" <?php if($this->input->post('visibility') == 0) { echo 'checked'; } ?>> False
                                                            <span></span>
                                                        </label>
                                                        
                                                    </div>
                                                </div>
                                            </div>          
                                                        </div>                                          
                                                </div>                            
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
                                        <button type="submit" name="btnadd" value="submit" class="btn green">Submit</button>
                                    </div>
                                </form>
                            </div>
                            <!-- /.modal-content -->
                        </div>
                        <!-- /.modal-dialog -->
                    </div>

                     <!-- MODAL ADD NEW SLIDESHOW -->
                    <div class="modal fade bs-modal-lg" id="editnews" tabindex="-1" role="basic" aria-hidden="true">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <form class="form-horizontal" method="post" action="<?php echo base_url('backend/news?cat='.$this->input->get('cat'));  ?>" enctype="multipart/form-data" role="form">
                                    <div id="containerarticle">
                                        <i class="fa fa-spinner fa-cog fa-3x"></i>
                                    </div>
                                </form>
                            </div>
                            <!-- /.modal-content -->
                        </div>
                        <!-- /.modal-dialog -->
                    </div>

                    <!-- BEGIN PAGE BREADCRUMB -->
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="<?php echo base_url('backend/home'); ?>">Home</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span class="active">News for <?php echo $company->name; ?> Company</span>
                        </li>
                    </ul>
                    <!-- END PAGE BREADCRUMB -->
                     <!-- BEGIN PAGE BASE CONTENT -->
                    

                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                            <div class="portlet light bordered">
                                <div class="portlet-title">
                                    <div class="caption font-dark">
                                        <i class="fa fa-laptop"></i>
                                        <span class="caption-subject bold uppercase"> <?php echo $company->name; ?> News Data</span>
                                    </div>                                    
                                </div>
                                <div class="portlet-body">
                                    <div class="table-toolbar">
                                        <div class="row">
                                            <div class="col-md-12">
                                                    <div class="col-md-6">
                                                        <a class="btn green sbold" data-toggle="modal" href="#addnews"> Add New <i class="fa fa-plus"></i> </a>
                                                    </div>
                                                    
                                            </div>
                                           
                                        </div>
                                    </div>
                                    <table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_1">
                                        <thead>
                                            <tr>
                                                <th> Title </th>
                                                <th> Published </th>
                                                <th> Viewed </th>
                                                <th> Visible </th>
                                                <th> Actions </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php if(@$news) {
                                                $k =0;
                                            foreach ($news as $key) { ?>
                                            <tr class="odd gradeX">
                                                <td class="col-md-4" style="text-align: left;">
                                                    <?php if($images[$k] != false) { ?>
                                                    <div class="col-md-5">
                                                        <img class="img-thumbnail" src="<?php echo base_url('images/news/'.$images[$k]); ?>"/>
                                                    </div>
                                                    <?php } $k++; ?>
                                                    <div class="col-md-7">
                                                    <?php echo $key->title.'<br/><small>'.$key->short_desc.'</small>'; ?>
                                                    </div>
                                                </td>
                                                <td>
                                                    <?php echo strftime("%d-%m-%Y", strtotime($key->published)); ?>        
                                                </td>
                                                <td>
                                                    <?php echo $key->viewed; ?>        
                                                </td>
                                                <td>
                                                    <?php if($key->visibility == 1) { ?>
                                                    <span class="label label-sm label-success">Visible</span>
                                                    <?php } else {  ?>
                                                    <span class="label label-sm label-danger">Not Visible</span>
                                                    <?php } ?>
                                                </td>
                                                <td>
                                                    <div class="btn-group">
                                                        <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Actions
                                                            <i class="fa fa-angle-down"></i>
                                                        </button>
                                                        <ul class="dropdown-menu pull-left" role="menu">
                                                            <li>
                                                                <a href="javascript:;" dataid="<?php echo $key->id; ?>" class="editdata ">
                                                                    <i class="icon-docs"></i> Edit Data </a>
                                                            </li>
                                                            <li>
                                                                <a href="javascript:;" class="mt-sweetalert delbtn" dataid="<?php echo $key->id; ?>" >
                                                                    <i class="icon-tag"></i> Delete Data </a>
                                                            </li>
                                                            
                                                        </ul>
                                                    </div>
                                                </td>
                                            </tr>
                                                 
                                            <?php } 
                                             } else {  ?>
                                             <tr class="odd gradeX">
                                                <td colspan="5">
                                                    <span class="text-center">No News Data Found</span>
                                                </td>
                                            </tr>
                                             <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- END EXAMPLE TABLE PORTLET-->
                        </div>
                    </div>
                    
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->
           
        </div>
        <!-- END CONTAINER -->