                    <!-- MODAL ADD NEW SLIDESHOW -->
                    <div class="modal fade bs-modal-lg" id="addslideshow" tabindex="-1" role="basic" aria-hidden="true">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <form class="form-horizontal" method="post" action="<?php echo base_url('backend/product'); ?>" enctype="multipart/form-data" role="form">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                        <h4 class="modal-title">Add New Product</h4>
                                    </div>
                                    <div class="modal-body">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <!-- BEGIN SAMPLE FORM PORTLET-->
                                                <div class="portlet-body form">
                                                       <div class="form-body">
                                                            <?php if(validation_errors() != '') { ?>
                                                            <div class="note note-warning">
                                                                <h4 class="block">Warning!</h4>
                                                                <?php echo validation_errors(); ?>
                                                            </div>
                                                            <?php  } ?>
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label">Name</label>
                                                                <div class="col-md-9">
                                                                    <input type="text" required name="name" class="form-control" value="<?php echo $this->input->post('name'); ?>" ></span>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label">Short Desc</label>
                                                                <div class="col-md-9">
                                                                    <textarea required class="form-control" name="short_desc" rows="3" ><?php echo $this->input->post('short_desc'); ?></textarea>
                                                                     
                                                                </div>
                                                            </div>  

                                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Category</label>
                                                <div class="col-md-9">
                                                    <select name="category_id" class="form-control">
                                                        <?php foreach ($category as $key) { ?>
                                                            <option value="<?php echo $key->id; ?>"><?php echo $key->category; ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Product Unit</label>
                                                <div class="col-md-9">
                                                    <select name="unit_id" class="form-control">
                                                        <?php foreach ($unit as $key) { ?>
                                                            <option value="<?php echo $key->id; ?>"><?php echo $key->unit; ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Status</label>
                                                <div class="col-md-9">
                                                    <select name="status" class="form-control">
                                                        <option <?php if($this->input->post('status') == 'ready') { echo 'selected="selected"';  } ?> value="ready">Ready</option>
                                                        <option <?php if($this->input->post('status') == 'outstock') { echo 'selected="selected"';  } ?> value="outstock">Out of stock</option>
                                                        
                                                    </select>
                                                </div>
                                            </div>



                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Price per Unit</label>
                                                <div class="col-md-9">
                                                    <div class="input-group">
                                                        <span class="input-group-addon">
                                                            IDR
                                                        </span>
                                                         <input type="number" min="0" required name="price" class="form-control" value="<?php echo $this->input->post('price'); ?>" />
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Price per Unit (for member)</label>
                                                <div class="col-md-9">
                                                     <div class="input-group">
                                                        <span class="input-group-addon">
                                                            IDR
                                                        </span>
                                                    <input type="number" min="0" required name="member_price" class="form-control" value="<?php echo $this->input->post('member_price'); ?>" /></div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-md-3">Description</label>
                                                <div class="col-md-9">
                                                    <textarea required name="desc" class="wysihtml5 form-control" rows="10"><?php echo $this->input->post('desc'); ?></textarea>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Meta Tags</label>
                                                <div class="col-md-9">
                                                    <textarea class="form-control" name="meta" rows="3" ><?php echo $this->input->post('meta'); ?></textarea>
                                                     
                                                </div>
                                            </div>  

                                                            
                                             <div class="form-group">
                                                <label for="exampleInputFile" class="col-md-3 control-label">Image Upload</label>
                                                <div class="col-md-9">
                                                    <input type="file" id="imageupload1" name="imageupload1">
                                                    <p class="help-block">(Required. Recommendation size: 500 x 550 pixels.)</p>
                                                    <input type="hidden" name="totalimg" id="totalimg" value="1"/>

                                                    <div id="container">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="exampleInputFile" class="col-md-3 control-label"></label>
                                                <div class="col-md-9">
                                                    <a id="btnaddmore" class=" btn green sbold " href="#">Add More</a>
                                                </div>
                                            </div>
                                                            
                                                            
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Visibility</label>
                                                <div class="col-md-9">
                                                    <div class="mt-radio-inline">
                                                        <label class="mt-radio">
                                                            <input type="radio" name="visibility" value="1" <?php if($this->input->post('visibility') == 1 || !$this->input->post('visibility')) { echo 'checked'; } ?>> True
                                                            <span></span>
                                                        </label>
                                                        <label class="mt-radio">
                                                            <input type="radio" name="visibility" value="0" <?php if($this->input->post('visibility') == 0) { echo 'checked'; } ?>> False
                                                            <span></span>
                                                        </label>
                                                        
                                                    </div>
                                                </div>
                                            </div>          
                                                        </div>                                          
                                                </div>                            
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
                                        <button type="submit" name="btnadd" value="submit" class="btn green">Submit</button>
                                    </div>
                                </form>
                            </div>
                            <!-- /.modal-content -->
                        </div>
                        <!-- /.modal-dialog -->
                    </div>

                     <!-- MODAL ADD NEW SLIDESHOW -->
                    <div class="modal fade bs-modal-lg" id="editproduct" tabindex="-1" role="basic" aria-hidden="true">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <form class="form-horizontal" method="post" action="<?php echo base_url('backend/product'); ?>" enctype="multipart/form-data" role="form">
                                    <div id="containerarticle">
                                        <i class="fa fa-spinner fa-cog fa-3x"></i>
                                    </div>
                                </form>
                            </div>
                            <!-- /.modal-content -->
                        </div>
                        <!-- /.modal-dialog -->
                    </div>

                    <!-- BEGIN PAGE BREADCRUMB -->
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="<?php echo base_url('backend'); ?>">Home</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span class="active">Web Setting</span>
                        </li>
                    </ul>
                    <!-- END PAGE BREADCRUMB -->
                     
                      <!-- BEGIN PAGE BASE CONTENT -->

                    <div class="row">
                        <div class="col-md-12">
                            <div class="tabbable-line boxless tabbable-reversed">
                                <ul class="nav nav-tabs">
                                    <li class="active">
                                        <a href="#tab_0" data-toggle="tab"> Company Data </a>
                                    </li>
                                    <li>
                                        <a href="#tab_1" data-toggle="tab"> Social Media </a>
                                    </li>
                                    <li>
                                        <a href="#tab_2" data-toggle="tab"> Meta Tags </a>
                                    </li>
                                    
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane active" id="tab_0">
                                        <div class="portlet box green">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <i class="fa fa-gift"></i>Company Data </div>
                                            </div>
                                            <div class="portlet-body form">
                                                <!-- BEGIN FORM-->
                                                <form action="<?php echo base_url('backend/setting'); ?>" method="post"  class="form-horizontal">
                                                    
                                                        <div class="form-body">
                                                            <?php if(validation_errors() != '') { ?>
                                                            <div class="note note-warning">
                                                                <h4 class="block">Warning!</h4>
                                                                <?php echo validation_errors(); ?>
                                                            </div>
                                                            <?php  } ?>
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label">Company Title</label>
                                                                <div class="col-md-9">
                                                                    <input type="text" required name="title" class="form-control" value="<?php echo $setting->judul; ?>" ></span>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label">Address</label>
                                                                <div class="col-md-9">
                                                                    <textarea required class="form-control" name="address" rows="3" ><?php echo $setting->address; ?></textarea>
                                                                     
                                                                </div>
                                                            </div>  

                                            
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Email</label>
                                                <div class="col-md-9">
                                                    <input type="email" min="0" required name="email" class="form-control" value="<?php echo $setting->email; ?>" />
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Phone</label>
                                                <div class="col-md-9">
                                                     <input type="text" min="0" required name="contact" class="form-control" value="<?php echo $setting->contact; ?>" />
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Fax</label>
                                                <div class="col-md-9">
                                                    <input type="text" min="0"  name="fax" class="form-control" value="<?php echo $setting->fax; ?>" />
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Longitude</label>
                                                <div class="col-md-9">
                                                   <input type="text" min="0" required name="long" class="form-control" value="<?php echo $setting->long; ?>" />
                                                         <span class="help-block"> (check location on <a href="http://www.latlong.net" target="_blank">http://www.latlong.net</a>). </span>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Latitude</label>
                                                <div class="col-md-9">
                                                    <input type="text" min="0" required name="lat" class="form-control" value="<?php echo $setting->lat; ?>" />
                                                         <span class="help-block"> (check location on <a href="http://www.latlong.net" target="_blank">http://www.latlong.net</a>). </span>
                                                </div>
                                            </div>


                                            <div class="form-group">
                                                <label class="control-label col-md-3">Web Description</label>
                                                <div class="col-md-9">
                                                    <textarea required name="desc" class="wysihtml5 form-control" rows="10"><?php echo $setting->desc; ?></textarea>
                                                     <span class="help-block"> (web description will be displayed on footer)</span>
                                                </div>
                                            </div>       
                                                        </div>
                                                    <div class="form-actions">
                                                        <div class="row">
                                                            <div class="col-md-offset-3 col-md-9">
                                                                <button type="submit" name="submitinfo" value="submit" class="btn btn-circle green">Submit</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                                <!-- END FORM-->
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="tab_1">
                                        <div class="portlet box green">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <i class="fa fa-gift"></i>Social Media </div>
                                            </div>
                                            <div class="portlet-body form">
                                                <!-- BEGIN FORM-->
                                                <form action="<?php echo base_url('backend/setting'); ?>" method="post"  class="form-horizontal">
                                                    <div class="form-body">

                                                    
                                                        <?php if(validation_errors() != '') { ?>
                                                        <div class="note note-warning">
                                                            <h4 class="block">Warning!</h4>
                                                            <?php echo validation_errors(); ?>
                                                        </div>
                                                        <?php  } ?>
                                                        <div class="form-group">
                                                            <label class="col-md-3 control-label"><i class="fa fa-facebook"></i> Facebook </label>
                                                            <div class="col-md-9">
                                                                <input type="text"  name="facebook" class="form-control" value="<?php echo $setting->facebook; ?>" ></span>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-md-3 control-label"><i class="fa fa-twitter"></i> Twitter </label>
                                                            <div class="col-md-9">
                                                                <input type="text"  name="twitter" class="form-control" value="<?php echo $setting->twitter; ?>" ></span>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="col-md-3 control-label"><i class="fa fa-google-plus"></i> Google Plus </label>
                                                            <div class="col-md-9">
                                                                <input type="text"  name="googleplus" class="form-control" value="<?php echo $setting->googleplus; ?>" ></span>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="col-md-3 control-label"><i class="fa fa-instagram"></i> Instagram </label>
                                                            <div class="col-md-9">
                                                                <input type="text"  name="instagram" class="form-control" value="<?php echo $setting->instagram; ?>" ></span>
                                                            </div>
                                                        </div>

                                                         <div class="form-group">
                                                            <label class="col-md-3 control-label"><i class="fa fa-line"></i> Line </label>
                                                            <div class="col-md-9">
                                                                <input type="text"  name="line" class="form-control" value="<?php echo $setting->line; ?>" ></span>
                                                            </div>
                                                        </div>

                                                         <div class="form-group">
                                                            <label class="col-md-3 control-label"><i class="fa fa-youtube"></i> Youtube </label>
                                                            <div class="col-md-9">
                                                                <input type="text"  name="youtube" class="form-control" value="<?php echo $setting->youtube; ?>" ></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-actions">
                                                        <div class="row">
                                                            <div class="col-md-offset-3 col-md-9">
                                                                <button type="submit" name="submitsocial" value="submit" class="btn btn-circle green">Submit</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                                <!-- END FORM-->
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="tab_2">
                                        <div class="portlet box green">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <i class="fa fa-gift"></i>Meta Tags </div>
                                            </div>
                                            <div class="portlet-body form">
                                                
                                                <!-- BEGIN FORM-->
                                                <form action="<?php echo base_url('backend/setting'); ?>" method="post"  class="form-horizontal">
                                                    
                                                    <div class="form-body">

                                                    <div class="m-heading-1 border-green m-bordered">
                        <h3>Meta Tags</h3>
                        <p> Meta tags could be used by web crawler (ex. Google search) to identify website faster. Visit <a href="https://www.metatags.org/meta_tags_code_generator" target="_blank">https://www.metatags.org/meta_tags_code_generator</a> to generate meta tags</p>
                    </div>
                                                        <?php if(validation_errors() != '') { ?>
                                                        <div class="note note-warning">
                                                            <h4 class="block">Warning!</h4>
                                                            <?php echo validation_errors(); ?>
                                                        </div>
                                                        <?php  } ?>
                                                         <div class="form-group">
                                                            <label class="col-md-3 control-label">Home Meta</label>
                                                            <div class="col-md-9">
                                                                <textarea required class="form-control" name="default_meta" rows="3" ><?php echo $setting->default_meta; ?></textarea>
                                                                 <span class="help-block"> (Meta tags used on home page ) </span>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-md-3 control-label">Product Meta</label>
                                                            <div class="col-md-9">
                                                                <textarea required class="form-control" name="products_meta" rows="3" ><?php echo $setting->products_meta; ?></textarea>
                                                                 <span class="help-block"> (Meta tags used on product home page ) </span>
                                                            </div>
                                                        </div>  
                                                        <div class="form-group">
                                                            <label class="col-md-3 control-label">News Meta</label>
                                                            <div class="col-md-9">
                                                                <textarea required class="form-control" name="news_meta" rows="3" ><?php echo $setting->news_meta; ?></textarea>
                                                                 <span class="help-block"> (News tags used on news home page ) </span>
                                                            </div>
                                                        </div> 

                                                        <div class="form-group">
                                                            <label class="col-md-3 control-label">Contact Meta</label>
                                                            <div class="col-md-9">
                                                                <textarea required class="form-control" name="contact_meta" rows="3" ><?php echo $setting->contact_meta; ?></textarea>
                                                                 <span class="help-block"> (Meta tags used on contact page ) </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-actions">
                                                        <div class="row">
                                                            <div class="col-md-offset-3 col-md-9">
                                                                <button type="submit" name="submitmeta" value="submit" class="btn btn-circle green">Submit</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                                <!-- END FORM-->
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="tab_3">
                                        <div class="portlet box blue">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <i class="fa fa-gift"></i>Form Sample </div>
                                                <div class="tools">
                                                    <a href="javascript:;" class="collapse"> </a>
                                                    <a href="#portlet-config" data-toggle="modal" class="config"> </a>
                                                    <a href="javascript:;" class="reload"> </a>
                                                    <a href="javascript:;" class="remove"> </a>
                                                </div>
                                            </div>
                                            <div class="portlet-body form">
                                                <!-- BEGIN FORM-->
                                                <form class="form-horizontal" role="form">
                                                    <div class="form-body">
                                                        <h2 class="margin-bottom-20"> View User Info - Bob Nilson </h2>
                                                        <h3 class="form-section">Person Info</h3>
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-3">First Name:</label>
                                                                    <div class="col-md-9">
                                                                        <p class="form-control-static"> Bob </p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!--/span-->
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-3">Last Name:</label>
                                                                    <div class="col-md-9">
                                                                        <p class="form-control-static"> Nilson </p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!--/span-->
                                                        </div>
                                                        <!--/row-->
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-3">Gender:</label>
                                                                    <div class="col-md-9">
                                                                        <p class="form-control-static"> Male </p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!--/span-->
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-3">Date of Birth:</label>
                                                                    <div class="col-md-9">
                                                                        <p class="form-control-static"> 20.01.1984 </p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!--/span-->
                                                        </div>
                                                        <!--/row-->
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-3">Category:</label>
                                                                    <div class="col-md-9">
                                                                        <p class="form-control-static"> Category1 </p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!--/span-->
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-3">Membership:</label>
                                                                    <div class="col-md-9">
                                                                        <p class="form-control-static"> Free </p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!--/span-->
                                                        </div>
                                                        <!--/row-->
                                                        <h3 class="form-section">Address</h3>
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-3">Address:</label>
                                                                    <div class="col-md-9">
                                                                        <p class="form-control-static"> #24 Sun Park Avenue, Rolton Str </p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-3">City:</label>
                                                                    <div class="col-md-9">
                                                                        <p class="form-control-static"> New York </p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!--/span-->
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-3">State:</label>
                                                                    <div class="col-md-9">
                                                                        <p class="form-control-static"> New York </p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!--/span-->
                                                        </div>
                                                        <!--/row-->
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-3">Post Code:</label>
                                                                    <div class="col-md-9">
                                                                        <p class="form-control-static"> 457890 </p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!--/span-->
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-3">Country:</label>
                                                                    <div class="col-md-9">
                                                                        <p class="form-control-static"> USA </p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!--/span-->
                                                        </div>
                                                    </div>
                                                    <div class="form-actions">
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="row">
                                                                    <div class="col-md-offset-3 col-md-9">
                                                                        <button type="submit" class="btn green">
                                                                            <i class="fa fa-pencil"></i> Edit</button>
                                                                        <button type="button" class="btn default">Cancel</button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6"> </div>
                                                        </div>
                                                    </div>
                                                </form>
                                                <!-- END FORM-->
                                            </div>
                                        </div>
                                        <div class="portlet light bordered">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <i class="icon-equalizer font-green-haze"></i>
                                                    <span class="caption-subject font-green-haze bold uppercase">Form Sample</span>
                                                    <span class="caption-helper">some info...</span>
                                                </div>
                                                <div class="tools">
                                                    <a href="" class="collapse"> </a>
                                                    <a href="#portlet-config" data-toggle="modal" class="config"> </a>
                                                    <a href="" class="reload"> </a>
                                                    <a href="" class="remove"> </a>
                                                </div>
                                            </div>
                                            <div class="portlet-body form">
                                                <!-- BEGIN FORM-->
                                                <form class="form-horizontal" role="form">
                                                    <div class="form-body">
                                                        <h2 class="margin-bottom-20"> View User Info - Bob Nilson </h2>
                                                        <h3 class="form-section">Person Info</h3>
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-3">First Name:</label>
                                                                    <div class="col-md-9">
                                                                        <p class="form-control-static"> Bob </p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!--/span-->
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-3">Last Name:</label>
                                                                    <div class="col-md-9">
                                                                        <p class="form-control-static"> Nilson </p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!--/span-->
                                                        </div>
                                                        <!--/row-->
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-3">Gender:</label>
                                                                    <div class="col-md-9">
                                                                        <p class="form-control-static"> Male </p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!--/span-->
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-3">Date of Birth:</label>
                                                                    <div class="col-md-9">
                                                                        <p class="form-control-static"> 20.01.1984 </p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!--/span-->
                                                        </div>
                                                        <!--/row-->
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-3">Category:</label>
                                                                    <div class="col-md-9">
                                                                        <p class="form-control-static"> Category1 </p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!--/span-->
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-3">Membership:</label>
                                                                    <div class="col-md-9">
                                                                        <p class="form-control-static"> Free </p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!--/span-->
                                                        </div>
                                                        <!--/row-->
                                                        <h3 class="form-section">Address</h3>
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-3">Address:</label>
                                                                    <div class="col-md-9">
                                                                        <p class="form-control-static"> #24 Sun Park Avenue, Rolton Str </p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-3">City:</label>
                                                                    <div class="col-md-9">
                                                                        <p class="form-control-static"> New York </p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!--/span-->
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-3">State:</label>
                                                                    <div class="col-md-9">
                                                                        <p class="form-control-static"> New York </p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!--/span-->
                                                        </div>
                                                        <!--/row-->
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-3">Post Code:</label>
                                                                    <div class="col-md-9">
                                                                        <p class="form-control-static"> 457890 </p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!--/span-->
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-3">Country:</label>
                                                                    <div class="col-md-9">
                                                                        <p class="form-control-static"> USA </p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!--/span-->
                                                        </div>
                                                    </div>
                                                    <div class="form-actions">
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="row">
                                                                    <div class="col-md-offset-3 col-md-9">
                                                                        <button type="submit" class="btn green">
                                                                            <i class="fa fa-pencil"></i> Edit</button>
                                                                        <button type="button" class="btn default">Cancel</button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6"> </div>
                                                        </div>
                                                    </div>
                                                </form>
                                                <!-- END FORM-->
                                            </div>
                                        </div>
                                        <div class="portlet light bg-inverse">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <i class="icon-equalizer font-red-sunglo"></i>
                                                    <span class="caption-subject font-red-sunglo bold uppercase">Form Sample</span>
                                                    <span class="caption-helper">some info...</span>
                                                </div>
                                                <div class="tools">
                                                    <a href="" class="collapse"> </a>
                                                    <a href="#portlet-config" data-toggle="modal" class="config"> </a>
                                                    <a href="" class="reload"> </a>
                                                    <a href="" class="remove"> </a>
                                                </div>
                                            </div>
                                            <div class="portlet-body form">
                                                <!-- BEGIN FORM-->
                                                <form class="form-horizontal" role="form">
                                                    <div class="form-body">
                                                        <h2 class="margin-bottom-20"> View User Info - Bob Nilson </h2>
                                                        <h3 class="form-section">Person Info</h3>
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-3">First Name:</label>
                                                                    <div class="col-md-9">
                                                                        <p class="form-control-static"> Bob </p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!--/span-->
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-3">Last Name:</label>
                                                                    <div class="col-md-9">
                                                                        <p class="form-control-static"> Nilson </p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!--/span-->
                                                        </div>
                                                        <!--/row-->
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-3">Gender:</label>
                                                                    <div class="col-md-9">
                                                                        <p class="form-control-static"> Male </p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!--/span-->
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-3">Date of Birth:</label>
                                                                    <div class="col-md-9">
                                                                        <p class="form-control-static"> 20.01.1984 </p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!--/span-->
                                                        </div>
                                                        <!--/row-->
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-3">Category:</label>
                                                                    <div class="col-md-9">
                                                                        <p class="form-control-static"> Category1 </p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!--/span-->
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-3">Membership:</label>
                                                                    <div class="col-md-9">
                                                                        <p class="form-control-static"> Free </p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!--/span-->
                                                        </div>
                                                        <!--/row-->
                                                        <h3 class="form-section">Address</h3>
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-3">Address:</label>
                                                                    <div class="col-md-9">
                                                                        <p class="form-control-static"> #24 Sun Park Avenue, Rolton Str </p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-3">City:</label>
                                                                    <div class="col-md-9">
                                                                        <p class="form-control-static"> New York </p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!--/span-->
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-3">State:</label>
                                                                    <div class="col-md-9">
                                                                        <p class="form-control-static"> New York </p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!--/span-->
                                                        </div>
                                                        <!--/row-->
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-3">Post Code:</label>
                                                                    <div class="col-md-9">
                                                                        <p class="form-control-static"> 457890 </p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!--/span-->
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label col-md-3">Country:</label>
                                                                    <div class="col-md-9">
                                                                        <p class="form-control-static"> USA </p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!--/span-->
                                                        </div>
                                                    </div>
                                                    <div class="form-actions">
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="row">
                                                                    <div class="col-md-offset-3 col-md-9">
                                                                        <button type="submit" class="btn green">
                                                                            <i class="fa fa-pencil"></i> Edit</button>
                                                                        <button type="button" class="btn default">Cancel</button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6"> </div>
                                                        </div>
                                                    </div>
                                                </form>
                                                <!-- END FORM-->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END PAGE BASE CONTENT -->
                    
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->
           
        </div>
        <!-- END CONTAINER -->