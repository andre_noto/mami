                    <!-- MODAL ADD NEW SLIDESHOW -->
                    <div class="modal fade bs-modal-lg" id="addslideshow" tabindex="-1" role="basic" aria-hidden="true">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <form class="form-horizontal" method="post" action="<?php if($this->input->get('cat')) { echo base_url('backend/content?cat='.$this->input->get('cat')); } else { echo base_url('backend/content'); } ?>" enctype="multipart/form-data" role="form">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                        <h4 class="modal-title">Add New Content</h4>
                                    </div>
                                    <div class="modal-body">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <!-- BEGIN SAMPLE FORM PORTLET-->
                                                <div class="portlet-body form">
                                                    <div class="form-body">
                                                        <?php if(validation_errors() != '') { ?>
                                                        <div class="note note-warning">
                                                            <h4 class="block">Warning!</h4>
                                                            <?php echo validation_errors(); ?>
                                                        </div>
                                                        <?php  } ?>
                                                        <div class="form-group">
                                                            <label class="col-md-3 control-label">Name</label>
                                                            <div class="col-md-9">
                                                                <input type="text" required name="name" id="name" class="form-control" value="<?php echo $this->input->post('name'); ?>" ></span>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-md-3 control-label">Alias</label>
                                                            <div class="col-md-3">
                                                                <input type="text" required name="alias" class="form-control" value="<?php echo $this->input->post('alias'); ?>" ></span>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Description</label>
                                                            <div class="col-md-9">
                                                                <textarea required name="description" class="wysihtml5 form-control" rows="10"><?php echo $this->input->post('description'); ?></textarea>
                                                            </div>
                                                        </div>
                                                         <div class="form-group">
                                                            <label class="control-label col-md-3">Description (English)</label>
                                                            <div class="col-md-9">
                                                                <textarea required name="description_english" class="wysihtml5 form-control" rows="10"><?php echo $this->input->post('description_english'); ?></textarea>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Company</label>
                                                            <div class="col-md-9">
                                                                <select class=" bs-select form-control input-small" name="company_id" data-style="btn-primary">
                                                        <?php foreach ($company as $key) { ?>
                                                            <option <?php if($this->input->get('cat') == $key->id) { echo 'selected="selected"'; } ?> value="<?php echo $key->id; ?>"><?php echo $key->name; ?></option>
                                                        <?php } ?>
                                                        </select>
                                                            </div>
                                                        </div>
                                                            <div class="form-group">
                                                            <label class="control-label col-md-3">Content Type</label>
                                                            <div class="col-md-9">
                                                                <select class=" bs-select form-control input-small" name="content_type" data-style="btn-primary">
                                                        <?php foreach ($content_type as $key) { ?>
                                                            <option value="<?php echo $key->id; ?>"><?php echo $key->name; ?></option>
                                                        <?php } ?>
                                                        </select>
                                                            </div>
                                                        </div>
                                                    </div>                                          
                                                </div>                            
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
                                        <button type="submit" name="btnadd" value="submit" class="btn green">Submit</button>
                                    </div>
                                </form>
                            </div>
                            <!-- /.modal-content -->
                        </div>
                        <!-- /.modal-dialog -->
                    </div>

                     <!-- MODAL ADD NEW SLIDESHOW -->
                    <div class="modal fade bs-modal-lg" id="editproduct" tabindex="-1" role="basic" aria-hidden="true">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <form class="form-horizontal" method="post" action="<?php if($this->input->get('cat')) { echo base_url('backend/content?cat='.$this->input->get('cat')); } else { echo base_url('backend/content'); } ?>" enctype="multipart/form-data" role="form">
                                    <div id="containerarticle">
                                        <i class="fa fa-spinner fa-cog fa-3x"></i>
                                    </div>
                                </form>
                            </div>
                            <!-- /.modal-content -->
                        </div>
                        <!-- /.modal-dialog -->
                    </div>

                    <!-- BEGIN PAGE BREADCRUMB -->
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="<?php echo base_url('backend/home'); ?>">Home</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span class="active">Menu</span>
                        </li>
                    </ul>
                    <!-- END PAGE BREADCRUMB -->
                     <!-- BEGIN PAGE BASE CONTENT -->
                    <div class="m-heading-1 border-green m-bordered">                        
                        <p> Switch <strong>company content menu</strong> by using rightmost combobox. If you want to edit a menu, click <strong>action > edit data</strong> on chosen menu. You can do drag and drop on menu title (inside table) to change its showing order (affected on frontend). Showing order position will automatically saved. </p>
                        <p> For more info please check out <strong>user manual</strong>
                        </p>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                            <div class="portlet light bordered">
                                <div class="portlet-title">
                                    <div class="caption font-dark">
                                        <i class="fa fa-laptop"></i>
                                        <span class="caption-subject bold uppercase"> Menu Data</span>
                                    </div>                                    
                                </div>
                                <div class="portlet-body">
                                    <form id="sortingform" action="<?php if($this->input->get('cat')) { echo base_url('backend/content?cat='.$this->input->get('cat')); } else { echo base_url('backend/content'); } ?>" method="post">
                                        <input type="hidden" name="sortingaction" value="true"/>
                                        <input type="hidden" name="activecategory" value="<?php echo $activecat; ?>"/>
                                        <div class="table-toolbar">
                                            <div class="row">
                                                <div class="col-md-12">
                                                        <div class="col-md-6">
                                                            <a class="btn green sbold" data-toggle="modal" href="#addslideshow"> Add New <i class="fa fa-plus"></i> </a>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <?php if($this->input->get('cat')) {
                                                                $catid=$this->input->get('cat');
                                                            } else {
                                                                $catid=1;
                                                            } ?>
                                                            <select class="pull-right bs-select form-control input-small" id="selfilter" data-style="btn-primary">
                                                            <?php foreach ($company as $key) { ?>
                                                                <option <?php if($catid == $key->id) { echo 'selected="selected"'; } ?> value="<?php echo $key->id; ?>"><?php echo $key->name; ?></option>
                                                            <?php } ?>
                                                            </select>
                                                        </div>
                                                </div>
                                               
                                            </div>
                                        </div>
                                        <table class="table table-striped table-bordered table-hover table-checkable order-column" >
                                            <thead>
                                                <tr>
                                                    <th> Menu Title </th>
                                                    <th> Alias </th>
                                                    <th> Content Type </th>
                                                    <th> Description </th>
                                                    <th> Actions </th>
                                                </tr>
                                            </thead>
                                            <tbody id="sortable"> 
                                                <?php if(@$content) {
                                                    $k =0;
                                                foreach ($content as $key) { ?>
                                                <tr class="odd gradeX" >
                                                    <td class="col-md-4" style="text-align: left;">
                                                        <?php echo $key->name; ?>   
                                                        <input type="hidden" name="sortid[]" value="<?php echo $key->id; ?>" />
                                                    </td>
                                                    <td>
                                                        <?php echo $key->alias; ?>        
                                                    </td> 
                                                    <td>
                                                        <?php echo $key->contentype; ?>        
                                                    </td> 
                                                    <td>
                                                        <?php echo $key->description; ?>        
                                                    </td>                                              
                                                    <td class="col-md-2" style="text-align: left;">
                                                        <div class="btn-group">
                                                            <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Actions
                                                                <i class="fa fa-angle-down"></i>
                                                            </button>
                                                            <ul class="dropdown-menu pull-left" role="menu">
                                                                <li>
                                                                    <a href="javascript:;" dataid="<?php echo $key->id; ?>" class="editdata ">
                                                                        <i class="icon-docs"></i> Edit Data </a>
                                                                </li>
                                                                <li>
                                                                    <a href="javascript:;" class="mt-sweetalert delbtn" dataid="<?php echo $key->id; ?>" >
                                                                        <i class="icon-tag"></i> Delete Data </a>
                                                                </li>
                                                                
                                                            </ul>
                                                        </div>
                                                    </td>
                                                </tr>
                                                     
                                                <?php } 
                                                 } ?>
                                            </tbody>
                                        </table>
                                    </form>
                                </div>
                            </div>
                            <!-- END EXAMPLE TABLE PORTLET-->
                        </div>
                    </div>
                    
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->
           
        </div>
        <!-- END CONTAINER -->