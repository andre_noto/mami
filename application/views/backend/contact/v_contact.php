               
                    <!-- BEGIN PAGE BREADCRUMB -->
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="<?php echo base_url('backend/home'); ?>">Home</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span class="active">Contact Setting for <?php echo $company->name; ?> Company</span>
                        </li>
                    </ul>
                    <!-- END PAGE BREADCRUMB -->
                   
                    

                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                            <div class="portlet light bordered">
                                <div class="portlet-title">
                                    <div class="caption font-dark">
                                        <i class="fa fa-laptop"></i>
                                        <span class="caption-subject bold uppercase"> Contact Data</span>
                                    </div>                                    
                                </div>
                                <div class="portlet-body">
                                    <form class="form-horizontal" method="post" action="<?php echo base_url('backend/contact?cat='.$this->input->get('cat'));  ?>" enctype="multipart/form-data" role="form">
                                  
                                       
                                         <?php if(validation_errors() != '') { ?>
                                        <div class="note note-warning">
                                            <h4 class="block">Warning!</h4>
                                            <?php echo validation_errors(); ?>
                                        </div>
                                        <?php  } ?>

                                       <div class="form-group">
                                            <label class="col-md-3 control-label">Email</label>
                                            <div class="col-md-9">
                                                <input type="text" required name="email" class="form-control" value="<?php echo $company->email; ?>" />
                                                <span class="help-block"> This is company email. All email from user will be sent here. </span>
                                            </div>
                                        </div>
                                         <div class="form-group">
                                            <label class="col-md-3 control-label">System Email</label>
                                            <div class="col-md-9">
                                                <input type="text" required name="email_system" class="form-control" value="<?php echo $company->email_system; ?>" />
                                                 <span class="help-block"> This is system email. Its used by internal system for email forwarding. <strong>This email must have same domain with main website (ex: support@mamigroup.co.id).</strong> </span>
                                            </div>
                                        </div>                                       

                                        <div class="form-group">
                                            <label class="control-label col-md-3">Feedback Email</label>
                                            <div class="col-md-9">
                                                <textarea required name="email_feedback" class="wysihtml5 form-control" rows="10"><?php echo $company->email_feedback; ?></textarea>
                                                <span class="help-block"> This is a message feedback that will be displayed after user sent an email. </span>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3">Feedback Email (English)</label>
                                            <div class="col-md-9">
                                                <textarea required name="email_feedback_en" class="wysihtml5 form-control" rows="10"><?php echo $company->email_feedback_en; ?></textarea>
                                            </div>
                                        </div>
                                                                        
                                        <div class="form-actions">
                                            <div class="row">
                                                <div class="col-md-offset-3 col-md-9">
                                                    <button type="submit" name="submitinfo" value="submit" class="btn btn-circle green">Submit</button>
                                                </div>
                                            </div>
                                        </div>                                                      
                                    </form>
                                </div>
                            </div>
                            <!-- END EXAMPLE TABLE PORTLET-->
                        </div>
                    </div>
                    
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->
           
        </div>
        <!-- END CONTAINER -->