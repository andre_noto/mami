               
                    <!-- BEGIN PAGE BREADCRUMB -->
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="<?php echo base_url('backend/home'); ?>">Home</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span class="active">About Content for <?php echo $company->name; ?> Company</span>
                        </li>
                    </ul>
                    <!-- END PAGE BREADCRUMB -->
                   
                    

                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                            <div class="portlet light bordered">
                                <div class="portlet-title">
                                    <div class="caption font-dark">
                                        <i class="fa fa-laptop"></i>
                                        <span class="caption-subject bold uppercase"> Content Data</span>
                                    </div>                                    
                                </div>
                                <div class="portlet-body">
                                    <form class="form-horizontal" method="post" action="<?php echo base_url('backend/about?cat='.$this->input->get('cat'));  ?>" enctype="multipart/form-data" role="form">
                                  
                                       
                                         <?php if(validation_errors() != '') { ?>
                                        <div class="note note-warning">
                                            <h4 class="block">Warning!</h4>
                                            <?php echo validation_errors(); ?>
                                        </div>
                                        <?php  } ?>

                                        <div class="form-group">
                                            <label class="control-label col-md-3">Vision</label>
                                            <div class="col-md-9">
                                                <textarea required name="vision" class="wysihtml5 form-control" rows="10"><?php echo $about->vision; ?></textarea>
                                            </div>
                                        </div>
                                         <div class="form-group">
                                            <label class="control-label col-md-3">Vision (English)</label>
                                            <div class="col-md-9">
                                                <textarea required name="vision_en" class="wysihtml5 form-control" rows="10"><?php echo $about->vision_en; ?></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Mission</label>
                                            <div class="col-md-9">
                                                <textarea required name="mission" class="wysihtml5 form-control" rows="10"><?php echo $about->mission; ?></textarea>
                                            </div>
                                        </div>
                                         <div class="form-group">
                                            <label class="control-label col-md-3">Mission (English)</label>
                                            <div class="col-md-9">
                                                <textarea required name="mission_en" class="wysihtml5 form-control" rows="10"><?php echo $about->mission_en; ?></textarea>
                                            </div>
                                        </div>

                                         <div class="form-group">
                                            <label class="control-label col-md-3">Value</label>
                                            <div class="col-md-9">
                                                <textarea required name="value" class="wysihtml5 form-control" rows="10"><?php echo $about->value; ?></textarea>
                                            </div>
                                        </div>
                                         <div class="form-group">
                                            <label class="control-label col-md-3">Value (English)</label>
                                            <div class="col-md-9">
                                                <textarea required name="value_en" class="wysihtml5 form-control" rows="10"><?php echo $about->value_en; ?></textarea>
                                            </div>
                                        </div>                                                                                                                  
                                        <div class="form-actions">
                                            <div class="row">
                                                <div class="col-md-offset-3 col-md-9">
                                                    <button type="submit" name="submitinfo" value="submit" class="btn btn-circle green">Submit</button>
                                                </div>
                                            </div>
                                        </div>                                                      
                                    </form>
                                </div>
                            </div>
                            <!-- END EXAMPLE TABLE PORTLET-->
                        </div>
                    </div>
                    
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->
           
        </div>
        <!-- END CONTAINER -->