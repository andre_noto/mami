                    <!-- MODAL ADD NEW SLIDESHOW -->
                    <div class="modal fade bs-modal-lg" id="addslideshow" tabindex="-1" role="basic" aria-hidden="true">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <form class="form-horizontal" method="post" action="<?php echo base_url('backend/article'); ?>" enctype="multipart/form-data" role="form">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                        <h4 class="modal-title">Add New Article</h4>
                                    </div>
                                    <div class="modal-body">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <!-- BEGIN SAMPLE FORM PORTLET-->
                                                <div class="portlet-body form">
                                                       <div class="form-body">
                                                            <?php if(validation_errors() != '') { ?>
                                                            <div class="note note-warning">
                                                                <h4 class="block">Warning!</h4>
                                                                <?php echo validation_errors(); ?>
                                                            </div>
                                                            <?php  } ?>
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label">Title</label>
                                                                <div class="col-md-9">
                                                                    <input type="text" required name="title" class="form-control" value="<?php echo $this->input->post('title'); ?>" ></span>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label">Short Desc</label>
                                                                <div class="col-md-9">
                                                                    <textarea required class="form-control" name="short_desc" rows="3" ><?php echo $this->input->post('short_desc'); ?></textarea>
                                                                     
                                                                </div>
                                                            </div>  

                                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Category</label>
                                                <div class="col-md-9">
                                                    <select name="category_id" class="form-control">
                                                        <?php foreach ($category as $key) { ?>
                                                            <option value="<?php echo $key->id; ?>"><?php echo $key->category; ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-md-3">Content</label>
                                                <div class="col-md-9">
                                                    <textarea required name="content" class="wysihtml5 form-control" rows="10"></textarea>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Meta Tags</label>
                                                <div class="col-md-9">
                                                    <textarea class="form-control" name="meta" rows="3" ><?php echo $this->input->post('meta'); ?></textarea>
                                                     
                                                </div>
                                            </div>  

                                                            
                                             <div class="form-group">
                                                <label for="exampleInputFile" class="col-md-3 control-label">Image Upload</label>
                                                <div class="col-md-9">
                                                    <input type="file" id="imageupload1" name="imageupload1">
                                                    <p class="help-block">(Required. Recommendation size: 1920 x 800 pixels.)</p>
                                                    <input type="hidden" name="totalimg" id="totalimg" value="1"/>

                                                    <div id="container">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="exampleInputFile" class="col-md-3 control-label"></label>
                                                <div class="col-md-9">
                                                    <a id="btnaddmore" class=" btn green sbold " href="#">Add More</a>
                                                </div>
                                            </div>
                                                            
                                                            
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Visibility</label>
                                                <div class="col-md-9">
                                                    <div class="mt-radio-inline">
                                                        <label class="mt-radio">
                                                            <input type="radio" name="visibility" value="1" <?php if($this->input->post('visibility') == 1 || !$this->input->post('visibility')) { echo 'checked'; } ?>> True
                                                            <span></span>
                                                        </label>
                                                        <label class="mt-radio">
                                                            <input type="radio" name="visibility" value="0" <?php if($this->input->post('visibility') == 0) { echo 'checked'; } ?>> False
                                                            <span></span>
                                                        </label>
                                                        
                                                    </div>
                                                </div>
                                            </div>          
                                                        </div>                                          
                                                </div>                            
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
                                        <button type="submit" name="btnadd" value="submit" class="btn green">Submit</button>
                                    </div>
                                </form>
                            </div>
                            <!-- /.modal-content -->
                        </div>
                        <!-- /.modal-dialog -->
                    </div>

                     <!-- MODAL ADD NEW SLIDESHOW -->
                    <div class="modal fade bs-modal-lg" id="editarticle" tabindex="-1" role="basic" aria-hidden="true">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <form class="form-horizontal" method="post" action="<?php echo base_url('backend/article'); ?>" enctype="multipart/form-data" role="form">
                                    <div id="containerarticle">
                                        <i class="fa fa-spinner fa-cog fa-3x"></i>
                                    </div>
                                </form>
                            </div>
                            <!-- /.modal-content -->
                        </div>
                        <!-- /.modal-dialog -->
                    </div>

                    <!-- BEGIN PAGE BREADCRUMB -->
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="<?php echo base_url('backend'); ?>">Home</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span class="active">Article</span>
                        </li>
                    </ul>
                    <!-- END PAGE BREADCRUMB -->
                     <!-- BEGIN PAGE BASE CONTENT -->
                    <div class="m-heading-1 border-green m-bordered">
                        <h3>Change Category</h3>
                        <p> Each article categorized into one of three segment: About, Guides, and Beauty Tips. Use combo box on the right to display article list on selected category. </p>
                        <p>To add new article, simply click Add New button on the left</p>
                        <p>To change how article ordered, click and drag article name up or down, and then release it on desired position.</p>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                            <div class="portlet light bordered">
                                <div class="portlet-title">
                                    <div class="caption font-dark">
                                        <i class="fa fa-laptop"></i>
                                        <span class="caption-subject bold uppercase"> Article Data</span>
                                    </div>                                    
                                </div>
                                <div class="portlet-body">
                                    <div class="table-toolbar">
                                        <div class="row">
                                            <div class="col-md-12">
                                                    <div class="col-md-6">
                                                        <a class="btn green sbold" data-toggle="modal" href="#addslideshow"> Add New <i class="fa fa-plus"></i> </a>
                                                    </div>
                                                    <div class="col-md-6">

                                                     <select class="pull-right bs-select form-control input-small" id="selfilter" data-style="btn-primary">
                                                        <?php foreach ($category as $key) { ?>
                                                            <option <?php if($this->input->get('cat') == $key->id) { echo 'selected="selected"'; } ?> value="<?php echo $key->id; ?>"><?php echo $key->category; ?></option>
                                                        <?php } ?>
                                                        </select>
                                                    </div>
                                            </div>
                                           
                                        </div>
                                    </div>
                                    <table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample1">
                                        <thead>
                                            <tr>
                                                <th>
                                                    Order <i class="fa fa-arrows-v"></i>
                                                </th>
                                                <th> Title </th>
                                                <th> Short Desc </th>
                                                <th> Visible </th>
                                                <th> Viewed </th>
                                                <th> Actions </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php if(@$article) {
                                                $k =1;
                                            foreach ($article as $key) { ?>
                                            <tr class="odd gradeX">
                                                <td>
                                                    <?php echo $k; $k++; ?>
                                                </td>
                                                <td>
                                                    <?php echo $key->title; ?>
                                                </td>
                                                <td>
                                                    <?php echo $key->short_desc; ?>        
                                                </td>
                                                <td>
                                                    <?php if($key->visibility == 1) { ?>
                                                    <span class="label label-sm label-success">Visible</span>
                                                    <?php } else {  ?>
                                                    <span class="label label-sm label-danger">Not Visible</span>
                                                    <?php } ?>
                                                </td>
                                                 <td>
                                                    <?php echo $key->viewed; ?>        
                                                </td>
                                                <td>
                                                    <div class="btn-group">
                                                        <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Actions
                                                            <i class="fa fa-angle-down"></i>
                                                        </button>
                                                        <ul class="dropdown-menu pull-left" role="menu">
                                                            <li>
                                                                <a href="javascript:;" dataid="<?php echo $key->id; ?>" class="editdata ">
                                                                    <i class="icon-docs"></i> Edit Data </a>
                                                            </li>
                                                            <li>
                                                                <a href="javascript:;" class="mt-sweetalert delbtn" dataid="<?php echo $key->id; ?>" >
                                                                    <i class="icon-tag"></i> Delete Data </a>
                                                            </li>
                                                            
                                                        </ul>
                                                    </div>
                                                </td>
                                            </tr>
                                                 
                                            <?php } 
                                             } else {  ?>
                                             <tr class="odd gradeX">
                                                <td colspan="5">
                                                    <span class="text-center">No Article Data Found</span>
                                                </td>
                                            </tr>
                                             <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- END EXAMPLE TABLE PORTLET-->
                        </div>
                    </div>
                    
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->
           
        </div>
        <!-- END CONTAINER -->