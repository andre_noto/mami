<div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            <h4 class="modal-title">Edit Article</h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN SAMPLE FORM PORTLET-->
                    <div class="portlet-body form">
                           <div class="form-body">
                                <?php if(@$errorvalidation != '' || @$errorlabel) { ?>
                                <div class="note note-warning">
                                    <h4 class="block">Warning!</h4>
                                    <?php echo @$errorvalidation; ?>
                                    <?php echo @$errorlabel; ?>
                                </div>
                                <?php  } ?>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Title</label>
                                    <div class="col-md-9">
                                        <input type="text" required name="title" class="form-control" value="<?php echo $article->title; ?>" ></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Short Desc</label>
                                    <div class="col-md-9">
                                        <textarea required class="form-control" name="short_desc" rows="3" ><?php echo $article->short_desc; ?></textarea>
                                         
                                    </div>
                                </div>  

                                <div class="form-group">
                    <label class="col-md-3 control-label">Category</label>
                    <div class="col-md-9">
                        <select disabled="disabled"  name="category_id" class="form-control">
                            <?php foreach ($category as $key) { ?>
                                <option value="<?php echo $key->id; ?>" <?php if($article->category_id == $key->id) { echo "selected='selected'"; } ?>><?php echo $key->category; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-md-3">Content</label>
                    <div class="col-md-9">
                        <textarea required name="content" class="wysihtml5 form-control" rows="10"><?php echo $article->content; ?></textarea>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 control-label">Meta Tags</label>
                    <div class="col-md-9">
                        <textarea class="form-control" name="meta" rows="3" ><?php echo $article->meta; ?></textarea>
                         
                    </div>
                </div>  

                <?php if(count($images) >0) { ?>
                 <div class="form-group">
                    <label for="exampleInputFile" class="col-md-3 control-label">Current Images</label>
                    <div class="col-md-9">
                        <?php foreach ($images as $key) { ?>
                            <div id="imgcontainer<?php echo $key->id; ?>" >
                                <img src="<?php echo base_url('images/articles/'.$key->thumbimg); ?>" class="img-thumbnail" />
                                <a href="#" class="btn delimg" dataid="<?php echo $key->id; ?>" ><i class="fa fa-times"></i></a>
                            </div>
                            <br/>
                        <?php } ?>
                    </div>
                </div>
                <?php } ?>

                                
                 <div class="form-group">
                    <label for="exampleInputFile" class="col-md-3 control-label">Image Upload</label>
                    <div class="col-md-9">
                        <input type="file" id="imageupload1" name="imageupload1">
                        <p class="help-block">(Required. Recommendation size: 1920 x 800 pixels.)</p>
                        <input type="hidden" name="totalimgedit" id="totalimgedit" value="1"/>

                        <div id="containeredit">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="exampleInputFile" class="col-md-3 control-label"></label>
                    <div class="col-md-9">
                        <a id="btnaddmoreedit" class=" btn green sbold " href="#">Add More</a>
                    </div>
                </div>
                                
                                
                <div class="form-group">
                    <label class="col-md-3 control-label">Visibility</label>
                    <div class="col-md-9">
                        <div class="mt-radio-inline">
                            <label class="mt-radio">
                                <input type="radio" name="visibility" value="1" <?php if($article->visibility == 1 ) { echo 'checked'; } ?>> True
                                <span></span>
                            </label>
                            <label class="mt-radio">
                                <input type="radio" name="visibility" value="0" <?php if($article->visibility == 0 ) { echo 'checked'; } ?>> False
                                <span></span>
                            </label>
                            
                        </div>
                    </div>
                </div>          
                            </div>                                          
                    </div>                            
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
            <input type="hidden" name="hiddenid" value="<?php echo $article->id; ?>" />
            
            <button type="submit" name="btnedit" value="submit" class="btn green">Submit</button>
        </div>