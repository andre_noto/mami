(function (lib, img, cjs, ss, an) {

var p; // shortcut to reference prototypes
lib.webFontTxtInst = {}; 
var loadedTypekitCount = 0;
var loadedGoogleCount = 0;
var gFontsUpdateCacheList = [];
var tFontsUpdateCacheList = [];
lib.ssMetadata = [];



lib.updateListCache = function (cacheList) {		
	for(var i = 0; i < cacheList.length; i++) {		
		if(cacheList[i].cacheCanvas)		
			cacheList[i].updateCache();		
	}		
};		

lib.addElementsToCache = function (textInst, cacheList) {		
	var cur = textInst;		
	while(cur != exportRoot) {		
		if(cacheList.indexOf(cur) != -1)		
			break;		
		cur = cur.parent;		
	}		
	if(cur != exportRoot) {		
		var cur2 = textInst;		
		var index = cacheList.indexOf(cur);		
		while(cur2 != cur) {		
			cacheList.splice(index, 0, cur2);		
			cur2 = cur2.parent;		
			index++;		
		}		
	}		
	else {		
		cur = textInst;		
		while(cur != exportRoot) {		
			cacheList.push(cur);		
			cur = cur.parent;		
		}		
	}		
};		

lib.gfontAvailable = function(family, totalGoogleCount) {		
	lib.properties.webfonts[family] = true;		
	var txtInst = lib.webFontTxtInst && lib.webFontTxtInst[family] || [];		
	for(var f = 0; f < txtInst.length; ++f)		
		lib.addElementsToCache(txtInst[f], gFontsUpdateCacheList);		

	loadedGoogleCount++;		
	if(loadedGoogleCount == totalGoogleCount) {		
		lib.updateListCache(gFontsUpdateCacheList);		
	}		
};		

lib.tfontAvailable = function(family, totalTypekitCount) {		
	lib.properties.webfonts[family] = true;		
	var txtInst = lib.webFontTxtInst && lib.webFontTxtInst[family] || [];		
	for(var f = 0; f < txtInst.length; ++f)		
		lib.addElementsToCache(txtInst[f], tFontsUpdateCacheList);		

	loadedTypekitCount++;		
	if(loadedTypekitCount == totalTypekitCount) {		
		lib.updateListCache(tFontsUpdateCacheList);		
	}		
};
// symbols:



(lib.earth = function() {
	this.initialize(img.earth);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,626,640);// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.yellow = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_3
	this.shape = new cjs.Shape();
	this.shape.graphics.lf(["#FFFFFF","rgba(255,255,255,0)"],[0,1],-6.9,-50,-6.9,5.5).s().p("AjxDEQhlhRABhzQgBhyBlhRQBkhRCNAAQCOAABkBRQBlBRAAByQAABzhlBRQhkBSiOAAQiNAAhkhSg");
	this.shape.setTransform(41.2,30.8);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	// Layer_2
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgRAUQgGgHAAgNQAAgOAIgGQAGgGAJAAQALAAAGAHQAHAHAAAMQAAAKgDAFQgDAGgGADQgFADgHAAQgKAAgHgHgAgKgOQgEAFAAAJQAAAKAEAFQAEAFAGAAQAGAAAFgFQAEgFAAgKQAAgJgEgFQgFgFgGAAQgGAAgEAFg");
	this.shape_1.setTransform(48.5,44.2);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgNAaIAAgyIAIAAIAAAIQADgGACgCQACgBADAAQAFAAAEADIgDAIQgDgCgDAAQgDAAgCACQgCABgBADIgBAKIAAAag");
	this.shape_2.setTransform(44.5,44.1);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgPAhQgFgEAAgJIAIACQABADADACQADADAFAAQAFAAAEgDQADgCABgFIABgLQgGAHgIAAQgKAAgGgIQgGgHAAgKQAAgIADgGQACgGAFgEQAGgDAGAAQAJAAAGAIIAAgHIAIAAIAAAsQAAAMgDAFQgCAFgGADQgFADgHAAQgJAAgGgEgAgIgYQgFAFAAAJQAAAKAFAEQADAEAGAAQAFAAAEgEQAEgEAAgKQAAgJgEgFQgEgFgFAAQgGAAgDAFg");
	this.shape_3.setTransform(39.5,45.2);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AAXAkIgJgWIgcAAIgIAWIgKAAIAbhHIAJAAIAdBHgAgEgOIgIAUIAYAAIgIgSIgFgPQgBAHgCAGg");
	this.shape_4.setTransform(33.6,43.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1}]}).wait(1));

	// Layer_1
	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.rf(["#00FF00","#00FF00","#006600","#00FF00","#00FF00","#BDC3C7","#666666"],[0,0.416,0.604,0.773,1,1,1],-7.9,-15.9,0,-7.9,-15.9,86.1).s().p("AkdEeQh3h2AAioQAAinB3h2QB2h3CnAAQCoAAB2B3QB3B2AACnQAACoh3B2Qh2B3ioAAQinAAh2h3g");
	this.shape_5.setTransform(40.5,40.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_5).wait(1));

}).prototype = getMCSymbolPrototype(lib.yellow, new cjs.Rectangle(0,0,81,81), null);


(lib.white = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_3
	this.shape = new cjs.Shape();
	this.shape.graphics.lf(["#FFFFFF","rgba(255,255,255,0)"],[0,1],-6.9,-50,-6.9,5.5).s().p("AjxDEQhlhRABhzQgBhyBlhRQBkhRCNAAQCOAABkBRQBlBRAAByQAABzhlBRQhkBSiOAAQiNAAhkhSg");
	this.shape.setTransform(41.2,30.8);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	// Layer_2
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgOAXQgFgEgCgJIAJgBQABAGAEACQACADAGAAQAGAAADgCQADgDAAgEQgBgDgCgBIgJgDIgNgEQgDgBgCgDQgCgDAAgEQAAgDACgDQABgDADgDIAGgCIAHgBQAFAAAFACQAFABABAEQADADABAEIgJACQAAgFgEgBQgCgDgFAAQgFAAgDACQgDACAAADIABADIAEACIAHACIANAFQAEAAACADQABADABAFQgBAEgCAEQgDAEgEACQgFACgGAAQgKAAgFgEg");
	this.shape_1.setTransform(66.3,45.3);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgQAUQgHgHAAgMQAAgMAHgIQAHgHAJAAQALAAAGAHQAHAHAAAMIAAACIgmAAQAAAJAFAFQAEAEAGAAQAFAAADgCQAEgDACgFIAJAAQgDAJgGAEQgFAEgJAAQgLAAgGgHgAgJgPQgEAEAAAHIAcAAQgBgHgCgDQgFgFgHAAQgEAAgFAEg");
	this.shape_2.setTransform(61,45.3);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgDAkIAAgzIAHAAIAAAzgAgDgZIAAgKIAHAAIAAAKg");
	this.shape_3.setTransform(57.2,44.3);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AgNAaIAAgzIAIAAIAAAIQADgFACgCQACgCADAAQAFAAAEAEIgDAIQgDgDgEAAQgCAAgCADQgCABgBADIgBAKIAAAag");
	this.shape_4.setTransform(54.8,45.3);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AgRAUQgGgHAAgNQAAgNAIgIQAGgFAJAAQALAAAGAHQAHAHAAAMQAAAKgDAFQgDAGgGADQgFADgHAAQgKAAgHgHgAgKgOQgEAFAAAJQAAAKAEAFQAEAFAGAAQAGAAAFgFQAEgFAAgKQAAgJgEgFQgFgFgGAAQgGAAgEAFg");
	this.shape_5.setTransform(49.9,45.3);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AgOAXQgFgEgBgJIAJgBQAAAGADACQAEADAEAAQAHAAACgCQADgDAAgEQAAgDgCgBIgJgDIgMgEQgEgBgCgDQgCgDAAgEQAAgDABgDQACgDADgDIAFgCIAIgBQAFAAAFACQAFABACAEQACADAAAEIgIACQAAgFgDgBQgDgDgFAAQgGAAgCACQgDACABADIABADIADACIAGACIAOAFQADAAACADQACADAAAFQAAAEgCAEQgDAEgFACQgEACgHAAQgJAAgFgEg");
	this.shape_6.setTransform(44.6,45.3);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AgQAUQgHgHAAgMQAAgMAHgIQAHgHAKAAQAKAAAHAHQAGAHAAAMIAAACIgmAAQABAJAEAFQAEAEAGAAQAFAAADgCQADgDACgFIAJAAQgBAJgHAEQgFAEgJAAQgLAAgGgHgAgIgPQgFAEgBAHIAcAAQAAgHgDgDQgEgFgGAAQgGAAgDAEg");
	this.shape_7.setTransform(39.4,45.3);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AgPAUQgHgHABgMQAAgIACgHQADgFAGgEQAFgDAGAAQAIAAAGAEQAFAFABAIIgIABQgBgFgDgDQgDgDgFAAQgFAAgFAFQgDAFAAAJQAAALADAEQAEAFAGAAQAFAAADgDQAEgDABgHIAJABQgCAJgFAFQgHAFgIAAQgKAAgGgHg");
	this.shape_8.setTransform(34.3,45.3);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AgPAUQgGgHgBgMQABgIACgHQADgFAFgEQAHgDAFAAQAIAAAGAEQAFAFACAIIgJABQgBgFgDgDQgDgDgEAAQgGAAgFAFQgEAFAAAJQAAALAEAEQAFAFAFAAQAFAAAEgDQADgDABgHIAJABQgCAJgGAFQgFAFgJAAQgJAAgHgHg");
	this.shape_9.setTransform(29.3,45.3);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AAXAkIgJgWIgcAAIgIAWIgKAAIAbhHIAJAAIAdBHgAgEgOIgIAUIAYAAIgIgSIgFgPQAAAHgDAGg");
	this.shape_10.setTransform(23.3,44.3);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AANAbQgFAFgEADQgEACgGAAQgLAAgHgHQgFgHAAgHQAAgHAEgFQAEgEAJgEQgFgGgCgDIgBgHQAAgHAFgEQAFgFAHAAQAHAAAEAEQAFAFAAAGQAAAKgNAIIANAPIADgKIAJACQgCAJgEAGQAFAHAGAEIgGAHQgGgDgFgHgAgRAIQgCAEgBADQAAAFAEAFQADAEAHAAQAEAAADgCQAEgDACgEIgOgUQgIAFgCADgAgJgbQgCADAAADIAAAEIADADIAEAFQAFgDACgDQABgCAAgEQAAgDgCgCQgCgDgDAAQgEAAgCACg");
	this.shape_11.setTransform(14,44.3);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AANAbIAAgfIgCgIIgDgEQgDgCgEAAQgEAAgEADQgEAEAAAKIAAAcIgJAAIAAgzIAIAAIAAAHQAGgIAKAAQAEgBAEACQAEACACACQACADAAAEIABAIIAAAgg");
	this.shape_12.setTransform(54.6,37.1);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("AgRAUQgGgHAAgNQAAgOAIgGQAGgGAJAAQALAAAGAHQAHAHAAAMQAAAKgDAFQgDAGgGADQgFADgHAAQgKAAgHgHgAgKgOQgEAFAAAJQAAAKAEAFQAEAFAGAAQAGAAAFgFQAEgFAAgKQAAgJgEgFQgFgFgGAAQgGAAgEAFg");
	this.shape_13.setTransform(49.1,37.2);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FFFFFF").s().p("AgEAkIAAgzIAIAAIAAAzgAgEgZIAAgKIAIAAIAAAKg");
	this.shape_14.setTransform(45.2,36.2);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FFFFFF").s().p("AAMAkIAAghQAAgGgDgDQgCgDgGAAQgDAAgDACQgDACgCAEQgBACAAAGIAAAdIgJAAIAAhHIAJAAIAAAaQAGgHAIAAQAGAAAEACQAEACACAEQACAEAAAHIAAAhg");
	this.shape_15.setTransform(41.3,36.2);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FFFFFF").s().p("AgNAXQgGgEgCgJIAJgBQABAFAEADQACADAGAAQAGAAADgDQADgCAAgDQgBgEgCgBIgJgDIgNgEQgDgBgCgDQgCgDAAgEQAAgEABgDQACgCADgCIAGgDIAHgBQAFAAAFABQAEADACACQADAEABAFIgJAAQAAgEgEgCQgCgCgFAAQgFAAgDACQgCACgBADIABADIAEACIAHADIANADQAEABACADQACADAAAFQgBAEgCAEQgDAEgEACQgFACgGAAQgKAAgEgEg");
	this.shape_16.setTransform(36,37.2);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#FFFFFF").s().p("AgTAXQgEgEAAgGQAAgFACgCQABgEADgCIAHgCIAIgBQAJgBAFgCIAAgCQAAgGgCgCQgDgDgHAAQgFAAgEACQgDACgBAGIgIgBQABgGACgDQADgEAFgBQAFgCAGAAQAGAAAFABQAEACACADQACACAAAEIABAHIAAAMIABAPIACAGIgJAAIgCgHQgFAFgEABQgEACgFAAQgJAAgFgEgAgBAEIgIABIgDADIgBAEQAAADACADQADADAFAAQAEAAAEgDQAEgCACgEIABgJIAAgDQgEACgJACg");
	this.shape_17.setTransform(30.8,37.2);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#FFFFFF").s().p("AgXAkIAAhHIAvAAIAAAJIgmAAIAAAWIAhAAIAAAHIghAAIAAAhg");
	this.shape_18.setTransform(25.1,36.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1}]}).wait(1));

	// Layer_1
	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.rf(["#9966CC","#9966CC","#663399","#9966CC","#9966CC","#BDC3C7","#666666"],[0,0.416,0.604,0.773,1,1,1],-7.9,-15.9,0,-7.9,-15.9,86.1).s().p("AkdEeQh3h2AAioQAAinB3h2QB2h3CnAAQCoAAB2B3QB3B2AACnQAACoh3B2Qh2B3ioAAQinAAh2h3g");
	this.shape_19.setTransform(40.5,40.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_19).wait(1));

}).prototype = getMCSymbolPrototype(lib.white, new cjs.Rectangle(0,0,81,81), null);


(lib.red = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_3
	this.shape = new cjs.Shape();
	this.shape.graphics.lf(["#FFFFFF","rgba(255,255,255,0)"],[0,1],-6.9,-50,-6.9,5.5).s().p("AjxDEQhlhRABhzQgBhyBlhRQBkhRCNAAQCOAABkBRQBlBRAAByQAABzhlBRQhkBSiOAAQiNAAhkhSg");
	this.shape.setTransform(41.2,30.8);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	// Layer_2
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AAMAbIAAgfIgBgIIgDgEQgDgCgEAAQgEAAgEADQgEAEAAAKIAAAcIgJAAIAAg0IAIAAIAAAIQAFgIAKAAQAFgBAEACQAEACACADQACADAAADIABAIIAAAgg");
	this.shape_1.setTransform(60.2,42.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgRAUQgGgHAAgNQAAgNAIgIQAGgFAJAAQALAAAGAHQAHAHAAAMQAAAJgDAGQgDAGgGADQgFADgHAAQgKAAgHgHgAgKgOQgEAFAAAJQAAAKAEAFQAEAFAGAAQAGAAAFgFQAEgFAAgKQAAgJgEgFQgFgFgGAAQgGAAgEAFg");
	this.shape_2.setTransform(54.6,42.7);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgDAkIAAgzIAIAAIAAAzgAgDgZIAAgKIAIAAIAAAKg");
	this.shape_3.setTransform(50.8,41.7);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AgBAiQgCgCgBgCQgCgDAAgHIAAgdIgFAAIAAgHIAFAAIAAgNIAIgFIAAASIAJAAIAAAHIgJAAIAAAdIABAFIACACIACAAIAEAAIACAIIgHAAQgFAAgCgBg");
	this.shape_4.setTransform(48.3,41.8);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AgSAXQgFgEAAgGQAAgFACgCQACgEACgCIAHgCIAIgBQAJgBAFgCIAAgCQAAgGgCgCQgEgDgGAAQgGAAgCACQgDADgCAFIgJgBQACgGADgDQACgEAFgBQAGgCAFAAQAGAAAFACQAEABACACQACADAAAEIABAIIAAALIAAAPIADAGIgJAAIgCgGQgFAEgFABQgDACgFAAQgJAAgEgEgAgBAEIgIABIgEADIgBAEQAAADADADQADADAFAAQAEAAAEgDQAEgCACgEIABgJIAAgDQgFACgIACg");
	this.shape_5.setTransform(44.1,42.7);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AgPAUQgGgGAAgOQgBgHADgGQADgHAFgDQAGgDAGAAQAJAAAFAFQAFAEACAHIgJABQgBgFgDgCQgDgDgFAAQgFAAgFAFQgDAFAAAJQAAAKADAFQAFAFAFAAQAFAAADgDQAEgDABgHIAJABQgCAJgGAFQgFAFgJAAQgKAAgGgHg");
	this.shape_6.setTransform(39,42.7);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AgLAZQgEgCgCgDQgCgCgBgEIAAgIIAAggIAIAAIAAAcIABAJQABAEADACQACACAFAAQADAAADgCQADgCACgEQABgDAAgHIAAgbIAJAAIAAA0IgIAAIAAgIQgGAJgKAAQgEgBgEgBg");
	this.shape_7.setTransform(33.5,42.7);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AgLAhQgFgDgDgHQgCgGAAgIQgBgHADgGQACgGAGgDQAFgEAGAAQAEAAADACQAFACACADIAAgZIAJAAIAABGIgJAAIAAgGQgEAHgKAAQgGAAgFgDgAgJgFQgDAFAAAJQAAAKADAFQAFAFAEAAQAGAAAFgEQAEgFgBgKQABgKgEgFQgFgFgGAAQgFAAgEAFg");
	this.shape_8.setTransform(27.8,41.7);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AgaAkIAAhHIAzAAIAAAJIgpAAIAAAWIAmAAIAAAHIgmAAIAAAYIArAAIAAAJg");
	this.shape_9.setTransform(22,41.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1}]}).wait(1));

	// Layer_1
	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.rf(["#FF9900","#FF9900","#996600","#FF9900","#FF9900","#BDC3C7","#666666"],[0,0.416,0.604,0.773,1,1,1],-7.9,-15.9,0,-7.9,-15.9,86.1).s().p("AkdEeQh3h2AAioQAAinB3h2QB2h3CnAAQCoAAB2B3QB3B2AACnQAACoh3B2Qh2B3ioAAQinAAh2h3g");
	this.shape_10.setTransform(40.5,40.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_10).wait(1));

}).prototype = getMCSymbolPrototype(lib.red, new cjs.Rectangle(0,0,81,81), null);


(lib.purple = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_3
	this.shape = new cjs.Shape();
	this.shape.graphics.lf(["#FFFFFF","rgba(255,255,255,0)"],[0,1],-6.9,-50,-6.9,5.5).s().p("AjyDEQhjhRgBhzQABhyBjhSQBlhRCNAAQCOAABkBRQBkBSAAByQAABzhkBRQhkBRiOABQiNgBhlhRg");
	this.shape.setTransform(40.7,30.6);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	// Layer_2
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AANAbIAAgfIgBgIIgEgEQgDgCgEAAQgEAAgEADQgEAEAAAKIAAAcIgJAAIAAgzIAIAAIAAAHQAGgIAKAAQAEgBAEACQAEACACACQACADABAEIAAAIIAAAgg");
	this.shape_1.setTransform(70,41.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgRAUQgGgHAAgNQAAgOAIgGQAGgGAJAAQALAAAGAHQAHAHAAAMQAAAKgDAFQgDAGgGADQgFADgHAAQgKAAgHgHgAgKgOQgEAFAAAJQAAAKAEAFQAEAFAGAAQAGAAAFgFQAEgFAAgKQAAgJgEgFQgFgFgGAAQgGAAgEAFg");
	this.shape_2.setTransform(64.5,41.7);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgEAkIAAgzIAIAAIAAAzgAgEgZIAAgKIAIAAIAAAKg");
	this.shape_3.setTransform(60.6,40.7);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AgBAiQgCgCgBgCQgCgDABgHIAAgdIgHAAIAAgHIAHAAIAAgNIAIgFIAAASIAJAAIAAAHIgJAAIAAAdIAAAFIABACIADAAIAFAAIAAAIIgGAAQgFAAgCgBg");
	this.shape_4.setTransform(58.2,40.8);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AgTAXQgEgEAAgGQAAgFACgCQACgEACgCIAGgCIAIgBQAKgBAFgCIAAgCQAAgGgCgCQgDgDgHAAQgFAAgEACQgDACgBAGIgIgBQABgGACgDQADgEAFgBQAGgCAFAAQAGAAAFABQAEACACADQACACABAEIAAAHIAAAMIABAPIACAGIgKAAIgBgHQgFAFgEABQgEACgFAAQgIAAgGgEgAgBAEIgIABIgDADIgBAEQgBADADADQADADAFAAQAEAAAEgDQAEgCACgEIABgJIAAgDQgFACgIACg");
	this.shape_5.setTransform(53.9,41.7);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AAAAiQgDgCgBgCQgBgDgBgHIAAgdIgGAAIAAgHIAGAAIAAgNIAIgFIAAASIAJAAIAAAHIgJAAIAAAdIABAFIABACIAEAAIADAAIABAIIgGAAQgFAAgBgBg");
	this.shape_6.setTransform(49.8,40.8);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AgNAbIAAgzIAIAAIAAAIQADgGACgCQACgCADABQAFAAAEACIgDAIQgDgBgDAAQgDAAgCABQgCACgBADIgBAKIAAAbg");
	this.shape_7.setTransform(47.1,41.6);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AgRAUQgGgHAAgNQAAgOAIgGQAGgGAJAAQALAAAGAHQAHAHAAAMQAAAKgDAFQgDAGgGADQgFADgHAAQgKAAgHgHgAgKgOQgEAFAAAJQAAAKAEAFQAEAFAGAAQAGAAAFgFQAEgFAAgKQAAgJgEgFQgFgFgGAAQgGAAgEAFg");
	this.shape_8.setTransform(42.2,41.7);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AgVAlIAAhHIAIAAIAAAHQADgFADgCQADgBAEAAQAHgBAGAEQAEAEADAFQACAHABAHQgBAIgCAFQgDAHgFADQgGAEgGAAQgDgBgEgBQgDgCgCgDIAAAagAgJgYQgFAGABAKQAAAJADAEQAFAGAFgBQAGABADgGQAFgEAAgKQAAgKgFgFQgEgEgFAAQgFAAgEAEg");
	this.shape_9.setTransform(36.8,42.6);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AgOAXQgFgEgCgJIAJgBQABAFAEADQACADAGAAQAGAAADgDQACgCAAgDQABgEgDgBIgJgDIgNgEQgDgBgCgDQgCgDAAgEQAAgEACgDQABgCADgCIAGgDIAHgBQAFAAAFABQAFADACACQACAEAAAFIgIAAQAAgEgDgCQgDgCgFAAQgFAAgDACQgCACAAADIABADIADACIAHADIANADQADABACADQACADAAAFQABAEgDAEQgDAEgFACQgEACgGAAQgJAAgGgEg");
	this.shape_10.setTransform(31.4,41.7);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AANAbIAAgfIgBgIIgEgEQgDgCgDAAQgFAAgEADQgEAEAAAKIAAAcIgJAAIAAgzIAIAAIAAAHQAFgIALAAQAEgBAEACQAEACACACQACADABAEIAAAIIAAAgg");
	this.shape_11.setTransform(26.1,41.6);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AgSAXQgFgEAAgGQAAgFACgCQACgEADgCIAGgCIAHgBQAKgBAFgCIAAgCQAAgGgCgCQgEgDgGAAQgGAAgCACQgDACgCAGIgJgBQACgGADgDQACgEAFgBQAGgCAFAAQAGAAAFABQAEACACADQACACAAAEIABAHIAAAMIAAAPIADAGIgKAAIgBgHQgFAFgFABQgDACgFAAQgJAAgEgEgAgBAEIgIABIgEADIgBAEQABADACADQADADAFAAQAEAAAEgDQAEgCACgEIABgJIAAgDQgFACgIACg");
	this.shape_12.setTransform(20.6,41.7);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("AgNAbIAAgzIAIAAIAAAIQADgGACgCQACgCADABQAFAAAEACIgDAIQgDgBgEAAQgCAAgCABQgCACgBADIgBAKIAAAbg");
	this.shape_13.setTransform(16.5,41.6);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FFFFFF").s().p("AgEAkIAAg+IgXAAIAAgJIA3AAIAAAJIgXAAIAAA+g");
	this.shape_14.setTransform(11.8,40.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1}]}).wait(1));

	// Layer_1
	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.rf(["#666666","#666666","#666666","#666666","#333333","#BDC3C7","#666666"],[0,0.416,0.604,0.773,1,1,1],-11.7,-24.5,0,-11.7,-24.5,72.9).s().p("AkdEeQh3h2AAioQAAinB3h2QB2h3CnAAQCoAAB2B3QB3B2AACnQAACoh3B2Qh2B3ioAAQinAAh2h3g");
	this.shape_15.setTransform(40.5,40.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_15).wait(1));

}).prototype = getMCSymbolPrototype(lib.purple, new cjs.Rectangle(0,0,81,81), null);


(lib.green = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_3
	this.shape = new cjs.Shape();
	this.shape.graphics.lf(["#FFFFFF","rgba(255,255,255,0)"],[0,1],-6.9,-50,-6.9,5.5).s().p("AjxDEQhlhRABhzQgBhyBlhRQBkhRCNAAQCOAABkBRQBlBRAAByQAABzhlBRQhkBSiOAAQiNAAhkhSg");
	this.shape.setTransform(41.2,30.8);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	// Layer_2
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgQAgQgHgEgEgJQgEgJAAgKQAAgKAFgJQAEgIAIgEQAIgFAIAAQALAAAHAGQAHAFADAKIgJACQgDgHgEgEQgFgDgHgBQgHAAgGAFQgGADgCAHQgCAHAAAGQAAAJADAGQACAIAGACQAGAEAFAAQAIAAAGgFQAFgEACgJIAKADQgDALgIAGQgIAGgLAAQgLAAgHgFg");
	this.shape_1.setTransform(46.4,45.8);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgVAkIAAhHIAJAAIAAA+IAiAAIAAAJg");
	this.shape_2.setTransform(40.2,45.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgaAkIAAhHIAaAAIAKABQAGABAEACQADADACAEQACAFABAFQAAAJgHAFQgFAHgPAAIgSAAIAAAdgAgRgBIASAAQAJAAADgDQAFgEAAgGQAAgEgCgEQgDgDgDgBIgJAAIgSAAg");
	this.shape_3.setTransform(34.1,45.8);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AAMAaIAAgeIAAgIIgEgFQgDgBgDAAQgFAAgEAEQgEADAAAKIAAAbIgJAAIAAgyIAIAAIAAAHQAFgJAKAAQAFAAAEACQAEACACACQACADAAAEIABAJIAAAeg");
	this.shape_4.setTransform(62.8,38.6);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AgRAUQgGgHAAgNQAAgOAIgGQAGgGAJAAQALAAAGAHQAHAHAAAMQAAAKgDAFQgDAGgGADQgFADgHAAQgKAAgHgHgAgKgOQgEAFAAAJQAAAKAEAFQAEAFAGAAQAGAAAFgFQAEgFAAgKQAAgJgEgFQgFgFgGAAQgGAAgEAFg");
	this.shape_5.setTransform(57.2,38.7);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AgDAkIAAgzIAIAAIAAAzgAgDgZIAAgKIAIAAIAAAKg");
	this.shape_6.setTransform(53.4,37.7);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AAAAiQgDgCgBgCQgCgDAAgHIAAgdIgFAAIAAgHIAFAAIAAgNIAIgFIAAASIAJAAIAAAHIgJAAIAAAdIABAFIACACIADAAIADAAIABAIIgGAAQgFAAgBgBg");
	this.shape_7.setTransform(50.9,37.8);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AgSAXQgFgEAAgHQAAgDACgDQACgEADgBIAGgCIAHgCQAKgBAFgCIAAgCQAAgFgCgDQgEgDgGAAQgGAAgCACQgDADgCAFIgJgBQACgGADgDQACgDAFgCQAGgCAFAAQAGAAAFABQAEACACADQACACAAADIABAIIAAALIAAAQIADAGIgKAAIgBgHQgFAFgFACQgDABgFAAQgJAAgEgEgAgBADIgIACIgEADIgBAEQABADACADQADADAFgBQAEABAEgDQAEgCACgEIABgJIAAgDQgFACgIABg");
	this.shape_8.setTransform(46.7,38.7);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AAaAaIAAgfIAAgHQgBgDgDgCQgCgBgDAAQgFAAgEAEQgEADAAAIIAAAdIgHAAIAAggQAAgGgCgDQgDgDgFAAQgDAAgDACQgEACgBAEQgBAEAAAGIAAAaIgJAAIAAgyIAIAAIAAAHQACgEAEgCQAEgDAFAAQAGAAAEADQADACABAEQAGgIAKgBQAIAAAEAFQAEAEAAAJIAAAig");
	this.shape_9.setTransform(39.7,38.6);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AgRAUQgGgHAAgNQAAgOAIgGQAGgGAJAAQALAAAGAHQAHAHAAAMQAAAKgDAFQgDAGgGADQgFADgHAAQgKAAgHgHgAgKgOQgEAFAAAJQAAAKAEAFQAEAFAGAAQAGAAAFgFQAEgFAAgKQAAgJgEgFQgFgFgGAAQgGAAgEAFg");
	this.shape_10.setTransform(32.8,38.7);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AAAAiQgDgCgBgCQgBgDgBgHIAAgdIgGAAIAAgHIAGAAIAAgNIAIgFIAAASIAJAAIAAAHIgJAAIAAAdIABAFIABACIAEAAIADAAIABAIIgGAAQgFAAgBgBg");
	this.shape_11.setTransform(28.7,37.8);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AgLAZQgEgCgCgCQgCgDgBgEIAAgIIAAgfIAIAAIAAAcIABAJQABADADACQACACAEAAQAEAAADgCQAEgCABgDQABgEAAgGIAAgbIAJAAIAAAyIgIAAIAAgHQgGAJgKgBQgEAAgEgBg");
	this.shape_12.setTransform(24.4,38.7);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("AAXAkIgJgWIgcAAIgIAWIgKAAIAbhHIAJAAIAdBHgAgEgOIgIAUIAYAAIgIgSIgFgPQAAAHgDAGg");
	this.shape_13.setTransform(18.4,37.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1}]}).wait(1));

	// Layer_1
	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.rf(["#3498DB","#3498DB","#0066CC","#3498DB","#3498DB","#BDC3C7","#666666"],[0,0.416,0.604,0.773,1,1,1],-7.9,-15.9,0,-7.9,-15.9,86.1).s().p("AkdEeQh3h2AAioQAAinB3h2QB2h3CnAAQCoAAB2B3QB3B2AACnQAACoh3B2Qh2B3ioAAQinAAh2h3g");
	this.shape_14.setTransform(40.5,40.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_14).wait(1));

}).prototype = getMCSymbolPrototype(lib.green, new cjs.Rectangle(0,0,81,81), null);


(lib.gray = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_3
	this.shape = new cjs.Shape();
	this.shape.graphics.lf(["#FFFFFF","rgba(255,255,255,0)"],[0,1],-6.9,-50,-6.9,5.5).s().p("AjxDEQhlhRABhzQgBhyBlhRQBkhRCNAAQCOAABkBRQBlBRAAByQAABzhlBRQhkBSiOAAQiNAAhkhSg");
	this.shape.setTransform(41.2,30.8);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	// Layer_2
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgQAUQgHgHAAgNQAAgLAHgIQAHgHAJAAQALAAAGAHQAHAHAAAMIAAACIgmAAQAAAJAFAFQAEAEAGAAQAFAAADgCQAEgDACgFIAJAAQgDAJgFAEQgGAEgJAAQgLAAgGgHgAgJgPQgEAFAAAGIAcAAQgBgGgCgEQgFgFgHAAQgEAAgFAEg");
	this.shape_1.setTransform(62.4,46.3);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgNAaIAAgyIAIAAIAAAHQADgFACgCQACgCADAAQAFABAEADIgDAIQgDgCgDgBQgDABgCACQgCABgBADIgBAKIAAAag");
	this.shape_2.setTransform(58.3,46.3);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgLAZQgEgCgCgCQgCgDgBgEIAAgIIAAgfIAJAAIAAAcIAAAJQABADACACQAEACADAAQAEAAADgCQADgCACgDQABgEAAgGIAAgbIAJAAIAAAyIgIAAIAAgHQgGAJgKgBQgEAAgEgBg");
	this.shape_3.setTransform(53.5,46.4);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AAAAiQgDgCgBgCQgBgDgBgHIAAgdIgGAAIAAgHIAGAAIAAgNIAIgFIAAASIAJAAIAAAHIgJAAIAAAdIABAFIABACIAEAAIADAAIABAIIgGAAQgFAAgBgBg");
	this.shape_4.setTransform(49.4,45.4);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AgEAkIAAgzIAJAAIAAAzgAgEgZIAAgKIAJAAIAAAKg");
	this.shape_5.setTransform(46.8,45.3);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AANAaIAAgeIgCgIIgDgFQgDgBgEAAQgEAAgEAEQgEADAAAKIAAAbIgJAAIAAgyIAIAAIAAAHQAGgJAKAAQAEAAAEACQAEACACACQACADAAAEIABAJIAAAeg");
	this.shape_6.setTransform(42.9,46.3);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AgNAaIAAgyIAIAAIAAAHQADgFACgCQACgCADAAQAFABAEADIgDAIQgDgCgDgBQgDABgCACQgCABgBADIgBAKIAAAag");
	this.shape_7.setTransform(38.9,46.3);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AgLAZQgEgCgCgCQgCgDAAgEIgBgIIAAgfIAJAAIAAAcIAAAJQABADACACQAEACADAAQADAAAEgCQAEgCAAgDQACgEAAgGIAAgbIAJAAIAAAyIgIAAIAAgHQgGAJgJgBQgFAAgEgBg");
	this.shape_8.setTransform(34,46.4);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AgXAkIAAhHIAvAAIAAAJIgmAAIAAAWIAhAAIAAAHIghAAIAAAhg");
	this.shape_9.setTransform(28.4,45.3);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AANAbQgEAFgFADQgFACgGAAQgKAAgHgHQgFgHAAgHQAAgHAEgFQAFgEAIgEQgFgGgCgDIgBgHQAAgHAFgEQAFgFAIAAQAGAAAEAEQAFAFAAAGQAAAKgNAIIANAPIADgKIAJACQgDAJgEAGQAGAHAGAEIgGAHQgFgDgGgHgAgRAIQgCAEAAADQgBAFADAFQAEAEAGAAQAFAAADgCQAEgDACgEIgPgUQgGAFgDADgAgJgbQgCADAAADIABAEIACADIAEAFQAFgDACgDQABgCAAgEQAAgDgCgCQgCgDgDAAQgDAAgDACg");
	this.shape_10.setTransform(19.2,45.3);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AgNAbIAAg0IAIAAIAAAJQADgGACgCQACgCADABQAEAAAFACIgDAIQgDgBgDAAQgDAAgCABQgCACgBADIgBAKIAAAbg");
	this.shape_11.setTransform(54.5,38.1);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AgQAUQgHgHAAgNQAAgMAHgHQAGgHAKAAQALAAAGAHQAHAHAAAMIAAADIgmAAQAAAIAFAEQAEAFAGAAQAFAAADgDQAEgCACgGIAJACQgDAHgFAFQgGAEgJAAQgLAAgGgHgAgJgOQgEAEAAAGIAcAAQgBgGgDgDQgEgGgHAAQgFAAgEAFg");
	this.shape_12.setTransform(49.7,38.2);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("AgNAdIAAAGIgIAAIAAhGIAJAAIAAAZQAFgHAHAAQAGAAADACQAFACACAEQADADACAFQACAEAAAGQAAANgHAHQgHAHgJAAQgIAAgFgHgAgJgFQgFAFABAJQAAAJACAEQAFAHAGAAQAFAAAEgFQAFgFAAgKQAAgJgFgFQgEgFgFAAQgFAAgEAFg");
	this.shape_13.setTransform(44.3,37.2);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FFFFFF").s().p("AAaAbIAAggIAAgIQgBgCgDgBQgCgCgDAAQgFAAgEAEQgEAEAAAHIAAAeIgHAAIAAghQAAgGgCgDQgDgDgFAAQgDAAgDACQgEACgBAEQgBADAAAHIAAAbIgJAAIAAg0IAIAAIAAAIQACgEAEgCQAEgDAFABQAGgBAEADQADADABAEQAGgKAKABQAIAAAEAEQAEAEAAAJIAAAjg");
	this.shape_14.setTransform(37.2,38.1);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FFFFFF").s().p("AgEAkIAAgzIAIAAIAAAzgAgEgZIAAgKIAIAAIAAAKg");
	this.shape_15.setTransform(31.9,37.2);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FFFFFF").s().p("AgEAkIAAg+IgXAAIAAgJIA3AAIAAAJIgXAAIAAA+g");
	this.shape_16.setTransform(28.1,37.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1}]}).wait(1));

	// Layer_1
	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.rf(["#BF955A","#BF955A","#996600","#BF955A","#BF955A","#BDC3C7","#666666"],[0,0.416,0.604,0.773,1,1,1],-7.9,-15.9,0,-7.9,-15.9,86.1).s().p("AkdEeQh3h2AAioQAAinB3h2QB2h3CnAAQCoAAB2B3QB3B2AACnQAACoh3B2Qh2B3ioAAQinAAh2h3g");
	this.shape_17.setTransform(40.5,40.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_17).wait(1));

}).prototype = getMCSymbolPrototype(lib.gray, new cjs.Rectangle(0,0,81,81), null);


(lib.earth_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.earth();
	this.instance.parent = this;
	this.instance.setTransform(-49,-52,0.16,0.16);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.earth_1, new cjs.Rectangle(-49,-52,99.9,102.1), null);


(lib.blue = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_3
	this.shape = new cjs.Shape();
	this.shape.graphics.lf(["#FFFFFF","rgba(255,255,255,0)"],[0,1],-6.9,-50,-6.9,5.5).s().p("AjxDEQhlhRABhzQgBhyBlhRQBkhRCNAAQCOAABkBRQBlBRAAByQAABzhlBRQhkBSiOAAQiNAAhkhSg");
	this.shape.setTransform(41.2,30.8);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	// Layer_2
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AAMAaIAAgeIgBgIIgDgFQgDgBgEAAQgEAAgEAEQgEADAAAKIAAAbIgJAAIAAgyIAIAAIAAAHQAGgJAJAAQAFAAAEACQAEACACACQACADAAAEIABAJIAAAeg");
	this.shape_1.setTransform(66.6,46.3);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgRAUQgGgHAAgNQAAgOAIgGQAGgGAJAAQALAAAGAHQAHAHAAAMQAAAKgDAFQgDAGgGADQgFADgHAAQgKAAgHgHgAgKgOQgEAFAAAJQAAAKAEAFQAEAFAGAAQAGAAAFgFQAEgFAAgKQAAgJgEgFQgFgFgGAAQgGAAgEAFg");
	this.shape_2.setTransform(61,46.3);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgDAkIAAgzIAHAAIAAAzgAgDgZIAAgKIAHAAIAAAKg");
	this.shape_3.setTransform(57.2,45.3);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AgBAiQgCgCgBgCQgBgDAAgHIAAgdIgGAAIAAgHIAGAAIAAgNIAIgFIAAASIAJAAIAAAHIgJAAIAAAdIAAAFIACACIACAAIAFAAIABAIIgHAAQgFAAgCgBg");
	this.shape_4.setTransform(54.7,45.4);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AgSAXQgFgEAAgHQAAgDACgDQABgEADgCIAHgBIAIgCQAJgBAFgCIAAgCQAAgFgCgDQgEgDgGAAQgFAAgEACQgCADgCAFIgIgBQABgGACgDQADgDAFgCQAGgCAFAAQAHAAAEABQAEACACADQACACAAADIABAIIAAALIAAAQIADAGIgJAAIgCgHQgFAFgFACQgDABgFAAQgJAAgEgEgAgBADIgIACIgDADIgCAEQAAADADADQADADAFgBQAEABAEgDQAEgCACgEIABgJIAAgDQgEACgJABg");
	this.shape_5.setTransform(50.5,46.3);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AgPAUQgGgHAAgNQgBgHADgHQADgFAFgEQAHgDAFAAQAJAAAFAEQAFAFACAIIgJABQgBgFgDgDQgDgDgEAAQgGAAgFAFQgDAFAAAJQAAALADAEQAFAFAFAAQAFAAADgDQAEgDABgHIAJABQgCAJgGAFQgFAFgJAAQgJAAgHgHg");
	this.shape_6.setTransform(45.4,46.3);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AgEAkIAAgzIAIAAIAAAzgAgEgZIAAgKIAIAAIAAAKg");
	this.shape_7.setTransform(41.6,45.3);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AgNAaIAAgyIAIAAIAAAHQADgFACgCQACgCADAAQAEABAFADIgDAIQgDgCgDgBQgDABgCACQgCABgBADIgBAKIAAAag");
	this.shape_8.setTransform(39.2,46.3);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AgOAdIAAAGIgIAAIAAhGIAJAAIAAAZQAGgHAIAAQAFAAAEACQAEACADAEQADADABAFQABAEAAAGQABANgHAHQgGAHgKAAQgIAAgGgHgAgJgFQgEAFgBAJQABAJACAEQAEAHAHAAQAFAAAFgFQADgFAAgKQAAgJgDgFQgEgFgGAAQgFAAgEAFg");
	this.shape_9.setTransform(34.5,45.4);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AgTAXQgEgEAAgHQAAgDACgDQABgEAEgCIAFgBIAIgCQAKgBAFgCIAAgCQAAgFgCgDQgDgDgHAAQgFAAgEACQgCADgCAFIgJgBQACgGACgDQADgDAFgCQAGgCAFAAQAHAAAEABQAEACACADQACACABADIAAAIIAAALIABAQIACAGIgKAAIgBgHQgFAFgEACQgEABgFAAQgIAAgGgEgAgBADIgIACIgEADIAAAEQgBADADADQADADAFgBQAEABAEgDQAEgCACgEIABgJIAAgDQgFACgIABg");
	this.shape_10.setTransform(28.8,46.3);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AgXAkIAAhHIAvAAIAAAJIgmAAIAAAWIAhAAIAAAHIghAAIAAAhg");
	this.shape_11.setTransform(23.2,45.3);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AANAbQgFAFgEADQgEACgGAAQgLAAgHgHQgFgHAAgHQAAgHAEgFQAEgEAJgEQgFgGgCgDIgBgHQAAgHAFgEQAFgFAHAAQAHAAAEAEQAFAFAAAGQAAAKgNAIIANAPIADgKIAJACQgCAJgEAGQAFAHAGAEIgGAHQgGgDgFgHgAgRAIQgCAEgBADQAAAFAEAFQADAEAHAAQAEAAADgCQAEgDACgEIgOgUQgIAFgCADgAgJgbQgCADAAADIAAAEIADADIAEAFQAFgDACgDQABgCAAgEQAAgDgCgCQgCgDgDAAQgEAAgCACg");
	this.shape_12.setTransform(14,45.3);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("AgNAXQgGgEgBgIIAIgCQABAGADACQAEADAEAAQAHAAACgDQAEgCAAgDQgBgEgCgBIgJgDIgNgEQgDgBgCgDQgCgDAAgEQAAgDABgEQACgDADgBIAFgDIAIgBQAFAAAFACQAEACACACQADAEABAEIgJABQgBgDgDgDQgCgCgFAAQgGAAgCACQgCACgBADIABADIAEADIAGACIAOADQADABADADQACADAAAFQgBAEgCAEQgCAEgFACQgFACgHAAQgIAAgFgEg");
	this.shape_13.setTransform(52.1,38.2);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FFFFFF").s().p("AgDAkIAAhHIAHAAIAABHg");
	this.shape_14.setTransform(48.5,37.2);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FFFFFF").s().p("AgTAXQgEgEAAgGQAAgFACgCQABgEAEgCIAFgCIAIgBQAKgBAFgCIAAgCQAAgGgCgCQgEgDgGAAQgGAAgCACQgEADgBAFIgJgBQACgGADgDQACgEAFgBQAGgCAFAAQAHAAAEACQAEABACACQACADABAEIAAAIIAAALIAAAPIADAGIgKAAIgBgGQgFAEgEABQgEACgFAAQgIAAgGgEgAgBAEIgIABIgEADIAAAEQAAADACADQADADAFAAQAEAAAEgDQAEgCACgEIABgJIAAgDQgEACgJACg");
	this.shape_15.setTransform(44.7,38.2);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FFFFFF").s().p("AAAAiQgDgCgBgCQgCgDAAgHIAAgdIgFAAIAAgHIAFAAIAAgNIAIgFIAAASIAJAAIAAAHIgJAAIAAAdIABAFIACACIADAAIADAAIABAIIgGAAQgFAAgBgBg");
	this.shape_16.setTransform(40.6,37.3);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#FFFFFF").s().p("AgQAUQgHgHAAgNQAAgMAHgHQAHgHAKAAQAKAAAHAHQAGAHAAAMIAAADIgmAAQABAIAEAEQAFAFAFAAQAFAAADgDQAEgCABgGIAJACQgCAHgFAFQgGAEgJAAQgKAAgHgHgAgIgOQgFAEgBAGIAcAAQAAgGgDgDQgEgGgGAAQgGAAgDAFg");
	this.shape_17.setTransform(36.3,38.2);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#FFFFFF").s().p("AAZAkIAAg7IgVA7IgHAAIgVg8IAAA8IgJAAIAAhHIAOAAIARAyIADAKIADgLIARgxIANAAIAABHg");
	this.shape_18.setTransform(29.4,37.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1}]}).wait(1));

	// Layer_1
	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.rf(["#666666","#999999","#CCCCCC","#999999","#666666","#BDC3C7","#666666"],[0,0.416,0.604,0.773,1,1,1],-7.9,-15.9,0,-7.9,-15.9,86.1).s().p("AkdEeQh3h2AAioQAAinB3h2QB2h3CnAAQCoAAB2B3QB3B2AACnQAACoh3B2Qh2B3ioAAQinAAh2h3g");
	this.shape_19.setTransform(40.5,40.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_19).wait(1));

}).prototype = getMCSymbolPrototype(lib.blue, new cjs.Rectangle(0,0,81,81), null);


(lib.black = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_3
	this.shape = new cjs.Shape();
	this.shape.graphics.lf(["#FFFFFF","rgba(255,255,255,0)"],[0,1],-6.9,-50,-6.9,5.5).s().p("AjxDEQhlhRABhzQgBhyBlhRQBkhRCNAAQCOAABkBRQBlBRAAByQAABzhlBRQhkBSiOAAQiNAAhkhSg");
	this.shape.setTransform(41.2,30.8);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	// Layer_2
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#666666").s().p("AgSAjIgBgIIAFABIAFgBIADgCIACgHIABgCIgUg0IAKAAIALAeIACAMIAEgLIALgfIAJAAIgUA1IgEALQgCAFgDACQgDABgEAAIgGgBg");
	this.shape_1.setTransform(60.8,43.7);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#666666").s().p("AgNAbIAAg0IAIAAIAAAJQADgGACgCQACgCADABQAFAAAEACIgDAIQgDgBgDAAQgDAAgCABQgCACgBADIgBAKIAAAbg");
	this.shape_2.setTransform(57,42.6);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#666666").s().p("AgQAUQgHgHAAgNQAAgMAHgHQAHgHAJAAQALAAAGAHQAHAHAAAMIAAADIgmAAQAAAIAFAEQAFAFAFAAQAFAAADgDQADgCACgGIAKACQgCAHgHAFQgFAEgJAAQgLAAgGgHgAgJgOQgEAEAAAGIAbAAQAAgGgCgDQgFgGgHAAQgEAAgFAFg");
	this.shape_3.setTransform(52.1,42.7);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#666666").s().p("AAMAbIAAgfIAAgIIgEgEQgDgCgDAAQgFAAgEADQgEAEAAAKIAAAcIgJAAIAAg0IAIAAIAAAIQAFgIAKAAQAFgBAEACQAEACACADQACADAAADIABAIIAAAgg");
	this.shape_4.setTransform(46.6,42.6);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#666666").s().p("AgRAUQgGgHAAgNQAAgNAIgIQAGgFAJAAQALAAAGAHQAHAHAAAMQAAAJgDAGQgDAGgGADQgFADgHAAQgKAAgHgHgAgKgOQgEAFAAAJQAAAKAEAFQAEAFAGAAQAGAAAFgFQAEgFAAgKQAAgJgEgFQgFgFgGAAQgGAAgEAFg");
	this.shape_5.setTransform(41,42.7);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#666666").s().p("AgDAkIAAgzIAIAAIAAAzgAgDgZIAAgKIAIAAIAAAKg");
	this.shape_6.setTransform(37.2,41.7);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#666666").s().p("AAAAiQgDgCgBgCQgCgDAAgHIAAgdIgFAAIAAgHIAFAAIAAgNIAIgFIAAASIAJAAIAAAHIgJAAIAAAdIABAFIACACIADAAIADAAIABAIIgGAAQgFAAgBgBg");
	this.shape_7.setTransform(34.7,41.8);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#666666").s().p("AgSAXQgFgEAAgGQAAgFACgCQACgEADgCIAGgCIAHgBQAKgBAFgCIAAgCQAAgGgCgCQgEgDgGAAQgGAAgCACQgDADgCAFIgJgBQACgGADgDQACgEAFgBQAGgCAFAAQAGAAAFACQAEABACACQACADAAAEIABAIIAAALIAAAPIADAGIgKAAIgBgGQgFAEgFABQgDACgFAAQgJAAgEgEgAgBAEIgIABIgEADIgBAEQABADACADQADADAFAAQAEAAAEgDQAEgCACgEIABgJIAAgDQgFACgIACg");
	this.shape_8.setTransform(30.5,42.7);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#666666").s().p("AgBAiQgCgCgBgCQgBgDAAgHIAAgdIgGAAIAAgHIAGAAIAAgNIAIgFIAAASIAJAAIAAAHIgJAAIAAAdIAAAFIACACIACAAIAFAAIABAIIgHAAQgFAAgCgBg");
	this.shape_9.setTransform(26.4,41.8);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#666666").s().p("AgNAiQgHgDgEgFQgDgGAAgHIAJgBQAAAGACADQADADAFACQAFACAEAAQAGAAAEgBQAFgBACgDQABgDAAgEQAAgDgBgDQgCgCgFgCIgMgDIgOgEQgEgCgDgEQgDgEAAgFQAAgGADgEQAEgFAFgDQAGgCAHAAQAHAAAGADQAHACADAFQADAGABAGIgKABQAAgIgFgDQgEgDgIgBQgIAAgDADQgFAEAAAEQABAEACACQADADALADIAQAEQAGACADAFQACAEAAAGQAAAGgDAEQgDAGgGACQgGADgIAAQgJAAgGgDg");
	this.shape_10.setTransform(21.6,41.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1}]}).wait(1));

	// Layer_1
	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.rf(["#FFFFFF","#CCCCCC","#999999","#CCCCCC","#FFFFFF","#BDC3C7","#666666"],[0,0.416,0.604,0.773,1,1,1],-7.9,-15.9,0,-7.9,-15.9,86.1).s().p("AkdEeQh3h2AAioQAAinB3h2QB2h3CnAAQCoAAB2B3QB3B2AACnQAACoh3B2Qh2B3ioAAQinAAh2h3g");
	this.shape_11.setTransform(40.5,40.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_11).wait(1));

}).prototype = getMCSymbolPrototype(lib.black, new cjs.Rectangle(0,0,81,81), null);


// stage content:
(lib.planet_new = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		var _this = this;
		/*
		Mousing over the specified symbol instance executes a function.
		'3' is the number of the times event should be triggered.
		*/
		stage.enableMouseOver(3);
		_this.agro.on('mouseover', function(){
		/*
		Stop the complete animation.
		*/
		createjs.Ticker.removeEventListener('tick', stage);
		});
		
		/*
		Mousing out of the specified symbol instance executes a function.
		'3' is the number of the times event should be triggered.
		*/
		stage.enableMouseOver(3);
		_this.agro.on('mouseout', function(){
		/*
		Start the complete animation.
		*/
		createjs.Ticker.removeEventListener('tick', stage);
		createjs.Ticker.addEventListener('tick', stage);
		});
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(481));

	// purplefront
	this.instance = new lib.purple();
	this.instance.parent = this;
	this.instance.setTransform(99.9,57.3,0.7,0.7,0,0,0,40.6,40.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1).to({regX:40.5,x:100.8,y:57.5},0).wait(1).to({x:101.8,y:57.7},0).wait(1).to({x:102.8,y:57.8},0).wait(1).to({x:103.7,y:58},0).wait(1).to({x:104.7,y:58.2},0).wait(1).to({x:105.7,y:58.3},0).wait(1).to({x:106.7,y:58.5},0).wait(1).to({x:107.6,y:58.6},0).wait(1).to({x:108.6,y:58.7},0).wait(1).to({x:109.6,y:58.9},0).wait(1).to({x:110.6,y:59},0).wait(1).to({x:111.6,y:59.1},0).wait(1).to({x:112.6,y:59.2},0).wait(1).to({x:113.5,y:59.3},0).wait(1).to({x:114.5,y:59.4},0).wait(1).to({x:115.5,y:59.5},0).wait(1).to({x:116.5,y:59.6},0).wait(1).to({x:117.5,y:59.7},0).wait(1).to({x:118.5,y:59.8},0).wait(1).to({x:119.4,y:59.9},0).wait(1).to({x:120.4,y:60},0).wait(1).to({x:121.4,y:60.1},0).wait(1).to({x:122.4},0).wait(1).to({x:123.4,y:60.2},0).wait(1).to({x:124.4,y:60.3},0).wait(1).to({x:125.4,y:60.4},0).wait(1).to({x:126.3},0).wait(1).to({x:127.3,y:60.5},0).wait(1).to({x:128.3,y:60.6},0).wait(1).to({x:129.3},0).wait(1).to({x:130.3,y:60.7},0).wait(1).to({x:131.3},0).wait(1).to({x:132.3,y:60.8},0).wait(1).to({x:133.2},0).wait(1).to({x:134.2,y:60.9},0).wait(1).to({x:135.2},0).wait(1).to({x:136.2,y:61},0).wait(1).to({x:137.2},0).wait(1).to({x:138.2},0).wait(1).to({x:139.2,y:61.1},0).wait(1).to({x:140.2},0).wait(1).to({x:141.1},0).wait(1).to({x:142.1,y:61.2},0).wait(1).to({x:143.1},0).wait(1).to({x:144.1},0).wait(1).to({x:145.1,y:61.3},0).wait(1).to({x:146.1},0).wait(1).to({x:147.1},0).wait(1).to({x:148.1},0).wait(1).to({x:149.1},0).wait(1).to({x:150,y:61.4},0).wait(1).to({x:151},0).wait(1).to({x:152},0).wait(1).to({x:153},0).wait(1).to({x:154},0).wait(1).to({x:155},0).wait(1).to({x:156},0).wait(1).to({x:157},0).wait(1).to({x:157.9},0).wait(1).to({x:158.9},0).wait(1).to({x:159.9},0).wait(1).to({x:160.9},0).wait(1).to({x:161.9},0).wait(1).to({x:162.9},0).wait(1).to({x:163.9},0).wait(1).to({x:164.9},0).wait(1).to({x:165.9},0).wait(1).to({x:166.8,y:61.3},0).wait(1).to({x:167.8},0).wait(1).to({x:168.8},0).wait(1).to({x:169.8},0).wait(1).to({x:170.8},0).wait(1).to({x:171.8,y:61.2},0).wait(1).to({x:172.8},0).wait(1).to({x:173.8},0).wait(1).to({x:174.8},0).wait(1).to({x:175.7,y:61.1},0).wait(1).to({x:176.7},0).wait(1).to({x:177.7},0).wait(1).to({x:178.7,y:61},0).wait(1).to({x:179.7},0).wait(1).to({x:180.7,y:60.9},0).wait(1).to({x:181.7},0).wait(1).to({x:182.7,y:60.8},0).wait(1).to({x:183.6},0).wait(1).to({x:184.6,y:60.7},0).wait(1).to({x:185.6},0).wait(1).to({x:186.6,y:60.6},0).wait(1).to({x:187.6},0).wait(1).to({x:188.6,y:60.5},0).wait(1).to({x:189.6},0).wait(1).to({x:190.5,y:60.4},0).wait(1).to({x:191.5,y:60.3},0).wait(1).to({x:192.5},0).wait(1).to({x:193.5,y:60.2},0).wait(1).to({x:194.5,y:60.1},0).wait(1).to({x:195.5,y:60},0).wait(1).to({x:196.5,y:59.9},0).wait(1).to({x:197.4},0).wait(1).to({x:198.4,y:59.8},0).wait(1).to({x:199.4,y:59.7},0).wait(1).to({x:200.4,y:59.6},0).wait(1).to({x:201.4,y:59.5},0).wait(1).to({x:202.4,y:59.4},0).wait(1).to({x:203.3,y:59.3},0).wait(1).to({x:204.3,y:59.2},0).wait(1).to({x:205.3,y:59},0).wait(1).to({x:206.3,y:58.9},0).wait(1).to({x:207.3,y:58.8},0).wait(1).to({x:208.3,y:58.7},0).wait(1).to({x:209.2,y:58.5},0).wait(1).to({x:210.2,y:58.4},0).wait(1).to({x:211.2,y:58.2},0).wait(1).to({x:212.2,y:58.1},0).wait(1).to({x:213.1,y:57.9},0).wait(1).to({x:214.1,y:57.7},0).wait(1).to({x:215.1,y:57.5},0).wait(1).to({x:216,y:57.3},0).to({_off:true},1).wait(120).to({_off:false,regX:40.6,x:99.9},0).wait(1).to({regX:40.5,x:100.8,y:57.5},0).wait(1).to({x:101.7,y:57.7},0).wait(1).to({x:102.6,y:57.8},0).wait(1).to({x:103.6,y:58},0).wait(1).to({x:104.5,y:58.1},0).wait(1).to({x:105.5,y:58.3},0).wait(1).to({x:106.4,y:58.4},0).wait(1).to({x:107.3,y:58.6},0).wait(1).to({x:108.3,y:58.7},0).wait(1).to({x:109.2,y:58.8},0).wait(1).to({x:110.1,y:58.9},0).wait(1).to({x:111.1,y:59},0).wait(1).to({x:112,y:59.2},0).wait(1).to({x:113,y:59.3},0).wait(1).to({x:113.9,y:59.4},0).wait(1).to({x:114.9,y:59.5},0).wait(1).to({x:115.8,y:59.6},0).wait(1).to({x:116.7,y:59.7},0).wait(1).to({x:117.7},0).wait(1).to({x:118.6,y:59.8},0).wait(1).to({x:119.6,y:59.9},0).wait(1).to({x:120.5,y:60},0).wait(1).to({x:121.5,y:60.1},0).wait(1).to({x:122.4},0).wait(1).to({x:123.4,y:60.2},0).wait(1).to({x:124.3,y:60.3},0).wait(1).to({x:125.3,y:60.4},0).wait(1).to({x:126.2},0).wait(1).to({x:127.1,y:60.5},0).wait(1).to({x:128.1},0).wait(1).to({x:129,y:60.6},0).wait(1).to({x:130},0).wait(1).to({x:130.9,y:60.7},0).wait(1).to({x:131.9,y:60.8},0).wait(1).to({x:132.8},0).wait(1).to({x:133.8},0).wait(1).to({x:134.7,y:60.9},0).wait(1).to({x:135.7},0).wait(1).to({x:136.6,y:61},0).wait(1).to({x:137.6},0).wait(1).to({x:138.5},0).wait(1).to({x:139.5,y:61.1},0).wait(1).to({x:140.4},0).wait(1).to({x:141.4},0).wait(1).to({x:142.3,y:61.2},0).wait(1).to({x:143.3},0).wait(1).to({x:144.2},0).wait(1).to({x:145.1,y:61.3},0).wait(1).to({x:146.1},0).wait(1).to({x:147},0).wait(1).to({x:148},0).wait(1).to({x:148.9},0).wait(1).to({x:149.9},0).wait(1).to({x:150.8,y:61.4},0).wait(1).to({x:151.8},0).wait(1).to({x:152.7},0).wait(1).to({x:153.7},0).wait(1).to({x:154.6},0).wait(1).to({x:155.6},0).wait(1).to({x:156.5},0).wait(1).to({x:157.5},0).wait(1).to({x:158.4},0).wait(1).to({x:159.4},0).wait(1).to({x:160.3},0).wait(1).to({x:161.3},0).wait(1).to({x:162.2},0).wait(1).to({x:163.2},0).wait(1).to({x:164.1},0).wait(1).to({x:165.1},0).wait(1).to({x:166},0).wait(1).to({x:167,y:61.3},0).wait(1).to({x:167.9},0).wait(1).to({x:168.9},0).wait(1).to({x:169.8},0).wait(1).to({x:170.7},0).wait(1).to({x:171.7,y:61.2},0).wait(1).to({x:172.6},0).wait(1).to({x:173.6},0).wait(1).to({x:174.5},0).wait(1).to({x:175.5,y:61.1},0).wait(1).to({x:176.4},0).wait(1).to({x:177.4},0).wait(1).to({x:178.3,y:61},0).wait(1).to({x:179.3},0).wait(1).to({x:180.2},0).wait(1).to({x:181.2,y:60.9},0).wait(1).to({x:182.1},0).wait(1).to({x:183.1,y:60.8},0).wait(1).to({x:184},0).wait(1).to({x:185,y:60.7},0).wait(1).to({x:185.9},0).wait(1).to({x:186.9,y:60.6},0).wait(1).to({x:187.8},0).wait(1).to({x:188.8,y:60.5},0).wait(1).to({x:189.7},0).wait(1).to({x:190.6,y:60.4},0).wait(1).to({x:191.6,y:60.3},0).wait(1).to({x:192.5},0).wait(1).to({x:193.5,y:60.2},0).wait(1).to({x:194.4,y:60.1},0).wait(1).to({x:195.4,y:60},0).wait(1).to({x:196.3},0).wait(1).to({x:197.3,y:59.9},0).wait(1).to({x:198.2,y:59.8},0).wait(1).to({x:199.2,y:59.7},0).wait(1).to({x:200.1,y:59.6},0).wait(1).to({x:201,y:59.5},0).wait(1).to({x:202,y:59.4},0).wait(1).to({x:202.9,y:59.3},0).wait(1).to({x:203.9,y:59.2},0).wait(1).to({x:204.8,y:59.1},0).wait(1).to({x:205.7,y:59},0).wait(1).to({x:206.7,y:58.9},0).wait(1).to({x:207.6,y:58.7},0).wait(1).to({x:208.6,y:58.6},0).wait(1).to({x:209.5,y:58.5},0).wait(1).to({x:210.4,y:58.3},0).wait(1).to({x:211.4,y:58.2},0).wait(1).to({x:212.3,y:58},0).wait(1).to({x:213.3,y:57.9},0).wait(1).to({x:214.2,y:57.7},0).wait(1).to({x:215.1,y:57.5},0).wait(1).to({x:216,y:57.3},0).to({_off:true},1).wait(118));

	// pbluefront
	this.instance_1 = new lib.blue();
	this.instance_1.parent = this;
	this.instance_1.setTransform(156.6,130.4,0.7,0.7,0,0,0,40.5,40.5);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(1).to({x:157.4},0).wait(1).to({x:158.3},0).wait(1).to({x:159.1},0).wait(1).to({x:160},0).wait(1).to({x:160.8},0).wait(1).to({x:161.7},0).wait(1).to({x:162.6},0).wait(1).to({x:163.4},0).wait(1).to({x:164.3},0).wait(1).to({x:165.1},0).wait(1).to({x:166},0).wait(1).to({x:166.9},0).wait(1).to({x:167.7},0).wait(1).to({x:168.6},0).wait(1).to({x:169.4},0).wait(1).to({x:170.3},0).wait(1).to({x:171.1,y:130.3},0).wait(1).to({x:172},0).wait(1).to({x:172.9},0).wait(1).to({x:173.7},0).wait(1).to({x:174.6,y:130.2},0).wait(1).to({x:175.4},0).wait(1).to({x:176.3},0).wait(1).to({x:177.1,y:130.1},0).wait(1).to({x:178},0).wait(1).to({x:178.9,y:130},0).wait(1).to({x:179.7},0).wait(1).to({x:180.6},0).wait(1).to({x:181.4,y:129.9},0).wait(1).to({x:182.3,y:129.8},0).wait(1).to({x:183.1},0).wait(1).to({x:184,y:129.7},0).wait(1).to({x:184.9},0).wait(1).to({x:185.7,y:129.6},0).wait(1).to({x:186.6,y:129.5},0).wait(1).to({x:187.4,y:129.4},0).wait(1).to({x:188.3},0).wait(1).to({x:189.1,y:129.3},0).wait(1).to({x:190,y:129.2},0).wait(1).to({x:190.8,y:129.1},0).wait(1).to({x:191.7,y:129},0).wait(1).to({x:192.5,y:128.9},0).wait(1).to({x:193.4,y:128.8},0).wait(1).to({x:194.3,y:128.7},0).wait(1).to({x:195.1,y:128.6},0).wait(1).to({x:196,y:128.5},0).wait(1).to({x:196.8,y:128.4},0).wait(1).to({x:197.7,y:128.3},0).wait(1).to({x:198.5,y:128.2},0).wait(1).to({x:199.4,y:128},0).wait(1).to({x:200.2,y:127.9},0).wait(1).to({x:201.1,y:127.8},0).wait(1).to({x:201.9,y:127.6},0).wait(1).to({x:202.7,y:127.5},0).wait(1).to({x:203.6,y:127.4},0).wait(1).to({x:204.4,y:127.2},0).wait(1).to({x:205.3,y:127.1},0).wait(1).to({x:206.1,y:126.9},0).wait(1).to({x:207,y:126.8},0).wait(1).to({x:207.8,y:126.6},0).wait(1).to({x:208.7,y:126.5},0).wait(1).to({x:209.5,y:126.3},0).wait(1).to({x:210.4,y:126.2},0).wait(1).to({x:211.2,y:126},0).wait(1).to({x:212,y:125.8},0).wait(1).to({x:212.9,y:125.7},0).wait(1).to({x:213.7,y:125.5},0).wait(1).to({x:214.6,y:125.3},0).wait(1).to({x:215.4,y:125.1},0).wait(1).to({x:216.2,y:124.9},0).wait(1).to({x:217.1,y:124.8},0).wait(1).to({x:217.9,y:124.6},0).wait(1).to({x:218.8,y:124.4},0).wait(1).to({x:219.6,y:124.2},0).wait(1).to({x:220.4,y:124},0).wait(1).to({x:221.3,y:123.8},0).wait(1).to({x:222.1,y:123.6},0).wait(1).to({x:222.9,y:123.4},0).wait(1).to({x:223.8,y:123.2},0).wait(1).to({x:224.6,y:123},0).wait(1).to({x:225.4,y:122.8},0).wait(1).to({x:226.3,y:122.6},0).wait(1).to({x:227.1,y:122.4},0).wait(1).to({x:227.9,y:122.2},0).wait(1).to({x:228.8,y:121.9},0).wait(1).to({x:229.6,y:121.7},0).wait(1).to({x:230.4,y:121.5},0).wait(1).to({x:231.3,y:121.3},0).wait(1).to({x:232.1,y:121.1},0).wait(1).to({x:232.9,y:120.8},0).wait(1).to({x:233.7,y:120.6},0).wait(1).to({x:234.6,y:120.4},0).wait(1).to({x:235.4,y:120.2},0).wait(1).to({x:236.2,y:120},0).wait(1).to({x:237.1,y:119.7},0).wait(1).to({x:237.9,y:119.5},0).wait(1).to({x:238.7,y:119.3},0).wait(1).to({x:239.5,y:119},0).wait(1).to({x:240.4,y:118.8},0).wait(1).to({x:241.2,y:118.6},0).wait(1).to({x:242,y:118.4},0).wait(1).to({x:242.8,y:118.1},0).wait(1).to({x:243.7,y:117.9},0).wait(1).to({x:244.5,y:117.7},0).wait(1).to({x:245.3,y:117.4},0).wait(1).to({x:246.1,y:117.2},0).wait(1).to({x:247,y:117},0).wait(1).to({x:247.8,y:116.7},0).wait(1).to({x:248.6,y:116.5},0).wait(1).to({x:249.4,y:116.2},0).wait(1).to({x:250.3,y:116},0).wait(1).to({x:251.1,y:115.8},0).wait(1).to({x:251.9,y:115.5},0).wait(1).to({x:252.7,y:115.3},0).wait(1).to({x:253.6,y:115.1},0).wait(1).to({x:254.4,y:114.8},0).wait(1).to({x:255.2,y:114.6},0).wait(1).to({x:256,y:114.3},0).to({_off:true},1).wait(243).to({_off:false,x:56.9},0).wait(1).to({x:57.7,y:114.5},0).wait(1).to({x:58.6,y:114.7},0).wait(1).to({x:59.4,y:115},0).wait(1).to({x:60.2,y:115.2},0).wait(1).to({x:61,y:115.4},0).wait(1).to({x:61.9,y:115.6},0).wait(1).to({x:62.7,y:115.8},0).wait(1).to({x:63.5,y:116},0).wait(1).to({x:64.4,y:116.3},0).wait(1).to({x:65.2,y:116.5},0).wait(1).to({x:66,y:116.7},0).wait(1).to({x:66.9,y:116.9},0).wait(1).to({x:67.7,y:117.1},0).wait(1).to({x:68.5,y:117.3},0).wait(1).to({x:69.4,y:117.5},0).wait(1).to({x:70.2,y:117.7},0).wait(1).to({x:71,y:117.9},0).wait(1).to({x:71.9,y:118.2},0).wait(1).to({x:72.7,y:118.4},0).wait(1).to({x:73.5,y:118.6},0).wait(1).to({x:74.3,y:118.8},0).wait(1).to({x:75.2,y:119},0).wait(1).to({x:76,y:119.2},0).wait(1).to({x:76.8,y:119.4},0).wait(1).to({x:77.7,y:119.6},0).wait(1).to({x:78.5,y:119.8},0).wait(1).to({x:79.4,y:120},0).wait(1).to({x:80.2,y:120.2},0).wait(1).to({x:81,y:120.4},0).wait(1).to({x:81.9,y:120.6},0).wait(1).to({x:82.7,y:120.8},0).wait(1).to({x:83.5,y:121},0).wait(1).to({x:84.4,y:121.1},0).wait(1).to({x:85.2,y:121.3},0).wait(1).to({x:86,y:121.5},0).wait(1).to({x:86.9,y:121.7},0).wait(1).to({x:87.7,y:121.9},0).wait(1).to({x:88.6,y:122.1},0).wait(1).to({x:89.4,y:122.3},0).wait(1).to({x:90.2,y:122.4},0).wait(1).to({x:91.1,y:122.6},0).wait(1).to({x:91.9,y:122.8},0).wait(1).to({x:92.7,y:123},0).wait(1).to({x:93.6,y:123.2},0).wait(1).to({x:94.4,y:123.3},0).wait(1).to({x:95.3,y:123.5},0).wait(1).to({x:96.1,y:123.7},0).wait(1).to({x:96.9,y:123.8},0).wait(1).to({x:97.8,y:124},0).wait(1).to({x:98.6,y:124.2},0).wait(1).to({x:99.5,y:124.3},0).wait(1).to({x:100.3,y:124.5},0).wait(1).to({x:101.2,y:124.7},0).wait(1).to({x:102,y:124.8},0).wait(1).to({x:102.8,y:125},0).wait(1).to({x:103.7,y:125.1},0).wait(1).to({x:104.5,y:125.3},0).wait(1).to({x:105.4,y:125.4},0).wait(1).to({x:106.2,y:125.6},0).wait(1).to({x:107.1,y:125.7},0).wait(1).to({x:107.9,y:125.9},0).wait(1).to({x:108.8,y:126},0).wait(1).to({x:109.6,y:126.1},0).wait(1).to({x:110.5,y:126.3},0).wait(1).to({x:111.3,y:126.4},0).wait(1).to({x:112.1,y:126.5},0).wait(1).to({x:113,y:126.7},0).wait(1).to({x:113.8,y:126.8},0).wait(1).to({x:114.7,y:126.9},0).wait(1).to({x:115.5,y:127.1},0).wait(1).to({x:116.4,y:127.2},0).wait(1).to({x:117.2,y:127.3},0).wait(1).to({x:118.1,y:127.4},0).wait(1).to({x:118.9,y:127.5},0).wait(1).to({x:119.8,y:127.6},0).wait(1).to({x:120.6,y:127.8},0).wait(1).to({x:121.5,y:127.9},0).wait(1).to({x:122.3,y:128},0).wait(1).to({x:123.2,y:128.1},0).wait(1).to({x:124,y:128.2},0).wait(1).to({x:124.9,y:128.3},0).wait(1).to({x:125.7,y:128.4},0).wait(1).to({x:126.6,y:128.5},0).wait(1).to({x:127.5,y:128.6},0).wait(1).to({x:128.3,y:128.7},0).wait(1).to({x:129.2,y:128.8},0).wait(1).to({x:130},0).wait(1).to({x:130.9,y:128.9},0).wait(1).to({x:131.7,y:129},0).wait(1).to({x:132.6,y:129.1},0).wait(1).to({x:133.4,y:129.2},0).wait(1).to({x:134.3},0).wait(1).to({x:135.1,y:129.3},0).wait(1).to({x:136,y:129.4},0).wait(1).to({x:136.8},0).wait(1).to({x:137.7,y:129.5},0).wait(1).to({x:138.6,y:129.6},0).wait(1).to({x:139.4},0).wait(1).to({x:140.3,y:129.7},0).wait(1).to({x:141.1,y:129.8},0).wait(1).to({x:142},0).wait(1).to({x:142.8,y:129.9},0).wait(1).to({x:143.7},0).wait(1).to({x:144.5,y:130},0).wait(1).to({x:145.4},0).wait(1).to({x:146.3},0).wait(1).to({x:147.1,y:130.1},0).wait(1).to({x:148},0).wait(1).to({x:148.8,y:130.2},0).wait(1).to({x:149.7},0).wait(1).to({x:150.5},0).wait(1).to({x:151.4,y:130.3},0).wait(1).to({x:152.3},0).wait(1).to({x:153.1},0).wait(1).to({x:154},0).wait(1).to({x:154.8,y:130.4},0).wait(1).to({x:155.7},0).wait(1).to({x:156.5},0).wait(1));

	// marblefront
	this.instance_2 = new lib.gray();
	this.instance_2.parent = this;
	this.instance_2.setTransform(99.9,57.3,0.7,0.7,0,0,0,40.6,40.6);
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(119).to({_off:false},0).wait(1).to({regX:40.5,regY:40.5,x:100.8,y:57.5},0).wait(1).to({x:101.7,y:57.8},0).wait(1).to({x:102.7,y:58.1},0).wait(1).to({x:103.6,y:58.3},0).wait(1).to({x:104.6,y:58.6},0).wait(1).to({x:105.5,y:58.8},0).wait(1).to({x:106.5,y:59},0).wait(1).to({x:107.4,y:59.2},0).wait(1).to({x:108.4,y:59.4},0).wait(1).to({x:109.3,y:59.6},0).wait(1).to({x:110.3,y:59.8},0).wait(1).to({x:111.3,y:60},0).wait(1).to({x:112.2,y:60.2},0).wait(1).to({x:113.2,y:60.4},0).wait(1).to({x:114.1,y:60.5},0).wait(1).to({x:115.1,y:60.7},0).wait(1).to({x:116.1,y:60.9},0).wait(1).to({x:117,y:61},0).wait(1).to({x:118,y:61.2},0).wait(1).to({x:119,y:61.3},0).wait(1).to({x:119.9,y:61.5},0).wait(1).to({x:120.9,y:61.6},0).wait(1).to({x:121.9,y:61.7},0).wait(1).to({x:122.8,y:61.8},0).wait(1).to({x:123.8,y:62},0).wait(1).to({x:124.8,y:62.1},0).wait(1).to({x:125.8,y:62.2},0).wait(1).to({x:126.7,y:62.3},0).wait(1).to({x:127.7,y:62.4},0).wait(1).to({x:128.7,y:62.5},0).wait(1).to({x:129.7,y:62.6},0).wait(1).to({x:130.6,y:62.7},0).wait(1).to({x:131.6,y:62.8},0).wait(1).to({x:132.6,y:62.9},0).wait(1).to({x:133.6,y:63},0).wait(1).to({x:134.5},0).wait(1).to({x:135.5,y:63.1},0).wait(1).to({x:136.5,y:63.2},0).wait(1).to({x:137.5,y:63.3},0).wait(1).to({x:138.4},0).wait(1).to({x:139.4,y:63.4},0).wait(1).to({x:140.4,y:63.5},0).wait(1).to({x:141.4},0).wait(1).to({x:142.3,y:63.6},0).wait(1).to({x:143.3},0).wait(1).to({x:144.3,y:63.7},0).wait(1).to({x:145.3},0).wait(1).to({x:146.2,y:63.8},0).wait(1).to({x:147.2},0).wait(1).to({x:148.2},0).wait(1).to({x:149.2,y:63.9},0).wait(1).to({x:150.2},0).wait(1).to({x:151.1},0).wait(1).to({x:152.1},0).wait(1).to({x:153.1,y:64},0).wait(1).to({x:154.1},0).wait(1).to({x:155.1},0).wait(1).to({x:156},0).wait(1).to({x:157},0).wait(1).to({x:158},0).wait(1).to({x:159},0).wait(1).to({x:159.9},0).wait(1).to({x:160.9},0).wait(1).to({x:161.9},0).wait(1).to({x:162.9},0).wait(1).to({x:163.9},0).wait(1).to({x:164.8},0).wait(1).to({x:165.8},0).wait(1).to({x:166.8},0).wait(1).to({x:167.8,y:63.9},0).wait(1).to({x:168.7},0).wait(1).to({x:169.7},0).wait(1).to({x:170.7},0).wait(1).to({x:171.7,y:63.8},0).wait(1).to({x:172.7},0).wait(1).to({x:173.6,y:63.7},0).wait(1).to({x:174.6},0).wait(1).to({x:175.6},0).wait(1).to({x:176.6,y:63.6},0).wait(1).to({x:177.5},0).wait(1).to({x:178.5,y:63.5},0).wait(1).to({x:179.5,y:63.4},0).wait(1).to({x:180.5},0).wait(1).to({x:181.5,y:63.3},0).wait(1).to({x:182.4,y:63.2},0).wait(1).to({x:183.4},0).wait(1).to({x:184.4,y:63.1},0).wait(1).to({x:185.4,y:63},0).wait(1).to({x:186.3,y:62.9},0).wait(1).to({x:187.3,y:62.8},0).wait(1).to({x:188.3,y:62.7},0).wait(1).to({x:189.3,y:62.6},0).wait(1).to({x:190.2,y:62.5},0).wait(1).to({x:191.2,y:62.4},0).wait(1).to({x:192.2,y:62.3},0).wait(1).to({x:193.1,y:62.2},0).wait(1).to({x:194.1,y:62.1},0).wait(1).to({x:195.1,y:61.9},0).wait(1).to({x:196.1,y:61.8},0).wait(1).to({x:197,y:61.7},0).wait(1).to({x:198,y:61.5},0).wait(1).to({x:199,y:61.4},0).wait(1).to({x:199.9,y:61.2},0).wait(1).to({x:200.9,y:61.1},0).wait(1).to({x:201.8,y:60.9},0).wait(1).to({x:202.8,y:60.7},0).wait(1).to({x:203.8,y:60.5},0).wait(1).to({x:204.7,y:60.3},0).wait(1).to({x:205.7,y:60.1},0).wait(1).to({x:206.6,y:59.9},0).wait(1).to({x:207.6,y:59.7},0).wait(1).to({x:208.6,y:59.5},0).wait(1).to({x:209.5,y:59.3},0).wait(1).to({x:210.5,y:59},0).wait(1).to({x:211.4,y:58.8},0).wait(1).to({x:212.3,y:58.5},0).wait(1).to({x:213.3,y:58.2},0).wait(1).to({x:214.2,y:57.9},0).wait(1).to({x:215.1,y:57.6},0).wait(1).to({x:216,y:57.2},0).to({_off:true},1).wait(122).to({_off:false,regX:40.6,regY:40.6,x:99.9,y:57.3},0).wait(1).to({regX:40.5,regY:40.5,x:100.8,y:57.5},0).wait(1).to({x:101.8,y:57.8},0).wait(1).to({x:102.7,y:58.1},0).wait(1).to({x:103.7,y:58.3},0).wait(1).to({x:104.6,y:58.6},0).wait(1).to({x:105.6,y:58.8},0).wait(1).to({x:106.6,y:59},0).wait(1).to({x:107.5,y:59.3},0).wait(1).to({x:108.5,y:59.5},0).wait(1).to({x:109.5,y:59.7},0).wait(1).to({x:110.5,y:59.9},0).wait(1).to({x:111.4,y:60.1},0).wait(1).to({x:112.4,y:60.2},0).wait(1).to({x:113.4,y:60.4},0).wait(1).to({x:114.4,y:60.6},0).wait(1).to({x:115.4,y:60.8},0).wait(1).to({x:116.3,y:60.9},0).wait(1).to({x:117.3,y:61.1},0).wait(1).to({x:118.3,y:61.2},0).wait(1).to({x:119.3,y:61.4},0).wait(1).to({x:120.3,y:61.5},0).wait(1).to({x:121.3,y:61.6},0).wait(1).to({x:122.3,y:61.8},0).wait(1).to({x:123.2,y:61.9},0).wait(1).to({x:124.2,y:62},0).wait(1).to({x:125.2,y:62.1},0).wait(1).to({x:126.2,y:62.3},0).wait(1).to({x:127.2,y:62.4},0).wait(1).to({x:128.2,y:62.5},0).wait(1).to({x:129.2,y:62.6},0).wait(1).to({x:130.2,y:62.7},0).wait(1).to({x:131.2,y:62.8},0).wait(1).to({x:132.1},0).wait(1).to({x:133.1,y:62.9},0).wait(1).to({x:134.1,y:63},0).wait(1).to({x:135.1,y:63.1},0).wait(1).to({x:136.1,y:63.2},0).wait(1).to({x:137.1},0).wait(1).to({x:138.1,y:63.3},0).wait(1).to({x:139.1,y:63.4},0).wait(1).to({x:140.1},0).wait(1).to({x:141.1,y:63.5},0).wait(1).to({x:142.1,y:63.6},0).wait(1).to({x:143.1},0).wait(1).to({x:144.1,y:63.7},0).wait(1).to({x:145.1},0).wait(1).to({x:146.1},0).wait(1).to({x:147,y:63.8},0).wait(1).to({x:148},0).wait(1).to({x:149,y:63.9},0).wait(1).to({x:150},0).wait(1).to({x:151},0).wait(1).to({x:152},0).wait(1).to({x:153,y:64},0).wait(1).to({x:154},0).wait(1).to({x:155},0).wait(1).to({x:156},0).wait(1).to({x:157},0).wait(1).to({x:158},0).wait(1).to({x:159},0).wait(1).to({x:160},0).wait(1).to({x:161},0).wait(1).to({x:162},0).wait(1).to({x:163},0).wait(1).to({x:164},0).wait(1).to({x:165},0).wait(1).to({x:165.9},0).wait(1).to({x:166.9},0).wait(1).to({x:167.9,y:63.9},0).wait(1).to({x:168.9},0).wait(1).to({x:169.9},0).wait(1).to({x:170.9},0).wait(1).to({x:171.9,y:63.8},0).wait(1).to({x:172.9},0).wait(1).to({x:173.9,y:63.7},0).wait(1).to({x:174.9},0).wait(1).to({x:175.9,y:63.6},0).wait(1).to({x:176.9},0).wait(1).to({x:177.9,y:63.5},0).wait(1).to({x:178.9},0).wait(1).to({x:179.9,y:63.4},0).wait(1).to({x:180.9,y:63.3},0).wait(1).to({x:181.9},0).wait(1).to({x:182.8,y:63.2},0).wait(1).to({x:183.8,y:63.1},0).wait(1).to({x:184.8,y:63},0).wait(1).to({x:185.8},0).wait(1).to({x:186.8,y:62.9},0).wait(1).to({x:187.8,y:62.8},0).wait(1).to({x:188.8,y:62.7},0).wait(1).to({x:189.8,y:62.6},0).wait(1).to({x:190.8,y:62.5},0).wait(1).to({x:191.8,y:62.3},0).wait(1).to({x:192.7,y:62.2},0).wait(1).to({x:193.7,y:62.1},0).wait(1).to({x:194.7,y:62},0).wait(1).to({x:195.7,y:61.8},0).wait(1).to({x:196.7,y:61.7},0).wait(1).to({x:197.7,y:61.6},0).wait(1).to({x:198.7,y:61.4},0).wait(1).to({x:199.6,y:61.3},0).wait(1).to({x:200.6,y:61.1},0).wait(1).to({x:201.6,y:60.9},0).wait(1).to({x:202.6,y:60.8},0).wait(1).to({x:203.6,y:60.6},0).wait(1).to({x:204.5,y:60.4},0).wait(1).to({x:205.5,y:60.2},0).wait(1).to({x:206.5,y:60},0).wait(1).to({x:207.5,y:59.7},0).wait(1).to({x:208.4,y:59.5},0).wait(1).to({x:209.4,y:59.3},0).wait(1).to({x:210.4,y:59},0).wait(1).to({x:211.3,y:58.8},0).wait(1).to({x:212.3,y:58.5},0).wait(1).to({x:213.2,y:58.2},0).wait(1).to({x:214.2,y:57.9},0).wait(1).to({x:215.1,y:57.6},0).wait(1).to({x:216,y:57.2},0).wait(1));

	// pgreenfront
	this.agro = new lib.green();
	this.agro.parent = this;
	this.agro.setTransform(56.9,114.3,0.7,0.7,0,0,0,40.5,40.5);

	this.timeline.addTween(cjs.Tween.get(this.agro).wait(1).to({x:57.7,y:114.5},0).wait(1).to({x:58.4,y:114.7},0).wait(1).to({x:59.2,y:114.9},0).wait(1).to({x:59.9,y:115.1},0).wait(1).to({x:60.7,y:115.3},0).wait(1).to({x:61.5,y:115.5},0).wait(1).to({x:62.2,y:115.7},0).wait(1).to({x:63,y:115.9},0).wait(1).to({x:63.7,y:116.1},0).wait(1).to({x:64.5,y:116.3},0).wait(1).to({x:65.2,y:116.5},0).wait(1).to({x:66,y:116.6},0).wait(1).to({x:66.8,y:116.8},0).wait(1).to({x:67.5,y:117},0).wait(1).to({x:68.3,y:117.2},0).wait(1).to({x:69,y:117.4},0).wait(1).to({x:69.8,y:117.6},0).wait(1).to({x:70.6,y:117.8},0).wait(1).to({x:71.3,y:118},0).wait(1).to({x:72.1,y:118.2},0).wait(1).to({x:72.8,y:118.4},0).wait(1).to({x:73.6,y:118.6},0).wait(1).to({x:74.4,y:118.8},0).wait(1).to({x:75.1,y:119},0).wait(1).to({x:75.9,y:119.2},0).wait(1).to({x:76.6,y:119.3},0).wait(1).to({x:77.4,y:119.5},0).wait(1).to({x:78.2,y:119.7},0).wait(1).to({x:78.9,y:119.9},0).wait(1).to({x:79.7,y:120.1},0).wait(1).to({x:80.4,y:120.3},0).wait(1).to({x:81.2,y:120.5},0).wait(1).to({x:82,y:120.7},0).wait(1).to({x:82.7,y:120.8},0).wait(1).to({x:83.5,y:121},0).wait(1).to({x:84.2,y:121.2},0).wait(1).to({x:85,y:121.4},0).wait(1).to({x:85.8,y:121.6},0).wait(1).to({x:86.5,y:121.7},0).wait(1).to({x:87.3,y:121.9},0).wait(1).to({x:88.1,y:122.1},0).wait(1).to({x:88.8,y:122.3},0).wait(1).to({x:89.6,y:122.5},0).wait(1).to({x:90.4,y:122.6},0).wait(1).to({x:91.1,y:122.8},0).wait(1).to({x:91.9,y:123},0).wait(1).to({x:92.6,y:123.1},0).wait(1).to({x:93.4,y:123.3},0).wait(1).to({x:94.2,y:123.5},0).wait(1).to({x:94.9,y:123.6},0).wait(1).to({x:95.7,y:123.8},0).wait(1).to({x:96.5,y:124},0).wait(1).to({x:97.2,y:124.1},0).wait(1).to({x:98,y:124.3},0).wait(1).to({x:98.8,y:124.4},0).wait(1).to({x:99.5,y:124.6},0).wait(1).to({x:100.3,y:124.7},0).wait(1).to({x:101.1,y:124.9},0).wait(1).to({x:101.9,y:125},0).wait(1).to({x:102.6,y:125.2},0).wait(1).to({x:103.4,y:125.3},0).wait(1).to({x:104.2,y:125.5},0).wait(1).to({x:104.9,y:125.6},0).wait(1).to({x:105.7,y:125.7},0).wait(1).to({x:106.5,y:125.9},0).wait(1).to({x:107.3,y:126},0).wait(1).to({x:108,y:126.1},0).wait(1).to({x:108.8,y:126.3},0).wait(1).to({x:109.6,y:126.4},0).wait(1).to({x:110.3,y:126.5},0).wait(1).to({x:111.1,y:126.6},0).wait(1).to({x:111.9,y:126.8},0).wait(1).to({x:112.7,y:126.9},0).wait(1).to({x:113.4,y:127},0).wait(1).to({x:114.2,y:127.1},0).wait(1).to({x:115,y:127.2},0).wait(1).to({x:115.8,y:127.3},0).wait(1).to({x:116.5,y:127.4},0).wait(1).to({x:117.3,y:127.5},0).wait(1).to({x:118.1,y:127.6},0).wait(1).to({x:118.9,y:127.7},0).wait(1).to({x:119.7,y:127.8},0).wait(1).to({x:120.4,y:127.9},0).wait(1).to({x:121.2,y:128},0).wait(1).to({x:122},0).wait(1).to({x:122.8,y:128.1},0).wait(1).to({x:123.6,y:128.2},0).wait(1).to({x:124.3,y:128.3},0).wait(1).to({x:125.1,y:128.4},0).wait(1).to({x:125.9},0).wait(1).to({x:126.7,y:128.5},0).wait(1).to({x:127.5,y:128.6},0).wait(1).to({x:128.2},0).wait(1).to({x:129,y:128.7},0).wait(1).to({x:129.8},0).wait(1).to({x:130.6,y:128.8},0).wait(1).to({x:131.4,y:128.9},0).wait(1).to({x:132.1},0).wait(1).to({x:132.9,y:129},0).wait(1).to({x:133.7},0).wait(1).to({x:134.5,y:129.1},0).wait(1).to({x:135.3},0).wait(1).to({x:136.1},0).wait(1).to({x:136.8,y:129.2},0).wait(1).to({x:137.6},0).wait(1).to({x:138.4},0).wait(1).to({x:139.2,y:129.3},0).wait(1).to({x:140},0).wait(1).to({x:140.7},0).wait(1).to({x:141.5,y:129.4},0).wait(1).to({x:142.3},0).wait(1).to({x:143.1},0).wait(1).to({x:143.9},0).wait(1).to({x:144.7},0).wait(1).to({x:145.4,y:129.5},0).wait(1).to({x:146.2},0).wait(1).to({x:147},0).wait(1).to({x:147.8},0).wait(1).to({x:148.7},0).wait(1).to({x:149.6},0).wait(1).to({x:150.5},0).wait(1).to({x:151.4,y:129.4},0).wait(1).to({x:152.3},0).wait(1).to({x:153.2},0).wait(1).to({x:154.1},0).wait(1).to({x:155},0).wait(1).to({x:156,y:129.3},0).wait(1).to({x:156.9},0).wait(1).to({x:157.8},0).wait(1).to({x:158.7},0).wait(1).to({x:159.6,y:129.2},0).wait(1).to({x:160.5},0).wait(1).to({x:161.4},0).wait(1).to({x:162.3,y:129.1},0).wait(1).to({x:163.2},0).wait(1).to({x:164.1},0).wait(1).to({x:165,y:129},0).wait(1).to({x:165.9},0).wait(1).to({x:166.8,y:128.9},0).wait(1).to({x:167.7},0).wait(1).to({x:168.6,y:128.8},0).wait(1).to({x:169.5},0).wait(1).to({x:170.4,y:128.7},0).wait(1).to({x:171.3},0).wait(1).to({x:172.2,y:128.6},0).wait(1).to({x:173.1},0).wait(1).to({x:174.1,y:128.5},0).wait(1).to({x:175,y:128.4},0).wait(1).to({x:175.9},0).wait(1).to({x:176.8,y:128.3},0).wait(1).to({x:177.7,y:128.2},0).wait(1).to({x:178.6},0).wait(1).to({x:179.5,y:128.1},0).wait(1).to({x:180.4,y:128},0).wait(1).to({x:181.3,y:127.9},0).wait(1).to({x:182.2},0).wait(1).to({x:183.1,y:127.8},0).wait(1).to({x:184,y:127.7},0).wait(1).to({x:184.9,y:127.6},0).wait(1).to({x:185.8,y:127.5},0).wait(1).to({x:186.7,y:127.4},0).wait(1).to({x:187.6,y:127.3},0).wait(1).to({x:188.5,y:127.2},0).wait(1).to({x:189.4,y:127.1},0).wait(1).to({x:190.3,y:127},0).wait(1).to({x:191.2,y:126.9},0).wait(1).to({x:192.1,y:126.8},0).wait(1).to({x:193,y:126.7},0).wait(1).to({x:193.9,y:126.6},0).wait(1).to({x:194.8,y:126.5},0).wait(1).to({x:195.7,y:126.4},0).wait(1).to({x:196.6,y:126.3},0).wait(1).to({x:197.5,y:126.2},0).wait(1).to({x:198.4,y:126.1},0).wait(1).to({x:199.3,y:125.9},0).wait(1).to({x:200.2,y:125.8},0).wait(1).to({x:201.1,y:125.7},0).wait(1).to({x:202,y:125.6},0).wait(1).to({x:202.9,y:125.4},0).wait(1).to({x:203.8,y:125.3},0).wait(1).to({x:204.7,y:125.2},0).wait(1).to({x:205.6,y:125},0).wait(1).to({x:206.5,y:124.9},0).wait(1).to({x:207.4,y:124.8},0).wait(1).to({x:208.3,y:124.6},0).wait(1).to({x:209.1,y:124.5},0).wait(1).to({x:210,y:124.3},0).wait(1).to({x:210.9,y:124.2},0).wait(1).to({x:211.8,y:124},0).wait(1).to({x:212.7,y:123.9},0).wait(1).to({x:213.6,y:123.7},0).wait(1).to({x:214.5,y:123.6},0).wait(1).to({x:215.4,y:123.4},0).wait(1).to({x:216.3,y:123.2},0).wait(1).to({x:217.2,y:123.1},0).wait(1).to({x:218.1,y:122.9},0).wait(1).to({x:219,y:122.7},0).wait(1).to({x:219.9,y:122.6},0).wait(1).to({x:220.7,y:122.4},0).wait(1).to({x:221.6,y:122.2},0).wait(1).to({x:222.5,y:122.1},0).wait(1).to({x:223.4,y:121.9},0).wait(1).to({x:224.3,y:121.7},0).wait(1).to({x:225.2,y:121.5},0).wait(1).to({x:226.1,y:121.3},0).wait(1).to({x:227,y:121.2},0).wait(1).to({x:227.8,y:121},0).wait(1).to({x:228.7,y:120.8},0).wait(1).to({x:229.6,y:120.6},0).wait(1).to({x:230.5,y:120.4},0).wait(1).to({x:231.4,y:120.2},0).wait(1).to({x:232.3,y:120},0).wait(1).to({x:233.2,y:119.8},0).wait(1).to({x:234,y:119.6},0).wait(1).to({x:234.9,y:119.4},0).wait(1).to({x:235.8,y:119.2},0).wait(1).to({x:236.7,y:119},0).wait(1).to({x:237.6,y:118.8},0).wait(1).to({x:238.5,y:118.6},0).wait(1).to({x:239.3,y:118.4},0).wait(1).to({x:240.2,y:118.2},0).wait(1).to({x:241.1,y:118},0).wait(1).to({x:242,y:117.8},0).wait(1).to({x:242.9,y:117.6},0).wait(1).to({x:243.8,y:117.4},0).wait(1).to({x:244.6,y:117.2},0).wait(1).to({x:245.5,y:116.9},0).wait(1).to({x:246.4,y:116.7},0).wait(1).to({x:247.3,y:116.5},0).wait(1).to({x:248.2,y:116.3},0).wait(1).to({x:249,y:116.1},0).wait(1).to({x:249.9,y:115.8},0).wait(1).to({x:250.8,y:115.6},0).wait(1).to({x:251.7,y:115.4},0).wait(1).to({x:252.5,y:115.2},0).wait(1).to({x:253.4,y:114.9},0).wait(1).to({x:254.3,y:114.7},0).wait(1).to({x:255.2,y:114.5},0).wait(1).to({x:256,y:114.2},0).to({_off:true},1).wait(241));

	// pyellowfront
	this.instance_3 = new lib.yellow();
	this.instance_3.parent = this;
	this.instance_3.setTransform(56.9,114.3,0.7,0.7,0,0,0,40.6,40.5);
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(118).to({_off:false},0).wait(1).to({regX:40.5,x:57.6,y:114.5},0).wait(1).to({x:58.3,y:114.7},0).wait(1).to({x:59.1,y:114.9},0).wait(1).to({x:59.8,y:115.1},0).wait(1).to({x:60.5,y:115.3},0).wait(1).to({x:61.3,y:115.5},0).wait(1).to({x:62,y:115.7},0).wait(1).to({x:62.7,y:115.8},0).wait(1).to({x:63.5,y:116},0).wait(1).to({x:64.2,y:116.2},0).wait(1).to({x:65,y:116.4},0).wait(1).to({x:65.7,y:116.6},0).wait(1).to({x:66.4,y:116.8},0).wait(1).to({x:67.2,y:117},0).wait(1).to({x:67.9,y:117.2},0).wait(1).to({x:68.6,y:117.4},0).wait(1).to({x:69.4,y:117.6},0).wait(1).to({x:70.1,y:117.8},0).wait(1).to({x:70.9,y:117.9},0).wait(1).to({x:71.6,y:118.1},0).wait(1).to({x:72.3,y:118.3},0).wait(1).to({x:73.1,y:118.5},0).wait(1).to({x:73.8,y:118.7},0).wait(1).to({x:74.5,y:118.9},0).wait(1).to({x:75.3,y:119.1},0).wait(1).to({x:76,y:119.2},0).wait(1).to({x:76.8,y:119.4},0).wait(1).to({x:77.5,y:119.6},0).wait(1).to({x:78.2,y:119.8},0).wait(1).to({x:79,y:120},0).wait(1).to({x:79.7,y:120.2},0).wait(1).to({x:80.5,y:120.3},0).wait(1).to({x:81.2,y:120.5},0).wait(1).to({x:81.9,y:120.7},0).wait(1).to({x:82.7,y:120.9},0).wait(1).to({x:83.4,y:121},0).wait(1).to({x:84.2,y:121.2},0).wait(1).to({x:84.9,y:121.4},0).wait(1).to({x:85.7,y:121.5},0).wait(1).to({x:86.4,y:121.7},0).wait(1).to({x:87.1,y:121.9},0).wait(1).to({x:87.9,y:122},0).wait(1).to({x:88.6,y:122.2},0).wait(1).to({x:89.4,y:122.4},0).wait(1).to({x:90.1,y:122.5},0).wait(1).to({x:90.9,y:122.7},0).wait(1).to({x:91.6,y:122.8},0).wait(1).to({x:92.3,y:123},0).wait(1).to({x:93.1,y:123.1},0).wait(1).to({x:93.8,y:123.3},0).wait(1).to({x:94.6,y:123.4},0).wait(1).to({x:95.3,y:123.6},0).wait(1).to({x:96.1,y:123.7},0).wait(1).to({x:96.8,y:123.9},0).wait(1).to({x:97.6,y:124},0).wait(1).to({x:98.3,y:124.2},0).wait(1).to({x:99.1,y:124.3},0).wait(1).to({x:99.8,y:124.4},0).wait(1).to({x:100.6,y:124.6},0).wait(1).to({x:101.3,y:124.7},0).wait(1).to({x:102.1,y:124.8},0).wait(1).to({x:102.8,y:125},0).wait(1).to({x:103.6,y:125.1},0).wait(1).to({x:104.3,y:125.2},0).wait(1).to({x:105.1,y:125.3},0).wait(1).to({x:105.8,y:125.4},0).wait(1).to({x:106.6,y:125.6},0).wait(1).to({x:107.3,y:125.7},0).wait(1).to({x:108.1,y:125.8},0).wait(1).to({x:108.8,y:125.9},0).wait(1).to({x:109.6,y:126},0).wait(1).to({x:110.3,y:126.1},0).wait(1).to({x:111.1,y:126.2},0).wait(1).to({x:111.9,y:126.3},0).wait(1).to({x:112.6,y:126.4},0).wait(1).to({x:113.4,y:126.5},0).wait(1).to({x:114.1,y:126.6},0).wait(1).to({x:114.9,y:126.7},0).wait(1).to({x:115.6,y:126.8},0).wait(1).to({x:116.4,y:126.9},0).wait(1).to({x:117.1,y:127},0).wait(1).to({x:117.9,y:127.1},0).wait(1).to({x:118.7,y:127.2},0).wait(1).to({x:119.4,y:127.3},0).wait(1).to({x:120.2,y:127.4},0).wait(1).to({x:120.9,y:127.5},0).wait(1).to({x:121.7},0).wait(1).to({x:122.4,y:127.6},0).wait(1).to({x:123.2,y:127.7},0).wait(1).to({x:124,y:127.8},0).wait(1).to({x:124.7},0).wait(1).to({x:125.5,y:127.9},0).wait(1).to({x:126.2,y:128},0).wait(1).to({x:127,y:128.1},0).wait(1).to({x:127.7},0).wait(1).to({x:128.5,y:128.2},0).wait(1).to({x:129.3,y:128.3},0).wait(1).to({x:130},0).wait(1).to({x:130.8,y:128.4},0).wait(1).to({x:131.5,y:128.5},0).wait(1).to({x:132.3},0).wait(1).to({x:133.1,y:128.6},0).wait(1).to({x:133.8},0).wait(1).to({x:134.6,y:128.7},0).wait(1).to({x:135.3},0).wait(1).to({x:136.1,y:128.8},0).wait(1).to({x:136.9},0).wait(1).to({x:137.6,y:128.9},0).wait(1).to({x:138.4},0).wait(1).to({x:139.1,y:129},0).wait(1).to({x:139.9},0).wait(1).to({x:140.7,y:129.1},0).wait(1).to({x:141.4},0).wait(1).to({x:142.2,y:129.2},0).wait(1).to({x:142.9},0).wait(1).to({x:143.7,y:129.3},0).wait(1).to({x:144.5},0).wait(1).to({x:145.2},0).wait(1).to({x:146,y:129.4},0).wait(1).to({x:146.7},0).wait(1).to({x:147.5},0).wait(1).to({x:148.4},0).wait(1).to({x:149.3,y:129.3},0).wait(1).to({x:150.2},0).wait(1).to({x:151.1,y:129.2},0).wait(1).to({x:152},0).wait(1).to({x:152.8,y:129.1},0).wait(1).to({x:153.7},0).wait(1).to({x:154.6,y:129},0).wait(1).to({x:155.5,y:128.9},0).wait(1).to({x:156.4},0).wait(1).to({x:157.3,y:128.8},0).wait(1).to({x:158.2,y:128.7},0).wait(1).to({x:159.1},0).wait(1).to({x:160,y:128.6},0).wait(1).to({x:160.9,y:128.5},0).wait(1).to({x:161.7},0).wait(1).to({x:162.6,y:128.4},0).wait(1).to({x:163.5,y:128.3},0).wait(1).to({x:164.4,y:128.2},0).wait(1).to({x:165.3},0).wait(1).to({x:166.2,y:128.1},0).wait(1).to({x:167.1,y:128},0).wait(1).to({x:168,y:127.9},0).wait(1).to({x:168.9,y:127.8},0).wait(1).to({x:169.7},0).wait(1).to({x:170.6,y:127.7},0).wait(1).to({x:171.5,y:127.6},0).wait(1).to({x:172.4,y:127.5},0).wait(1).to({x:173.3,y:127.4},0).wait(1).to({x:174.2,y:127.3},0).wait(1).to({x:175.1,y:127.2},0).wait(1).to({x:176},0).wait(1).to({x:176.8,y:127.1},0).wait(1).to({x:177.7,y:127},0).wait(1).to({x:178.6,y:126.9},0).wait(1).to({x:179.5,y:126.8},0).wait(1).to({x:180.4,y:126.7},0).wait(1).to({x:181.3,y:126.6},0).wait(1).to({x:182.2,y:126.5},0).wait(1).to({x:183.1,y:126.4},0).wait(1).to({x:183.9,y:126.3},0).wait(1).to({x:184.8,y:126.2},0).wait(1).to({x:185.7,y:126.1},0).wait(1).to({x:186.6,y:126},0).wait(1).to({x:187.5,y:125.9},0).wait(1).to({x:188.4,y:125.8},0).wait(1).to({x:189.3,y:125.6},0).wait(1).to({x:190.1,y:125.5},0).wait(1).to({x:191,y:125.4},0).wait(1).to({x:191.9,y:125.3},0).wait(1).to({x:192.8,y:125.2},0).wait(1).to({x:193.7,y:125.1},0).wait(1).to({x:194.6,y:125},0).wait(1).to({x:195.4,y:124.8},0).wait(1).to({x:196.3,y:124.7},0).wait(1).to({x:197.2,y:124.6},0).wait(1).to({x:198.1,y:124.5},0).wait(1).to({x:199,y:124.4},0).wait(1).to({x:199.9,y:124.2},0).wait(1).to({x:200.8,y:124.1},0).wait(1).to({x:201.6,y:124},0).wait(1).to({x:202.5,y:123.9},0).wait(1).to({x:203.4,y:123.7},0).wait(1).to({x:204.3,y:123.6},0).wait(1).to({x:205.2,y:123.5},0).wait(1).to({x:206,y:123.3},0).wait(1).to({x:206.9,y:123.2},0).wait(1).to({x:207.8,y:123.1},0).wait(1).to({x:208.7,y:122.9},0).wait(1).to({x:209.6,y:122.8},0).wait(1).to({x:210.5,y:122.7},0).wait(1).to({x:211.3,y:122.5},0).wait(1).to({x:212.2,y:122.4},0).wait(1).to({x:213.1,y:122.2},0).wait(1).to({x:214,y:122.1},0).wait(1).to({x:214.9,y:121.9},0).wait(1).to({x:215.7,y:121.8},0).wait(1).to({x:216.6,y:121.7},0).wait(1).to({x:217.5,y:121.5},0).wait(1).to({x:218.4,y:121.4},0).wait(1).to({x:219.3,y:121.2},0).wait(1).to({x:220.1,y:121.1},0).wait(1).to({x:221,y:120.9},0).wait(1).to({x:221.9,y:120.8},0).wait(1).to({x:222.8,y:120.6},0).wait(1).to({x:223.7,y:120.5},0).wait(1).to({x:224.5,y:120.3},0).wait(1).to({x:225.4,y:120.2},0).wait(1).to({x:226.3,y:120},0).wait(1).to({x:227.2,y:119.8},0).wait(1).to({x:228.1,y:119.7},0).wait(1).to({x:228.9,y:119.5},0).wait(1).to({x:229.8,y:119.4},0).wait(1).to({x:230.7,y:119.2},0).wait(1).to({x:231.6,y:119.1},0).wait(1).to({x:232.4,y:118.9},0).wait(1).to({x:233.3,y:118.7},0).wait(1).to({x:234.2,y:118.6},0).wait(1).to({x:235.1,y:118.4},0).wait(1).to({x:236,y:118.2},0).wait(1).to({x:236.8,y:118.1},0).wait(1).to({x:237.7,y:117.9},0).wait(1).to({x:238.6,y:117.8},0).wait(1).to({x:239.5,y:117.6},0).wait(1).to({x:240.3,y:117.4},0).wait(1).to({x:241.2,y:117.3},0).wait(1).to({x:242.1,y:117.1},0).wait(1).to({x:243,y:116.9},0).wait(1).to({x:243.8,y:116.8},0).wait(1).to({x:244.7,y:116.6},0).wait(1).to({x:245.6,y:116.4},0).wait(1).to({x:246.5,y:116.2},0).wait(1).to({x:247.3,y:116.1},0).wait(1).to({x:248.2,y:115.9},0).wait(1).to({x:249.1,y:115.7},0).wait(1).to({x:250,y:115.6},0).wait(1).to({x:250.8,y:115.4},0).wait(1).to({x:251.7,y:115.2},0).wait(1).to({x:252.6,y:115},0).wait(1).to({x:253.5,y:114.9},0).wait(1).to({x:254.3,y:114.7},0).wait(1).to({x:255.2,y:114.5},0).wait(1).to({x:256.1,y:114.3},0).to({_off:true},1).wait(118));

	// pwhitefront
	this.instance_4 = new lib.white();
	this.instance_4.parent = this;
	this.instance_4.setTransform(99.9,174.3,0.7,0.7,0,0,0,40.6,40.5);

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(1).to({regX:40.5,x:100.8,y:174.5},0).wait(1).to({x:101.8,y:174.7},0).wait(1).to({x:102.8,y:174.9},0).wait(1).to({x:103.7,y:175.2},0).wait(1).to({x:104.7,y:175.4},0).wait(1).to({x:105.7,y:175.6},0).wait(1).to({x:106.7,y:175.8},0).wait(1).to({x:107.6,y:176},0).wait(1).to({x:108.6,y:176.2},0).wait(1).to({x:109.6,y:176.4},0).wait(1).to({x:110.6,y:176.6},0).wait(1).to({x:111.5,y:176.8},0).wait(1).to({x:112.5,y:176.9},0).wait(1).to({x:113.5,y:177.1},0).wait(1).to({x:114.5,y:177.3},0).wait(1).to({x:115.4,y:177.5},0).wait(1).to({x:116.4,y:177.7},0).wait(1).to({x:117.4,y:177.8},0).wait(1).to({x:118.4,y:178},0).wait(1).to({x:119.4,y:178.2},0).wait(1).to({x:120.3,y:178.3},0).wait(1).to({x:121.3,y:178.5},0).wait(1).to({x:122.3,y:178.7},0).wait(1).to({x:123.3,y:178.8},0).wait(1).to({x:124.3,y:179},0).wait(1).to({x:125.2,y:179.1},0).wait(1).to({x:126.2,y:179.3},0).wait(1).to({x:127.2,y:179.4},0).wait(1).to({x:128.2,y:179.5},0).wait(1).to({x:129.2,y:179.7},0).wait(1).to({x:130.2,y:179.8},0).wait(1).to({x:131.2,y:179.9},0).wait(1).to({x:132.1,y:180.1},0).wait(1).to({x:133.1,y:180.2},0).wait(1).to({x:134.1,y:180.3},0).wait(1).to({x:135.1,y:180.4},0).wait(1).to({x:136.1,y:180.5},0).wait(1).to({x:137.1,y:180.6},0).wait(1).to({x:138.1,y:180.7},0).wait(1).to({x:139.1,y:180.8},0).wait(1).to({x:140.1,y:180.9},0).wait(1).to({x:141.1,y:181},0).wait(1).to({x:142},0).wait(1).to({x:143,y:181.1},0).wait(1).to({x:144,y:181.2},0).wait(1).to({x:145},0).wait(1).to({x:146,y:181.3},0).wait(1).to({x:147,y:181.4},0).wait(1).to({x:148},0).wait(1).to({x:149,y:181.5},0).wait(1).to({x:150},0).wait(1).to({x:151},0).wait(1).to({x:152,y:181.6},0).wait(1).to({x:153},0).wait(1).to({x:154},0).wait(1).to({x:155},0).wait(1).to({x:156},0).wait(1).to({x:156.9},0).wait(1).to({x:157.9},0).wait(1).to({x:158.9},0).wait(1).to({x:159.9},0).wait(1).to({x:160.9},0).wait(1).to({x:161.9},0).wait(1).to({x:162.9},0).wait(1).to({x:163.9,y:181.5},0).wait(1).to({x:164.9},0).wait(1).to({x:165.9},0).wait(1).to({x:166.9,y:181.4},0).wait(1).to({x:167.9},0).wait(1).to({x:168.9,y:181.3},0).wait(1).to({x:169.9},0).wait(1).to({x:170.9,y:181.2},0).wait(1).to({x:171.9,y:181.1},0).wait(1).to({x:172.8},0).wait(1).to({x:173.8,y:181},0).wait(1).to({x:174.8,y:180.9},0).wait(1).to({x:175.8,y:180.8},0).wait(1).to({x:176.8,y:180.7},0).wait(1).to({x:177.8,y:180.6},0).wait(1).to({x:178.8,y:180.5},0).wait(1).to({x:179.8,y:180.4},0).wait(1).to({x:180.8,y:180.3},0).wait(1).to({x:181.8,y:180.2},0).wait(1).to({x:182.7,y:180.1},0).wait(1).to({x:183.7,y:180},0).wait(1).to({x:184.7,y:179.9},0).wait(1).to({x:185.7,y:179.8},0).wait(1).to({x:186.7,y:179.6},0).wait(1).to({x:187.7,y:179.5},0).wait(1).to({x:188.7,y:179.4},0).wait(1).to({x:189.6,y:179.2},0).wait(1).to({x:190.6,y:179.1},0).wait(1).to({x:191.6,y:178.9},0).wait(1).to({x:192.6,y:178.8},0).wait(1).to({x:193.6,y:178.6},0).wait(1).to({x:194.6,y:178.5},0).wait(1).to({x:195.5,y:178.3},0).wait(1).to({x:196.5,y:178.1},0).wait(1).to({x:197.5,y:178},0).wait(1).to({x:198.5,y:177.8},0).wait(1).to({x:199.5,y:177.6},0).wait(1).to({x:200.4,y:177.5},0).wait(1).to({x:201.4,y:177.3},0).wait(1).to({x:202.4,y:177.1},0).wait(1).to({x:203.4,y:176.9},0).wait(1).to({x:204.4,y:176.7},0).wait(1).to({x:205.3,y:176.6},0).wait(1).to({x:206.3,y:176.4},0).wait(1).to({x:207.3,y:176.2},0).wait(1).to({x:208.3,y:176},0).wait(1).to({x:209.2,y:175.8},0).wait(1).to({x:210.2,y:175.6},0).wait(1).to({x:211.2,y:175.4},0).wait(1).to({x:212.2,y:175.2},0).wait(1).to({x:213.1,y:175},0).wait(1).to({x:214.1,y:174.8},0).wait(1).to({x:215.1,y:174.6},0).wait(1).to({x:216,y:174.3},0).to({_off:true},1).wait(120).to({_off:false,regX:40.6,x:99.9},0).wait(1).to({regX:40.5,x:100.8,y:174.5},0).wait(1).to({x:101.7,y:174.7},0).wait(1).to({x:102.6,y:174.9},0).wait(1).to({x:103.6,y:175.1},0).wait(1).to({x:104.5,y:175.3},0).wait(1).to({x:105.4,y:175.5},0).wait(1).to({x:106.4,y:175.7},0).wait(1).to({x:107.3,y:175.9},0).wait(1).to({x:108.2,y:176.1},0).wait(1).to({x:109.2,y:176.3},0).wait(1).to({x:110.1,y:176.5},0).wait(1).to({x:111.1,y:176.7},0).wait(1).to({x:112,y:176.8},0).wait(1).to({x:112.9,y:177},0).wait(1).to({x:113.9,y:177.2},0).wait(1).to({x:114.8,y:177.4},0).wait(1).to({x:115.7,y:177.5},0).wait(1).to({x:116.7,y:177.7},0).wait(1).to({x:117.6,y:177.9},0).wait(1).to({x:118.6,y:178},0).wait(1).to({x:119.5,y:178.2},0).wait(1).to({x:120.4,y:178.4},0).wait(1).to({x:121.4,y:178.5},0).wait(1).to({x:122.3,y:178.7},0).wait(1).to({x:123.3,y:178.8},0).wait(1).to({x:124.2,y:179},0).wait(1).to({x:125.2,y:179.1},0).wait(1).to({x:126.1,y:179.2},0).wait(1).to({x:127,y:179.4},0).wait(1).to({x:128,y:179.5},0).wait(1).to({x:128.9,y:179.6},0).wait(1).to({x:129.9,y:179.8},0).wait(1).to({x:130.8,y:179.9},0).wait(1).to({x:131.8,y:180},0).wait(1).to({x:132.7,y:180.1},0).wait(1).to({x:133.7,y:180.2},0).wait(1).to({x:134.6,y:180.3},0).wait(1).to({x:135.6,y:180.4},0).wait(1).to({x:136.5,y:180.5},0).wait(1).to({x:137.5,y:180.6},0).wait(1).to({x:138.4,y:180.7},0).wait(1).to({x:139.4,y:180.8},0).wait(1).to({x:140.3,y:180.9},0).wait(1).to({x:141.3,y:181},0).wait(1).to({x:142.2},0).wait(1).to({x:143.2,y:181.1},0).wait(1).to({x:144.1,y:181.2},0).wait(1).to({x:145.1},0).wait(1).to({x:146,y:181.3},0).wait(1).to({x:147,y:181.4},0).wait(1).to({x:147.9},0).wait(1).to({x:148.9},0).wait(1).to({x:149.8,y:181.5},0).wait(1).to({x:150.8},0).wait(1).to({x:151.7,y:181.6},0).wait(1).to({x:152.7},0).wait(1).to({x:153.6},0).wait(1).to({x:154.6},0).wait(1).to({x:155.6},0).wait(1).to({x:156.5},0).wait(1).to({x:157.5},0).wait(1).to({x:158.4},0).wait(1).to({x:159.4},0).wait(1).to({x:160.3},0).wait(1).to({x:161.3},0).wait(1).to({x:162.2},0).wait(1).to({x:163.2},0).wait(1).to({x:164.1,y:181.5},0).wait(1).to({x:165.1},0).wait(1).to({x:166},0).wait(1).to({x:167,y:181.4},0).wait(1).to({x:168},0).wait(1).to({x:168.9,y:181.3},0).wait(1).to({x:169.9},0).wait(1).to({x:170.8,y:181.2},0).wait(1).to({x:171.8,y:181.1},0).wait(1).to({x:172.7},0).wait(1).to({x:173.7,y:181},0).wait(1).to({x:174.6,y:180.9},0).wait(1).to({x:175.6,y:180.8},0).wait(1).to({x:176.5},0).wait(1).to({x:177.5,y:180.7},0).wait(1).to({x:178.4,y:180.6},0).wait(1).to({x:179.4,y:180.5},0).wait(1).to({x:180.3,y:180.4},0).wait(1).to({x:181.3,y:180.3},0).wait(1).to({x:182.2,y:180.2},0).wait(1).to({x:183.2,y:180.1},0).wait(1).to({x:184.1,y:180},0).wait(1).to({x:185.1,y:179.8},0).wait(1).to({x:186,y:179.7},0).wait(1).to({x:186.9,y:179.6},0).wait(1).to({x:187.9,y:179.5},0).wait(1).to({x:188.8,y:179.3},0).wait(1).to({x:189.8,y:179.2},0).wait(1).to({x:190.7,y:179.1},0).wait(1).to({x:191.7,y:178.9},0).wait(1).to({x:192.6,y:178.8},0).wait(1).to({x:193.6,y:178.6},0).wait(1).to({x:194.5,y:178.5},0).wait(1).to({x:195.4,y:178.3},0).wait(1).to({x:196.4,y:178.2},0).wait(1).to({x:197.3,y:178},0).wait(1).to({x:198.3,y:177.8},0).wait(1).to({x:199.2,y:177.7},0).wait(1).to({x:200.1,y:177.5},0).wait(1).to({x:201.1,y:177.3},0).wait(1).to({x:202,y:177.2},0).wait(1).to({x:203,y:177},0).wait(1).to({x:203.9,y:176.8},0).wait(1).to({x:204.8,y:176.7},0).wait(1).to({x:205.8,y:176.5},0).wait(1).to({x:206.7,y:176.3},0).wait(1).to({x:207.6,y:176.1},0).wait(1).to({x:208.6,y:175.9},0).wait(1).to({x:209.5,y:175.7},0).wait(1).to({x:210.4,y:175.5},0).wait(1).to({x:211.4,y:175.3},0).wait(1).to({x:212.3,y:175.1},0).wait(1).to({x:213.2,y:175},0).wait(1).to({x:214.2,y:174.8},0).wait(1).to({x:215.1,y:174.6},0).wait(1).to({x:216,y:174.3},0).to({_off:true},1).wait(118));

	// predfront
	this.instance_5 = new lib.red();
	this.instance_5.parent = this;
	this.instance_5.setTransform(56.9,114.3,0.7,0.7,0,0,0,40.6,40.5);
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(239).to({_off:false},0).wait(1).to({regX:40.5,x:57.2,y:114.9},0).wait(1).to({x:57.8,y:115.5},0).wait(1).to({x:58.5,y:116},0).wait(1).to({x:59.1,y:116.5},0).wait(1).to({x:59.8,y:116.9},0).wait(1).to({x:60.5,y:117.4},0).wait(1).to({x:61.2,y:117.7},0).wait(1).to({x:61.9,y:118.1},0).wait(1).to({x:62.7,y:118.4},0).wait(1).to({x:63.4,y:118.8},0).wait(1).to({x:64.2,y:119},0).wait(1).to({x:64.9,y:119.3},0).wait(1).to({x:65.7,y:119.6},0).wait(1).to({x:66.5,y:119.8},0).wait(1).to({x:67.2,y:120},0).wait(1).to({x:68,y:120.3},0).wait(1).to({x:68.8,y:120.5},0).wait(1).to({x:69.5,y:120.7},0).wait(1).to({x:70.3,y:121},0).wait(1).to({x:71.1,y:121.2},0).wait(1).to({x:71.9,y:121.4},0).wait(1).to({x:72.6,y:121.6},0).wait(1).to({x:73.4,y:121.9},0).wait(1).to({x:74.2,y:122.1},0).wait(1).to({x:75,y:122.3},0).wait(1).to({x:75.8,y:122.5},0).wait(1).to({x:76.5,y:122.7},0).wait(1).to({x:77.3,y:122.9},0).wait(1).to({x:78.1,y:123.2},0).wait(1).to({x:78.9,y:123.4},0).wait(1).to({x:79.6,y:123.6},0).wait(1).to({x:80.4,y:123.8},0).wait(1).to({x:81.2,y:124},0).wait(1).to({x:82,y:124.2},0).wait(1).to({x:82.8,y:124.4},0).wait(1).to({x:83.6,y:124.6},0).wait(1).to({x:84.3,y:124.8},0).wait(1).to({x:85.1,y:124.9},0).wait(1).to({x:85.9,y:125.1},0).wait(1).to({x:86.7,y:125.2},0).wait(1).to({x:87.5,y:125.3},0).wait(1).to({x:88.3,y:125.4},0).wait(1).to({x:89.1,y:125.5},0).wait(1).to({x:89.9,y:125.7},0).wait(1).to({x:90.7,y:125.8},0).wait(1).to({x:91.5,y:125.9},0).wait(1).to({x:92.3,y:126},0).wait(1).to({x:93.1,y:126.1},0).wait(1).to({x:93.9,y:126.2},0).wait(1).to({x:94.7,y:126.3},0).wait(1).to({x:95.5,y:126.4},0).wait(1).to({x:96.3,y:126.5},0).wait(1).to({x:97.1,y:126.6},0).wait(1).to({x:97.9,y:126.7},0).wait(1).to({x:98.7,y:126.8},0).wait(1).to({x:99.5,y:126.9},0).wait(1).to({x:100.3,y:127},0).wait(1).to({x:101.1,y:127.1},0).wait(1).to({x:101.9,y:127.2},0).wait(1).to({x:102.7},0).wait(1).to({x:103.5,y:127.3},0).wait(1).to({x:104.3,y:127.4},0).wait(1).to({x:105.1,y:127.5},0).wait(1).to({x:105.9,y:127.6},0).wait(1).to({x:106.7,y:127.7},0).wait(1).to({x:107.5},0).wait(1).to({x:108.3,y:127.8},0).wait(1).to({x:109.1,y:127.9},0).wait(1).to({x:109.9,y:128},0).wait(1).to({x:110.7},0).wait(1).to({x:111.5,y:128.1},0).wait(1).to({x:112.3,y:128.2},0).wait(1).to({x:113.1},0).wait(1).to({x:113.9,y:128.3},0).wait(1).to({x:114.7,y:128.4},0).wait(1).to({x:115.5},0).wait(1).to({x:116.3,y:128.5},0).wait(1).to({x:117.2,y:128.6},0).wait(1).to({x:118},0).wait(1).to({x:118.8,y:128.7},0).wait(1).to({x:119.6},0).wait(1).to({x:120.4,y:128.8},0).wait(1).to({x:121.2},0).wait(1).to({x:122,y:128.9},0).wait(1).to({x:122.8},0).wait(1).to({x:123.6,y:129},0).wait(1).to({x:124.4},0).wait(1).to({x:125.2,y:129.1},0).wait(1).to({x:126},0).wait(1).to({x:126.8,y:129.2},0).wait(1).to({x:127.6},0).wait(1).to({x:128.4,y:129.3},0).wait(1).to({x:129.2},0).wait(1).to({x:130},0).wait(1).to({x:130.8,y:129.4},0).wait(1).to({x:131.6},0).wait(1).to({x:132.4},0).wait(1).to({x:133.3,y:129.5},0).wait(1).to({x:134.1},0).wait(1).to({x:134.9},0).wait(1).to({x:135.7},0).wait(1).to({x:136.5,y:129.6},0).wait(1).to({x:137.3},0).wait(1).to({x:138.1},0).wait(1).to({x:138.9},0).wait(1).to({x:139.7,y:129.7},0).wait(1).to({x:140.5},0).wait(1).to({x:141.3},0).wait(1).to({x:142.1},0).wait(1).to({x:142.9},0).wait(1).to({x:143.7},0).wait(1).to({x:144.5},0).wait(1).to({x:145.3},0).wait(1).to({x:146.2},0).wait(1).to({x:147},0).wait(1).to({x:147.8},0).wait(1).to({x:148.6},0).wait(1).to({x:149.4},0).wait(1).to({x:150.2,y:129.6},0).wait(1).to({x:151},0).wait(1).to({x:151.8},0).wait(1).to({x:152.6,y:129.5},0).wait(1).to({x:153.4,y:129.4},0).wait(1).to({x:154.3,y:129.5},0).wait(1).to({x:155.1,y:129.6},0).wait(1).to({x:156},0).wait(1).to({x:156.9},0).wait(1).to({x:157.7,y:129.7},0).wait(1).to({x:158.6},0).wait(1).to({x:159.5},0).wait(1).to({x:160.3},0).wait(1).to({x:161.2},0).wait(1).to({x:162.1,y:129.8},0).wait(1).to({x:162.9},0).wait(1).to({x:163.8},0).wait(1).to({x:164.7},0).wait(1).to({x:165.6},0).wait(1).to({x:166.4},0).wait(1).to({x:167.3},0).wait(1).to({x:168.2,y:129.7},0).wait(1).to({x:169},0).wait(1).to({x:169.9},0).wait(1).to({x:170.8},0).wait(1).to({x:171.6},0).wait(1).to({x:172.5},0).wait(1).to({x:173.4,y:129.6},0).wait(1).to({x:174.2},0).wait(1).to({x:175.1},0).wait(1).to({x:176,y:129.5},0).wait(1).to({x:176.8},0).wait(1).to({x:177.7},0).wait(1).to({x:178.6,y:129.4},0).wait(1).to({x:179.5},0).wait(1).to({x:180.3},0).wait(1).to({x:181.2,y:129.3},0).wait(1).to({x:182.1},0).wait(1).to({x:182.9,y:129.2},0).wait(1).to({x:183.8},0).wait(1).to({x:184.7,y:129.1},0).wait(1).to({x:185.5},0).wait(1).to({x:186.4,y:129},0).wait(1).to({x:187.3,y:128.9},0).wait(1).to({x:188.1},0).wait(1).to({x:189,y:128.8},0).wait(1).to({x:189.9,y:128.7},0).wait(1).to({x:190.7,y:128.6},0).wait(1).to({x:191.6},0).wait(1).to({x:192.5,y:128.5},0).wait(1).to({x:193.3,y:128.4},0).wait(1).to({x:194.2,y:128.3},0).wait(1).to({x:195,y:128.2},0).wait(1).to({x:195.9,y:128.1},0).wait(1).to({x:196.8,y:128},0).wait(1).to({x:197.6,y:127.9},0).wait(1).to({x:198.5,y:127.8},0).wait(1).to({x:199.4,y:127.7},0).wait(1).to({x:200.2,y:127.6},0).wait(1).to({x:201.1,y:127.4},0).wait(1).to({x:201.9,y:127.3},0).wait(1).to({x:202.8,y:127.2},0).wait(1).to({x:203.6,y:127},0).wait(1).to({x:204.5,y:126.9},0).wait(1).to({x:205.4,y:126.7},0).wait(1).to({x:206.2,y:126.6},0).wait(1).to({x:207.1,y:126.4},0).wait(1).to({x:207.9,y:126.2},0).wait(1).to({x:208.8,y:126.1},0).wait(1).to({x:209.6,y:125.9},0).wait(1).to({x:210.5,y:125.7},0).wait(1).to({x:211.3,y:125.5},0).wait(1).to({x:212.2,y:125.3},0).wait(1).to({x:213,y:125.1},0).wait(1).to({x:213.9,y:125},0).wait(1).to({x:214.7,y:124.8},0).wait(1).to({x:215.6,y:124.6},0).wait(1).to({x:216.4,y:124.4},0).wait(1).to({x:217.2,y:124.2},0).wait(1).to({x:218.1,y:124},0).wait(1).to({x:218.9,y:123.7},0).wait(1).to({x:219.8,y:123.5},0).wait(1).to({x:220.6,y:123.3},0).wait(1).to({x:221.5,y:123.1},0).wait(1).to({x:222.3,y:122.9},0).wait(1).to({x:223.2,y:122.7},0).wait(1).to({x:224,y:122.5},0).wait(1).to({x:224.8,y:122.2},0).wait(1).to({x:225.7,y:122},0).wait(1).to({x:226.5,y:121.8},0).wait(1).to({x:227.4,y:121.6},0).wait(1).to({x:228.2,y:121.4},0).wait(1).to({x:229,y:121.1},0).wait(1).to({x:229.9,y:120.9},0).wait(1).to({x:230.7,y:120.7},0).wait(1).to({x:231.5,y:120.4},0).wait(1).to({x:232.4,y:120.2},0).wait(1).to({x:233.2,y:120},0).wait(1).to({x:234.1,y:119.8},0).wait(1).to({x:234.9,y:119.5},0).wait(1).to({x:235.7,y:119.3},0).wait(1).to({x:236.6,y:119.1},0).wait(1).to({x:237.4,y:118.8},0).wait(1).to({x:238.2,y:118.6},0).wait(1).to({x:239.1,y:118.4},0).wait(1).to({x:239.9,y:118.1},0).wait(1).to({x:240.8,y:117.9},0).wait(1).to({x:241.6,y:117.7},0).wait(1).to({x:242.4,y:117.4},0).wait(1).to({x:243.3,y:117.2},0).wait(1).to({x:244.1,y:117},0).wait(1).to({x:244.9,y:116.7},0).wait(1).to({x:245.8,y:116.5},0).wait(1).to({x:246.6,y:116.3},0).wait(1).to({x:247.5,y:116.1},0).wait(1).to({x:248.3,y:115.8},0).wait(1).to({x:249.1,y:115.6},0).wait(1).to({x:250,y:115.4},0).wait(1).to({x:250.8,y:115.2},0).wait(1).to({x:251.7,y:115},0).wait(1).to({x:252.5,y:114.8},0).wait(1).to({x:253.4,y:114.6},0).wait(1).to({x:254.2,y:114.3},0).wait(1));

	// pblackfront
	this.instance_6 = new lib.black();
	this.instance_6.parent = this;
	this.instance_6.setTransform(99.9,174.4,0.7,0.7,0,0,0,40.6,40.5);
	this.instance_6._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(119).to({_off:false},0).wait(1).to({regX:40.5,x:100.8,y:174.7},0).wait(1).to({x:101.7,y:175},0).wait(1).to({x:102.7,y:175.3},0).wait(1).to({x:103.6,y:175.5},0).wait(1).to({x:104.6,y:175.8},0).wait(1).to({x:105.5,y:176.1},0).wait(1).to({x:106.5,y:176.4},0).wait(1).to({x:107.4,y:176.6},0).wait(1).to({x:108.4,y:176.9},0).wait(1).to({x:109.3,y:177.2},0).wait(1).to({x:110.3,y:177.4},0).wait(1).to({x:111.3,y:177.7},0).wait(1).to({x:112.2,y:177.9},0).wait(1).to({x:113.2,y:178.2},0).wait(1).to({x:114.1,y:178.4},0).wait(1).to({x:115.1,y:178.7},0).wait(1).to({x:116.1,y:178.9},0).wait(1).to({x:117,y:179.2},0).wait(1).to({x:118,y:179.4},0).wait(1).to({x:118.9,y:179.6},0).wait(1).to({x:119.9,y:179.9},0).wait(1).to({x:120.9,y:180.1},0).wait(1).to({x:121.8,y:180.3},0).wait(1).to({x:122.8,y:180.5},0).wait(1).to({x:123.8,y:180.8},0).wait(1).to({x:124.7,y:181},0).wait(1).to({x:125.7,y:181.2},0).wait(1).to({x:126.7,y:181.4},0).wait(1).to({x:127.6,y:181.6},0).wait(1).to({x:128.6,y:181.8},0).wait(1).to({x:129.6,y:182},0).wait(1).to({x:130.5,y:182.2},0).wait(1).to({x:131.5,y:182.4},0).wait(1).to({x:132.5,y:182.6},0).wait(1).to({x:133.4,y:182.8},0).wait(1).to({x:134.4,y:183},0).wait(1).to({x:135.4,y:183.2},0).wait(1).to({x:136.4,y:183.4},0).wait(1).to({x:137.3,y:183.6},0).wait(1).to({x:138.3,y:183.7},0).wait(1).to({x:139.3,y:183.9},0).wait(1).to({x:140.3,y:184.1},0).wait(1).to({x:141.2,y:184.2},0).wait(1).to({x:142.2,y:184.4},0).wait(1).to({x:143.2,y:184.5},0).wait(1).to({x:144.2,y:184.7},0).wait(1).to({x:145.2,y:184.8},0).wait(1).to({x:146.1,y:185},0).wait(1).to({x:147.1,y:185.1},0).wait(1).to({x:148.1,y:185.2},0).wait(1).to({x:149.1,y:185.3},0).wait(1).to({x:150.1,y:185.4},0).wait(1).to({x:151.1,y:185.5},0).wait(1).to({x:152.1,y:185.6},0).wait(1).to({x:153,y:185.7},0).wait(1).to({x:154,y:185.8},0).wait(1).to({x:154.9,y:185.9},0).wait(1).to({x:156},0).wait(1).to({x:157,y:186},0).wait(1).to({x:158},0).wait(1).to({x:158.9},0).wait(1).to({x:159.9},0).wait(1).to({x:160.9},0).wait(1).to({x:161.9,y:185.9},0).wait(1).to({x:162.9},0).wait(1).to({x:163.9},0).wait(1).to({x:164.9,y:185.8},0).wait(1).to({x:165.9,y:185.7},0).wait(1).to({x:166.9},0).wait(1).to({x:167.9,y:185.6},0).wait(1).to({x:168.8,y:185.4},0).wait(1).to({x:169.8,y:185.3},0).wait(1).to({x:170.8,y:185.2},0).wait(1).to({x:171.8,y:185},0).wait(1).to({x:172.8,y:184.9},0).wait(1).to({x:173.7,y:184.7},0).wait(1).to({x:174.7,y:184.5},0).wait(1).to({x:175.7,y:184.3},0).wait(1).to({x:176.7,y:184.1},0).wait(1).to({x:177.6,y:183.9},0).wait(1).to({x:178.6,y:183.7},0).wait(1).to({x:179.6,y:183.5},0).wait(1).to({x:180.6,y:183.3},0).wait(1).to({x:181.5,y:183},0).wait(1).to({x:182.4,y:182.8},0).wait(1).to({x:183.4,y:182.6},0).wait(1).to({x:184.4,y:182.3},0).wait(1).to({x:185.3,y:182.1},0).wait(1).to({x:186.3,y:181.8},0).wait(1).to({x:187.3,y:181.6},0).wait(1).to({x:188.2,y:181.4},0).wait(1).to({x:189.2,y:181.1},0).wait(1).to({x:190.1,y:180.9},0).wait(1).to({x:191.1,y:180.6},0).wait(1).to({x:192.1,y:180.4},0).wait(1).to({x:193,y:180.1},0).wait(1).to({x:194,y:179.9},0).wait(1).to({x:194.9,y:179.7},0).wait(1).to({x:195.9,y:179.4},0).wait(1).to({x:196.9,y:179.2},0).wait(1).to({x:197.8,y:178.9},0).wait(1).to({x:198.8,y:178.7},0).wait(1).to({x:199.7,y:178.4},0).wait(1).to({x:200.7,y:178.2},0).wait(1).to({x:201.7,y:178},0).wait(1).to({x:202.6,y:177.7},0).wait(1).to({x:203.6,y:177.5},0).wait(1).to({x:204.5,y:177.2},0).wait(1).to({x:205.5,y:177},0).wait(1).to({x:206.5,y:176.7},0).wait(1).to({x:207.4,y:176.5},0).wait(1).to({x:208.4,y:176.2},0).wait(1).to({x:209.3,y:176},0).wait(1).to({x:210.3,y:175.7},0).wait(1).to({x:211.3,y:175.5},0).wait(1).to({x:212.2,y:175.2},0).wait(1).to({x:213.2,y:175},0).wait(1).to({x:214.1,y:174.7},0).wait(1).to({x:215.1,y:174.5},0).wait(1).to({x:216,y:174.2},0).to({_off:true},1).wait(122).to({_off:false,regX:40.6,x:99.9,y:174.4},0).wait(1).to({regX:40.5,x:100.8,y:174.7},0).wait(1).to({x:101.8,y:175},0).wait(1).to({x:102.7,y:175.3},0).wait(1).to({x:103.7,y:175.6},0).wait(1).to({x:104.7,y:175.8},0).wait(1).to({x:105.6,y:176.1},0).wait(1).to({x:106.6,y:176.4},0).wait(1).to({x:107.6,y:176.7},0).wait(1).to({x:108.5,y:176.9},0).wait(1).to({x:109.5,y:177.2},0).wait(1).to({x:110.5,y:177.5},0).wait(1).to({x:111.5,y:177.7},0).wait(1).to({x:112.4,y:178},0).wait(1).to({x:113.4,y:178.2},0).wait(1).to({x:114.4,y:178.5},0).wait(1).to({x:115.4,y:178.7},0).wait(1).to({x:116.3,y:179},0).wait(1).to({x:117.3,y:179.2},0).wait(1).to({x:118.3,y:179.5},0).wait(1).to({x:119.3,y:179.7},0).wait(1).to({x:120.2,y:179.9},0).wait(1).to({x:121.2,y:180.2},0).wait(1).to({x:122.2,y:180.4},0).wait(1).to({x:123.2,y:180.6},0).wait(1).to({x:124.2,y:180.9},0).wait(1).to({x:125.2,y:181.1},0).wait(1).to({x:126.1,y:181.3},0).wait(1).to({x:127.1,y:181.5},0).wait(1).to({x:128.1,y:181.7},0).wait(1).to({x:129.1,y:181.9},0).wait(1).to({x:130.1,y:182.1},0).wait(1).to({x:131.1,y:182.3},0).wait(1).to({x:132,y:182.5},0).wait(1).to({x:133,y:182.7},0).wait(1).to({x:134,y:182.9},0).wait(1).to({x:135,y:183.1},0).wait(1).to({x:136,y:183.3},0).wait(1).to({x:137,y:183.5},0).wait(1).to({x:138,y:183.7},0).wait(1).to({x:139,y:183.9},0).wait(1).to({x:140,y:184},0).wait(1).to({x:141,y:184.2},0).wait(1).to({x:142,y:184.4},0).wait(1).to({x:143,y:184.5},0).wait(1).to({x:143.9,y:184.7},0).wait(1).to({x:144.9,y:184.8},0).wait(1).to({x:145.9,y:184.9},0).wait(1).to({x:146.9,y:185.1},0).wait(1).to({x:147.9,y:185.2},0).wait(1).to({x:148.9,y:185.3},0).wait(1).to({x:149.9,y:185.4},0).wait(1).to({x:150.9,y:185.5},0).wait(1).to({x:152,y:185.6},0).wait(1).to({x:153,y:185.7},0).wait(1).to({x:154,y:185.8},0).wait(1).to({x:154.9,y:185.9},0).wait(1).to({x:155.9},0).wait(1).to({x:156.9,y:186},0).wait(1).to({x:158},0).wait(1).to({x:159},0).wait(1).to({x:160},0).wait(1).to({x:161},0).wait(1).to({x:162,y:185.9},0).wait(1).to({x:163},0).wait(1).to({x:164},0).wait(1).to({x:165,y:185.8},0).wait(1).to({x:166,y:185.7},0).wait(1).to({x:167},0).wait(1).to({x:168,y:185.5},0).wait(1).to({x:169,y:185.4},0).wait(1).to({x:170,y:185.3},0).wait(1).to({x:171,y:185.1},0).wait(1).to({x:172,y:185},0).wait(1).to({x:173,y:184.8},0).wait(1).to({x:174,y:184.6},0).wait(1).to({x:175,y:184.5},0).wait(1).to({x:176,y:184.3},0).wait(1).to({x:177,y:184.1},0).wait(1).to({x:178,y:183.9},0).wait(1).to({x:178.9,y:183.6},0).wait(1).to({x:179.9,y:183.4},0).wait(1).to({x:180.9,y:183.2},0).wait(1).to({x:181.9,y:182.9},0).wait(1).to({x:182.9,y:182.7},0).wait(1).to({x:183.8,y:182.5},0).wait(1).to({x:184.8,y:182.2},0).wait(1).to({x:185.8,y:182},0).wait(1).to({x:186.8,y:181.7},0).wait(1).to({x:187.7,y:181.5},0).wait(1).to({x:188.7,y:181.2},0).wait(1).to({x:189.7,y:181},0).wait(1).to({x:190.7,y:180.7},0).wait(1).to({x:191.7,y:180.5},0).wait(1).to({x:192.6,y:180.2},0).wait(1).to({x:193.6,y:180},0).wait(1).to({x:194.6,y:179.8},0).wait(1).to({x:195.6,y:179.5},0).wait(1).to({x:196.5,y:179.3},0).wait(1).to({x:197.5,y:179},0).wait(1).to({x:198.5,y:178.8},0).wait(1).to({x:199.5,y:178.5},0).wait(1).to({x:200.4,y:178.3},0).wait(1).to({x:201.4,y:178},0).wait(1).to({x:202.4,y:177.8},0).wait(1).to({x:203.4,y:177.5},0).wait(1).to({x:204.3,y:177.3},0).wait(1).to({x:205.3,y:177},0).wait(1).to({x:206.3,y:176.8},0).wait(1).to({x:207.3,y:176.5},0).wait(1).to({x:208.3,y:176.3},0).wait(1).to({x:209.2,y:176},0).wait(1).to({x:210.2,y:175.8},0).wait(1).to({x:211.2,y:175.5},0).wait(1).to({x:212.2,y:175.3},0).wait(1).to({x:213.1,y:175},0).wait(1).to({x:214.1,y:174.8},0).wait(1).to({x:215.1,y:174.5},0).wait(1).to({x:216,y:174.2},0).wait(1));

	// Layer_2
	this.center = new lib.earth_1();
	this.center.parent = this;
	this.center.setTransform(193.6,155.4,0.751,0.75,0,0,0,50,51.2);

	this.timeline.addTween(cjs.Tween.get(this.center).wait(481));

	// marbleback
	this.instance_7 = new lib.gray();
	this.instance_7.parent = this;
	this.instance_7.setTransform(216.1,57.3,0.7,0.7,0,0,0,40.6,40.6);

	this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(1).to({regX:40.5,regY:40.5,x:215.1,y:57.1},0).wait(1).to({x:214.1,y:56.9},0).wait(1).to({x:213.1,y:56.7},0).wait(1).to({x:212.2,y:56.6},0).wait(1).to({x:211.2,y:56.5},0).wait(1).to({x:210.2,y:56.3},0).wait(1).to({x:209.2,y:56.2},0).wait(1).to({x:208.2,y:56.1},0).wait(1).to({x:207.3,y:56},0).wait(1).to({x:206.3,y:55.9},0).wait(1).to({x:205.3,y:55.8},0).wait(1).to({x:204.3,y:55.7},0).wait(1).to({x:203.3,y:55.6},0).wait(1).to({x:202.3,y:55.5},0).wait(1).to({x:201.4,y:55.4},0).wait(1).to({x:200.4,y:55.3},0).wait(1).to({x:199.4},0).wait(1).to({x:198.4,y:55.2},0).wait(1).to({x:197.4,y:55.1},0).wait(1).to({x:196.4},0).wait(1).to({x:195.4,y:55},0).wait(1).to({x:194.5,y:54.9},0).wait(1).to({x:193.5},0).wait(1).to({x:192.5,y:54.8},0).wait(1).to({x:191.5},0).wait(1).to({x:190.5,y:54.7},0).wait(1).to({x:189.5},0).wait(1).to({x:188.5,y:54.6},0).wait(1).to({x:187.6},0).wait(1).to({x:186.6,y:54.5},0).wait(1).to({x:185.6},0).wait(1).to({x:184.6,y:54.4},0).wait(1).to({x:183.6},0).wait(1).to({x:182.6},0).wait(1).to({x:181.6,y:54.3},0).wait(1).to({x:180.6},0).wait(1).to({x:179.7},0).wait(1).to({x:178.7,y:54.2},0).wait(1).to({x:177.7},0).wait(1).to({x:176.7},0).wait(1).to({x:175.7,y:54.1},0).wait(1).to({x:174.7},0).wait(1).to({x:173.7},0).wait(1).to({x:172.7},0).wait(1).to({x:171.8,y:54},0).wait(1).to({x:170.8},0).wait(1).to({x:169.8},0).wait(1).to({x:168.8},0).wait(1).to({x:167.8},0).wait(1).to({x:166.8},0).wait(1).to({x:165.8,y:53.9},0).wait(1).to({x:164.8},0).wait(1).to({x:163.9},0).wait(1).to({x:162.9},0).wait(1).to({x:161.9},0).wait(1).to({x:160.9},0).wait(1).to({x:159.9},0).wait(1).to({x:158.9},0).wait(1).to({x:157.9},0).wait(1).to({x:156.9},0).wait(1).to({x:156},0).wait(1).to({x:155},0).wait(1).to({x:154},0).wait(1).to({x:153},0).wait(1).to({x:152},0).wait(1).to({x:151},0).wait(1).to({x:150},0).wait(1).to({x:149},0).wait(1).to({x:148.1},0).wait(1).to({x:147.1},0).wait(1).to({x:146.1},0).wait(1).to({x:145.1},0).wait(1).to({x:144.1,y:54},0).wait(1).to({x:143.1},0).wait(1).to({x:142.1},0).wait(1).to({x:141.1},0).wait(1).to({x:140.2},0).wait(1).to({x:139.2},0).wait(1).to({x:138.2,y:54.1},0).wait(1).to({x:137.2},0).wait(1).to({x:136.2},0).wait(1).to({x:135.2},0).wait(1).to({x:134.2,y:54.2},0).wait(1).to({x:133.2},0).wait(1).to({x:132.3},0).wait(1).to({x:131.3,y:54.3},0).wait(1).to({x:130.3},0).wait(1).to({x:129.3},0).wait(1).to({x:128.3,y:54.4},0).wait(1).to({x:127.3},0).wait(1).to({x:126.3,y:54.5},0).wait(1).to({x:125.3},0).wait(1).to({x:124.4,y:54.6},0).wait(1).to({x:123.4},0).wait(1).to({x:122.4,y:54.7},0).wait(1).to({x:121.4},0).wait(1).to({x:120.4,y:54.8},0).wait(1).to({x:119.4},0).wait(1).to({x:118.4,y:54.9},0).wait(1).to({x:117.5,y:55},0).wait(1).to({x:116.5},0).wait(1).to({x:115.5,y:55.1},0).wait(1).to({x:114.5,y:55.2},0).wait(1).to({x:113.5,y:55.3},0).wait(1).to({x:112.5},0).wait(1).to({x:111.5,y:55.4},0).wait(1).to({x:110.6,y:55.5},0).wait(1).to({x:109.6,y:55.6},0).wait(1).to({x:108.6,y:55.7},0).wait(1).to({x:107.6,y:55.9},0).wait(1).to({x:106.6,y:56},0).wait(1).to({x:105.7,y:56.1},0).wait(1).to({x:104.7,y:56.3},0).wait(1).to({x:103.7,y:56.4},0).wait(1).to({x:102.7,y:56.6},0).wait(1).to({x:101.8,y:56.8},0).wait(1).to({x:100.8,y:57},0).wait(1).to({x:99.8,y:57.2},0).to({_off:true},1).wait(120).to({_off:false,regX:40.6,regY:40.6,x:216.1,y:57.3},0).wait(1).to({regX:40.5,regY:40.5,x:215.1,y:57.1},0).wait(1).to({x:214.2,y:56.9},0).wait(1).to({x:213.3,y:56.8},0).wait(1).to({x:212.3,y:56.6},0).wait(1).to({x:211.4,y:56.5},0).wait(1).to({x:210.4,y:56.4},0).wait(1).to({x:209.5,y:56.2},0).wait(1).to({x:208.6,y:56.1},0).wait(1).to({x:207.6,y:56},0).wait(1).to({x:206.7,y:55.9},0).wait(1).to({x:205.7,y:55.8},0).wait(1).to({x:204.8,y:55.7},0).wait(1).to({x:203.8,y:55.6},0).wait(1).to({x:202.9},0).wait(1).to({x:202,y:55.5},0).wait(1).to({x:201,y:55.4},0).wait(1).to({x:200.1,y:55.3},0).wait(1).to({x:199.1},0).wait(1).to({x:198.2,y:55.2},0).wait(1).to({x:197.2,y:55.1},0).wait(1).to({x:196.3},0).wait(1).to({x:195.3,y:55},0).wait(1).to({x:194.4,y:54.9},0).wait(1).to({x:193.4},0).wait(1).to({x:192.5,y:54.8},0).wait(1).to({x:191.6},0).wait(1).to({x:190.6,y:54.7},0).wait(1).to({x:189.7},0).wait(1).to({x:188.7,y:54.6},0).wait(1).to({x:187.8},0).wait(1).to({x:186.8,y:54.5},0).wait(1).to({x:185.9},0).wait(1).to({x:184.9},0).wait(1).to({x:184,y:54.4},0).wait(1).to({x:183},0).wait(1).to({x:182.1,y:54.3},0).wait(1).to({x:181.1},0).wait(1).to({x:180.2},0).wait(1).to({x:179.2,y:54.2},0).wait(1).to({x:178.3},0).wait(1).to({x:177.4},0).wait(1).to({x:176.4},0).wait(1).to({x:175.5,y:54.1},0).wait(1).to({x:174.5},0).wait(1).to({x:173.6},0).wait(1).to({x:172.6},0).wait(1).to({x:171.7,y:54},0).wait(1).to({x:170.7},0).wait(1).to({x:169.8},0).wait(1).to({x:168.8},0).wait(1).to({x:167.9},0).wait(1).to({x:166.9},0).wait(1).to({x:166,y:53.9},0).wait(1).to({x:165},0).wait(1).to({x:164.1},0).wait(1).to({x:163.1},0).wait(1).to({x:162.2},0).wait(1).to({x:161.2},0).wait(1).to({x:160.3},0).wait(1).to({x:159.4},0).wait(1).to({x:158.4},0).wait(1).to({x:157.5},0).wait(1).to({x:156.5},0).wait(1).to({x:155.6},0).wait(1).to({x:154.6},0).wait(1).to({x:153.7},0).wait(1).to({x:152.7},0).wait(1).to({x:151.8},0).wait(1).to({x:150.8},0).wait(1).to({x:149.9},0).wait(1).to({x:148.9},0).wait(1).to({x:148},0).wait(1).to({x:147},0).wait(1).to({x:146.1},0).wait(1).to({x:145.1},0).wait(1).to({x:144.2,y:54},0).wait(1).to({x:143.2},0).wait(1).to({x:142.3},0).wait(1).to({x:141.3},0).wait(1).to({x:140.4},0).wait(1).to({x:139.5},0).wait(1).to({x:138.5,y:54.1},0).wait(1).to({x:137.6},0).wait(1).to({x:136.6},0).wait(1).to({x:135.7},0).wait(1).to({x:134.7,y:54.2},0).wait(1).to({x:133.8},0).wait(1).to({x:132.8},0).wait(1).to({x:131.9},0).wait(1).to({x:130.9,y:54.3},0).wait(1).to({x:130},0).wait(1).to({x:129},0).wait(1).to({x:128.1,y:54.4},0).wait(1).to({x:127.1},0).wait(1).to({x:126.2,y:54.5},0).wait(1).to({x:125.2},0).wait(1).to({x:124.3,y:54.6},0).wait(1).to({x:123.4},0).wait(1).to({x:122.4,y:54.7},0).wait(1).to({x:121.5},0).wait(1).to({x:120.5,y:54.8},0).wait(1).to({x:119.6},0).wait(1).to({x:118.6,y:54.9},0).wait(1).to({x:117.7},0).wait(1).to({x:116.7,y:55},0).wait(1).to({x:115.8,y:55.1},0).wait(1).to({x:114.8},0).wait(1).to({x:113.9,y:55.2},0).wait(1).to({x:113,y:55.3},0).wait(1).to({x:112,y:55.4},0).wait(1).to({x:111.1,y:55.5},0).wait(1).to({x:110.1,y:55.6},0).wait(1).to({x:109.2,y:55.7},0).wait(1).to({x:108.2,y:55.8},0).wait(1).to({x:107.3,y:55.9},0).wait(1).to({x:106.4,y:56},0).wait(1).to({x:105.4,y:56.1},0).wait(1).to({x:104.5,y:56.3},0).wait(1).to({x:103.5,y:56.4},0).wait(1).to({x:102.6,y:56.6},0).wait(1).to({x:101.7,y:56.8},0).wait(1).to({x:100.8,y:57},0).wait(1).to({x:99.8,y:57.2},0).to({_off:true},1).wait(118));

	// purpleback
	this.instance_8 = new lib.purple();
	this.instance_8.parent = this;
	this.instance_8.setTransform(216.1,57.3,0.7,0.7,0,0,0,40.6,40.6);
	this.instance_8._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_8).wait(119).to({_off:false},0).wait(1).to({regX:40.5,regY:40.5,x:215.1,y:57},0).wait(1).to({x:214.2,y:56.8},0).wait(1).to({x:213.2,y:56.5},0).wait(1).to({x:212.3,y:56.3},0).wait(1).to({x:211.3,y:56.1},0).wait(1).to({x:210.4,y:55.9},0).wait(1).to({x:209.4,y:55.8},0).wait(1).to({x:208.4,y:55.6},0).wait(1).to({x:207.5,y:55.4},0).wait(1).to({x:206.5,y:55.3},0).wait(1).to({x:205.6,y:55.1},0).wait(1).to({x:204.6,y:55},0).wait(1).to({x:203.6,y:54.8},0).wait(1).to({x:202.7,y:54.7},0).wait(1).to({x:201.7,y:54.6},0).wait(1).to({x:200.7,y:54.4},0).wait(1).to({x:199.8,y:54.3},0).wait(1).to({x:198.8,y:54.2},0).wait(1).to({x:197.8,y:54.1},0).wait(1).to({x:196.9,y:54},0).wait(1).to({x:195.9,y:53.9},0).wait(1).to({x:194.9,y:53.8},0).wait(1).to({x:194,y:53.7},0).wait(1).to({x:193,y:53.6},0).wait(1).to({x:192},0).wait(1).to({x:191,y:53.5},0).wait(1).to({x:190.1,y:53.4},0).wait(1).to({x:189.1,y:53.3},0).wait(1).to({x:188.1},0).wait(1).to({x:187.2,y:53.2},0).wait(1).to({x:186.2,y:53.1},0).wait(1).to({x:185.2},0).wait(1).to({x:184.3,y:53},0).wait(1).to({x:183.3,y:52.9},0).wait(1).to({x:182.3},0).wait(1).to({x:181.3,y:52.8},0).wait(1).to({x:180.4},0).wait(1).to({x:179.4,y:52.7},0).wait(1).to({x:178.4},0).wait(1).to({x:177.4},0).wait(1).to({x:176.5,y:52.6},0).wait(1).to({x:175.5},0).wait(1).to({x:174.5},0).wait(1).to({x:173.5,y:52.5},0).wait(1).to({x:172.6},0).wait(1).to({x:171.6},0).wait(1).to({x:170.6,y:52.4},0).wait(1).to({x:169.7},0).wait(1).to({x:168.7},0).wait(1).to({x:167.7},0).wait(1).to({x:166.7},0).wait(1).to({x:165.8},0).wait(1).to({x:164.8,y:52.3},0).wait(1).to({x:163.8},0).wait(1).to({x:162.8},0).wait(1).to({x:161.9},0).wait(1).to({x:160.9},0).wait(1).to({x:159.9},0).wait(1).to({x:158.9},0).wait(1).to({x:158},0).wait(1).to({x:157},0).wait(1).to({x:156},0).wait(1).to({x:155,y:52.4},0).wait(1).to({x:154.1},0).wait(1).to({x:153.1},0).wait(1).to({x:152.1},0).wait(1).to({x:151.2},0).wait(1).to({x:150.2},0).wait(1).to({x:149.2,y:52.5},0).wait(1).to({x:148.2},0).wait(1).to({x:147.3},0).wait(1).to({x:146.3},0).wait(1).to({x:145.3,y:52.6},0).wait(1).to({x:144.3},0).wait(1).to({x:143.4},0).wait(1).to({x:142.4,y:52.7},0).wait(1).to({x:141.4},0).wait(1).to({x:140.4,y:52.8},0).wait(1).to({x:139.5},0).wait(1).to({x:138.5},0).wait(1).to({x:137.5,y:52.9},0).wait(1).to({x:136.6},0).wait(1).to({x:135.6,y:53},0).wait(1).to({x:134.6,y:53.1},0).wait(1).to({x:133.6},0).wait(1).to({x:132.7,y:53.2},0).wait(1).to({x:131.7},0).wait(1).to({x:130.7,y:53.3},0).wait(1).to({x:129.8,y:53.4},0).wait(1).to({x:128.8},0).wait(1).to({x:127.8,y:53.5},0).wait(1).to({x:126.8,y:53.6},0).wait(1).to({x:125.9,y:53.7},0).wait(1).to({x:124.9,y:53.8},0).wait(1).to({x:123.9},0).wait(1).to({x:123,y:53.9},0).wait(1).to({x:122,y:54},0).wait(1).to({x:121,y:54.1},0).wait(1).to({x:120.1,y:54.2},0).wait(1).to({x:119.1,y:54.3},0).wait(1).to({x:118.1,y:54.4},0).wait(1).to({x:117.1,y:54.5},0).wait(1).to({x:116.2,y:54.6},0).wait(1).to({x:115.2,y:54.8},0).wait(1).to({x:114.2,y:54.9},0).wait(1).to({x:113.3,y:55},0).wait(1).to({x:112.3,y:55.1},0).wait(1).to({x:111.4,y:55.3},0).wait(1).to({x:110.4,y:55.4},0).wait(1).to({x:109.4,y:55.5},0).wait(1).to({x:108.5,y:55.7},0).wait(1).to({x:107.5,y:55.8},0).wait(1).to({x:106.5,y:56},0).wait(1).to({x:105.6,y:56.2},0).wait(1).to({x:104.6,y:56.3},0).wait(1).to({x:103.7,y:56.5},0).wait(1).to({x:102.7,y:56.7},0).wait(1).to({x:101.8,y:56.9},0).wait(1).to({x:100.8,y:57.1},0).wait(1).to({x:99.8,y:57.3},0).to({_off:true},1).wait(122).to({_off:false,regX:40.6,regY:40.6,x:216.1},0).wait(1).to({regX:40.5,regY:40.5,x:215.1,y:57},0).wait(1).to({x:214.1,y:56.8},0).wait(1).to({x:213.2,y:56.5},0).wait(1).to({x:212.2,y:56.3},0).wait(1).to({x:211.2,y:56.1},0).wait(1).to({x:210.3,y:55.9},0).wait(1).to({x:209.3,y:55.7},0).wait(1).to({x:208.3,y:55.6},0).wait(1).to({x:207.3,y:55.4},0).wait(1).to({x:206.4,y:55.2},0).wait(1).to({x:205.4,y:55.1},0).wait(1).to({x:204.4,y:54.9},0).wait(1).to({x:203.4,y:54.8},0).wait(1).to({x:202.4,y:54.7},0).wait(1).to({x:201.5,y:54.5},0).wait(1).to({x:200.5,y:54.4},0).wait(1).to({x:199.5,y:54.3},0).wait(1).to({x:198.5,y:54.2},0).wait(1).to({x:197.5,y:54.1},0).wait(1).to({x:196.5,y:54},0).wait(1).to({x:195.6,y:53.9},0).wait(1).to({x:194.6,y:53.8},0).wait(1).to({x:193.6,y:53.7},0).wait(1).to({x:192.6,y:53.6},0).wait(1).to({x:191.6,y:53.5},0).wait(1).to({x:190.6,y:53.4},0).wait(1).to({x:189.6},0).wait(1).to({x:188.6,y:53.3},0).wait(1).to({x:187.7,y:53.2},0).wait(1).to({x:186.7,y:53.1},0).wait(1).to({x:185.7},0).wait(1).to({x:184.7,y:53},0).wait(1).to({x:183.7},0).wait(1).to({x:182.7,y:52.9},0).wait(1).to({x:181.7},0).wait(1).to({x:180.7,y:52.8},0).wait(1).to({x:179.8},0).wait(1).to({x:178.8,y:52.7},0).wait(1).to({x:177.8},0).wait(1).to({x:176.8,y:52.6},0).wait(1).to({x:175.8},0).wait(1).to({x:174.8},0).wait(1).to({x:173.8,y:52.5},0).wait(1).to({x:172.8},0).wait(1).to({x:171.8},0).wait(1).to({x:170.8},0).wait(1).to({x:169.9,y:52.4},0).wait(1).to({x:168.9},0).wait(1).to({x:167.9},0).wait(1).to({x:166.9},0).wait(1).to({x:165.9},0).wait(1).to({x:164.9,y:52.3},0).wait(1).to({x:163.9},0).wait(1).to({x:162.9},0).wait(1).to({x:161.9},0).wait(1).to({x:160.9},0).wait(1).to({x:160},0).wait(1).to({x:159},0).wait(1).to({x:158},0).wait(1).to({x:157},0).wait(1).to({x:156},0).wait(1).to({x:155,y:52.4},0).wait(1).to({x:154},0).wait(1).to({x:153},0).wait(1).to({x:152},0).wait(1).to({x:151},0).wait(1).to({x:150},0).wait(1).to({x:149.1,y:52.5},0).wait(1).to({x:148.1},0).wait(1).to({x:147.1},0).wait(1).to({x:146.1},0).wait(1).to({x:145.1,y:52.6},0).wait(1).to({x:144.1},0).wait(1).to({x:143.1},0).wait(1).to({x:142.1,y:52.7},0).wait(1).to({x:141.1},0).wait(1).to({x:140.2,y:52.8},0).wait(1).to({x:139.2},0).wait(1).to({x:138.2,y:52.9},0).wait(1).to({x:137.2},0).wait(1).to({x:136.2,y:53},0).wait(1).to({x:135.2},0).wait(1).to({x:134.2,y:53.1},0).wait(1).to({x:133.2},0).wait(1).to({x:132.2,y:53.2},0).wait(1).to({x:131.3,y:53.3},0).wait(1).to({x:130.3},0).wait(1).to({x:129.3,y:53.4},0).wait(1).to({x:128.3,y:53.5},0).wait(1).to({x:127.3,y:53.6},0).wait(1).to({x:126.3},0).wait(1).to({x:125.3,y:53.7},0).wait(1).to({x:124.3,y:53.8},0).wait(1).to({x:123.4,y:53.9},0).wait(1).to({x:122.4,y:54},0).wait(1).to({x:121.4,y:54.1},0).wait(1).to({x:120.4,y:54.2},0).wait(1).to({x:119.4,y:54.3},0).wait(1).to({x:118.4,y:54.4},0).wait(1).to({x:117.4,y:54.5},0).wait(1).to({x:116.5,y:54.6},0).wait(1).to({x:115.5,y:54.7},0).wait(1).to({x:114.5,y:54.8},0).wait(1).to({x:113.5,y:55},0).wait(1).to({x:112.5,y:55.1},0).wait(1).to({x:111.5,y:55.2},0).wait(1).to({x:110.6,y:55.4},0).wait(1).to({x:109.6,y:55.5},0).wait(1).to({x:108.6,y:55.7},0).wait(1).to({x:107.6,y:55.8},0).wait(1).to({x:106.7,y:56},0).wait(1).to({x:105.7,y:56.2},0).wait(1).to({x:104.7,y:56.3},0).wait(1).to({x:103.7,y:56.5},0).wait(1).to({x:102.8,y:56.7},0).wait(1).to({x:101.8,y:56.9},0).wait(1).to({x:100.8,y:57.1},0).wait(1).to({x:99.8,y:57.3},0).wait(1));

	// pyellowback
	this.instance_9 = new lib.yellow();
	this.instance_9.parent = this;
	this.instance_9.setTransform(156.6,106.4,0.7,0.7,0,0,0,40.5,40.5);

	this.timeline.addTween(cjs.Tween.get(this.instance_9).wait(1).to({x:155.7,y:106.3},0).wait(1).to({x:154.9},0).wait(1).to({x:154,y:106.2},0).wait(1).to({x:153.2},0).wait(1).to({x:152.3,y:106.1},0).wait(1).to({x:151.5},0).wait(1).to({x:150.7,y:106},0).wait(1).to({x:149.8},0).wait(1).to({x:149},0).wait(1).to({x:148.1,y:105.9},0).wait(1).to({x:147.3},0).wait(1).to({x:146.4},0).wait(1).to({x:145.6,y:105.8},0).wait(1).to({x:144.8},0).wait(1).to({x:143.9},0).wait(1).to({x:143.1},0).wait(1).to({x:142.2,y:105.7},0).wait(1).to({x:141.4},0).wait(1).to({x:140.6},0).wait(1).to({x:139.7},0).wait(1).to({x:138.9},0).wait(1).to({x:138,y:105.6},0).wait(1).to({x:137.2},0).wait(1).to({x:136.3},0).wait(1).to({x:135.5},0).wait(1).to({x:134.7},0).wait(1).to({x:133.8},0).wait(1).to({x:133},0).wait(1).to({x:132.1},0).wait(1).to({x:131.3},0).wait(1).to({x:130.4},0).wait(1).to({x:129.6},0).wait(1).to({x:128.8},0).wait(1).to({x:127.9},0).wait(1).to({x:127.1,y:105.7},0).wait(1).to({x:126.2},0).wait(1).to({x:125.4},0).wait(1).to({x:124.5},0).wait(1).to({x:123.7},0).wait(1).to({x:122.9,y:105.8},0).wait(1).to({x:122},0).wait(1).to({x:121.2},0).wait(1).to({x:120.3,y:105.9},0).wait(1).to({x:119.5},0).wait(1).to({x:118.6},0).wait(1).to({x:117.8,y:106},0).wait(1).to({x:117},0).wait(1).to({x:116.1,y:106.1},0).wait(1).to({x:115.3},0).wait(1).to({x:114.4,y:106.2},0).wait(1).to({x:113.6},0).wait(1).to({x:112.8,y:106.3},0).wait(1).to({x:111.9},0).wait(1).to({x:111.1,y:106.4},0).wait(1).to({x:110.2,y:106.5},0).wait(1).to({x:109.4},0).wait(1).to({x:108.6,y:106.6},0).wait(1).to({x:107.7,y:106.7},0).wait(1).to({x:106.9,y:106.8},0).wait(1).to({x:106},0).wait(1).to({x:105.2,y:106.9},0).wait(1).to({x:104.4,y:107},0).wait(1).to({x:103.5,y:107.1},0).wait(1).to({x:102.7,y:107.2},0).wait(1).to({x:101.8},0).wait(1).to({x:101,y:107.3},0).wait(1).to({x:100.2,y:107.4},0).wait(1).to({x:99.3,y:107.5},0).wait(1).to({x:98.5,y:107.6},0).wait(1).to({x:97.7,y:107.7},0).wait(1).to({x:96.8,y:107.8},0).wait(1).to({x:96,y:107.9},0).wait(1).to({x:95.1,y:108},0).wait(1).to({x:94.3,y:108.1},0).wait(1).to({x:93.5,y:108.2},0).wait(1).to({x:92.6,y:108.3},0).wait(1).to({x:91.8,y:108.5},0).wait(1).to({x:91,y:108.6},0).wait(1).to({x:90.1,y:108.7},0).wait(1).to({x:89.3,y:108.8},0).wait(1).to({x:88.5,y:108.9},0).wait(1).to({x:87.6,y:109},0).wait(1).to({x:86.8,y:109.2},0).wait(1).to({x:86,y:109.3},0).wait(1).to({x:85.1,y:109.4},0).wait(1).to({x:84.3,y:109.5},0).wait(1).to({x:83.5,y:109.7},0).wait(1).to({x:82.6,y:109.8},0).wait(1).to({x:81.8,y:109.9},0).wait(1).to({x:81,y:110.1},0).wait(1).to({x:80.1,y:110.2},0).wait(1).to({x:79.3,y:110.3},0).wait(1).to({x:78.5,y:110.5},0).wait(1).to({x:77.6,y:110.6},0).wait(1).to({x:76.8,y:110.7},0).wait(1).to({x:76,y:110.9},0).wait(1).to({x:75.1,y:111},0).wait(1).to({x:74.3,y:111.2},0).wait(1).to({x:73.5,y:111.3},0).wait(1).to({x:72.7,y:111.5},0).wait(1).to({x:71.8,y:111.6},0).wait(1).to({x:71,y:111.7},0).wait(1).to({x:70.2,y:111.9},0).wait(1).to({x:69.3,y:112},0).wait(1).to({x:68.5,y:112.2},0).wait(1).to({x:67.7,y:112.3},0).wait(1).to({x:66.8,y:112.5},0).wait(1).to({x:66,y:112.6},0).wait(1).to({x:65.2,y:112.8},0).wait(1).to({x:64.4,y:112.9},0).wait(1).to({x:63.5,y:113.1},0).wait(1).to({x:62.7,y:113.3},0).wait(1).to({x:61.9,y:113.4},0).wait(1).to({x:61,y:113.6},0).wait(1).to({x:60.2,y:113.7},0).wait(1).to({x:59.4,y:113.9},0).wait(1).to({x:58.6,y:114},0).wait(1).to({x:57.7,y:114.2},0).wait(1).to({x:56.9,y:114.3},0).to({_off:true},1).wait(242).to({_off:false,regX:40.6,x:256.1},0).wait(1).to({regX:40.5,x:255.2,y:114.1},0).wait(1).to({x:254.3,y:113.9},0).wait(1).to({x:253.5,y:113.8},0).wait(1).to({x:252.7,y:113.6},0).wait(1).to({x:251.9,y:113.4},0).wait(1).to({x:251,y:113.2},0).wait(1).to({x:250.2,y:113.1},0).wait(1).to({x:249.4,y:112.9},0).wait(1).to({x:248.5,y:112.7},0).wait(1).to({x:247.7,y:112.6},0).wait(1).to({x:246.9,y:112.4},0).wait(1).to({x:246,y:112.2},0).wait(1).to({x:245.2,y:112.1},0).wait(1).to({x:244.4,y:111.9},0).wait(1).to({x:243.5,y:111.7},0).wait(1).to({x:242.7,y:111.6},0).wait(1).to({x:241.9,y:111.4},0).wait(1).to({x:241,y:111.3},0).wait(1).to({x:240.2,y:111.1},0).wait(1).to({x:239.4,y:111},0).wait(1).to({x:238.5,y:110.8},0).wait(1).to({x:237.7,y:110.7},0).wait(1).to({x:236.9,y:110.5},0).wait(1).to({x:236,y:110.4},0).wait(1).to({x:235.2,y:110.3},0).wait(1).to({x:234.4,y:110.1},0).wait(1).to({x:233.5,y:110},0).wait(1).to({x:232.7,y:109.9},0).wait(1).to({x:231.8,y:109.7},0).wait(1).to({x:231,y:109.6},0).wait(1).to({x:230.2,y:109.5},0).wait(1).to({x:229.3,y:109.3},0).wait(1).to({x:228.5,y:109.2},0).wait(1).to({x:227.7,y:109.1},0).wait(1).to({x:226.8,y:109},0).wait(1).to({x:226,y:108.9},0).wait(1).to({x:225.1,y:108.8},0).wait(1).to({x:224.3,y:108.6},0).wait(1).to({x:223.5,y:108.5},0).wait(1).to({x:222.6,y:108.4},0).wait(1).to({x:221.8,y:108.3},0).wait(1).to({x:220.9,y:108.2},0).wait(1).to({x:220.1,y:108.1},0).wait(1).to({x:219.2,y:108},0).wait(1).to({x:218.4,y:107.9},0).wait(1).to({x:217.6,y:107.8},0).wait(1).to({x:216.7,y:107.7},0).wait(1).to({x:215.9},0).wait(1).to({x:215,y:107.6},0).wait(1).to({x:214.2,y:107.5},0).wait(1).to({x:213.3,y:107.4},0).wait(1).to({x:212.5,y:107.3},0).wait(1).to({x:211.7},0).wait(1).to({x:210.8,y:107.2},0).wait(1).to({x:210,y:107.1},0).wait(1).to({x:209.1,y:107},0).wait(1).to({x:208.3},0).wait(1).to({x:207.4,y:106.9},0).wait(1).to({x:206.6},0).wait(1).to({x:205.7,y:106.8},0).wait(1).to({x:204.9,y:106.7},0).wait(1).to({x:204},0).wait(1).to({x:203.2,y:106.6},0).wait(1).to({x:202.4},0).wait(1).to({x:201.5,y:106.5},0).wait(1).to({x:200.7},0).wait(1).to({x:199.8,y:106.4},0).wait(1).to({x:199},0).wait(1).to({x:198.1},0).wait(1).to({x:197.3,y:106.3},0).wait(1).to({x:196.4},0).wait(1).to({x:195.6},0).wait(1).to({x:194.7,y:106.2},0).wait(1).to({x:193.9},0).wait(1).to({x:193},0).wait(1).to({x:192.2},0).wait(1).to({x:191.3},0).wait(1).to({x:190.5,y:106.1},0).wait(1).to({x:189.6},0).wait(1).to({x:188.8},0).wait(1).to({x:188},0).wait(1).to({x:187.1},0).wait(1).to({x:186.3},0).wait(1).to({x:185.4},0).wait(1).to({x:184.6},0).wait(1).to({x:183.7},0).wait(1).to({x:182.9},0).wait(1).to({x:182},0).wait(1).to({x:181.2},0).wait(1).to({x:180.3},0).wait(1).to({x:179.5},0).wait(1).to({x:178.6},0).wait(1).to({x:177.8},0).wait(1).to({x:176.9},0).wait(1).to({x:176.1},0).wait(1).to({x:175.2,y:106.2},0).wait(1).to({x:174.4},0).wait(1).to({x:173.5},0).wait(1).to({x:172.7},0).wait(1).to({x:171.8,y:106.3},0).wait(1).to({x:171},0).wait(1).to({x:170.1},0).wait(1).to({x:169.3},0).wait(1).to({x:168.5,y:106.4},0).wait(1).to({x:167.6},0).wait(1).to({x:166.8},0).wait(1).to({x:165.9,y:106.5},0).wait(1).to({x:165.1},0).wait(1).to({x:164.2},0).wait(1).to({x:163.4,y:106.6},0).wait(1).to({x:162.5},0).wait(1).to({x:161.7,y:106.7},0).wait(1).to({x:160.8},0).wait(1).to({x:160,y:106.8},0).wait(1).to({x:159.1},0).wait(1).to({x:158.3,y:106.9},0).wait(1).to({x:157.4},0).wait(1).to({x:156.6},0).wait(1));

	// pblueback
	this.instance_10 = new lib.blue();
	this.instance_10.parent = this;
	this.instance_10.setTransform(256.1,114.3,0.7,0.7,0,0,0,40.6,40.5);
	this.instance_10._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_10).wait(119).to({_off:false},0).wait(1).to({regX:40.5,x:255.2,y:114.1},0).wait(1).to({x:254.3,y:113.9},0).wait(1).to({x:253.5,y:113.8},0).wait(1).to({x:252.6,y:113.6},0).wait(1).to({x:251.8,y:113.4},0).wait(1).to({x:250.9,y:113.2},0).wait(1).to({x:250,y:113},0).wait(1).to({x:249.2,y:112.9},0).wait(1).to({x:248.3,y:112.7},0).wait(1).to({x:247.5,y:112.5},0).wait(1).to({x:246.6,y:112.3},0).wait(1).to({x:245.7,y:112.2},0).wait(1).to({x:244.9,y:112},0).wait(1).to({x:244,y:111.8},0).wait(1).to({x:243.2,y:111.7},0).wait(1).to({x:242.3,y:111.5},0).wait(1).to({x:241.4,y:111.3},0).wait(1).to({x:240.6,y:111.2},0).wait(1).to({x:239.7,y:111},0).wait(1).to({x:238.8,y:110.9},0).wait(1).to({x:238,y:110.7},0).wait(1).to({x:237.1,y:110.6},0).wait(1).to({x:236.3,y:110.4},0).wait(1).to({x:235.4,y:110.3},0).wait(1).to({x:234.5,y:110.1},0).wait(1).to({x:233.7,y:110},0).wait(1).to({x:232.8,y:109.8},0).wait(1).to({x:231.9,y:109.7},0).wait(1).to({x:231.1,y:109.5},0).wait(1).to({x:230.2,y:109.4},0).wait(1).to({x:229.3,y:109.3},0).wait(1).to({x:228.5,y:109.1},0).wait(1).to({x:227.6,y:109},0).wait(1).to({x:226.7,y:108.9},0).wait(1).to({x:225.9,y:108.7},0).wait(1).to({x:225,y:108.6},0).wait(1).to({x:224.1,y:108.5},0).wait(1).to({x:223.3,y:108.4},0).wait(1).to({x:222.4,y:108.2},0).wait(1).to({x:221.5,y:108.1},0).wait(1).to({x:220.7,y:108},0).wait(1).to({x:219.8,y:107.9},0).wait(1).to({x:218.9,y:107.8},0).wait(1).to({x:218.1,y:107.7},0).wait(1).to({x:217.2,y:107.5},0).wait(1).to({x:216.3,y:107.4},0).wait(1).to({x:215.4,y:107.3},0).wait(1).to({x:214.6,y:107.2},0).wait(1).to({x:213.7,y:107.1},0).wait(1).to({x:212.8,y:107},0).wait(1).to({x:212},0).wait(1).to({x:211.1,y:106.9},0).wait(1).to({x:210.2,y:106.8},0).wait(1).to({x:209.3,y:106.7},0).wait(1).to({x:208.5,y:106.6},0).wait(1).to({x:207.6,y:106.5},0).wait(1).to({x:206.7,y:106.4},0).wait(1).to({x:205.8},0).wait(1).to({x:205,y:106.3},0).wait(1).to({x:204.1,y:106.2},0).wait(1).to({x:203.2},0).wait(1).to({x:202.3,y:106.1},0).wait(1).to({x:201.5,y:106},0).wait(1).to({x:200.6},0).wait(1).to({x:199.7,y:105.9},0).wait(1).to({x:198.8},0).wait(1).to({x:198,y:105.8},0).wait(1).to({x:197.1},0).wait(1).to({x:196.2,y:105.7},0).wait(1).to({x:195.3},0).wait(1).to({x:194.5,y:105.6},0).wait(1).to({x:193.6},0).wait(1).to({x:192.7},0).wait(1).to({x:191.8,y:105.5},0).wait(1).to({x:191},0).wait(1).to({x:190.1},0).wait(1).to({x:189.2,y:105.4},0).wait(1).to({x:188.3},0).wait(1).to({x:187.5},0).wait(1).to({x:186.6},0).wait(1).to({x:185.7},0).wait(1).to({x:184.8},0).wait(1).to({x:183.9},0).wait(1).to({x:183.1},0).wait(1).to({x:182.2},0).wait(1).to({x:181.3},0).wait(1).to({x:180.4},0).wait(1).to({x:179.6},0).wait(1).to({x:178.7},0).wait(1).to({x:177.8},0).wait(1).to({x:176.9},0).wait(1).to({x:176.1},0).wait(1).to({x:175.2},0).wait(1).to({x:174.3,y:105.5},0).wait(1).to({x:173.4},0).wait(1).to({x:172.5},0).wait(1).to({x:171.7,y:105.6},0).wait(1).to({x:170.8},0).wait(1).to({x:169.9},0).wait(1).to({x:169,y:105.7},0).wait(1).to({x:168.2},0).wait(1).to({x:167.3},0).wait(1).to({x:166.4,y:105.8},0).wait(1).to({x:165.5},0).wait(1).to({x:164.7,y:105.9},0).wait(1).to({x:163.8},0).wait(1).to({x:162.9,y:106},0).wait(1).to({x:162,y:106.1},0).wait(1).to({x:161.2},0).wait(1).to({x:160.3,y:106.2},0).wait(1).to({x:159.4,y:106.3},0).wait(1).to({x:158.5},0).wait(1).to({x:157.7,y:106.4},0).wait(1).to({x:156.8,y:106.5},0).wait(1).to({x:155.9},0).wait(1).to({x:155,y:106.6},0).wait(1).to({x:154.2,y:106.7},0).wait(1).to({x:153.3,y:106.8},0).wait(1).to({x:152.4,y:106.9},0).wait(1).to({x:151.5},0).wait(1).to({x:150.8},0).wait(1).to({x:150,y:106.8},0).wait(1).to({x:149.2},0).wait(1).to({x:148.4},0).wait(1).to({x:147.7,y:106.7},0).wait(1).to({x:146.9},0).wait(1).to({x:146.1},0).wait(1).to({x:145.3,y:106.6},0).wait(1).to({x:144.6},0).wait(1).to({x:143.8},0).wait(1).to({x:143},0).wait(1).to({x:142.2,y:106.5},0).wait(1).to({x:141.4},0).wait(1).to({x:140.7},0).wait(1).to({x:139.9},0).wait(1).to({x:139.1},0).wait(1).to({x:138.3,y:106.4},0).wait(1).to({x:137.6},0).wait(1).to({x:136.8},0).wait(1).to({x:136},0).wait(1).to({x:135.2},0).wait(1).to({x:134.4},0).wait(1).to({x:133.7},0).wait(1).to({x:132.9},0).wait(1).to({x:132.1},0).wait(1).to({x:131.3,y:106.3},0).wait(1).to({x:130.5},0).wait(1).to({x:129.8},0).wait(1).to({x:129,y:106.4},0).wait(1).to({x:128.2},0).wait(1).to({x:127.4},0).wait(1).to({x:126.7},0).wait(1).to({x:125.9},0).wait(1).to({x:125.1},0).wait(1).to({x:124.3},0).wait(1).to({x:123.5},0).wait(1).to({x:122.8},0).wait(1).to({x:122,y:106.5},0).wait(1).to({x:121.2},0).wait(1).to({x:120.4},0).wait(1).to({x:119.7},0).wait(1).to({x:118.9},0).wait(1).to({x:118.1},0).wait(1).to({x:117.3,y:106.6},0).wait(1).to({x:116.5},0).wait(1).to({x:115.8},0).wait(1).to({x:115},0).wait(1).to({x:114.2},0).wait(1).to({x:113.4,y:106.7},0).wait(1).to({x:112.7},0).wait(1).to({x:111.9},0).wait(1).to({x:111.1,y:106.8},0).wait(1).to({x:110.3},0).wait(1).to({x:109.5},0).wait(1).to({x:108.8},0).wait(1).to({x:108,y:106.9},0).wait(1).to({x:107.2},0).wait(1).to({x:106.4,y:107},0).wait(1).to({x:105.6},0).wait(1).to({x:104.9},0).wait(1).to({x:104.1,y:107.1},0).wait(1).to({x:103.3},0).wait(1).to({x:102.5,y:107.2},0).wait(1).to({x:101.8,y:107.3},0).wait(1).to({x:101},0).wait(1).to({x:100.2,y:107.4},0).wait(1).to({x:99.5,y:107.5},0).wait(1).to({x:98.7,y:107.6},0).wait(1).to({x:97.9,y:107.7},0).wait(1).to({x:97.1,y:107.8},0).wait(1).to({x:96.4,y:107.9},0).wait(1).to({x:95.6},0).wait(1).to({x:94.8,y:108},0).wait(1).to({x:94,y:108.1},0).wait(1).to({x:93.3,y:108.2},0).wait(1).to({x:92.5,y:108.3},0).wait(1).to({x:91.7,y:108.4},0).wait(1).to({x:90.9},0).wait(1).to({x:90.2,y:108.5},0).wait(1).to({x:89.4,y:108.6},0).wait(1).to({x:88.6,y:108.7},0).wait(1).to({x:87.8,y:108.8},0).wait(1).to({x:87.1,y:108.9},0).wait(1).to({x:86.3,y:109},0).wait(1).to({x:85.5},0).wait(1).to({x:84.7,y:109.1},0).wait(1).to({x:84,y:109.2},0).wait(1).to({x:83.2,y:109.3},0).wait(1).to({x:82.4,y:109.4},0).wait(1).to({x:81.7,y:109.5},0).wait(1).to({x:80.9},0).wait(1).to({x:80.1,y:109.6},0).wait(1).to({x:79.3,y:109.7},0).wait(1).to({x:78.6,y:109.8},0).wait(1).to({x:77.8,y:109.9},0).wait(1).to({x:77,y:110},0).wait(1).to({x:76.2,y:110.1},0).wait(1).to({x:75.5},0).wait(1).to({x:74.7,y:110.2},0).wait(1).to({x:73.9,y:110.3},0).wait(1).to({x:73.1,y:110.4},0).wait(1).to({x:72.4,y:110.5},0).wait(1).to({x:71.6,y:110.6},0).wait(1).to({x:70.8,y:110.7},0).wait(1).to({x:70},0).wait(1).to({x:69.2,y:110.8},0).wait(1).to({x:68.5,y:110.9},0).wait(1).to({x:67.7,y:111},0).wait(1).to({x:66.9,y:111.1},0).wait(1).to({x:66.2,y:111.2},0).wait(1).to({x:65.4,y:111.3},0).wait(1).to({x:64.6,y:111.4},0).wait(1).to({x:63.8,y:111.5},0).wait(1).to({x:63.1,y:111.6},0).wait(1).to({x:62.3,y:111.8},0).wait(1).to({x:61.6,y:111.9},0).wait(1).to({x:60.8,y:112.1},0).wait(1).to({x:60,y:112.3},0).wait(1).to({x:59.3,y:112.6},0).wait(1).to({x:58.6,y:112.9},0).wait(1).to({x:57.9,y:113.2},0).wait(1).to({x:57.4,y:113.8},0).wait(1).to({x:56.9,y:114.3},0).to({_off:true},1).wait(118));

	// pgreenback
	this.agro_1 = new lib.green();
	this.agro_1.parent = this;
	this.agro_1.setTransform(256.1,114.3,0.7,0.7,0,0,0,40.6,40.5);
	this.agro_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.agro_1).wait(239).to({_off:false},0).wait(1).to({regX:40.5,x:255.1},0).wait(1).to({x:254.1},0).wait(1).to({x:253.3,y:114.2},0).wait(1).to({x:252.4,y:114.1},0).wait(1).to({x:251.6,y:114},0).wait(1).to({x:250.7,y:113.9},0).wait(1).to({x:249.9,y:113.7},0).wait(1).to({x:249,y:113.6},0).wait(1).to({x:248.2,y:113.5},0).wait(1).to({x:247.3,y:113.4},0).wait(1).to({x:246.5,y:113.3},0).wait(1).to({x:245.6,y:113.2},0).wait(1).to({x:244.8,y:113},0).wait(1).to({x:243.9,y:112.9},0).wait(1).to({x:243.1,y:112.8},0).wait(1).to({x:242.2,y:112.7},0).wait(1).to({x:241.4,y:112.6},0).wait(1).to({x:240.5,y:112.5},0).wait(1).to({x:239.7,y:112.4},0).wait(1).to({x:238.8,y:112.2},0).wait(1).to({x:237.9,y:112.1},0).wait(1).to({x:237.1,y:112},0).wait(1).to({x:236.2,y:111.9},0).wait(1).to({x:235.4,y:111.8},0).wait(1).to({x:234.5,y:111.7},0).wait(1).to({x:233.7,y:111.6},0).wait(1).to({x:232.8,y:111.5},0).wait(1).to({x:232,y:111.4},0).wait(1).to({x:231.1,y:111.3},0).wait(1).to({x:230.3,y:111.2},0).wait(1).to({x:229.4,y:111},0).wait(1).to({x:228.6,y:110.9},0).wait(1).to({x:227.7,y:110.8},0).wait(1).to({x:226.9,y:110.7},0).wait(1).to({x:226,y:110.6},0).wait(1).to({x:225.2,y:110.5},0).wait(1).to({x:224.3,y:110.4},0).wait(1).to({x:223.4,y:110.3},0).wait(1).to({x:222.6,y:110.2},0).wait(1).to({x:221.7,y:110.1},0).wait(1).to({x:220.9,y:110},0).wait(1).to({x:220,y:109.9},0).wait(1).to({x:219.2,y:109.8},0).wait(1).to({x:218.3,y:109.7},0).wait(1).to({x:217.5,y:109.6},0).wait(1).to({x:216.6},0).wait(1).to({x:215.8,y:109.5},0).wait(1).to({x:214.9,y:109.4},0).wait(1).to({x:214.1,y:109.3},0).wait(1).to({x:213.2,y:109.2},0).wait(1).to({x:212.3,y:109.1},0).wait(1).to({x:211.5,y:109},0).wait(1).to({x:210.6,y:108.9},0).wait(1).to({x:209.8,y:108.8},0).wait(1).to({x:208.9,y:108.7},0).wait(1).to({x:208.1},0).wait(1).to({x:207.2,y:108.6},0).wait(1).to({x:206.4,y:108.5},0).wait(1).to({x:205.5,y:108.4},0).wait(1).to({x:204.6,y:108.3},0).wait(1).to({x:203.8,y:108.2},0).wait(1).to({x:202.9},0).wait(1).to({x:202.1,y:108.1},0).wait(1).to({x:201.2,y:108},0).wait(1).to({x:200.4,y:107.9},0).wait(1).to({x:199.5},0).wait(1).to({x:198.7,y:107.8},0).wait(1).to({x:197.8,y:107.7},0).wait(1).to({x:196.9,y:107.6},0).wait(1).to({x:196.1},0).wait(1).to({x:195.2,y:107.5},0).wait(1).to({x:194.4,y:107.4},0).wait(1).to({x:193.5},0).wait(1).to({x:192.7,y:107.3},0).wait(1).to({x:191.8,y:107.2},0).wait(1).to({x:190.9},0).wait(1).to({x:190.1,y:107.1},0).wait(1).to({x:189.2,y:107},0).wait(1).to({x:188.4},0).wait(1).to({x:187.5,y:106.9},0).wait(1).to({x:186.7},0).wait(1).to({x:185.8,y:106.8},0).wait(1).to({x:184.9},0).wait(1).to({x:184.1,y:106.7},0).wait(1).to({x:183.2,y:106.6},0).wait(1).to({x:182.4},0).wait(1).to({x:181.5,y:106.5},0).wait(1).to({x:180.7},0).wait(1).to({x:179.8,y:106.4},0).wait(1).to({x:178.9},0).wait(1).to({x:178.1,y:106.3},0).wait(1).to({x:177.2},0).wait(1).to({x:176.4},0).wait(1).to({x:175.5,y:106.2},0).wait(1).to({x:174.7},0).wait(1).to({x:173.8,y:106.1},0).wait(1).to({x:172.9},0).wait(1).to({x:172.1},0).wait(1).to({x:171.2,y:106},0).wait(1).to({x:170.4},0).wait(1).to({x:169.5,y:105.9},0).wait(1).to({x:168.6},0).wait(1).to({x:167.8},0).wait(1).to({x:166.9,y:105.8},0).wait(1).to({x:166.1},0).wait(1).to({x:165.2},0).wait(1).to({x:164.3},0).wait(1).to({x:163.5,y:105.7},0).wait(1).to({x:162.6},0).wait(1).to({x:161.8},0).wait(1).to({x:160.9},0).wait(1).to({x:160.1,y:105.6},0).wait(1).to({x:159.2},0).wait(1).to({x:158.3},0).wait(1).to({x:157.5},0).wait(1).to({x:156.6,y:105.5},0).wait(1).to({x:155.8},0).wait(1).to({x:154.9},0).wait(1).to({x:154},0).wait(1).to({x:153.2},0).wait(1).to({x:152.3},0).wait(1).to({x:151.5},0).wait(1).to({x:150.6,y:105.4},0).wait(1).to({x:149.8,y:105.5},0).wait(1).to({x:149},0).wait(1).to({x:148.2},0).wait(1).to({x:147.4},0).wait(1).to({x:146.6},0).wait(1).to({x:145.8},0).wait(1).to({x:145},0).wait(1).to({x:144.2},0).wait(1).to({x:143.4,y:105.6},0).wait(1).to({x:142.6},0).wait(1).to({x:141.8},0).wait(1).to({x:141},0).wait(1).to({x:140.2},0).wait(1).to({x:139.4},0).wait(1).to({x:138.6,y:105.7},0).wait(1).to({x:137.8},0).wait(1).to({x:137},0).wait(1).to({x:136.2},0).wait(1).to({x:135.4,y:105.8},0).wait(1).to({x:134.6},0).wait(1).to({x:133.8},0).wait(1).to({x:133},0).wait(1).to({x:132.2,y:105.9},0).wait(1).to({x:131.4},0).wait(1).to({x:130.6},0).wait(1).to({x:129.8,y:106},0).wait(1).to({x:129},0).wait(1).to({x:128.2},0).wait(1).to({x:127.4,y:106.1},0).wait(1).to({x:126.7},0).wait(1).to({x:125.9,y:106.2},0).wait(1).to({x:125.1},0).wait(1).to({x:124.3},0).wait(1).to({x:123.5,y:106.3},0).wait(1).to({x:122.7},0).wait(1).to({x:121.9,y:106.4},0).wait(1).to({x:121.1},0).wait(1).to({x:120.3,y:106.5},0).wait(1).to({x:119.5},0).wait(1).to({x:118.7,y:106.6},0).wait(1).to({x:117.9},0).wait(1).to({x:117.1,y:106.7},0).wait(1).to({x:116.3},0).wait(1).to({x:115.5,y:106.8},0).wait(1).to({x:114.7,y:106.9},0).wait(1).to({x:113.9},0).wait(1).to({x:113.1,y:107},0).wait(1).to({x:112.3},0).wait(1).to({x:111.5,y:107.1},0).wait(1).to({x:110.7,y:107.2},0).wait(1).to({x:109.9},0).wait(1).to({x:109.1,y:107.3},0).wait(1).to({x:108.3,y:107.4},0).wait(1).to({x:107.5,y:107.5},0).wait(1).to({x:106.7},0).wait(1).to({x:105.9,y:107.6},0).wait(1).to({x:105.1,y:107.7},0).wait(1).to({x:104.4,y:107.8},0).wait(1).to({x:103.6},0).wait(1).to({x:102.8,y:107.9},0).wait(1).to({x:102,y:108},0).wait(1).to({x:101.2,y:108.1},0).wait(1).to({x:100.4,y:108.2},0).wait(1).to({x:99.6,y:108.3},0).wait(1).to({x:98.8,y:108.4},0).wait(1).to({x:98},0).wait(1).to({x:97.2,y:108.5},0).wait(1).to({x:96.4,y:108.6},0).wait(1).to({x:95.6,y:108.7},0).wait(1).to({x:94.8,y:108.8},0).wait(1).to({x:94,y:108.9},0).wait(1).to({x:93.2,y:109},0).wait(1).to({x:92.4,y:109.1},0).wait(1).to({x:91.7,y:109.2},0).wait(1).to({x:90.9,y:109.3},0).wait(1).to({x:90.1,y:109.4},0).wait(1).to({x:89.3,y:109.5},0).wait(1).to({x:88.5,y:109.6},0).wait(1).to({x:87.7,y:109.7},0).wait(1).to({x:86.9,y:109.8},0).wait(1).to({x:86.1,y:109.9},0).wait(1).to({x:85.3,y:110.1},0).wait(1).to({x:84.5,y:110.2},0).wait(1).to({x:83.7,y:110.3},0).wait(1).to({x:82.9,y:110.4},0).wait(1).to({x:82.2,y:110.5},0).wait(1).to({x:81.4,y:110.6},0).wait(1).to({x:80.6,y:110.7},0).wait(1).to({x:79.8,y:110.8},0).wait(1).to({x:79,y:111},0).wait(1).to({x:78.2,y:111.1},0).wait(1).to({x:77.4,y:111.2},0).wait(1).to({x:76.6,y:111.3},0).wait(1).to({x:75.8,y:111.4},0).wait(1).to({x:75,y:111.5},0).wait(1).to({x:74.3,y:111.7},0).wait(1).to({x:73.5,y:111.8},0).wait(1).to({x:72.7,y:111.9},0).wait(1).to({x:71.9,y:112},0).wait(1).to({x:71.1,y:112.1},0).wait(1).to({x:70.3,y:112.3},0).wait(1).to({x:69.5,y:112.4},0).wait(1).to({x:68.7,y:112.5},0).wait(1).to({x:67.9,y:112.6},0).wait(1).to({x:67.2,y:112.8},0).wait(1).to({x:66.4,y:112.9},0).wait(1).to({x:65.6,y:113},0).wait(1).to({x:64.8,y:113.1},0).wait(1).to({x:64,y:113.3},0).wait(1).to({x:63.2,y:113.4},0).wait(1).to({x:62.4,y:113.5},0).wait(1).to({x:61.6,y:113.6},0).wait(1).to({x:60.8,y:113.8},0).wait(1).to({x:60.1,y:113.9},0).wait(1).to({x:59.3,y:114},0).wait(1).to({x:58.5,y:114.1},0).wait(1).to({x:57.7,y:114.3},0).wait(1).to({x:56.9,y:114.4},0).wait(1));

	// predback
	this.instance_11 = new lib.red();
	this.instance_11.parent = this;
	this.instance_11.setTransform(256.1,114.3,0.7,0.7,0,0,0,40.6,40.5);

	this.timeline.addTween(cjs.Tween.get(this.instance_11).wait(1).to({regX:40.5,x:255.2,y:114.2},0).wait(1).to({x:254.4,y:114.1},0).wait(1).to({x:253.6,y:114},0).wait(1).to({x:252.7,y:113.9},0).wait(1).to({x:251.9,y:113.7},0).wait(1).to({x:251.1,y:113.6},0).wait(1).to({x:250.3,y:113.5},0).wait(1).to({x:249.4,y:113.4},0).wait(1).to({x:248.6,y:113.3},0).wait(1).to({x:247.8,y:113.2},0).wait(1).to({x:247,y:113.1},0).wait(1).to({x:246.1,y:113},0).wait(1).to({x:245.3,y:112.9},0).wait(1).to({x:244.5,y:112.8},0).wait(1).to({x:243.6,y:112.7},0).wait(1).to({x:242.8,y:112.6},0).wait(1).to({x:242,y:112.5},0).wait(1).to({x:241.2,y:112.3},0).wait(1).to({x:240.3,y:112.2},0).wait(1).to({x:239.5,y:112.1},0).wait(1).to({x:238.7,y:112},0).wait(1).to({x:237.9,y:111.9},0).wait(1).to({x:237,y:111.8},0).wait(1).to({x:236.2,y:111.7},0).wait(1).to({x:235.4,y:111.6},0).wait(1).to({x:234.5,y:111.5},0).wait(1).to({x:233.7,y:111.4},0).wait(1).to({x:232.9,y:111.3},0).wait(1).to({x:232.1,y:111.2},0).wait(1).to({x:231.2,y:111.1},0).wait(1).to({x:230.4,y:111},0).wait(1).to({x:229.6},0).wait(1).to({x:228.7,y:110.9},0).wait(1).to({x:227.9,y:110.8},0).wait(1).to({x:227.1,y:110.7},0).wait(1).to({x:226.3,y:110.6},0).wait(1).to({x:225.4,y:110.5},0).wait(1).to({x:224.6,y:110.4},0).wait(1).to({x:223.8,y:110.3},0).wait(1).to({x:222.9,y:110.2},0).wait(1).to({x:222.1,y:110.1},0).wait(1).to({x:221.3,y:110},0).wait(1).to({x:220.4},0).wait(1).to({x:219.6,y:109.9},0).wait(1).to({x:218.8,y:109.8},0).wait(1).to({x:218,y:109.7},0).wait(1).to({x:217.1,y:109.6},0).wait(1).to({x:216.3,y:109.5},0).wait(1).to({x:215.5},0).wait(1).to({x:214.6,y:109.4},0).wait(1).to({x:213.8,y:109.3},0).wait(1).to({x:213,y:109.2},0).wait(1).to({x:212.1,y:109.1},0).wait(1).to({x:211.3},0).wait(1).to({x:210.5,y:109},0).wait(1).to({x:209.7,y:108.9},0).wait(1).to({x:208.8,y:108.8},0).wait(1).to({x:208},0).wait(1).to({x:207.2,y:108.7},0).wait(1).to({x:206.3,y:108.6},0).wait(1).to({x:205.5,y:108.5},0).wait(1).to({x:204.7},0).wait(1).to({x:203.8,y:108.4},0).wait(1).to({x:203,y:108.3},0).wait(1).to({x:202.2},0).wait(1).to({x:201.3,y:108.2},0).wait(1).to({x:200.5,y:108.1},0).wait(1).to({x:199.7},0).wait(1).to({x:198.9,y:108},0).wait(1).to({x:198,y:107.9},0).wait(1).to({x:197.2},0).wait(1).to({x:196.4,y:107.8},0).wait(1).to({x:195.5},0).wait(1).to({x:194.7,y:107.7},0).wait(1).to({x:193.9,y:107.6},0).wait(1).to({x:193},0).wait(1).to({x:192.2,y:107.5},0).wait(1).to({x:191.4},0).wait(1).to({x:190.5,y:107.4},0).wait(1).to({x:189.7},0).wait(1).to({x:188.9,y:107.3},0).wait(1).to({x:188},0).wait(1).to({x:187.2,y:107.2},0).wait(1).to({x:186.4},0).wait(1).to({x:185.5,y:107.1},0).wait(1).to({x:184.7},0).wait(1).to({x:183.9,y:107},0).wait(1).to({x:183},0).wait(1).to({x:182.2,y:106.9},0).wait(1).to({x:181.4},0).wait(1).to({x:180.5,y:106.8},0).wait(1).to({x:179.7},0).wait(1).to({x:178.9},0).wait(1).to({x:178,y:106.7},0).wait(1).to({x:177.2},0).wait(1).to({x:176.4,y:106.6},0).wait(1).to({x:175.5},0).wait(1).to({x:174.7},0).wait(1).to({x:173.9,y:106.5},0).wait(1).to({x:173},0).wait(1).to({x:172.2},0).wait(1).to({x:171.4,y:106.4},0).wait(1).to({x:170.5},0).wait(1).to({x:169.7},0).wait(1).to({x:168.9,y:106.3},0).wait(1).to({x:168},0).wait(1).to({x:167.2},0).wait(1).to({x:166.4,y:106.2},0).wait(1).to({x:165.5},0).wait(1).to({x:164.7},0).wait(1).to({x:163.9,y:106.1},0).wait(1).to({x:163},0).wait(1).to({x:162.2},0).wait(1).to({x:161.4},0).wait(1).to({x:160.5,y:106},0).wait(1).to({x:159.7},0).wait(1).to({x:158.9},0).wait(1).to({x:158},0).wait(1).to({x:157.2,y:105.9},0).wait(1).to({x:156.4},0).wait(1).to({x:155.5},0).wait(1).to({x:154.7},0).wait(1).to({x:153.8},0).wait(1).to({x:153},0).wait(1).to({x:152.2,y:105.8},0).wait(1).to({x:151.3},0).wait(1).to({x:150.5},0).wait(1).to({x:149.6},0).wait(1).to({x:148.8},0).wait(1).to({x:148},0).wait(1).to({x:147.1},0).wait(1).to({x:146.3},0).wait(1).to({x:145.4},0).wait(1).to({x:144.6},0).wait(1).to({x:143.8},0).wait(1).to({x:142.9},0).wait(1).to({x:142.1},0).wait(1).to({x:141.2},0).wait(1).to({x:140.4},0).wait(1).to({x:139.6,y:105.9},0).wait(1).to({x:138.7},0).wait(1).to({x:137.9},0).wait(1).to({x:137},0).wait(1).to({x:136.2},0).wait(1).to({x:135.4,y:106},0).wait(1).to({x:134.5},0).wait(1).to({x:133.7},0).wait(1).to({x:132.8},0).wait(1).to({x:132,y:106.1},0).wait(1).to({x:131.2},0).wait(1).to({x:130.3},0).wait(1).to({x:129.5,y:106.2},0).wait(1).to({x:128.7},0).wait(1).to({x:127.8,y:106.3},0).wait(1).to({x:127},0).wait(1).to({x:126.1,y:106.4},0).wait(1).to({x:125.3},0).wait(1).to({x:124.5,y:106.5},0).wait(1).to({x:123.6},0).wait(1).to({x:122.8,y:106.6},0).wait(1).to({x:121.9},0).wait(1).to({x:121.1,y:106.7},0).wait(1).to({x:120.3},0).wait(1).to({x:119.4,y:106.8},0).wait(1).to({x:118.6,y:106.9},0).wait(1).to({x:117.8},0).wait(1).to({x:116.9,y:107},0).wait(1).to({x:116.1,y:107.1},0).wait(1).to({x:115.2},0).wait(1).to({x:114.4,y:107.2},0).wait(1).to({x:113.6,y:107.3},0).wait(1).to({x:112.7,y:107.4},0).wait(1).to({x:111.9},0).wait(1).to({x:111.1,y:107.5},0).wait(1).to({x:110.2,y:107.6},0).wait(1).to({x:109.4,y:107.7},0).wait(1).to({x:108.6,y:107.8},0).wait(1).to({x:107.7,y:107.9},0).wait(1).to({x:106.9},0).wait(1).to({x:106.1,y:108},0).wait(1).to({x:105.2,y:108.1},0).wait(1).to({x:104.4,y:108.2},0).wait(1).to({x:103.5,y:108.3},0).wait(1).to({x:102.7,y:108.4},0).wait(1).to({x:101.9,y:108.5},0).wait(1).to({x:101,y:108.6},0).wait(1).to({x:100.2,y:108.7},0).wait(1).to({x:99.4,y:108.8},0).wait(1).to({x:98.5,y:108.9},0).wait(1).to({x:97.7,y:109},0).wait(1).to({x:96.9,y:109.1},0).wait(1).to({x:96,y:109.2},0).wait(1).to({x:95.2,y:109.3},0).wait(1).to({x:94.4,y:109.4},0).wait(1).to({x:93.5,y:109.5},0).wait(1).to({x:92.7,y:109.6},0).wait(1).to({x:91.9,y:109.7},0).wait(1).to({x:91,y:109.8},0).wait(1).to({x:90.2,y:110},0).wait(1).to({x:89.4,y:110.1},0).wait(1).to({x:88.5,y:110.2},0).wait(1).to({x:87.7,y:110.3},0).wait(1).to({x:86.9,y:110.4},0).wait(1).to({x:86,y:110.5},0).wait(1).to({x:85.2,y:110.6},0).wait(1).to({x:84.4,y:110.7},0).wait(1).to({x:83.5,y:110.8},0).wait(1).to({x:82.7,y:111},0).wait(1).to({x:81.9,y:111.1},0).wait(1).to({x:81,y:111.2},0).wait(1).to({x:80.2,y:111.3},0).wait(1).to({x:79.4,y:111.4},0).wait(1).to({x:78.6,y:111.5},0).wait(1).to({x:77.7,y:111.6},0).wait(1).to({x:76.9,y:111.7},0).wait(1).to({x:76.1,y:111.9},0).wait(1).to({x:75.2,y:112},0).wait(1).to({x:74.4,y:112.1},0).wait(1).to({x:73.6,y:112.2},0).wait(1).to({x:72.7,y:112.3},0).wait(1).to({x:71.9,y:112.4},0).wait(1).to({x:71.1,y:112.5},0).wait(1).to({x:70.2,y:112.6},0).wait(1).to({x:69.4,y:112.7},0).wait(1).to({x:68.6,y:112.9},0).wait(1).to({x:67.7,y:113},0).wait(1).to({x:66.9,y:113.1},0).wait(1).to({x:66.1,y:113.2},0).wait(1).to({x:65.2,y:113.3},0).wait(1).to({x:64.4,y:113.4},0).wait(1).to({x:63.6,y:113.5},0).wait(1).to({x:62.7,y:113.6},0).wait(1).to({x:61.9,y:113.7},0).wait(1).to({x:61.1,y:113.8},0).wait(1).to({x:60.2,y:113.9},0).wait(1).to({x:59.4,y:114},0).wait(1).to({x:58.6,y:114.1},0).wait(1).to({x:57.7,y:114.2},0).wait(1).to({x:56.9,y:114.3},0).to({_off:true},1).wait(241));

	// pblackback
	this.instance_12 = new lib.black();
	this.instance_12.parent = this;
	this.instance_12.setTransform(216.1,174.3,0.7,0.7,0,0,0,40.6,40.5);

	this.timeline.addTween(cjs.Tween.get(this.instance_12).wait(1).to({regX:40.5,x:215.1,y:174.1},0).wait(1).to({x:214.1,y:173.8},0).wait(1).to({x:213.2,y:173.6},0).wait(1).to({x:212.2,y:173.4},0).wait(1).to({x:211.2,y:173.2},0).wait(1).to({x:210.3,y:173},0).wait(1).to({x:209.3,y:172.7},0).wait(1).to({x:208.4,y:172.5},0).wait(1).to({x:207.4,y:172.3},0).wait(1).to({x:206.4,y:172.1},0).wait(1).to({x:205.5,y:171.9},0).wait(1).to({x:204.5,y:171.7},0).wait(1).to({x:203.5,y:171.5},0).wait(1).to({x:202.6,y:171.3},0).wait(1).to({x:201.6,y:171.1},0).wait(1).to({x:200.6,y:171},0).wait(1).to({x:199.7,y:170.8},0).wait(1).to({x:198.7,y:170.6},0).wait(1).to({x:197.7,y:170.4},0).wait(1).to({x:196.7,y:170.2},0).wait(1).to({x:195.8,y:170.1},0).wait(1).to({x:194.8,y:169.9},0).wait(1).to({x:193.8,y:169.7},0).wait(1).to({x:192.9,y:169.6},0).wait(1).to({x:191.9,y:169.4},0).wait(1).to({x:190.9,y:169.3},0).wait(1).to({x:189.9,y:169.1},0).wait(1).to({x:189,y:169},0).wait(1).to({x:188,y:168.8},0).wait(1).to({x:187,y:168.7},0).wait(1).to({x:186,y:168.6},0).wait(1).to({x:185,y:168.5},0).wait(1).to({x:184.1,y:168.3},0).wait(1).to({x:183.1,y:168.2},0).wait(1).to({x:182.1,y:168.1},0).wait(1).to({x:181.1,y:168},0).wait(1).to({x:180.1,y:167.9},0).wait(1).to({x:179.2,y:167.8},0).wait(1).to({x:178.2,y:167.7},0).wait(1).to({x:177.2,y:167.6},0).wait(1).to({x:176.2,y:167.5},0).wait(1).to({x:175.2},0).wait(1).to({x:174.2,y:167.4},0).wait(1).to({x:173.3,y:167.3},0).wait(1).to({x:172.3,y:167.2},0).wait(1).to({x:171.3},0).wait(1).to({x:170.3,y:167.1},0).wait(1).to({x:169.3},0).wait(1).to({x:168.3,y:167},0).wait(1).to({x:167.3},0).wait(1).to({x:166.4},0).wait(1).to({x:165.4,y:166.9},0).wait(1).to({x:164.4},0).wait(1).to({x:163.4},0).wait(1).to({x:162.4},0).wait(1).to({x:161.4},0).wait(1).to({x:160.4},0).wait(1).to({x:159.5},0).wait(1).to({x:158.5},0).wait(1).to({x:157.5},0).wait(1).to({x:156.5},0).wait(1).to({x:155.5},0).wait(1).to({x:154.5},0).wait(1).to({x:153.5,y:167},0).wait(1).to({x:152.6},0).wait(1).to({x:151.6},0).wait(1).to({x:150.6,y:167.1},0).wait(1).to({x:149.6},0).wait(1).to({x:148.6,y:167.2},0).wait(1).to({x:147.6,y:167.3},0).wait(1).to({x:146.6},0).wait(1).to({x:145.7,y:167.4},0).wait(1).to({x:144.7,y:167.5},0).wait(1).to({x:143.7},0).wait(1).to({x:142.7,y:167.6},0).wait(1).to({x:141.7,y:167.7},0).wait(1).to({x:140.7,y:167.8},0).wait(1).to({x:139.8,y:167.9},0).wait(1).to({x:138.8,y:168},0).wait(1).to({x:137.8,y:168.1},0).wait(1).to({x:136.8,y:168.2},0).wait(1).to({x:135.8,y:168.3},0).wait(1).to({x:134.9,y:168.4},0).wait(1).to({x:133.9,y:168.5},0).wait(1).to({x:132.9,y:168.6},0).wait(1).to({x:131.9,y:168.7},0).wait(1).to({x:130.9,y:168.9},0).wait(1).to({x:130,y:169},0).wait(1).to({x:129,y:169.1},0).wait(1).to({x:128,y:169.3},0).wait(1).to({x:127,y:169.4},0).wait(1).to({x:126,y:169.6},0).wait(1).to({x:125.1,y:169.7},0).wait(1).to({x:124.1,y:169.8},0).wait(1).to({x:123.1,y:170},0).wait(1).to({x:122.1,y:170.2},0).wait(1).to({x:121.2,y:170.3},0).wait(1).to({x:120.2,y:170.5},0).wait(1).to({x:119.2,y:170.6},0).wait(1).to({x:118.3,y:170.8},0).wait(1).to({x:117.3,y:171},0).wait(1).to({x:116.3,y:171.1},0).wait(1).to({x:115.3,y:171.3},0).wait(1).to({x:114.4,y:171.5},0).wait(1).to({x:113.4,y:171.7},0).wait(1).to({x:112.4,y:171.8},0).wait(1).to({x:111.5,y:172},0).wait(1).to({x:110.5,y:172.2},0).wait(1).to({x:109.5,y:172.4},0).wait(1).to({x:108.6,y:172.6},0).wait(1).to({x:107.6,y:172.8},0).wait(1).to({x:106.6,y:173},0).wait(1).to({x:105.6,y:173.2},0).wait(1).to({x:104.7,y:173.3},0).wait(1).to({x:103.7,y:173.5},0).wait(1).to({x:102.7,y:173.7},0).wait(1).to({x:101.8,y:173.9},0).wait(1).to({x:100.8,y:174.1},0).wait(1).to({x:99.8,y:174.3},0).to({_off:true},1).wait(119).to({_off:false,regX:40.6,x:216.1},0).wait(1).to({regX:40.5,x:215.1,y:174.1},0).wait(1).to({x:214.2,y:173.9},0).wait(1).to({x:213.3,y:173.6},0).wait(1).to({x:212.3,y:173.4},0).wait(1).to({x:211.4,y:173.2},0).wait(1).to({x:210.5,y:173},0).wait(1).to({x:209.5,y:172.8},0).wait(1).to({x:208.6,y:172.6},0).wait(1).to({x:207.7,y:172.4},0).wait(1).to({x:206.7,y:172.2},0).wait(1).to({x:205.8,y:172},0).wait(1).to({x:204.9,y:171.8},0).wait(1).to({x:203.9,y:171.6},0).wait(1).to({x:203,y:171.4},0).wait(1).to({x:202.1,y:171.2},0).wait(1).to({x:201.1,y:171},0).wait(1).to({x:200.2,y:170.9},0).wait(1).to({x:199.3,y:170.7},0).wait(1).to({x:198.3,y:170.5},0).wait(1).to({x:197.4,y:170.4},0).wait(1).to({x:196.4,y:170.2},0).wait(1).to({x:195.5,y:170},0).wait(1).to({x:194.6,y:169.9},0).wait(1).to({x:193.6,y:169.7},0).wait(1).to({x:192.7,y:169.6},0).wait(1).to({x:191.7,y:169.4},0).wait(1).to({x:190.8,y:169.3},0).wait(1).to({x:189.8,y:169.1},0).wait(1).to({x:188.9,y:169},0).wait(1).to({x:188,y:168.8},0).wait(1).to({x:187,y:168.7},0).wait(1).to({x:186.1,y:168.6},0).wait(1).to({x:185.1,y:168.5},0).wait(1).to({x:184.2,y:168.3},0).wait(1).to({x:183.2,y:168.2},0).wait(1).to({x:182.3,y:168.1},0).wait(1).to({x:181.3,y:168},0).wait(1).to({x:180.4,y:167.9},0).wait(1).to({x:179.4,y:167.8},0).wait(1).to({x:178.5,y:167.7},0).wait(1).to({x:177.5,y:167.6},0).wait(1).to({x:176.6},0).wait(1).to({x:175.6,y:167.5},0).wait(1).to({x:174.7,y:167.4},0).wait(1).to({x:173.7,y:167.3},0).wait(1).to({x:172.8},0).wait(1).to({x:171.8,y:167.2},0).wait(1).to({x:170.9},0).wait(1).to({x:169.9,y:167.1},0).wait(1).to({x:168.9},0).wait(1).to({x:168,y:167},0).wait(1).to({x:167},0).wait(1).to({x:166.1},0).wait(1).to({x:165.1,y:166.9},0).wait(1).to({x:164.2},0).wait(1).to({x:163.2},0).wait(1).to({x:162.3},0).wait(1).to({x:161.3},0).wait(1).to({x:160.4},0).wait(1).to({x:159.4},0).wait(1).to({x:158.5},0).wait(1).to({x:157.5},0).wait(1).to({x:156.5},0).wait(1).to({x:155.6},0).wait(1).to({x:154.6},0).wait(1).to({x:153.7,y:167},0).wait(1).to({x:152.7},0).wait(1).to({x:151.8},0).wait(1).to({x:150.8,y:167.1},0).wait(1).to({x:149.9},0).wait(1).to({x:148.9,y:167.2},0).wait(1).to({x:148},0).wait(1).to({x:147,y:167.3},0).wait(1).to({x:146.1,y:167.4},0).wait(1).to({x:145.1},0).wait(1).to({x:144.2,y:167.5},0).wait(1).to({x:143.2,y:167.6},0).wait(1).to({x:142.3},0).wait(1).to({x:141.3,y:167.7},0).wait(1).to({x:140.3,y:167.8},0).wait(1).to({x:139.4,y:167.9},0).wait(1).to({x:138.4,y:168},0).wait(1).to({x:137.5,y:168.1},0).wait(1).to({x:136.6,y:168.2},0).wait(1).to({x:135.6,y:168.3},0).wait(1).to({x:134.7,y:168.4},0).wait(1).to({x:133.7,y:168.5},0).wait(1).to({x:132.8,y:168.6},0).wait(1).to({x:131.8,y:168.8},0).wait(1).to({x:130.9,y:168.9},0).wait(1).to({x:129.9,y:169},0).wait(1).to({x:129,y:169.1},0).wait(1).to({x:128,y:169.3},0).wait(1).to({x:127.1,y:169.4},0).wait(1).to({x:126.1,y:169.5},0).wait(1).to({x:125.2,y:169.7},0).wait(1).to({x:124.2,y:169.8},0).wait(1).to({x:123.3,y:170},0).wait(1).to({x:122.4,y:170.1},0).wait(1).to({x:121.4,y:170.3},0).wait(1).to({x:120.5,y:170.4},0).wait(1).to({x:119.5,y:170.6},0).wait(1).to({x:118.6,y:170.7},0).wait(1).to({x:117.7,y:170.9},0).wait(1).to({x:116.7,y:171.1},0).wait(1).to({x:115.8,y:171.2},0).wait(1).to({x:114.8,y:171.4},0).wait(1).to({x:113.9,y:171.6},0).wait(1).to({x:113,y:171.7},0).wait(1).to({x:112,y:171.9},0).wait(1).to({x:111.1,y:172.1},0).wait(1).to({x:110.1,y:172.3},0).wait(1).to({x:109.2,y:172.4},0).wait(1).to({x:108.3,y:172.6},0).wait(1).to({x:107.3,y:172.8},0).wait(1).to({x:106.4,y:173},0).wait(1).to({x:105.5,y:173.2},0).wait(1).to({x:104.5,y:173.4},0).wait(1).to({x:103.6,y:173.6},0).wait(1).to({x:102.7,y:173.8},0).wait(1).to({x:101.7,y:174},0).wait(1).to({x:100.8,y:174.2},0).wait(1).to({x:99.8,y:174.3},0).to({_off:true},1).wait(118));

	// pwhiteback
	this.instance_13 = new lib.white();
	this.instance_13.parent = this;
	this.instance_13.setTransform(216.1,174.4,0.7,0.7,0,0,0,40.6,40.5);
	this.instance_13._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_13).wait(119).to({_off:false},0).wait(1).to({regX:40.5,x:215.1,y:174.1},0).wait(1).to({x:214.1,y:173.9},0).wait(1).to({x:213.2,y:173.7},0).wait(1).to({x:212.2,y:173.5},0).wait(1).to({x:211.3,y:173.2},0).wait(1).to({x:210.3,y:173},0).wait(1).to({x:209.4,y:172.8},0).wait(1).to({x:208.4,y:172.6},0).wait(1).to({x:207.5,y:172.4},0).wait(1).to({x:206.5,y:172.2},0).wait(1).to({x:205.6,y:172},0).wait(1).to({x:204.6,y:171.8},0).wait(1).to({x:203.6,y:171.6},0).wait(1).to({x:202.7,y:171.4},0).wait(1).to({x:201.7,y:171.2},0).wait(1).to({x:200.8,y:171},0).wait(1).to({x:199.8,y:170.8},0).wait(1).to({x:198.8,y:170.7},0).wait(1).to({x:197.9,y:170.5},0).wait(1).to({x:196.9,y:170.3},0).wait(1).to({x:195.9,y:170.1},0).wait(1).to({x:195,y:170},0).wait(1).to({x:194,y:169.8},0).wait(1).to({x:193,y:169.7},0).wait(1).to({x:192.1,y:169.5},0).wait(1).to({x:191.1,y:169.3},0).wait(1).to({x:190.1,y:169.2},0).wait(1).to({x:189.2,y:169.1},0).wait(1).to({x:188.2,y:168.9},0).wait(1).to({x:187.2,y:168.8},0).wait(1).to({x:186.3,y:168.7},0).wait(1).to({x:185.3,y:168.5},0).wait(1).to({x:184.3,y:168.4},0).wait(1).to({x:183.4,y:168.3},0).wait(1).to({x:182.4,y:168.2},0).wait(1).to({x:181.4,y:168.1},0).wait(1).to({x:180.4,y:168},0).wait(1).to({x:179.5,y:167.9},0).wait(1).to({x:178.5,y:167.8},0).wait(1).to({x:177.5,y:167.7},0).wait(1).to({x:176.5,y:167.6},0).wait(1).to({x:175.6,y:167.5},0).wait(1).to({x:174.6,y:167.4},0).wait(1).to({x:173.6},0).wait(1).to({x:172.6,y:167.3},0).wait(1).to({x:171.7,y:167.2},0).wait(1).to({x:170.7},0).wait(1).to({x:169.7,y:167.1},0).wait(1).to({x:168.7},0).wait(1).to({x:167.8,y:167},0).wait(1).to({x:166.8},0).wait(1).to({x:165.8},0).wait(1).to({x:164.8,y:166.9},0).wait(1).to({x:163.9},0).wait(1).to({x:162.9},0).wait(1).to({x:161.9},0).wait(1).to({x:160.9},0).wait(1).to({x:159.9},0).wait(1).to({x:159},0).wait(1).to({x:158},0).wait(1).to({x:157},0).wait(1).to({x:156},0).wait(1).to({x:155},0).wait(1).to({x:154.1},0).wait(1).to({x:153.1,y:167},0).wait(1).to({x:152.1},0).wait(1).to({x:151.1,y:167.1},0).wait(1).to({x:150.2},0).wait(1).to({x:149.2},0).wait(1).to({x:148.2,y:167.2},0).wait(1).to({x:147.2,y:167.3},0).wait(1).to({x:146.3},0).wait(1).to({x:145.3,y:167.4},0).wait(1).to({x:144.3,y:167.5},0).wait(1).to({x:143.3},0).wait(1).to({x:142.3,y:167.6},0).wait(1).to({x:141.4,y:167.7},0).wait(1).to({x:140.4,y:167.8},0).wait(1).to({x:139.4,y:167.9},0).wait(1).to({x:138.5,y:168},0).wait(1).to({x:137.5,y:168.1},0).wait(1).to({x:136.5,y:168.2},0).wait(1).to({x:135.5,y:168.3},0).wait(1).to({x:134.6,y:168.4},0).wait(1).to({x:133.6,y:168.5},0).wait(1).to({x:132.6,y:168.6},0).wait(1).to({x:131.6,y:168.7},0).wait(1).to({x:130.7,y:168.9},0).wait(1).to({x:129.7,y:169},0).wait(1).to({x:128.7,y:169.1},0).wait(1).to({x:127.8,y:169.3},0).wait(1).to({x:126.8,y:169.4},0).wait(1).to({x:125.8,y:169.5},0).wait(1).to({x:124.9,y:169.7},0).wait(1).to({x:123.9,y:169.8},0).wait(1).to({x:122.9,y:170},0).wait(1).to({x:122,y:170.1},0).wait(1).to({x:121,y:170.3},0).wait(1).to({x:120,y:170.5},0).wait(1).to({x:119.1,y:170.6},0).wait(1).to({x:118.1,y:170.8},0).wait(1).to({x:117.1,y:170.9},0).wait(1).to({x:116.2,y:171.1},0).wait(1).to({x:115.2,y:171.3},0).wait(1).to({x:114.2,y:171.5},0).wait(1).to({x:113.3,y:171.6},0).wait(1).to({x:112.3,y:171.8},0).wait(1).to({x:111.4,y:172},0).wait(1).to({x:110.4,y:172.2},0).wait(1).to({x:109.4,y:172.4},0).wait(1).to({x:108.5,y:172.5},0).wait(1).to({x:107.5,y:172.7},0).wait(1).to({x:106.6,y:172.9},0).wait(1).to({x:105.6,y:173.1},0).wait(1).to({x:104.6,y:173.3},0).wait(1).to({x:103.7,y:173.5},0).wait(1).to({x:102.7,y:173.7},0).wait(1).to({x:101.8,y:173.9},0).wait(1).to({x:100.8,y:174.1},0).wait(1).to({x:99.8,y:174.3},0).to({_off:true},1).wait(122).to({_off:false,regX:40.6,x:216.1,y:174.4},0).wait(1).to({regX:40.5,x:215.1,y:174.1},0).wait(1).to({x:214.1,y:173.9},0).wait(1).to({x:213.1,y:173.7},0).wait(1).to({x:212.2,y:173.4},0).wait(1).to({x:211.2,y:173.2},0).wait(1).to({x:210.2,y:173},0).wait(1).to({x:209.3,y:172.8},0).wait(1).to({x:208.3,y:172.6},0).wait(1).to({x:207.3,y:172.4},0).wait(1).to({x:206.3,y:172.2},0).wait(1).to({x:205.4,y:171.9},0).wait(1).to({x:204.4,y:171.7},0).wait(1).to({x:203.4,y:171.5},0).wait(1).to({x:202.4,y:171.4},0).wait(1).to({x:201.5,y:171.2},0).wait(1).to({x:200.5,y:171},0).wait(1).to({x:199.5,y:170.8},0).wait(1).to({x:198.5,y:170.6},0).wait(1).to({x:197.6,y:170.4},0).wait(1).to({x:196.6,y:170.3},0).wait(1).to({x:195.6,y:170.1},0).wait(1).to({x:194.6,y:169.9},0).wait(1).to({x:193.6,y:169.7},0).wait(1).to({x:192.7,y:169.6},0).wait(1).to({x:191.7,y:169.4},0).wait(1).to({x:190.7,y:169.3},0).wait(1).to({x:189.7,y:169.1},0).wait(1).to({x:188.7,y:169},0).wait(1).to({x:187.7,y:168.9},0).wait(1).to({x:186.8,y:168.7},0).wait(1).to({x:185.8,y:168.6},0).wait(1).to({x:184.8,y:168.5},0).wait(1).to({x:183.8,y:168.3},0).wait(1).to({x:182.8,y:168.2},0).wait(1).to({x:181.8,y:168.1},0).wait(1).to({x:180.8,y:168},0).wait(1).to({x:179.8,y:167.9},0).wait(1).to({x:178.8,y:167.8},0).wait(1).to({x:177.9,y:167.7},0).wait(1).to({x:176.9,y:167.6},0).wait(1).to({x:175.9,y:167.5},0).wait(1).to({x:174.9,y:167.4},0).wait(1).to({x:173.9},0).wait(1).to({x:172.9,y:167.3},0).wait(1).to({x:171.9,y:167.2},0).wait(1).to({x:170.9},0).wait(1).to({x:169.9,y:167.1},0).wait(1).to({x:168.9},0).wait(1).to({x:167.9,y:167},0).wait(1).to({x:166.9},0).wait(1).to({x:165.9},0).wait(1).to({x:164.9,y:166.9},0).wait(1).to({x:164},0).wait(1).to({x:163},0).wait(1).to({x:162},0).wait(1).to({x:161},0).wait(1).to({x:160},0).wait(1).to({x:159},0).wait(1).to({x:158},0).wait(1).to({x:157},0).wait(1).to({x:156},0).wait(1).to({x:155},0).wait(1).to({x:154},0).wait(1).to({x:153,y:167},0).wait(1).to({x:152},0).wait(1).to({x:151,y:167.1},0).wait(1).to({x:150},0).wait(1).to({x:149,y:167.2},0).wait(1).to({x:148},0).wait(1).to({x:147,y:167.3},0).wait(1).to({x:146.1},0).wait(1).to({x:145.1,y:167.4},0).wait(1).to({x:144.1,y:167.5},0).wait(1).to({x:143.1,y:167.6},0).wait(1).to({x:142.1},0).wait(1).to({x:141.1,y:167.7},0).wait(1).to({x:140.1,y:167.8},0).wait(1).to({x:139.1,y:167.9},0).wait(1).to({x:138.1,y:168},0).wait(1).to({x:137.1,y:168.1},0).wait(1).to({x:136.1,y:168.2},0).wait(1).to({x:135.2,y:168.3},0).wait(1).to({x:134.2,y:168.4},0).wait(1).to({x:133.2,y:168.6},0).wait(1).to({x:132.2,y:168.7},0).wait(1).to({x:131.2,y:168.8},0).wait(1).to({x:130.2,y:168.9},0).wait(1).to({x:129.2,y:169.1},0).wait(1).to({x:128.2,y:169.2},0).wait(1).to({x:127.3,y:169.3},0).wait(1).to({x:126.3,y:169.5},0).wait(1).to({x:125.3,y:169.6},0).wait(1).to({x:124.3,y:169.8},0).wait(1).to({x:123.3,y:169.9},0).wait(1).to({x:122.3,y:170.1},0).wait(1).to({x:121.4,y:170.2},0).wait(1).to({x:120.4,y:170.4},0).wait(1).to({x:119.4,y:170.6},0).wait(1).to({x:118.4,y:170.7},0).wait(1).to({x:117.4,y:170.9},0).wait(1).to({x:116.5,y:171.1},0).wait(1).to({x:115.5,y:171.2},0).wait(1).to({x:114.5,y:171.4},0).wait(1).to({x:113.5,y:171.6},0).wait(1).to({x:112.5,y:171.8},0).wait(1).to({x:111.6,y:172},0).wait(1).to({x:110.6,y:172.1},0).wait(1).to({x:109.6,y:172.3},0).wait(1).to({x:108.6,y:172.5},0).wait(1).to({x:107.6,y:172.7},0).wait(1).to({x:106.7,y:172.9},0).wait(1).to({x:105.7,y:173.1},0).wait(1).to({x:104.7,y:173.3},0).wait(1).to({x:103.7,y:173.5},0).wait(1).to({x:102.8,y:173.7},0).wait(1).to({x:101.8,y:173.9},0).wait(1).to({x:100.8,y:174.1},0).wait(1).to({x:99.8,y:174.3},0).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(181.1,148.4,255.8,173.8);
// library properties:
lib.properties = {
	width: 305,
	height: 239,
	fps: 24,
	color: "#FFFFFF",
	opacity: 0.00,
	webfonts: {},
	manifest: [
		{src:"images/earth.png", id:"earth"}
	],
	preloads: []
};




})(lib = lib||{}, images = images||{}, createjs = createjs||{}, ss = ss||{}, AdobeAn = AdobeAn||{});
var lib, images, createjs, ss, AdobeAn;